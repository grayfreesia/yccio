﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitPanel : MonoBehaviour {

	
	public void OnClickYes()
	{
		Application.Quit();
	}

	public void OnClickCancel()
	{
		gameObject.SetActive(false);
		LobbyPanel.Instance.isExitOn = false;
        SoundManager.Instance.PlaySound(0);

    }
}
