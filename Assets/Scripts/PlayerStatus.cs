﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatus : MonoBehaviour {

	public Image activeColor;
	public Image playerColor;
	public Image rating;
	public Text playerName;
	public Text remainStone;
	public Text playerChips;
	public int chips;
	public int order;
	public Slider slider;
	public Image imojiBG;
	public Image imoji;

	void Awake()
	{
		activeColor = GetComponent<Image>();
		slider = GetComponentInChildren<Slider>();
	}

	public void SetPlayerColor(int playOrder)
	{
		order = playOrder;
		switch(playOrder)
		{
			case 0:
				playerColor.color = new Color32(255, 31, 31, 255);
				break;
            case 1:
                playerColor.color = new Color32(31, 140, 255, 255);
                break;
            case 2:
                playerColor.color = new Color32(31, 195, 31, 255);
                break;
            case 3:
                playerColor.color = new Color32(255, 196, 31, 255);
                break;
            case 4:
                playerColor.color = new Color32(195, 86, 255, 255);
                break;
            default:
                break;

		}
	}

	public void SetPlayerChip(int chip)
	{
		chips = chip;
		playerChips.text = chips.ToString();
        if (chip > 1999)
        {
            rating.sprite = SpriteContainer.Instance.ratings[4];
        }
        else if (chip > 999)
        {
            rating.sprite = SpriteContainer.Instance.ratings[3];
        }
        else if (chip > 499)
        {
            rating.sprite = SpriteContainer.Instance.ratings[2];
        }
        else if (chip > 199)
        {
            rating.sprite = SpriteContainer.Instance.ratings[1];
        }
        else
        {
            rating.sprite = SpriteContainer.Instance.ratings[0];
        }
	}

	public void SetPlayerName(string n)
	{
		playerName.text = n;
	}

	public void SetPlayerStone(int num)
	{
		remainStone.text = "x"+num;
	}

	public void SetActiveColor(int active)
	{
		activeColor.color = order == active ? Color.cyan : Color.gray;
		if(order != active) UpdateTime(0f);
	}

	public void UpdateRemainStone(int active, int num)
	{
		if(active == order) remainStone.text = "x"+num;
	}

	public void UpdateTime(float time)
	{
		slider.value = time;
	}

	public void Imoji(int num)
	{
		imojiBG.gameObject.SetActive(true);
		imoji.sprite = ImojiButton.Instance.imojis[num];
		imoji.color = Color.white;
		imojiBG.color = Color.white;
		Invoke("ImojiFade", 1f);
	}

    public void ImojiFade()
    {
        StartCoroutine(CR_ImojiFade());
    }

    IEnumerator CR_ImojiFade()
    {
        for (int i = 0; i < 100; i++)
        {
            imojiBG.color -= new Color(0f, 0f, 0f, 0.01f);
            imoji.color -= new Color(0f, 0f, 0f, 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
        imojiBG.gameObject.SetActive(false);
    }

}
