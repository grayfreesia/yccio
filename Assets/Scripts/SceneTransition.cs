﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
	public bool isTestMode;
    private YccioNetworkManager ynm;

	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
		ynm = GetComponent<YccioNetworkManager>();

		if(isTestMode) GoToLobbyScene(true);
	}
	public void GoToLobbyScene(bool isEnter)
	{
		SceneManager.LoadScene("Lobby", LoadSceneMode.Single);
		if(isEnter || !PhotonNetwork.connected) ynm.EnterLobby();
		
	}
    public void GoToGameScene()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
}
