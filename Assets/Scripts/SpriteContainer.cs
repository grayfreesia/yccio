﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteContainer : MonoBehaviour {

	public Sprite[] ratings;
	public static SpriteContainer Instance{get; private set;}
	void Awake()
	{
		if(Instance == null) Instance = this;
	}
}
