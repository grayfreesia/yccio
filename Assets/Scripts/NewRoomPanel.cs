﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewRoomPanel : MonoBehaviour {

	public InputField input;

	public void OnClickCreate()
	{
		string roomName = "";
		if(input.textComponent.text == "") roomName = "Room " + Random.Range(0, 10000).ToString();
		else roomName = input.textComponent.text;

        YccioNetworkManager.Instance.NewGame(roomName);
        SoundManager.Instance.PlaySound(0);

    }

	public void OnClickCancel()
	{
		gameObject.SetActive(false);
        SoundManager.Instance.PlaySound(0);

    }
}
