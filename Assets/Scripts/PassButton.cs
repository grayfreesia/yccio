﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PassButton : MonoBehaviour {

	public static PassButton Instance{get; private set;}
	private Button b;
	void Awake()
	{
		if(Instance == null) Instance = this;
		b = GetComponent<Button>();
	}

    public void ActiveButton(bool active)
    {
        b.interactable = active;
    }

	public void OnClickPass()
	{
		Deck.Instance.UnselectOnPass();
        int[] data = { 0, -1, -1, -1, -1, -1, -1, -1 };
        YccioNetworkManager.Instance.turnManager.SendMove(data, true);
	}
}
