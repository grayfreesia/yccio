﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	//0 click
	//1 coin
	//2 hover
	//3 myturn
	//4 tada
	//5 tap
	//6 wahwah
	//7 yourturn
	//8 ding-dong
	//9 close door
	//10-14 imoji
	public AudioClip[] clips;
	public AudioSource source;
	public static SoundManager Instance{get; private set;}

	void Awake()
	{
		if(Instance == null) Instance = this;
		source = GetComponent<AudioSource>();
	}

	void Update()
	{
		// if(Input.GetMouseButtonDown(0))
		// {
		// 	PlaySound(0);
		// }
	}

	public void PlaySound(int num)
	{
		source.PlayOneShot(clips[num]);
	}


}
