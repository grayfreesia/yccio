﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorLock : MonoBehaviour
{
    private string password;
    private bool isActivce;
    private Text[] buttonTexts;
	private SceneTransition st;
    private void Awake()
    {
        password = "";
        isActivce = false;
        buttonTexts = GetComponentsInChildren<Text>();
		st = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SceneTransition>();
    }

    public void OnClickLockButton(string input)
    {
        if(password.Length > 10)
        {
            ActiveDoorLock(false);
        }
        else if (input == "*" && !isActivce)
        {
            //active
            ActiveDoorLock(true);
        }
        else if (input == "*" && isActivce)
        {
            //check
            //correct ->load lobby scene
            if (password == "153880")
            {
                st.GoToLobbyScene(true);
            }
            //reset
            ActiveDoorLock(false);

        }
        else if (isActivce)
        {
            password += input;
        }
    }

    private void ActiveDoorLock(bool active)
    {
        if (active)
        {
            //active color
            for (int i = 0; i < buttonTexts.Length; i++)
            {
                buttonTexts[i].color = Color.cyan;
            }
        }
        else
        {
            //inactive color
            for (int i = 0; i < buttonTexts.Length; i++)
            {
                buttonTexts[i].color = Color.white;
            }
            password = "";
        }
        isActivce = !isActivce;
    }
}
