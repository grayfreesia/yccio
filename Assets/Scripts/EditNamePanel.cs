﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditNamePanel : MonoBehaviour {

	public InputField input;

	public void OnClickConfirm()
	{
		if(input.textComponent.text == "") PlayerData.Instance.PlayerName = "Unknown";
		else PlayerData.Instance.PlayerName = input.textComponent.text;
		LobbyPanel.Instance.playerName.text = PlayerData.Instance.PlayerName;
		gameObject.SetActive(false);
        LobbyPanel.Instance.isEditOn = false;
        SoundManager.Instance.PlaySound(0);

    }

	public void OnClickCancel()
	{
		gameObject.SetActive(false);
		LobbyPanel.Instance.isEditOn = false;
        SoundManager.Instance.PlaySound(0);

    }

	public void OnEnable()
	{
		input.text = PlayerData.Instance.PlayerName;
		
	}
	
}
