﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartButton : MonoBehaviour
{

    public Button button;
	private YccioNetworkManager ynm;
    
    private void Start()
    {
        button = GetComponent<Button>();
        ynm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<YccioNetworkManager>();
    }

    public void OnClickStartButton()
    {
		ynm.StartGame();
		ActiveButton(false);
    }

    public void ActiveButton(bool active)
    {
        button.interactable = active;
    }
}
