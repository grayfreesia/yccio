﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGamePanel : MonoBehaviour
{

    public Text playerName;
    public Text chips;
    public GameObject exitPanel;
    public bool isExitOn = false;
    public GameObject increaseMoney;
    public Image rating;
    public GameObject playinfoPanel;

    int isReadPlayInfo = 0;

    public static InGamePanel Instance { get; private set; }

    void Awake()
    {
        if (Instance == null) Instance = this;
    }
    void Start()
    {
        playerName.text = PlayerData.Instance.PlayerName;
        int point = PlayerData.Instance.Chips;
        chips.text = point.ToString();

        if (point > 1999)
        {
            rating.sprite = SpriteContainer.Instance.ratings[4];
        }
        else if (point > 999)
        {
            rating.sprite = SpriteContainer.Instance.ratings[3];
        }
        else if (point > 499)
        {
            rating.sprite = SpriteContainer.Instance.ratings[2];
        }
        else if (point > 199)
        {
            rating.sprite = SpriteContainer.Instance.ratings[1];
        }
        else
        {
            rating.sprite = SpriteContainer.Instance.ratings[0];
        }

        isReadPlayInfo = PlayerPrefs.GetInt("playinfo", 0);
        if(isReadPlayInfo == 0 && PhotonNetwork.isMasterClient)
        {
            playinfoPanel.SetActive(true);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            if (!isExitOn)
            {
                exitPanel.SetActive(true);
                isExitOn = true;
            }
			else
			{
				exitPanel.SetActive(false);
				isExitOn = false;
			}
        }
    }
    public void OnClickExitButton()
    {
        if (!isExitOn)
        {
            isExitOn = true;
            exitPanel.SetActive(true);
        }
        SoundManager.Instance.PlaySound(0);

    }

   
}
