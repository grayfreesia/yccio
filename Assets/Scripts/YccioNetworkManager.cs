﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Photon;

public enum PhotonEventCodes
{
    Start = 0,
    Shuffle = 10,
    DistributeRequest = 11,
    DistributeStatus = 12,
    FirstOrderRequest = 13,
    FirstOrderSet = 14,
    RemainStoneRequest = 15,
    SendImoji = 16
}

public class YccioNetworkManager : PunBehaviour, IPunTurnManagerCallbacks
{
    public PunTurnManager turnManager;
    private SceneTransition st;
    private PlayerData pd;
    private string myRoomName = "";
    private RoomList roomList;
    private StartButton startButton;
    private PlayerStatusPanel statusPanel;

    //in Game
    public int maxStone = 1;//최대 숫자
    private int numStone = 1;//명당 가지는 돌 갯수
    private int totalStone = 1;//총 플레이 돌 갯수
    public int playOrder = 0;//나의 순서 플레이어 번호
    private int orderOffset = 0;//선//구름3
    public int playerNumber = 0;//플레이어 수
    public bool isMyTurn = false;
    private int fieldOwner = -1;
    public int curOrder = 0;
    public bool isNoCloud3 = false;
    //public bool isPlaying = false;
    public bool isWaiting = true;

    //end game
    private int[] remainStones;
    public bool isWinner = false;

    //sudden leave
    //private bool isLeave = false;
    //private int invokeIndex = 1;

    public static YccioNetworkManager Instance { get; private set; }
    void Awake()
    {
        if (Instance == null) Instance = this;

        turnManager = GetComponent<PunTurnManager>();
        turnManager.TurnManagerListener = this;
        turnManager.TurnDuration = 20f;
        st = GetComponent<SceneTransition>();
        pd = GetComponent<PlayerData>();
    }

  

    private void OnEnable()
    {
        PhotonNetwork.OnEventCall += OnPhotonEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.OnEventCall -= OnPhotonEvent;
    }

    private void OnPhotonEvent(byte eventCode, object content, int senderId)
    {
        PhotonEventCodes code = (PhotonEventCodes)eventCode;
        if (code == PhotonEventCodes.Start)
        {
            playerNumber = PhotonNetwork.room.PlayerCount;
            isWaiting = false;
            remainStones = new int[playerNumber];
            PhotonNetwork.room.IsPlaying = true;
            PlayerData.Instance.Chips -= 2 * playerNumber;
            SortByColor.Instance.ActiveButton(true);
            SortByNumber.Instance.ActiveButton(true);
            Deck.Instance.ClearDeck();
            SetPlayOrder();
            ShuffleSetting();
            Shuffle();
            ClearField();
        }
        else if (code == PhotonEventCodes.Shuffle)
        {
            GetMyStone(content as int[]);
        }
        else if (code == PhotonEventCodes.DistributeRequest)
        {
            DistributeStatus();
        }
        else if (code == PhotonEventCodes.DistributeStatus)
        {
            UpdateStatus(content as object[]);
        }
        else if (code == PhotonEventCodes.FirstOrderRequest)
        {
            Invoke("SetFirstOrder", 0.1f);
        }
        else if (code == PhotonEventCodes.FirstOrderSet)
        {
            orderOffset = (int)content - 1;
            turnManager.BeginTurn();
        }
        else if (code == PhotonEventCodes.RemainStoneRequest)
        {
            int[] data = content as int[];
            if (isWaiting) remainStones = new int[PhotonNetwork.room.PlayerCount];
            remainStones[data[0]] = data[1];
            //Debug.Log("data0: "+data[0]+"  /data1: "+data[1]);
            UpdateRemainStone(data[0], data[1]);
        }
        else if (code == PhotonEventCodes.SendImoji)
        {
            GetImoji(content as int[]);
        }
    }

    #region Built-in
    public void OnTurnBegins(int turn)
    {
        curOrder = (turn + orderOffset) % playerNumber;
        if (fieldOwner == curOrder)
        {
            ClearField();
        }
        if (playOrder == curOrder)
        {
            MyTurn();
        }
        else
        {
            NotMyTurn();
        }
    }
    public void OnPlayerMove(PhotonPlayer photonPlayer, int turn, object move)
    {

    }
    public void OnTurnCompleted(int obj)
    {

    }
    public void OnPlayerFinished(PhotonPlayer photonPlayer, int turn, object move)
    {
        int[] data = move as int[];
        if (data[0] == -1) return;
        else if (data[0] == 0)
        {
            //pass
            turnManager.BeginTurn();
        }
        else if (data[0] > 0)
        {
            //submit
            Field.Instance.prevPoint = data[1];
            Field.Instance.numOfStone = data[2];
            Field.Instance.RemoveAll();
            for (int i = 3; i < 3 + data[2]; i++)
            {
                Field.Instance.AddStone(data[i]);
            }
            StartCoroutine(CR_BetSound(data[2]));
            Field.Instance.isFirstField = false;
            //Field.Instance.TestPoint(data[1]);
            Field.Instance.SortStone(data[1]);

            if (data[0] == 2)//finish
            {
                EndGame(false);

            }
            else
            {
                fieldOwner = (turn + orderOffset) % playerNumber;
                Field.Instance.SetFieldColor(fieldOwner);
                turnManager.BeginTurn();
            }
        }

        //isLeave = false;
    }
    IEnumerator CR_BetSound(int num)
    {
        for (int i = 0; i < num; i++)
        {
            SoundManager.Instance.PlaySound(5);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void OnTurnTimeEnds(int obj)
    {
        Debug.Log("time out");
        turnManager.BeginTurn();
        //isLeave = true;
        // if (isMyTurn)
        // {
        //     Deck.Instance.UnselectOnPass();
        //     int[] data = { 0, -1, -1, -1, -1, -1, -1, -1 };//pass
        //     turnManager.SendMove(data, true);
        // }
        // invokeIndex = 1;
        // InvokeRepeating("PassByOther", 0.1f, 0.1f);
    }
    #endregion


    #region Override
    public override void OnJoinedLobby()
    {
        UpdateRoom();
    }

    public override void OnJoinedRoom()
    {
        //TestGetRoomData();
        st.GoToGameScene();
        SetPlayOrder();
        isWaiting = true;
        playerNumber = PhotonNetwork.room.PlayerCount - 1;

    }
    public override void OnPhotonCreateRoomFailed(object[] codeAndMsg)
    {
        Debug.Log("fail" + codeAndMsg);
    }

    public override void OnReceivedRoomListUpdate()
    {
        UpdateRoom();
    }

    public override void OnCreatedRoom()
    {
        //PhotonNetwork.JoinRoom(myRoomName);
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        Debug.Log("someone join");
        SoundManager.Instance.PlaySound(8);
        UpdatePlayersInfo();
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        Debug.Log("someone leave");
        SoundManager.Instance.PlaySound(9);
        if (!PhotonNetwork.room.IsPlaying)
        {
            UpdatePlayersInfo();
        }
    }

    public override void OnPhotonMaxCccuReached()
    {

    }
    #endregion
    public void UpdatePlayersInfo()
    {
        SetPlayOrder();
        if (!PhotonNetwork.room.IsPlaying) SetStartButton();
        Invoke("RequestDistribute", 0.12f);
    }

    public void EnterLobby()
    {
        PhotonNetwork.ConnectUsingSettings("1.07");
    }

    public void NewGame(string name)
    {
        myRoomName = name;
        PhotonNetwork.CreateRoom(myRoomName,
                                new RoomOptions() { MaxPlayers = 5, IsVisible = true, IsOpen = true },
                                TypedLobby.Default);
    }
    public void JoinGame(string roomName)
    {
        PhotonNetwork.JoinOrCreateRoom(roomName,
                                        new RoomOptions() { MaxPlayers = 5, IsVisible = true, IsOpen = true },
                                        TypedLobby.Default);
    }

    public RoomInfo[] GetGameList()
    {
        return PhotonNetwork.GetRoomList();
    }

    public void UpdateRoom()
    {
        if (roomList == null)
        {
            roomList = GameObject.FindGameObjectWithTag("RoomList").GetComponent<RoomList>();
        }
        roomList.UpdateList();
    }

    public void StartGame()
    {

        SetPlayOrder();
        if (PhotonNetwork.room.PlayerCount < 2) return;
        Invoke("RequestDistribute", 0.1f);

        PhotonNetwork.room.IsPlaying = true;
        RaiseEventOptions option = new RaiseEventOptions()
        {
            CachingOption = EventCaching.DoNotCache,
            Receivers = ReceiverGroup.All
        };
        PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.Start, null, false, option);
        PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.FirstOrderRequest, null, false, option);
    }

    public void SetStartButton()
    {
        if (startButton == null)
        {
            startButton = GameObject.FindGameObjectWithTag("StartButton").GetComponent<StartButton>();
        }
        if (PhotonNetwork.room.PlayerCount > 1 && PhotonNetwork.isMasterClient)
        {
            startButton.ActiveButton(true);
        }
        else
        {
            startButton.ActiveButton(false);
        }
    }

    public void LeaveGame()
    {
        //PhotonNetwork.room.IsPlaying = false;
        PhotonNetwork.LeaveRoom();
        st.GoToLobbyScene(false);
    }

    public void SetPlayOrder()
    {
        int id = 0;
        playOrder = 0;

        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            if (PhotonNetwork.playerList[i].UserId == PhotonNetwork.player.UserId)
            {
                id = PhotonNetwork.playerList[i].ID - 1;
            }
        }
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            if (PhotonNetwork.playerList[i].ID - 1 < id)
            {
                playOrder++;
            }
        }
    }

    public void ShuffleSetting()
    {
        //Debug.Log("shuffle setting");
        switch (PhotonNetwork.room.PlayerCount)
        {
            case 2:
                maxStone = 10;
                numStone = 16;
                totalStone = 40;
                break;

            case 3:
                maxStone = 9;
                numStone = 12;
                totalStone = 36;
                break;

            case 4:
                maxStone = 13;
                numStone = 13;
                totalStone = 52;
                break;

            case 5:
                maxStone = 15;
                numStone = 12;
                totalStone = 60;
                break;

            default:
                maxStone = 10;
                numStone = 16;
                totalStone = 40;
                Debug.Log("player number error");
                break;
        }
        for (int i = 0; i < remainStones.Length; i++)
        {
            remainStones[i] = numStone;
        }
    }

    public void Shuffle()
    {
        if (!PhotonNetwork.isMasterClient) return;
        //Debug.Log("shuffle");
        int[] stone = new int[totalStone];
        for (int i = 0; i < totalStone; i++)
        {
            stone[i] = i;
        }
        for (int i = 0; i < totalStone; i++)
        {
            int temp = stone[i];
            int r = UnityEngine.Random.Range(i, totalStone);
            stone[i] = stone[r];
            stone[r] = temp;
            //Debug.Log(stone[i]);
        }
        RaiseEventOptions option = new RaiseEventOptions()
        {
            CachingOption = EventCaching.DoNotCache,
            Receivers = ReceiverGroup.All
        };
        PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.Shuffle, stone, false, option);

    }

    public void GetMyStone(int[] stone)
    {
        int startPos = playOrder * numStone;

        for (int i = 0; i < numStone; i++)
        {
            Deck.Instance.AddStone(stone[startPos + i]);
        }

        if (playerNumber == 2)
        {
            isNoCloud3 = false;
            for (int i = 32; i < 40; i++)
            {
                if (stone[i] == 8)
                {
                    isNoCloud3 = true;
                }
            }
        }
    }

    public void RequestDistribute()
    {
        //Debug.Log("distribute request");
        if (!PhotonNetwork.isMasterClient) return;
        RaiseEventOptions option = new RaiseEventOptions()
        {
            CachingOption = EventCaching.DoNotCache,
            Receivers = ReceiverGroup.All
        };
        PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.DistributeRequest, null, false, option);
    }

    public void DistributeStatus()
    {
        //Debug.Log("distribute status");
        if (statusPanel == null) statusPanel = PlayerStatusPanel.Instance;
        statusPanel.RemoveAll();

        object[] status = new object[4];
        status[0] = playOrder;
        status[1] = pd.PlayerName;
        status[2] = pd.Chips;
        status[3] = Deck.Instance.numOfStone;
        RaiseEventOptions option = new RaiseEventOptions()
        {
            CachingOption = EventCaching.DoNotCache,
            Receivers = ReceiverGroup.All
        };
        PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.DistributeStatus, status, false, option);

    }

    public void UpdateStatus(object[] data)
    {
        //Debug.Log("update status");
        if ((int)data[0] != playOrder)
        {
            try
            {
                statusPanel.AddStatus((int)data[0], (string)data[1], (int)data[2], (int)data[3]);
            }

            catch (System.IndexOutOfRangeException e)  // CS0168
            {
                statusPanel.AddStatus((int)data[0], (string)data[1], (int)data[2], numStone);

            }
        }
    }

    public int IsFirstOrder()
    {
        if (Deck.Instance.isFirst)
        {
            Deck.Instance.isFirst = false;
            return playOrder;
        }
        else if (isNoCloud3 && playOrder == 0)
        {
            return UnityEngine.Random.Range(0, 2);
        }
        else return -1;
    }

    public void SetFirstOrder()
    {
        int order = IsFirstOrder();

        RaiseEventOptions option = new RaiseEventOptions()
        {
            CachingOption = EventCaching.DoNotCache,
            Receivers = ReceiverGroup.All
        };
        Debug.Log("order" + order + "/" + playOrder);
        if (order > -1)
        {
            PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.FirstOrderSet, order, false, option);
        }
    }

    public void MyTurn()
    {
        if (isWaiting)
        {
            PassButton.Instance.OnClickPass();
            return;
        }
        isMyTurn = true;
        SubmitButton.Instance.ActiveButton(true);
        PassButton.Instance.ActiveButton(true);
        Deck.Instance.SetActiveColor(true);
        PlayerStatusPanel.Instance.SetActiveColor(curOrder);
        Invoke("MyTurnSound", (Field.Instance.transform.childCount + 2) * 0.1f);
    }

    public void NotMyTurn()
    {
        isMyTurn = false;
        SubmitButton.Instance.ActiveButton(false);
        PassButton.Instance.ActiveButton(false);
        Deck.Instance.SetActiveColor(false);
        PlayerStatusPanel.Instance.SetActiveColor(curOrder);
        int[] data = { -1, -1, -1, -1, -1, -1, -1, -1 };
        turnManager.SendMove(data, true);
        RemainTime.Instance.SetCurrentPlayerStatus();
        Invoke("NotMyTurnSound", (Field.Instance.transform.childCount + 2) * 0.1f);

    }
    public void MyTurnSound()
    {
        SoundManager.Instance.PlaySound(3);
    }
    public void NotMyTurnSound()
    {
        SoundManager.Instance.PlaySound(7);
    }


    public void ClearField()
    {
        Field.Instance.ClearField();
        Field.Instance.SetFieldColor(-1);
    }

    public void EndGame(bool reset)
    {
        PhotonNetwork.room.SetTurn(0, false);
        SetStartButton();
        //RequestDistribute();
        Deck.Instance.CalRemain();
        PhotonNetwork.room.IsPlaying = false;
        RemainTime.Instance.Invisible();
        if (!reset)
        {
            Invoke("WinOrLose", 0.2f);
            Invoke("CalChips", 0.3f);
        }
        PlayerData.Instance.Chips += 2 * playerNumber;

        //isPlaying = false;
        SubmitButton.Instance.ActiveButton(false);
        PassButton.Instance.ActiveButton(false);
        SortByColor.Instance.ActiveButton(false);
        SortByNumber.Instance.ActiveButton(false);
    }

    public void SendRemainStones(int order, int remain)
    {
        // int remain = Deck.Instance.CalRemain();
        if (isWaiting) return;
        int[] data = new int[2];
        data[0] = order;
        data[1] = remain;
        RaiseEventOptions option = new RaiseEventOptions()
        {
            CachingOption = EventCaching.DoNotCache,
            Receivers = ReceiverGroup.All
        };
        PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.RemainStoneRequest, data, false, option);
    }

    public void CalChips()
    {
        if(isWaiting) return;
        StartCoroutine(CR_Calchips());
    }
    IEnumerator CR_Calchips()
    {
        int my = remainStones[playOrder];
        IncreaseEffectPanel.Instance.InitPanel();
        for (int i = 0; i < remainStones.Length; i++)
        {
            Debug.Log(i + ": remain:" + remainStones[i]);
            if (i == playOrder) continue;
            else
            {
                yield return new WaitForSeconds(0.2f);
                //Debug.Log("calchip" + (remainStones[i] - my));
                IncreaseEffectPanel.Instance.NewIncrease(remainStones[i] - my, i > playOrder ? i : i + 1);
                PlayerData.Instance.Chips += ((remainStones[i] - my) == 0 && isWinner) ? 40 : (remainStones[i] - my);
                InGamePanel.Instance.chips.text = PlayerData.Instance.Chips.ToString();
                SoundManager.Instance.PlaySound(1);
            }

        }
    }

    // public void PassByOther()
    // {
    //     Debug.Log("pass by other");
    //     if (isLeave)
    //     {
    //         if (playOrder == (PhotonNetwork.room.GetTurn() + invokeIndex + orderOffset) % playerNumber)
    //         {
    //             turnManager.BeginTurn();
    //             CancelInvoke();
    //         }
    //     }
    //     if (invokeIndex > playerNumber)
    //     {
    //         CancelInvoke();
    //         invokeIndex = 0;
    //     }
    //     invokeIndex++;
    // }

    public void UpdateRemainStone(int order, int remain)
    {
        for (int i = 0; i < playerNumber; i++)
        {
            Debug.Log(i + " /updateremainstone" + remainStones[i]);

        }
        PlayerStatusPanel.Instance.UpdateRemainStone(order, remain);

    }

    public void WinOrLose()
    {
        if(isWaiting) return;
        if (isWinner) WinLosePanel.Instance.OnWin();
        else WinLosePanel.Instance.OnLose();

        isWinner = false;
    }

    public void SendImoji(int num)
    {
        int[] data = new int[2];
        data[0] = playOrder;
        data[1] = num;
        RaiseEventOptions option = new RaiseEventOptions()
        {
            CachingOption = EventCaching.DoNotCache,
            Receivers = ReceiverGroup.All
        };
        PhotonNetwork.RaiseEvent((byte)PhotonEventCodes.SendImoji, data, false, option);
    }

    public void GetImoji(int[] data)
    {
        PlayerStatusPanel.Instance.GetImoji(data);
    }



}

