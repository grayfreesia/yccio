﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLosePanel : MonoBehaviour {

	public GameObject win;
	public GameObject lose;
	public GameObject particle;

	public static WinLosePanel Instance{get; private set;}
	void Awake()
	{
		if(Instance == null) Instance = this;
	}

	void Update()
	{
        if (Input.GetKeyDown(KeyCode.X))
        {
            OnWin();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            OnLose();
        }
	}

	public void OnWin()
	{
		StartCoroutine(C_OnWin());
	}

	public void OnLose()
	{
        StartCoroutine(C_OnLose());
    }

	IEnumerator C_OnWin()
	{
		SoundManager.Instance.PlaySound(4);
        win.SetActive(true);
        Text t = win.GetComponent<Text>();
		yield return null;
		t.color = Color.white;
		for(int i = 0; i < 50; i++)
		{
            GameObject p = Instantiate(particle, transform);
            p.GetComponent<WinLoseParticle>().Init(true);

        }
        yield return new WaitForSeconds(3f);
        for(int i = 0; i < 30; i++)
		{
			t.color = t.color - new Color(0f, 0f, 0f, 0.03f);
			yield return new WaitForSeconds(0.01f);
		}
		win.SetActive(false);
	}

	IEnumerator C_OnLose()
	{
        SoundManager.Instance.PlaySound(6);

        lose.SetActive(true);
        Text t = lose.GetComponent<Text>();
        yield return null;

        t.color = Color.gray;
        for (int i = 0; i < 50; i++)
        {
            GameObject p = Instantiate(particle, transform);
            p.GetComponent<WinLoseParticle>().Init(false);
        }
        yield return new WaitForSeconds(3f);

        for (int i = 0; i < 30; i++)
        {
            t.color = t.color - new Color(0f, 0f, 0f, 0.03f);
            yield return new WaitForSeconds(0.01f);
        }
        lose.SetActive(false);
	}
}
