﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomList : MonoBehaviour {
	public GameObject roomButtonPrefab;
	public GameObject roomPanel;
	public GameObject createRoomPanel;
	public static RoomList Instance{get; private set;}
	void Awake()
	{
		if(Instance == null) Instance = this;
	}
	public void UpdateList()
	{
		for(int i = 0; i < transform.childCount-1; i++)
		{
			Destroy(transform.GetChild(i).gameObject);
		}
		int j = 0;
		foreach(RoomInfo game in YccioNetworkManager.Instance.GetGameList())
		{
			GameObject newGame = Instantiate(roomButtonPrefab, transform);
			RoomData data = newGame.GetComponent<RoomData>();
			data.isPlaying = game.IsPlaying;
			data.gameName = game.Name;
			data.playerNum = game.PlayerCount;
			string roomNumber = "";
			if(j > 99) roomNumber = j.ToString();
			else if (j > 9) roomNumber = "0"+j.ToString();
			else roomNumber = "00"+j.ToString();
			data.roomName.text = data.gameName;
			data.roomName.color = data.isPlaying ? Color.gray : Color.white;
			data.roomNumber.text = roomNumber;
            
			newGame.transform.SetAsFirstSibling();
			j++;
		}
	}


	public void OnClickNewGame()
	{
        createRoomPanel.SetActive(true);
        SoundManager.Instance.PlaySound(0);

    }

	public void OnClickJoinGame(string gameName)
	{
        YccioNetworkManager.Instance.JoinGame(gameName);

    }

	public void OnClickRoom(RoomData roomData)
	{
		roomPanel.SetActive(true);
		RoomData data = roomData;
        RoomPanel panel = roomPanel.GetComponent<RoomPanel>();

        panel.gameName.text = data.gameName;
		panel.playerNum.text = data.playerNum > 1 ? data.playerNum + " Players" : data.playerNum + " Player";
		panel.roomState.text = data.isPlaying ? "Playing" : "Waiting";
		panel.joinButton.interactable = data.isPlaying ? false : true;
        SoundManager.Instance.PlaySound(0);

    }




}
