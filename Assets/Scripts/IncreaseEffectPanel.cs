﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseEffectPanel : MonoBehaviour {

	public GameObject increaseMoney;
	public GameObject posPrefab;
	
	private Transform[] pos;
	public static IncreaseEffectPanel Instance{get; private set;}
	
	void Awake()
	{
		if(Instance == null) Instance = this;
	}

	public void InitPanel()
	{
		for(int i = 0; i < YccioNetworkManager.Instance.playerNumber - 1; i++)
		{
			Instantiate(posPrefab, transform);
		}
        pos = GetComponentsInChildren<Transform>();

    }

    public void NewIncrease(int value, int num)
    {
        IncreaseEffect increase = Instantiate(increaseMoney, pos[num]).GetComponent<IncreaseEffect>();
		increase.SetValue(value);
    }

}
