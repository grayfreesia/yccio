﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPanel : MonoBehaviour {

	public void OnClickCancel()
	{
		gameObject.SetActive(false);
        SoundManager.Instance.PlaySound(0);

    }

	public void OnClickRule()
	{
        Application.OpenURL("http://www.dagoygames.com/shop/main/html.php?htmid=proc/lexio02.htm");
    }
}
