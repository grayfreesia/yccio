﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    private int chips = 0;
    public int Chips
    {
        get { return chips; }
        set
        {
			chips = value > 0 ? value : 0;
			PlayerPrefs.SetInt("chips", chips);
			PlayerPrefs.Save();
        }
    }
    private string playerName = "Unknown";
	public string PlayerName
	{
		get{return playerName;}
		set
		{
			playerName = value;
			PlayerPrefs.SetString("playername", value);
            PlayerPrefs.Save();
        }
	}
    public static PlayerData Instance { get; private set; }

    void Awake()
    {
        if (Instance == null) Instance = this;
    }
    private void Start()
    {
        PlayerName = PlayerPrefs.GetString("playername", "Unknown" + Random.Range(0, 10).ToString());
		Chips = PlayerPrefs.GetInt("chips", 200);
    }
}
