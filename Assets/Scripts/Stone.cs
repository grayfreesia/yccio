﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum StoneType
{
    Cloud = 0,
    Star = 1,
    Moon = 2,
    Sun = 3
}
public class Stone : MonoBehaviour
{
    public int stoneNumber = 0;
    public int PointNumber
    {
        get
        {
            return stoneNumber > 7 ? stoneNumber - 8 : stoneNumber + 52;
        }
    }
    public int ColorNumber
    {
        get
        {
            return (int)type * 15 + ((number < 3) ? number + 12 : number - 3);
        }
    }
    public int number = 0;
    public StoneType type = StoneType.Cloud;
    public GameObject selectImage;


    private bool isSelected = false;
    public bool IsSelected
    {
        get
        {
            return isSelected;
        }
        set
        {
            OnSelectStone(value);
        }
    }

    public void InitStone(int stone)
    {
        Image image = GetComponent<Image>();
        stoneNumber = stone;
        number = (stoneNumber / 4) + 1;
        type = (StoneType)(stoneNumber % 4);


        switch (type)
        {
            case StoneType.Cloud:
                image.sprite = Deck.Instance.cloud[number - 1];
                break;
            case StoneType.Star:
                image.sprite = Deck.Instance.star[number - 1];
                break;
            case StoneType.Moon:
                image.sprite = Deck.Instance.moon[number - 1];
                break;
            case StoneType.Sun:
                image.sprite = Deck.Instance.sun[number - 1];
                break;
            default:
                break;
        }

    }

    public void OnStoneClick()
    {
        GetComponent<Button>().Select();
        if (YccioNetworkManager.Instance.isMyTurn)
        {
            IsSelected = !IsSelected;
            SoundManager.Instance.PlaySound(2);
        }
    }

    public void OnSelectStone(bool select)
    {
        if (select && Deck.Instance.maxSubmit <= Deck.Instance.curSubmit) return;
        isSelected = select;
        selectImage.SetActive(select);
        Deck.Instance.curSubmit += select ? 1 : -1;
    }

}
