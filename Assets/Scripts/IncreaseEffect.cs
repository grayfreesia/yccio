﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncreaseEffect : MonoBehaviour
{

    public Text text;
    void Awake()
    {
        text = GetComponent<Text>();
        Destroy(transform.parent.gameObject, 1f);
    }

    void Update()
    {
        transform.Translate(0f, 100f * Time.deltaTime, 0f);
        text.color = text.color - new Color(0f, 0f, 0f, 0.02f);
    }

    public void SetValue(int value)
    {

        text.text = value >= 0 ? "+" + value.ToString() : value.ToString();
        text.color = value >= 0 ? Color.yellow : Color.red;

    }

}
