﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public enum PointOffset
{
    Single = 0,
    Pair = 100,
    Triple = 200,
    Straight = 300,
    Flush = 400,
    Fullhouse = 500,
    Fourcard = 600,
    StraightFlush = 700
}
public class Deck : MonoBehaviour
{
    public Image deckImage;
    public GameObject stonePrefab;
    public bool isFirst = false;
    public int maxSubmit = 5;
    public int curSubmit = 0;
    public int numOfStone = 0;
    public int[] stoneSubmit;
    public int[] stoneForPoint;
    public int[] stoneForNumber;
    public Stone[] stoneToDestory;

    public Sprite[] cloud;
    public Sprite[] star;
    public Sprite[] moon;
    public Sprite[] sun;

    public static Deck Instance { get; private set; }
    void Awake()
    {
        if (Instance == null) Instance = this;
        deckImage = transform.parent.parent.GetComponent<Image>();
    }

    public void SetActiveColor(bool active)
    {
        deckImage.color = active ? Color.white : Color.gray;
    }

    public void ClearDeck()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        numOfStone = 0;
    }

    public void UnselectOnPass()
    {
        Stone[] stones = GetComponentsInChildren<Stone>();
        for (int i = 0; i < stones.Length; i++)
        {
            if (stones[i].IsSelected)
            {
                stones[i].IsSelected = false;
            }
        }
    }

    public int CalRemain()
    {
        Stone[] stones = GetComponentsInChildren<Stone>();
        int remain = stones.Length;
        for (int i = 0; i < stones.Length; i++)
        {
            if (stones[i].number == 2)
            {
                remain *= 2;
            }
        }
        YccioNetworkManager.Instance.SendRemainStones(YccioNetworkManager.Instance.playOrder, remain);

        return remain;
    }

    public void AddStone(int stoneNum)
    {
        if (stoneNum == 8) isFirst = true;//cloud 3
        GameObject stone = Instantiate(stonePrefab, transform);
        stone.GetComponent<Stone>().InitStone(stoneNum);
        numOfStone++;
    }

    public void InitStones()
    {
        //Debug.Log("init stone");
        stoneSubmit = new int[5];
        stoneForPoint = new int[5];
        stoneForNumber = new int[5];
        stoneToDestory = new Stone[5];
        for (int i = 0; i < stoneSubmit.Length; i++)
        {
            stoneSubmit[i] = -1;
            stoneForPoint[i] = -1;
            stoneForNumber[i] = -1;
        }
    }

    public void Submit()
    {
        InitStones();

        Stone[] stones = GetComponentsInChildren<Stone>();
        int j = 0;
        for (int i = 0; i < stones.Length; i++)
        {
            if (stones[i].IsSelected)
            {
                stoneSubmit[j] = stones[i].stoneNumber;
                stoneToDestory[j++] = stones[i];
            }
        }
        if (j == 4) return;
        if (!Field.Instance.isFirstField && Field.Instance.numOfStone != j) return;

        int point = CalRating(j);
        if (Field.Instance.prevPoint > point) return;

        for (int i = 0; i < j; i++)
        {
            DestroyImmediate(stoneToDestory[i].gameObject);
        }
        numOfStone -= j;
        YccioNetworkManager.Instance.SendRemainStones(YccioNetworkManager.Instance.playOrder, numOfStone);
        int[] data = new int[8];
        data[0] = 1;//submit
        data[1] = point;//previous point
        data[2] = j;//num of stone
        for (int i = 0; i < j; i++)
        {
            data[i + 3] = stoneSubmit[i];//stone data
        }
        if (numOfStone == 0)
        {
            data[0] = 2;//finish
            YccioNetworkManager.Instance.isWinner = true;
        }
        YccioNetworkManager.Instance.turnManager.SendMove(data, true);
        curSubmit = 0;


    }

    public int CalRating(int stoneNum)
    {
        //calculate rating
        ArrangeStoneReference();

        switch (stoneNum)
        {
            case 1:
                //single
                return stoneForPoint[0];
            case 2:
                //pair
                if ((stoneSubmit[0] / 4) == (stoneSubmit[1] / 4))
                {
                    return (int)PointOffset.Pair + Mathf.Max(stoneForPoint);
                }
                else return -1;
            case 3:
                //triple
                if ((stoneSubmit[0] / 4) == (stoneSubmit[1] / 4) && (stoneSubmit[0] / 4) == (stoneSubmit[2] / 4))
                {
                    return (int)PointOffset.Triple + stoneForPoint[0];
                }
                else return -1;
            case 5:

                int straight = IsStraight();
                int flush = IsFlush();
                int fourcard = IsFourcard();
                int fullhouse = IsFullhouse();
                if (straight > 0 && flush > 0)
                {
                    return (int)PointOffset.StraightFlush + straight;
                }
                else if (fourcard > 0)
                {
                    return (int)PointOffset.Fourcard + fourcard;
                }
                else if (fullhouse > 0)
                {
                    return (int)PointOffset.Fullhouse + fullhouse;
                }
                else if (flush > 0)
                {
                    return (int)PointOffset.Flush + flush;
                }
                else if (straight > 0)
                {
                    return (int)PointOffset.Straight + straight;
                }
                else return -1;
            default:
                return -1;
        }

    }

    private void ArrangeStoneReference()
    {
        //if stone number 1 or 2, point up else down
        for (int i = 0; i < stoneForPoint.Length; i++)
        {
            if (stoneSubmit[i] == -1)
            {
                ;
            }
            else if (stoneSubmit[i] < 8)
            {
                stoneForPoint[i] = stoneSubmit[i] + 52;
            }
            else
            {
                stoneForPoint[i] = stoneSubmit[i] - 8;
            }
        }

        for (int i = 0; i < stoneForNumber.Length; i++)
        {
            stoneForNumber[i] = stoneSubmit[i] / 4 + 1;
        }
        Array.Sort(stoneForNumber);
    }

    public int IsStraight()
    {
        //int[] stoneForStraight = new int[5];
        //init

        bool hasOne = false;
        bool hasTwo = false;
        for (int i = 0; i < stoneForNumber.Length; i++)
        {
            if (stoneForNumber[i] == 1) hasOne = true;
            else if (stoneForNumber[i] == 2) hasTwo = true;
        }

        if (hasOne && hasTwo)
        {
            if (stoneForNumber[2] == 3 && stoneForNumber[3] == 4 && stoneForNumber[4] == 5)
            {
                return Mathf.Max(stoneForPoint);
            }
        }
        else if (hasOne)
        {
            int max = YccioNetworkManager.Instance.maxStone;
            if (stoneForNumber[1] == max - 3 && stoneForNumber[2] == max - 2 && stoneForNumber[3] == max - 1 && stoneForNumber[4] == max)
            {
                return Mathf.Max(stoneForPoint) - 4;
            }
        }
        else
        {
            int top = stoneForNumber[4];
            if (stoneForNumber[0] == top - 4 && stoneForNumber[1] == top - 3 && stoneForNumber[2] == top - 2 && stoneForNumber[3] == top - 1)
            {
                return Mathf.Max(stoneForPoint) - 4;
            }
        }
        return -1;
    }

    public int IsFlush()
    {
        int type = stoneSubmit[0] % 4;
        for (int i = 1; i < stoneSubmit.Length; i++)
        {
            if (type != stoneSubmit[i] % 4)
            {
                return -1;
            }
        }
        return Mathf.Max(stoneForPoint);
    }

    public int IsFourcard()
    {
        if (stoneForNumber[1] == stoneForNumber[4] || stoneForNumber[0] == stoneForNumber[3])
        {
            return stoneForNumber[2] < 3 ? stoneForNumber[2] + 12 : stoneForNumber[2] - 3;
        }
        return -1;
    }

    public int IsFullhouse()
    {
        if (stoneForNumber[0] == stoneForNumber[1] && stoneForNumber[3] == stoneForNumber[4])
        {
            if (stoneForNumber[2] == stoneForNumber[3] || stoneForNumber[2] == stoneForNumber[1])
            {
                return stoneForNumber[2] < 3 ? stoneForNumber[2] + 12 : stoneForNumber[2] - 3;
            }
        }
        return -1;
    }
}