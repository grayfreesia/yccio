﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomPanel : MonoBehaviour {

	public Text gameName;
	public Text playerNum;
	public Text roomState;
	public Button joinButton;

	public void OnClickJoin()
	{
		RoomList.Instance.OnClickJoinGame(gameName.text);
        SoundManager.Instance.PlaySound(0);

    }

	public void OnClickCancel()
	{
		gameObject.SetActive(false);
        SoundManager.Instance.PlaySound(0);

    }
}
