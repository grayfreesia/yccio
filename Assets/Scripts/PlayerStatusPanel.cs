﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatusPanel : MonoBehaviour
{
	public GameObject playerStatusPrefab;
	public GameObject waitingPanel;
	public bool isWaiting = false;
	public bool[] isExist = new bool[5]{false, false, false, false, false};

	float timer = 0f;

	public static PlayerStatusPanel Instance{get; private set;}

	void Awake()
	{
		if(Instance == null) Instance = this;
	}

	void Update()
	{
		if(PhotonNetwork.room.IsPlaying && transform.childCount < 1 && !isWaiting)
		{
			waitingPanel.SetActive(true);
			isWaiting = true;
		}
		else if(isWaiting && transform.childCount > 0)
		{
			waitingPanel.SetActive(false);
			isWaiting = false;
		}
		
		
	}

	public void AddStatus(int playOrder, string playerName, int chip, int remain)
	{
		if(isExist[playOrder])
		{
			return;
		}
		else
		{
			isExist[playOrder] = true;
		}
		PlayerStatus status = Instantiate(playerStatusPrefab, transform).GetComponent<PlayerStatus>();
		status.SetPlayerColor(playOrder);
		status.SetPlayerName(playerName);
		status.SetPlayerChip(chip);
		status.SetPlayerStone(remain);
	}

	public void RemoveAll()
	{
		for(int i = 0; i < transform.childCount; i++)
		{
			Destroy(transform.GetChild(i).gameObject);
		}
        isExist = new bool[5] { false, false, false, false, false };
	}

	public void SetActiveColor(int activePlayOrder)
	{
		PlayerStatus[] statuses = GetComponentsInChildren<PlayerStatus>();
		for(int i = 0; i < statuses.Length; i++)
		{
			statuses[i].SetActiveColor(activePlayOrder);
		}
	}

    public void UpdateRemainStone(int activePlayOrder, int remain)
    {
        StartCoroutine(CR_UpdateRemainStone(activePlayOrder, remain));
    }

	IEnumerator CR_UpdateRemainStone(int activePlayOrder, int remain)
	{
        PlayerStatus[] statuses = GetComponentsInChildren<PlayerStatus>();
		if(YccioNetworkManager.Instance.playerNumber != statuses.Length + 1)
		{
			yield return null;
		}
        for (int i = 0; i < statuses.Length; i++)
        {
            statuses[i].UpdateRemainStone(activePlayOrder, remain);
        }
	}

	public PlayerStatus CurrentPlayerStatus()
	{
        PlayerStatus[] statuses = GetComponentsInChildren<PlayerStatus>();
        for (int i = 0; i < statuses.Length; i++)
        {
            if(YccioNetworkManager.Instance.curOrder == statuses[i].order)
			{
				return statuses[i];
			}
        }
		return null;
	}

	public void GetImoji(int[] data)
	{
		int order = data[0];
		int num = data[1];
		SoundManager.Instance.PlaySound(10+num);
		if(order == YccioNetworkManager.Instance.playOrder) return;

		PlayerStatus sender = new PlayerStatus();
        PlayerStatus[] statuses = GetComponentsInChildren<PlayerStatus>();
        for (int i = 0; i < statuses.Length; i++)
        {
            if (order == statuses[i].order)
            {
                sender = statuses[i];
            }
        }
		sender.Imoji(num);

	}

}
