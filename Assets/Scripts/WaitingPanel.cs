﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitingPanel : MonoBehaviour {

	public void OnClickReset()
	{
		YccioNetworkManager.Instance.EndGame(true);
		PlayerStatusPanel.Instance.isWaiting = false;
		gameObject.SetActive(false);
	}
}
