﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseParticle : MonoBehaviour {


	public void Init(bool win)
	{
		if(win)
		{
            switch (Random.Range(0, 5))
            {
                case 0:
                    GetComponent<Image>().color = new Color32(255, 31, 31, 255);
                    break;
                case 1:
                    GetComponent<Image>().color = new Color32(31, 140, 255, 255);
                    break;
                case 2:
                    GetComponent<Image>().color = new Color32(31, 195, 31, 255);
                    break;
                case 3:
                    GetComponent<Image>().color = new Color32(255, 196, 31, 255);
                    break;
                case 4:
                    GetComponent<Image>().color = new Color32(195, 86, 255, 255);
                    break;
                default:
                    break;

            }
            GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f, 1f), Random.Range(0f, 2f)) * 60000f, ForceMode2D.Force);
		}
		else
		{
            switch (Random.Range(0, 2))
            {
                case 0:
                    GetComponent<Image>().color = Color.gray;
                    break;
                case 1:
                    GetComponent<Image>().color = new Color(.2f, .2f, .2f, 1f);
                    break;
                default:
                    break;

            }
            GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f, 1f), Random.Range(0f, 2f)) * 30000f, ForceMode2D.Force);
        }
        Destroy(gameObject, 5f);

    }
	
	
}
