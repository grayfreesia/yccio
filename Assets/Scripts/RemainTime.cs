﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemainTime : MonoBehaviour
{
    private Text text;
    private Slider slider;
    private PlayerStatus current;
	public static RemainTime Instance{get; private set;}
    void Awake()
    {
		if(Instance == null) Instance = this;
        text = GetComponent<Text>();
        slider = GetComponentInChildren<Slider>();
    }
    void Update()
    {
		if(!PhotonNetwork.room.IsPlaying) 
        {
            Invisible();
            return;
        }
        float time = YccioNetworkManager.Instance.turnManager.RemainingSecondsInTurn;
        if(time > 20) time = 20f;
        text.text = time.ToString("F0");
        if (!YccioNetworkManager.Instance.isMyTurn)
        {
			if(current != null) current.UpdateTime(time/ YccioNetworkManager.Instance.turnManager.TurnDuration);
            slider.value = 0;
        }
        else
        {

            slider.value = time / YccioNetworkManager.Instance.turnManager.TurnDuration;
        }
    }

    public void SetCurrentPlayerStatus()
    {
        if (current != null) current.UpdateTime(0f);
        PlayerStatus temp = PlayerStatusPanel.Instance.CurrentPlayerStatus();
		if(temp != null)
		{
			current = temp;
		}
    }

	public void Invisible()
	{
		text.text = "";
		slider.value = 0f;
	}
}
