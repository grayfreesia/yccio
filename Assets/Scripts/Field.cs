﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Field : MonoBehaviour
{
    public GameObject stonePrefab;
    public Image fieldImage;
    public Text t;
    public bool isFirstField = true;//올패스 이후 처음 낼 때
    public int prevPoint = 0;
    public int numOfStone = 5;
    public static Field Instance { get; private set; }
    void Awake()
    {
        if (Instance == null) Instance = this;
        fieldImage = GetComponent<Image>();
    }

    public void AddStone(int stoneNum)
    {
        GameObject stone = Instantiate(stonePrefab, transform);
        stone.GetComponent<Stone>().InitStone(stoneNum);
    }

    public void SortStone(int point)
    {
        bool isStraight = false;
        bool hasTwo = false;

        if (point >= 700 || (point >= 300 && point < 400))
        {
            isStraight = true;
        }
        Stone[] stones = GetComponentsInChildren<Stone>();
        int[] sorted = new int[stones.Length];
        for (int i = 0; i < stones.Length; i++)
        {
            if (stones[i].number == 2)
            {
                hasTwo = true;
            }
        }
        if (hasTwo && isStraight)
        {
            for (int i = 0; i < stones.Length; i++)
            {
                sorted[i] = stones[i].number;
            }
            Array.Sort(sorted);
            for (int i = 0; i < stones.Length; i++)
            {
                for (int j = 0; j < stones.Length; j++)
                {
                    if (sorted[i] == stones[j].number)
                    {
                        stones[j].transform.SetSiblingIndex(i);
                    }
                }
            }
        }
		else
		{
            for (int i = 0; i < stones.Length; i++)
            {
                sorted[i] = stones[i].PointNumber;
            }
            Array.Sort(sorted);
            for (int i = 0; i < stones.Length; i++)
            {
                for (int j = 0; j < stones.Length; j++)
                {
                    if (sorted[i] == stones[j].PointNumber)
                    {
                        stones[j].transform.SetSiblingIndex(i);
                    }
                }
            }
		}

    }

    public void RemoveAll()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public void ClearField()
    {
        RemoveAll();
        isFirstField = true;
        prevPoint = 0;
        numOfStone = 5;
        //t.text = "0";

    }

    public void SetFieldColor(int num)
    {
        switch (num)
        {
            case -1:
                fieldImage.color = Color.white;
                break;
            case 0:
                fieldImage.color = new Color32(255, 31, 31, 255);
                break;
            case 1:
                fieldImage.color = new Color32(31, 140, 255, 255);
                break;
            case 2:
                fieldImage.color = new Color32(31, 195, 31, 255);
                break;
            case 3:
                fieldImage.color = new Color32(255, 196, 31, 255);
                break;
            case 4:
                fieldImage.color = new Color32(195, 86, 255, 255);
                break;
            default:
                break;

        }

    }

    // public void TestPoint(int p)
    // {
    //     t.text = prevPoint.ToString() + "/" + p.ToString();
    // }
}
