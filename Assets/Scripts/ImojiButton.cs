﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ImojiButton : MonoBehaviour
{

    public Sprite[] imojis;
    public GameObject container;
    public GameObject my;
    bool isOn = false;
	bool isImojing = false;

	public static ImojiButton Instance{get; private set;}

	void Awake()
	{
		if(Instance == null) Instance = this;
	}
    
    public void OnClickImoji()
    {
        container.SetActive(!isOn);
        isOn = !isOn;
    }

    public void OnSelectImoji(int num)
    {
        OnClickImoji();
		if(isImojing) return;
        my.SetActive(true);
        my.GetComponent<Image>().sprite = imojis[num];
        my.GetComponent<Image>().color = Color.white;
		isImojing = true;
        Invoke("ImojiFade", 1f);

		YccioNetworkManager.Instance.SendImoji(num);
    }

    public void ImojiFade()
    {
        StartCoroutine(CR_ImojiFade());
    }

    IEnumerator CR_ImojiFade()
    {
        for (int i = 0; i < 100; i++)
        {
			my.GetComponent<Image>().color -= new Color(0f, 0f, 0f, 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
		isImojing = false;
		my.SetActive(false);
    }
}
