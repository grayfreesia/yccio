﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayInfoPanel : MonoBehaviour {

	public void OnClickOK()
	{
		gameObject.SetActive(false);
		PlayerPrefs.SetInt("playinfo", 1);
		PlayerPrefs.Save();
	}
}
