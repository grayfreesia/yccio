﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubmitButton : MonoBehaviour {

	public static SubmitButton Instance{get; private set;}
	private Button b;
	void Awake()
	{
		if(Instance == null) Instance = this;
		b = GetComponent<Button>();
	}

	public void OnClickSubmit()
	{
		Deck.Instance.Submit();
	}

	public void ActiveButton(bool active)
	{
		b.interactable = active;
	}
}
