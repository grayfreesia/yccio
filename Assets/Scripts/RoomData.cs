﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomData : MonoBehaviour
{
    public string gameName;
    public bool isPlaying;
    public int playerNum;
    public Text roomNumber;
    public Text roomName;

    public void OnClickRoom()
    {
        RoomList.Instance.OnClickRoom(this);
    }
}
