﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyPanel : MonoBehaviour
{

    public Text playerName;
    public Text chip;
    public GameObject editNamePanel;
    public GameObject exitPanel;
    public GameObject howtoPanel;
    public Image rating;

    public bool isEditOn = false;
    public bool isExitOn = false;

    public static LobbyPanel Instance { get; private set; }

    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    void Start()
    {
        int point = PlayerData.Instance.Chips;
        chip.text = point.ToString();
        playerName.text = PlayerData.Instance.PlayerName;
        if(point > 1999)
        {
            rating.sprite = SpriteContainer.Instance.ratings[4];
        }
        else if (point > 999)
        {
            rating.sprite = SpriteContainer.Instance.ratings[3];
        }
        else if (point > 499)
        {
            rating.sprite = SpriteContainer.Instance.ratings[2];
        }
        else if (point > 199)
        {
            rating.sprite = SpriteContainer.Instance.ratings[1];
        }
        else
        {
            rating.sprite = SpriteContainer.Instance.ratings[0];
        }
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            if (!isEditOn && !isExitOn)
            {
                OnClickExit();
            }
			else if(isEditOn)
			{
				editNamePanel.SetActive(false);
				isEditOn = false;
			}
			else if(isExitOn)
			{
				exitPanel.SetActive(false);
				isExitOn = false;
			}
        }

        if(Input.GetKeyDown(KeyCode.X))
        {
            PlayerData.Instance.Chips += 200;
            LobbyPanel.Instance.chip.text = PlayerData.Instance.Chips.ToString();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            PlayerData.Instance.Chips -= 200;
            LobbyPanel.Instance.chip.text = PlayerData.Instance.Chips.ToString();
        }
    }

    public void OnClickEditNameButton()
    {
        editNamePanel.SetActive(true);
		isEditOn = true;
        SoundManager.Instance.PlaySound(0);
    }

    public void OnClickGetChip()
    {
        if (PlayerData.Instance.Chips < 100)
        {
            PlayerData.Instance.Chips = 100;
            LobbyPanel.Instance.chip.text = 100.ToString();
        }
        SoundManager.Instance.PlaySound(0);

    }

    public void OnClickRefresh()
    {
        YccioNetworkManager.Instance.EnterLobby();
        YccioNetworkManager.Instance.UpdateRoom();
        SoundManager.Instance.PlaySound(0);

    }

    public void OnClickRule()
    {
        howtoPanel.SetActive(true);
        SoundManager.Instance.PlaySound(0);

    }

    public void OnClickExit()
    {
        exitPanel.SetActive(true);
		isExitOn = true;
        SoundManager.Instance.PlaySound(0);

    }
}
