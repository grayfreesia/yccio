﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameExitPanel : MonoBehaviour {

    public Text t;

    public void OnEnable()
    {
        if (PhotonNetwork.room.IsPlaying)
        {
            t.text = @"Do You Really Want to Exit?
You May Lose Some Points.";
        }
        else
        {
            t.text = "Do You Really Want to Exit?";
        }
    }
    public void OnClickYes()
    {
        SoundManager.Instance.PlaySound(9);

        YccioNetworkManager.Instance.LeaveGame();
    }

    public void OnClickCancel()
    {
        gameObject.SetActive(false);
        InGamePanel.Instance.isExitOn = false;
        SoundManager.Instance.PlaySound(0);

    }
}
