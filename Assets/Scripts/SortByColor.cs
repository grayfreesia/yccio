﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SortByColor : MonoBehaviour {

    private Button b;
    public static SortByColor Instance { get; private set; }
    void Awake()
    {
        if (Instance == null) Instance = this;
        b = GetComponent<Button>();
    }

    public void OnClickSortByColor()
    {
        Stone[] stones = Deck.Instance.GetComponentsInChildren<Stone>();
        int[] sorted = new int[stones.Length];
        for (int i = 0; i < stones.Length; i++)
        {
            sorted[i] = stones[i].ColorNumber;
        }
        Array.Sort(sorted);
        for (int i = 0; i < stones.Length; i++)
        {
            for (int j = 0; j < stones.Length; j++)
            {
                if (sorted[i] == stones[j].ColorNumber)
                {
                    stones[j].transform.SetSiblingIndex(i);
                }
            }
        }
        SoundManager.Instance.PlaySound(0);


    }

    public void ActiveButton(bool active)
    {
        b.interactable = active;
    }
}
