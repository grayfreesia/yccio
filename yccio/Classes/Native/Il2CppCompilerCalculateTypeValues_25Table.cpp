﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ExitGames.Client.Photon.Chat.AuthenticationValues
struct AuthenticationValues_t187933346;
// ExitGames.Client.Photon.Chat.ChatPeer
struct ChatPeer_t2186541770;
// ExitGames.Client.Photon.Chat.IChatClientListener
struct IChatClientListener_t915757805;
// ExitGames.Client.Photon.EncryptorManaged.Decryptor
struct Decryptor_t2116099858;
// ExitGames.Client.Photon.EncryptorManaged.Encryptor
struct Encryptor_t200327285;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t2581629031;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t2956237011;
// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t1302902347;
// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t4013908777;
// ImojiButton
struct ImojiButton_t701272134;
// PhotonView
struct PhotonView_t2207721820;
// PlayerData
struct PlayerData_t220878115;
// PlayerStatus
struct PlayerStatus_t402953766;
// PlayerStatusPanel
struct PlayerStatusPanel_t1209538314;
// PlayerStatus[]
struct PlayerStatusU5BU5D_t1007451779;
// PunTurnManager
struct PunTurnManager_t1223962931;
// RoomList
struct RoomList_t2314265074;
// SceneTransition
struct SceneTransition_t1138091307;
// StartButton
struct StartButton_t1998348287;
// Stone[]
struct StoneU5BU5D_t1191912092;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t1720840067;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type>
struct Dictionary_2_t1253839074;
// System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel>
struct Dictionary_2_t3099565493;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// WinLosePanel
struct WinLosePanel_t3114026018;
// YccioNetworkManager
struct YccioNetworkManager_t3600504787;




#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TIMEKEEPER_T3694205465_H
#define TIMEKEEPER_T3694205465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.DemoParticle.TimeKeeper
struct  TimeKeeper_t3694205465  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.DemoParticle.TimeKeeper::lastExecutionTime
	int32_t ___lastExecutionTime_0;
	// System.Boolean ExitGames.Client.DemoParticle.TimeKeeper::shouldExecute
	bool ___shouldExecute_1;
	// System.Int32 ExitGames.Client.DemoParticle.TimeKeeper::<Interval>k__BackingField
	int32_t ___U3CIntervalU3Ek__BackingField_2;
	// System.Boolean ExitGames.Client.DemoParticle.TimeKeeper::<IsEnabled>k__BackingField
	bool ___U3CIsEnabledU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_lastExecutionTime_0() { return static_cast<int32_t>(offsetof(TimeKeeper_t3694205465, ___lastExecutionTime_0)); }
	inline int32_t get_lastExecutionTime_0() const { return ___lastExecutionTime_0; }
	inline int32_t* get_address_of_lastExecutionTime_0() { return &___lastExecutionTime_0; }
	inline void set_lastExecutionTime_0(int32_t value)
	{
		___lastExecutionTime_0 = value;
	}

	inline static int32_t get_offset_of_shouldExecute_1() { return static_cast<int32_t>(offsetof(TimeKeeper_t3694205465, ___shouldExecute_1)); }
	inline bool get_shouldExecute_1() const { return ___shouldExecute_1; }
	inline bool* get_address_of_shouldExecute_1() { return &___shouldExecute_1; }
	inline void set_shouldExecute_1(bool value)
	{
		___shouldExecute_1 = value;
	}

	inline static int32_t get_offset_of_U3CIntervalU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TimeKeeper_t3694205465, ___U3CIntervalU3Ek__BackingField_2)); }
	inline int32_t get_U3CIntervalU3Ek__BackingField_2() const { return ___U3CIntervalU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CIntervalU3Ek__BackingField_2() { return &___U3CIntervalU3Ek__BackingField_2; }
	inline void set_U3CIntervalU3Ek__BackingField_2(int32_t value)
	{
		___U3CIntervalU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsEnabledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TimeKeeper_t3694205465, ___U3CIsEnabledU3Ek__BackingField_3)); }
	inline bool get_U3CIsEnabledU3Ek__BackingField_3() const { return ___U3CIsEnabledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsEnabledU3Ek__BackingField_3() { return &___U3CIsEnabledU3Ek__BackingField_3; }
	inline void set_U3CIsEnabledU3Ek__BackingField_3(bool value)
	{
		___U3CIsEnabledU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEKEEPER_T3694205465_H
#ifndef CHATCHANNEL_T3314309194_H
#define CHATCHANNEL_T3314309194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ChatChannel
struct  ChatChannel_t3314309194  : public RuntimeObject
{
public:
	// System.String ExitGames.Client.Photon.Chat.ChatChannel::Name
	String_t* ___Name_0;
	// System.Collections.Generic.List`1<System.String> ExitGames.Client.Photon.Chat.ChatChannel::Senders
	List_1_t3319525431 * ___Senders_1;
	// System.Collections.Generic.List`1<System.Object> ExitGames.Client.Photon.Chat.ChatChannel::Messages
	List_1_t257213610 * ___Messages_2;
	// System.Int32 ExitGames.Client.Photon.Chat.ChatChannel::MessageLimit
	int32_t ___MessageLimit_3;
	// System.Boolean ExitGames.Client.Photon.Chat.ChatChannel::<IsPrivate>k__BackingField
	bool ___U3CIsPrivateU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ChatChannel_t3314309194, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Senders_1() { return static_cast<int32_t>(offsetof(ChatChannel_t3314309194, ___Senders_1)); }
	inline List_1_t3319525431 * get_Senders_1() const { return ___Senders_1; }
	inline List_1_t3319525431 ** get_address_of_Senders_1() { return &___Senders_1; }
	inline void set_Senders_1(List_1_t3319525431 * value)
	{
		___Senders_1 = value;
		Il2CppCodeGenWriteBarrier((&___Senders_1), value);
	}

	inline static int32_t get_offset_of_Messages_2() { return static_cast<int32_t>(offsetof(ChatChannel_t3314309194, ___Messages_2)); }
	inline List_1_t257213610 * get_Messages_2() const { return ___Messages_2; }
	inline List_1_t257213610 ** get_address_of_Messages_2() { return &___Messages_2; }
	inline void set_Messages_2(List_1_t257213610 * value)
	{
		___Messages_2 = value;
		Il2CppCodeGenWriteBarrier((&___Messages_2), value);
	}

	inline static int32_t get_offset_of_MessageLimit_3() { return static_cast<int32_t>(offsetof(ChatChannel_t3314309194, ___MessageLimit_3)); }
	inline int32_t get_MessageLimit_3() const { return ___MessageLimit_3; }
	inline int32_t* get_address_of_MessageLimit_3() { return &___MessageLimit_3; }
	inline void set_MessageLimit_3(int32_t value)
	{
		___MessageLimit_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsPrivateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ChatChannel_t3314309194, ___U3CIsPrivateU3Ek__BackingField_4)); }
	inline bool get_U3CIsPrivateU3Ek__BackingField_4() const { return ___U3CIsPrivateU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsPrivateU3Ek__BackingField_4() { return &___U3CIsPrivateU3Ek__BackingField_4; }
	inline void set_U3CIsPrivateU3Ek__BackingField_4(bool value)
	{
		___U3CIsPrivateU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCHANNEL_T3314309194_H
#ifndef CHATEVENTCODE_T3158930382_H
#define CHATEVENTCODE_T3158930382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ChatEventCode
struct  ChatEventCode_t3158930382  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATEVENTCODE_T3158930382_H
#ifndef CHATOPERATIONCODE_T2600569862_H
#define CHATOPERATIONCODE_T2600569862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ChatOperationCode
struct  ChatOperationCode_t2600569862  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATOPERATIONCODE_T2600569862_H
#ifndef CHATPARAMETERCODE_T1934080102_H
#define CHATPARAMETERCODE_T1934080102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ChatParameterCode
struct  ChatParameterCode_t1934080102  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATPARAMETERCODE_T1934080102_H
#ifndef CHATUSERSTATUS_T1535268910_H
#define CHATUSERSTATUS_T1535268910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ChatUserStatus
struct  ChatUserStatus_t1535268910  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATUSERSTATUS_T1535268910_H
#ifndef ERRORCODE_T3704103031_H
#define ERRORCODE_T3704103031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ErrorCode
struct  ErrorCode_t3704103031  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCODE_T3704103031_H
#ifndef PARAMETERCODE_T1075756884_H
#define PARAMETERCODE_T1075756884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ParameterCode
struct  ParameterCode_t1075756884  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERCODE_T1075756884_H
#ifndef U3CCR_IMOJIFADEU3EC__ITERATOR0_T3182661401_H
#define U3CCR_IMOJIFADEU3EC__ITERATOR0_T3182661401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImojiButton/<CR_ImojiFade>c__Iterator0
struct  U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401  : public RuntimeObject
{
public:
	// System.Int32 ImojiButton/<CR_ImojiFade>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// ImojiButton ImojiButton/<CR_ImojiFade>c__Iterator0::$this
	ImojiButton_t701272134 * ___U24this_1;
	// System.Object ImojiButton/<CR_ImojiFade>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ImojiButton/<CR_ImojiFade>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ImojiButton/<CR_ImojiFade>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401, ___U24this_1)); }
	inline ImojiButton_t701272134 * get_U24this_1() const { return ___U24this_1; }
	inline ImojiButton_t701272134 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ImojiButton_t701272134 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCR_IMOJIFADEU3EC__ITERATOR0_T3182661401_H
#ifndef U3CCR_IMOJIFADEU3EC__ITERATOR0_T4226668569_H
#define U3CCR_IMOJIFADEU3EC__ITERATOR0_T4226668569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerStatus/<CR_ImojiFade>c__Iterator0
struct  U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569  : public RuntimeObject
{
public:
	// System.Int32 PlayerStatus/<CR_ImojiFade>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// PlayerStatus PlayerStatus/<CR_ImojiFade>c__Iterator0::$this
	PlayerStatus_t402953766 * ___U24this_1;
	// System.Object PlayerStatus/<CR_ImojiFade>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean PlayerStatus/<CR_ImojiFade>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 PlayerStatus/<CR_ImojiFade>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569, ___U24this_1)); }
	inline PlayerStatus_t402953766 * get_U24this_1() const { return ___U24this_1; }
	inline PlayerStatus_t402953766 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PlayerStatus_t402953766 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCR_IMOJIFADEU3EC__ITERATOR0_T4226668569_H
#ifndef U3CCR_UPDATEREMAINSTONEU3EC__ITERATOR0_T3674447058_H
#define U3CCR_UPDATEREMAINSTONEU3EC__ITERATOR0_T3674447058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerStatusPanel/<CR_UpdateRemainStone>c__Iterator0
struct  U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058  : public RuntimeObject
{
public:
	// PlayerStatus[] PlayerStatusPanel/<CR_UpdateRemainStone>c__Iterator0::<statuses>__0
	PlayerStatusU5BU5D_t1007451779* ___U3CstatusesU3E__0_0;
	// System.Int32 PlayerStatusPanel/<CR_UpdateRemainStone>c__Iterator0::activePlayOrder
	int32_t ___activePlayOrder_1;
	// System.Int32 PlayerStatusPanel/<CR_UpdateRemainStone>c__Iterator0::remain
	int32_t ___remain_2;
	// PlayerStatusPanel PlayerStatusPanel/<CR_UpdateRemainStone>c__Iterator0::$this
	PlayerStatusPanel_t1209538314 * ___U24this_3;
	// System.Object PlayerStatusPanel/<CR_UpdateRemainStone>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean PlayerStatusPanel/<CR_UpdateRemainStone>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 PlayerStatusPanel/<CR_UpdateRemainStone>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CstatusesU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058, ___U3CstatusesU3E__0_0)); }
	inline PlayerStatusU5BU5D_t1007451779* get_U3CstatusesU3E__0_0() const { return ___U3CstatusesU3E__0_0; }
	inline PlayerStatusU5BU5D_t1007451779** get_address_of_U3CstatusesU3E__0_0() { return &___U3CstatusesU3E__0_0; }
	inline void set_U3CstatusesU3E__0_0(PlayerStatusU5BU5D_t1007451779* value)
	{
		___U3CstatusesU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusesU3E__0_0), value);
	}

	inline static int32_t get_offset_of_activePlayOrder_1() { return static_cast<int32_t>(offsetof(U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058, ___activePlayOrder_1)); }
	inline int32_t get_activePlayOrder_1() const { return ___activePlayOrder_1; }
	inline int32_t* get_address_of_activePlayOrder_1() { return &___activePlayOrder_1; }
	inline void set_activePlayOrder_1(int32_t value)
	{
		___activePlayOrder_1 = value;
	}

	inline static int32_t get_offset_of_remain_2() { return static_cast<int32_t>(offsetof(U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058, ___remain_2)); }
	inline int32_t get_remain_2() const { return ___remain_2; }
	inline int32_t* get_address_of_remain_2() { return &___remain_2; }
	inline void set_remain_2(int32_t value)
	{
		___remain_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058, ___U24this_3)); }
	inline PlayerStatusPanel_t1209538314 * get_U24this_3() const { return ___U24this_3; }
	inline PlayerStatusPanel_t1209538314 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayerStatusPanel_t1209538314 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCR_UPDATEREMAINSTONEU3EC__ITERATOR0_T3674447058_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CC_ONLOSEU3EC__ITERATOR1_T1181911525_H
#define U3CC_ONLOSEU3EC__ITERATOR1_T1181911525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinLosePanel/<C_OnLose>c__Iterator1
struct  U3CC_OnLoseU3Ec__Iterator1_t1181911525  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text WinLosePanel/<C_OnLose>c__Iterator1::<t>__0
	Text_t1901882714 * ___U3CtU3E__0_0;
	// System.Int32 WinLosePanel/<C_OnLose>c__Iterator1::<i>__1
	int32_t ___U3CiU3E__1_1;
	// WinLosePanel WinLosePanel/<C_OnLose>c__Iterator1::$this
	WinLosePanel_t3114026018 * ___U24this_2;
	// System.Object WinLosePanel/<C_OnLose>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WinLosePanel/<C_OnLose>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 WinLosePanel/<C_OnLose>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U3CtU3E__0_0)); }
	inline Text_t1901882714 * get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline Text_t1901882714 ** get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(Text_t1901882714 * value)
	{
		___U3CtU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U24this_2)); }
	inline WinLosePanel_t3114026018 * get_U24this_2() const { return ___U24this_2; }
	inline WinLosePanel_t3114026018 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WinLosePanel_t3114026018 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CC_ONLOSEU3EC__ITERATOR1_T1181911525_H
#ifndef U3CC_ONWINU3EC__ITERATOR0_T1703244554_H
#define U3CC_ONWINU3EC__ITERATOR0_T1703244554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinLosePanel/<C_OnWin>c__Iterator0
struct  U3CC_OnWinU3Ec__Iterator0_t1703244554  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text WinLosePanel/<C_OnWin>c__Iterator0::<t>__0
	Text_t1901882714 * ___U3CtU3E__0_0;
	// System.Int32 WinLosePanel/<C_OnWin>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_1;
	// WinLosePanel WinLosePanel/<C_OnWin>c__Iterator0::$this
	WinLosePanel_t3114026018 * ___U24this_2;
	// System.Object WinLosePanel/<C_OnWin>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WinLosePanel/<C_OnWin>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 WinLosePanel/<C_OnWin>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U3CtU3E__0_0)); }
	inline Text_t1901882714 * get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline Text_t1901882714 ** get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(Text_t1901882714 * value)
	{
		___U3CtU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U24this_2)); }
	inline WinLosePanel_t3114026018 * get_U24this_2() const { return ___U24this_2; }
	inline WinLosePanel_t3114026018 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WinLosePanel_t3114026018 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CC_ONWINU3EC__ITERATOR0_T1703244554_H
#ifndef U3CCR_BETSOUNDU3EC__ITERATOR0_T1415787259_H
#define U3CCR_BETSOUNDU3EC__ITERATOR0_T1415787259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YccioNetworkManager/<CR_BetSound>c__Iterator0
struct  U3CCR_BetSoundU3Ec__Iterator0_t1415787259  : public RuntimeObject
{
public:
	// System.Int32 YccioNetworkManager/<CR_BetSound>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.Int32 YccioNetworkManager/<CR_BetSound>c__Iterator0::num
	int32_t ___num_1;
	// System.Object YccioNetworkManager/<CR_BetSound>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean YccioNetworkManager/<CR_BetSound>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 YccioNetworkManager/<CR_BetSound>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_num_1() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___num_1)); }
	inline int32_t get_num_1() const { return ___num_1; }
	inline int32_t* get_address_of_num_1() { return &___num_1; }
	inline void set_num_1(int32_t value)
	{
		___num_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCR_BETSOUNDU3EC__ITERATOR0_T1415787259_H
#ifndef U3CCR_CALCHIPSU3EC__ITERATOR1_T2144217578_H
#define U3CCR_CALCHIPSU3EC__ITERATOR1_T2144217578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YccioNetworkManager/<CR_Calchips>c__Iterator1
struct  U3CCR_CalchipsU3Ec__Iterator1_t2144217578  : public RuntimeObject
{
public:
	// System.Int32 YccioNetworkManager/<CR_Calchips>c__Iterator1::<my>__0
	int32_t ___U3CmyU3E__0_0;
	// System.Int32 YccioNetworkManager/<CR_Calchips>c__Iterator1::<i>__1
	int32_t ___U3CiU3E__1_1;
	// YccioNetworkManager YccioNetworkManager/<CR_Calchips>c__Iterator1::$this
	YccioNetworkManager_t3600504787 * ___U24this_2;
	// System.Object YccioNetworkManager/<CR_Calchips>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean YccioNetworkManager/<CR_Calchips>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 YccioNetworkManager/<CR_Calchips>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CmyU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U3CmyU3E__0_0)); }
	inline int32_t get_U3CmyU3E__0_0() const { return ___U3CmyU3E__0_0; }
	inline int32_t* get_address_of_U3CmyU3E__0_0() { return &___U3CmyU3E__0_0; }
	inline void set_U3CmyU3E__0_0(int32_t value)
	{
		___U3CmyU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U24this_2)); }
	inline YccioNetworkManager_t3600504787 * get_U24this_2() const { return ___U24this_2; }
	inline YccioNetworkManager_t3600504787 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(YccioNetworkManager_t3600504787 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCR_CALCHIPSU3EC__ITERATOR1_T2144217578_H
#ifndef U24ARRAYTYPEU3D16_T3253128244_H
#define U24ARRAYTYPEU3D16_T3253128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128244 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128244__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128244_H
#ifndef U24ARRAYTYPEU3D32_T3651253610_H
#define U24ARRAYTYPEU3D32_T3651253610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=32
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D32_t3651253610 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D32_t3651253610__padding[32];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D32_T3651253610_H
#ifndef U24ARRAYTYPEU3D48_T1336283963_H
#define U24ARRAYTYPEU3D48_T1336283963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=48
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D48_t1336283963 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D48_t1336283963__padding[48];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D48_T1336283963_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-7A078F136EEAACFF264DA2E9994AF7D3D3536738
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-8658990BAD6546E619D8A5C4F90BCF3F089E0953
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-739C505E9F0985CE1E08892BC46BE5E839FF061A
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-35FDBB6669F521B572D4AD71DD77E77F43C1B71B
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-641FED42B144F98E4D3581E150975A5B388B5358
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-9E5175008751D08F361488C9927086B276B965FA
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5;

public:
	inline static int32_t get_offset_of_U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0() const { return ___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0() { return &___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0; }
	inline void set_U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1() const { return ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1() { return &___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1; }
	inline void set_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2() const { return ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2() { return &___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2; }
	inline void set_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3() const { return ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3() { return &___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3; }
	inline void set_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4() const { return ___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4() { return &___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4; }
	inline void set_U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5() const { return ___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5() { return &___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5; }
	inline void set_U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef CHATDISCONNECTCAUSE_T3824038781_H
#define CHATDISCONNECTCAUSE_T3824038781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ChatDisconnectCause
struct  ChatDisconnectCause_t3824038781 
{
public:
	// System.Int32 ExitGames.Client.Photon.Chat.ChatDisconnectCause::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ChatDisconnectCause_t3824038781, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATDISCONNECTCAUSE_T3824038781_H
#ifndef CHATSTATE_T3709194201_H
#define CHATSTATE_T3709194201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ChatState
struct  ChatState_t3709194201 
{
public:
	// System.Int32 ExitGames.Client.Photon.Chat.ChatState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ChatState_t3709194201, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATSTATE_T3709194201_H
#ifndef CUSTOMAUTHENTICATIONTYPE_T3431816196_H
#define CUSTOMAUTHENTICATIONTYPE_T3431816196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.CustomAuthenticationType
struct  CustomAuthenticationType_t3431816196 
{
public:
	// System.Byte ExitGames.Client.Photon.Chat.CustomAuthenticationType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CustomAuthenticationType_t3431816196, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMAUTHENTICATIONTYPE_T3431816196_H
#ifndef CONNECTIONPROTOCOL_T2586603950_H
#define CONNECTIONPROTOCOL_T2586603950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.ConnectionProtocol
struct  ConnectionProtocol_t2586603950 
{
public:
	// System.Byte ExitGames.Client.Photon.ConnectionProtocol::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionProtocol_t2586603950, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONPROTOCOL_T2586603950_H
#ifndef DEBUGLEVEL_T3671880145_H
#define DEBUGLEVEL_T3671880145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.DebugLevel
struct  DebugLevel_t3671880145 
{
public:
	// System.Byte ExitGames.Client.Photon.DebugLevel::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugLevel_t3671880145, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVEL_T3671880145_H
#ifndef SERIALIZATIONPROTOCOL_T4091957412_H
#define SERIALIZATIONPROTOCOL_T4091957412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializationProtocol
struct  SerializationProtocol_t4091957412 
{
public:
	// System.Int32 ExitGames.Client.Photon.SerializationProtocol::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SerializationProtocol_t4091957412, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONPROTOCOL_T4091957412_H
#ifndef PHOTONEVENTCODES_T582153385_H
#define PHOTONEVENTCODES_T582153385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonEventCodes
struct  PhotonEventCodes_t582153385 
{
public:
	// System.Int32 PhotonEventCodes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonEventCodes_t582153385, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONEVENTCODES_T582153385_H
#ifndef POINTOFFSET_T2729089990_H
#define POINTOFFSET_T2729089990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointOffset
struct  PointOffset_t2729089990 
{
public:
	// System.Int32 PointOffset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PointOffset_t2729089990, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTOFFSET_T2729089990_H
#ifndef STONETYPE_T314252901_H
#define STONETYPE_T314252901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StoneType
struct  StoneType_t314252901 
{
public:
	// System.Int32 StoneType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StoneType_t314252901, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STONETYPE_T314252901_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef AUTHENTICATIONVALUES_T187933346_H
#define AUTHENTICATIONVALUES_T187933346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.AuthenticationValues
struct  AuthenticationValues_t187933346  : public RuntimeObject
{
public:
	// ExitGames.Client.Photon.Chat.CustomAuthenticationType ExitGames.Client.Photon.Chat.AuthenticationValues::authType
	uint8_t ___authType_0;
	// System.String ExitGames.Client.Photon.Chat.AuthenticationValues::<AuthGetParameters>k__BackingField
	String_t* ___U3CAuthGetParametersU3Ek__BackingField_1;
	// System.Object ExitGames.Client.Photon.Chat.AuthenticationValues::<AuthPostData>k__BackingField
	RuntimeObject * ___U3CAuthPostDataU3Ek__BackingField_2;
	// System.String ExitGames.Client.Photon.Chat.AuthenticationValues::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_3;
	// System.String ExitGames.Client.Photon.Chat.AuthenticationValues::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_authType_0() { return static_cast<int32_t>(offsetof(AuthenticationValues_t187933346, ___authType_0)); }
	inline uint8_t get_authType_0() const { return ___authType_0; }
	inline uint8_t* get_address_of_authType_0() { return &___authType_0; }
	inline void set_authType_0(uint8_t value)
	{
		___authType_0 = value;
	}

	inline static int32_t get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AuthenticationValues_t187933346, ___U3CAuthGetParametersU3Ek__BackingField_1)); }
	inline String_t* get_U3CAuthGetParametersU3Ek__BackingField_1() const { return ___U3CAuthGetParametersU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAuthGetParametersU3Ek__BackingField_1() { return &___U3CAuthGetParametersU3Ek__BackingField_1; }
	inline void set_U3CAuthGetParametersU3Ek__BackingField_1(String_t* value)
	{
		___U3CAuthGetParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthGetParametersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAuthPostDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AuthenticationValues_t187933346, ___U3CAuthPostDataU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CAuthPostDataU3Ek__BackingField_2() const { return ___U3CAuthPostDataU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CAuthPostDataU3Ek__BackingField_2() { return &___U3CAuthPostDataU3Ek__BackingField_2; }
	inline void set_U3CAuthPostDataU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CAuthPostDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthPostDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AuthenticationValues_t187933346, ___U3CTokenU3Ek__BackingField_3)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_3() const { return ___U3CTokenU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_3() { return &___U3CTokenU3Ek__BackingField_3; }
	inline void set_U3CTokenU3Ek__BackingField_3(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AuthenticationValues_t187933346, ___U3CUserIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_4() const { return ___U3CUserIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_4() { return &___U3CUserIdU3Ek__BackingField_4; }
	inline void set_U3CUserIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONVALUES_T187933346_H
#ifndef CHATCLIENT_T3322764984_H
#define CHATCLIENT_T3322764984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ChatClient
struct  ChatClient_t3322764984  : public RuntimeObject
{
public:
	// System.String ExitGames.Client.Photon.Chat.ChatClient::<NameServerAddress>k__BackingField
	String_t* ___U3CNameServerAddressU3Ek__BackingField_1;
	// System.String ExitGames.Client.Photon.Chat.ChatClient::<FrontendAddress>k__BackingField
	String_t* ___U3CFrontendAddressU3Ek__BackingField_2;
	// System.String ExitGames.Client.Photon.Chat.ChatClient::chatRegion
	String_t* ___chatRegion_3;
	// ExitGames.Client.Photon.Chat.ChatState ExitGames.Client.Photon.Chat.ChatClient::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_4;
	// ExitGames.Client.Photon.Chat.ChatDisconnectCause ExitGames.Client.Photon.Chat.ChatClient::<DisconnectedCause>k__BackingField
	int32_t ___U3CDisconnectedCauseU3Ek__BackingField_5;
	// System.String ExitGames.Client.Photon.Chat.ChatClient::<AppVersion>k__BackingField
	String_t* ___U3CAppVersionU3Ek__BackingField_6;
	// System.String ExitGames.Client.Photon.Chat.ChatClient::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_7;
	// ExitGames.Client.Photon.Chat.AuthenticationValues ExitGames.Client.Photon.Chat.ChatClient::<AuthValues>k__BackingField
	AuthenticationValues_t187933346 * ___U3CAuthValuesU3Ek__BackingField_8;
	// System.Int32 ExitGames.Client.Photon.Chat.ChatClient::MessageLimit
	int32_t ___MessageLimit_9;
	// System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel> ExitGames.Client.Photon.Chat.ChatClient::PublicChannels
	Dictionary_2_t3099565493 * ___PublicChannels_10;
	// System.Collections.Generic.Dictionary`2<System.String,ExitGames.Client.Photon.Chat.ChatChannel> ExitGames.Client.Photon.Chat.ChatClient::PrivateChannels
	Dictionary_2_t3099565493 * ___PrivateChannels_11;
	// System.Collections.Generic.HashSet`1<System.String> ExitGames.Client.Photon.Chat.ChatClient::PublicChannelsUnsubscribing
	HashSet_1_t412400163 * ___PublicChannelsUnsubscribing_12;
	// ExitGames.Client.Photon.Chat.IChatClientListener ExitGames.Client.Photon.Chat.ChatClient::listener
	RuntimeObject* ___listener_13;
	// ExitGames.Client.Photon.Chat.ChatPeer ExitGames.Client.Photon.Chat.ChatClient::chatPeer
	ChatPeer_t2186541770 * ___chatPeer_14;
	// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::didAuthenticate
	bool ___didAuthenticate_16;
	// System.Int32 ExitGames.Client.Photon.Chat.ChatClient::msDeltaForServiceCalls
	int32_t ___msDeltaForServiceCalls_17;
	// System.Int32 ExitGames.Client.Photon.Chat.ChatClient::msTimestampOfLastServiceCall
	int32_t ___msTimestampOfLastServiceCall_18;
	// System.Boolean ExitGames.Client.Photon.Chat.ChatClient::<UseBackgroundWorkerForSending>k__BackingField
	bool ___U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_U3CNameServerAddressU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___U3CNameServerAddressU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameServerAddressU3Ek__BackingField_1() const { return ___U3CNameServerAddressU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameServerAddressU3Ek__BackingField_1() { return &___U3CNameServerAddressU3Ek__BackingField_1; }
	inline void set_U3CNameServerAddressU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameServerAddressU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameServerAddressU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CFrontendAddressU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___U3CFrontendAddressU3Ek__BackingField_2)); }
	inline String_t* get_U3CFrontendAddressU3Ek__BackingField_2() const { return ___U3CFrontendAddressU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CFrontendAddressU3Ek__BackingField_2() { return &___U3CFrontendAddressU3Ek__BackingField_2; }
	inline void set_U3CFrontendAddressU3Ek__BackingField_2(String_t* value)
	{
		___U3CFrontendAddressU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFrontendAddressU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_chatRegion_3() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___chatRegion_3)); }
	inline String_t* get_chatRegion_3() const { return ___chatRegion_3; }
	inline String_t** get_address_of_chatRegion_3() { return &___chatRegion_3; }
	inline void set_chatRegion_3(String_t* value)
	{
		___chatRegion_3 = value;
		Il2CppCodeGenWriteBarrier((&___chatRegion_3), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___U3CStateU3Ek__BackingField_4)); }
	inline int32_t get_U3CStateU3Ek__BackingField_4() const { return ___U3CStateU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_4() { return &___U3CStateU3Ek__BackingField_4; }
	inline void set_U3CStateU3Ek__BackingField_4(int32_t value)
	{
		___U3CStateU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CDisconnectedCauseU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___U3CDisconnectedCauseU3Ek__BackingField_5)); }
	inline int32_t get_U3CDisconnectedCauseU3Ek__BackingField_5() const { return ___U3CDisconnectedCauseU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CDisconnectedCauseU3Ek__BackingField_5() { return &___U3CDisconnectedCauseU3Ek__BackingField_5; }
	inline void set_U3CDisconnectedCauseU3Ek__BackingField_5(int32_t value)
	{
		___U3CDisconnectedCauseU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CAppVersionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___U3CAppVersionU3Ek__BackingField_6)); }
	inline String_t* get_U3CAppVersionU3Ek__BackingField_6() const { return ___U3CAppVersionU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CAppVersionU3Ek__BackingField_6() { return &___U3CAppVersionU3Ek__BackingField_6; }
	inline void set_U3CAppVersionU3Ek__BackingField_6(String_t* value)
	{
		___U3CAppVersionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppVersionU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CAppIdU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___U3CAppIdU3Ek__BackingField_7)); }
	inline String_t* get_U3CAppIdU3Ek__BackingField_7() const { return ___U3CAppIdU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CAppIdU3Ek__BackingField_7() { return &___U3CAppIdU3Ek__BackingField_7; }
	inline void set_U3CAppIdU3Ek__BackingField_7(String_t* value)
	{
		___U3CAppIdU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppIdU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CAuthValuesU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___U3CAuthValuesU3Ek__BackingField_8)); }
	inline AuthenticationValues_t187933346 * get_U3CAuthValuesU3Ek__BackingField_8() const { return ___U3CAuthValuesU3Ek__BackingField_8; }
	inline AuthenticationValues_t187933346 ** get_address_of_U3CAuthValuesU3Ek__BackingField_8() { return &___U3CAuthValuesU3Ek__BackingField_8; }
	inline void set_U3CAuthValuesU3Ek__BackingField_8(AuthenticationValues_t187933346 * value)
	{
		___U3CAuthValuesU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthValuesU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_MessageLimit_9() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___MessageLimit_9)); }
	inline int32_t get_MessageLimit_9() const { return ___MessageLimit_9; }
	inline int32_t* get_address_of_MessageLimit_9() { return &___MessageLimit_9; }
	inline void set_MessageLimit_9(int32_t value)
	{
		___MessageLimit_9 = value;
	}

	inline static int32_t get_offset_of_PublicChannels_10() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___PublicChannels_10)); }
	inline Dictionary_2_t3099565493 * get_PublicChannels_10() const { return ___PublicChannels_10; }
	inline Dictionary_2_t3099565493 ** get_address_of_PublicChannels_10() { return &___PublicChannels_10; }
	inline void set_PublicChannels_10(Dictionary_2_t3099565493 * value)
	{
		___PublicChannels_10 = value;
		Il2CppCodeGenWriteBarrier((&___PublicChannels_10), value);
	}

	inline static int32_t get_offset_of_PrivateChannels_11() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___PrivateChannels_11)); }
	inline Dictionary_2_t3099565493 * get_PrivateChannels_11() const { return ___PrivateChannels_11; }
	inline Dictionary_2_t3099565493 ** get_address_of_PrivateChannels_11() { return &___PrivateChannels_11; }
	inline void set_PrivateChannels_11(Dictionary_2_t3099565493 * value)
	{
		___PrivateChannels_11 = value;
		Il2CppCodeGenWriteBarrier((&___PrivateChannels_11), value);
	}

	inline static int32_t get_offset_of_PublicChannelsUnsubscribing_12() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___PublicChannelsUnsubscribing_12)); }
	inline HashSet_1_t412400163 * get_PublicChannelsUnsubscribing_12() const { return ___PublicChannelsUnsubscribing_12; }
	inline HashSet_1_t412400163 ** get_address_of_PublicChannelsUnsubscribing_12() { return &___PublicChannelsUnsubscribing_12; }
	inline void set_PublicChannelsUnsubscribing_12(HashSet_1_t412400163 * value)
	{
		___PublicChannelsUnsubscribing_12 = value;
		Il2CppCodeGenWriteBarrier((&___PublicChannelsUnsubscribing_12), value);
	}

	inline static int32_t get_offset_of_listener_13() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___listener_13)); }
	inline RuntimeObject* get_listener_13() const { return ___listener_13; }
	inline RuntimeObject** get_address_of_listener_13() { return &___listener_13; }
	inline void set_listener_13(RuntimeObject* value)
	{
		___listener_13 = value;
		Il2CppCodeGenWriteBarrier((&___listener_13), value);
	}

	inline static int32_t get_offset_of_chatPeer_14() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___chatPeer_14)); }
	inline ChatPeer_t2186541770 * get_chatPeer_14() const { return ___chatPeer_14; }
	inline ChatPeer_t2186541770 ** get_address_of_chatPeer_14() { return &___chatPeer_14; }
	inline void set_chatPeer_14(ChatPeer_t2186541770 * value)
	{
		___chatPeer_14 = value;
		Il2CppCodeGenWriteBarrier((&___chatPeer_14), value);
	}

	inline static int32_t get_offset_of_didAuthenticate_16() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___didAuthenticate_16)); }
	inline bool get_didAuthenticate_16() const { return ___didAuthenticate_16; }
	inline bool* get_address_of_didAuthenticate_16() { return &___didAuthenticate_16; }
	inline void set_didAuthenticate_16(bool value)
	{
		___didAuthenticate_16 = value;
	}

	inline static int32_t get_offset_of_msDeltaForServiceCalls_17() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___msDeltaForServiceCalls_17)); }
	inline int32_t get_msDeltaForServiceCalls_17() const { return ___msDeltaForServiceCalls_17; }
	inline int32_t* get_address_of_msDeltaForServiceCalls_17() { return &___msDeltaForServiceCalls_17; }
	inline void set_msDeltaForServiceCalls_17(int32_t value)
	{
		___msDeltaForServiceCalls_17 = value;
	}

	inline static int32_t get_offset_of_msTimestampOfLastServiceCall_18() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___msTimestampOfLastServiceCall_18)); }
	inline int32_t get_msTimestampOfLastServiceCall_18() const { return ___msTimestampOfLastServiceCall_18; }
	inline int32_t* get_address_of_msTimestampOfLastServiceCall_18() { return &___msTimestampOfLastServiceCall_18; }
	inline void set_msTimestampOfLastServiceCall_18(int32_t value)
	{
		___msTimestampOfLastServiceCall_18 = value;
	}

	inline static int32_t get_offset_of_U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(ChatClient_t3322764984, ___U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19)); }
	inline bool get_U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19() const { return ___U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19() { return &___U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19; }
	inline void set_U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19(bool value)
	{
		___U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCLIENT_T3322764984_H
#ifndef PHOTONPEER_T1608153861_H
#define PHOTONPEER_T1608153861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonPeer
struct  PhotonPeer_t1608153861  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ClientSdkId
	uint8_t ___ClientSdkId_3;
	// System.String ExitGames.Client.Photon.PhotonPeer::clientVersion
	String_t* ___clientVersion_5;
	// ExitGames.Client.Photon.SerializationProtocol ExitGames.Client.Photon.PhotonPeer::<SerializationProtocolType>k__BackingField
	int32_t ___U3CSerializationProtocolTypeU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type> ExitGames.Client.Photon.PhotonPeer::SocketImplementationConfig
	Dictionary_2_t1253839074 * ___SocketImplementationConfig_7;
	// System.Type ExitGames.Client.Photon.PhotonPeer::<SocketImplementation>k__BackingField
	Type_t * ___U3CSocketImplementationU3Ek__BackingField_8;
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PhotonPeer::DebugOut
	uint8_t ___DebugOut_9;
	// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::<Listener>k__BackingField
	RuntimeObject* ___U3CListenerU3Ek__BackingField_10;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<EnableServerTracing>k__BackingField
	bool ___U3CEnableServerTracingU3Ek__BackingField_11;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::quickResendAttempts
	uint8_t ___quickResendAttempts_12;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMinConnections
	int32_t ___RhttpMinConnections_13;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMaxConnections
	int32_t ___RhttpMaxConnections_14;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::<LimitOfUnreliableCommands>k__BackingField
	int32_t ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ChannelCount
	uint8_t ___ChannelCount_16;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::crcEnabled
	bool ___crcEnabled_17;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::WarningSize
	int32_t ___WarningSize_18;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::SentCountAllowance
	int32_t ___SentCountAllowance_19;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::TimePingInterval
	int32_t ___TimePingInterval_20;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::DisconnectTimeout
	int32_t ___DisconnectTimeout_21;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::<TransportProtocol>k__BackingField
	uint8_t ___U3CTransportProtocolU3Ek__BackingField_22;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::mtu
	int32_t ___mtu_24;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<IsSendingOnlyAcks>k__BackingField
	bool ___U3CIsSendingOnlyAcksU3Ek__BackingField_25;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsIncoming>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsIncomingU3Ek__BackingField_26;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsOutgoing>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsOutgoingU3Ek__BackingField_27;
	// ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::<TrafficStatsGameLevel>k__BackingField
	TrafficStatsGameLevel_t4013908777 * ___U3CTrafficStatsGameLevelU3Ek__BackingField_28;
	// System.Diagnostics.Stopwatch ExitGames.Client.Photon.PhotonPeer::trafficStatsStopwatch
	Stopwatch_t305734070 * ___trafficStatsStopwatch_29;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::trafficStatsEnabled
	bool ___trafficStatsEnabled_30;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PhotonPeer::peerBase
	PeerBase_t2956237011 * ___peerBase_31;
	// System.Object ExitGames.Client.Photon.PhotonPeer::SendOutgoingLockObject
	RuntimeObject * ___SendOutgoingLockObject_32;
	// System.Object ExitGames.Client.Photon.PhotonPeer::DispatchLockObject
	RuntimeObject * ___DispatchLockObject_33;
	// System.Object ExitGames.Client.Photon.PhotonPeer::EnqueueLock
	RuntimeObject * ___EnqueueLock_34;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::PayloadEncryptionSecret
	ByteU5BU5D_t4116647657* ___PayloadEncryptionSecret_35;
	// ExitGames.Client.Photon.EncryptorManaged.Encryptor ExitGames.Client.Photon.PhotonPeer::DgramEncryptor
	Encryptor_t200327285 * ___DgramEncryptor_36;
	// ExitGames.Client.Photon.EncryptorManaged.Decryptor ExitGames.Client.Photon.PhotonPeer::DgramDecryptor
	Decryptor_t2116099858 * ___DgramDecryptor_37;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::randomizeSequenceNumbers
	bool ___randomizeSequenceNumbers_38;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::sequenceNumberSource
	ByteU5BU5D_t4116647657* ___sequenceNumberSource_39;

public:
	inline static int32_t get_offset_of_ClientSdkId_3() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ClientSdkId_3)); }
	inline uint8_t get_ClientSdkId_3() const { return ___ClientSdkId_3; }
	inline uint8_t* get_address_of_ClientSdkId_3() { return &___ClientSdkId_3; }
	inline void set_ClientSdkId_3(uint8_t value)
	{
		___ClientSdkId_3 = value;
	}

	inline static int32_t get_offset_of_clientVersion_5() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___clientVersion_5)); }
	inline String_t* get_clientVersion_5() const { return ___clientVersion_5; }
	inline String_t** get_address_of_clientVersion_5() { return &___clientVersion_5; }
	inline void set_clientVersion_5(String_t* value)
	{
		___clientVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___clientVersion_5), value);
	}

	inline static int32_t get_offset_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSerializationProtocolTypeU3Ek__BackingField_6)); }
	inline int32_t get_U3CSerializationProtocolTypeU3Ek__BackingField_6() const { return ___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return &___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline void set_U3CSerializationProtocolTypeU3Ek__BackingField_6(int32_t value)
	{
		___U3CSerializationProtocolTypeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_SocketImplementationConfig_7() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SocketImplementationConfig_7)); }
	inline Dictionary_2_t1253839074 * get_SocketImplementationConfig_7() const { return ___SocketImplementationConfig_7; }
	inline Dictionary_2_t1253839074 ** get_address_of_SocketImplementationConfig_7() { return &___SocketImplementationConfig_7; }
	inline void set_SocketImplementationConfig_7(Dictionary_2_t1253839074 * value)
	{
		___SocketImplementationConfig_7 = value;
		Il2CppCodeGenWriteBarrier((&___SocketImplementationConfig_7), value);
	}

	inline static int32_t get_offset_of_U3CSocketImplementationU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSocketImplementationU3Ek__BackingField_8)); }
	inline Type_t * get_U3CSocketImplementationU3Ek__BackingField_8() const { return ___U3CSocketImplementationU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CSocketImplementationU3Ek__BackingField_8() { return &___U3CSocketImplementationU3Ek__BackingField_8; }
	inline void set_U3CSocketImplementationU3Ek__BackingField_8(Type_t * value)
	{
		___U3CSocketImplementationU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketImplementationU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_DebugOut_9() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DebugOut_9)); }
	inline uint8_t get_DebugOut_9() const { return ___DebugOut_9; }
	inline uint8_t* get_address_of_DebugOut_9() { return &___DebugOut_9; }
	inline void set_DebugOut_9(uint8_t value)
	{
		___DebugOut_9 = value;
	}

	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CListenerU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CListenerU3Ek__BackingField_10() const { return ___U3CListenerU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CListenerU3Ek__BackingField_10() { return &___U3CListenerU3Ek__BackingField_10; }
	inline void set_U3CListenerU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CListenerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListenerU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CEnableServerTracingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CEnableServerTracingU3Ek__BackingField_11)); }
	inline bool get_U3CEnableServerTracingU3Ek__BackingField_11() const { return ___U3CEnableServerTracingU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CEnableServerTracingU3Ek__BackingField_11() { return &___U3CEnableServerTracingU3Ek__BackingField_11; }
	inline void set_U3CEnableServerTracingU3Ek__BackingField_11(bool value)
	{
		___U3CEnableServerTracingU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_quickResendAttempts_12() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___quickResendAttempts_12)); }
	inline uint8_t get_quickResendAttempts_12() const { return ___quickResendAttempts_12; }
	inline uint8_t* get_address_of_quickResendAttempts_12() { return &___quickResendAttempts_12; }
	inline void set_quickResendAttempts_12(uint8_t value)
	{
		___quickResendAttempts_12 = value;
	}

	inline static int32_t get_offset_of_RhttpMinConnections_13() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMinConnections_13)); }
	inline int32_t get_RhttpMinConnections_13() const { return ___RhttpMinConnections_13; }
	inline int32_t* get_address_of_RhttpMinConnections_13() { return &___RhttpMinConnections_13; }
	inline void set_RhttpMinConnections_13(int32_t value)
	{
		___RhttpMinConnections_13 = value;
	}

	inline static int32_t get_offset_of_RhttpMaxConnections_14() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMaxConnections_14)); }
	inline int32_t get_RhttpMaxConnections_14() const { return ___RhttpMaxConnections_14; }
	inline int32_t* get_address_of_RhttpMaxConnections_14() { return &___RhttpMaxConnections_14; }
	inline void set_RhttpMaxConnections_14(int32_t value)
	{
		___RhttpMaxConnections_14 = value;
	}

	inline static int32_t get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15)); }
	inline int32_t get_U3CLimitOfUnreliableCommandsU3Ek__BackingField_15() const { return ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_15() { return &___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15; }
	inline void set_U3CLimitOfUnreliableCommandsU3Ek__BackingField_15(int32_t value)
	{
		___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_ChannelCount_16() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ChannelCount_16)); }
	inline uint8_t get_ChannelCount_16() const { return ___ChannelCount_16; }
	inline uint8_t* get_address_of_ChannelCount_16() { return &___ChannelCount_16; }
	inline void set_ChannelCount_16(uint8_t value)
	{
		___ChannelCount_16 = value;
	}

	inline static int32_t get_offset_of_crcEnabled_17() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___crcEnabled_17)); }
	inline bool get_crcEnabled_17() const { return ___crcEnabled_17; }
	inline bool* get_address_of_crcEnabled_17() { return &___crcEnabled_17; }
	inline void set_crcEnabled_17(bool value)
	{
		___crcEnabled_17 = value;
	}

	inline static int32_t get_offset_of_WarningSize_18() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___WarningSize_18)); }
	inline int32_t get_WarningSize_18() const { return ___WarningSize_18; }
	inline int32_t* get_address_of_WarningSize_18() { return &___WarningSize_18; }
	inline void set_WarningSize_18(int32_t value)
	{
		___WarningSize_18 = value;
	}

	inline static int32_t get_offset_of_SentCountAllowance_19() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SentCountAllowance_19)); }
	inline int32_t get_SentCountAllowance_19() const { return ___SentCountAllowance_19; }
	inline int32_t* get_address_of_SentCountAllowance_19() { return &___SentCountAllowance_19; }
	inline void set_SentCountAllowance_19(int32_t value)
	{
		___SentCountAllowance_19 = value;
	}

	inline static int32_t get_offset_of_TimePingInterval_20() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___TimePingInterval_20)); }
	inline int32_t get_TimePingInterval_20() const { return ___TimePingInterval_20; }
	inline int32_t* get_address_of_TimePingInterval_20() { return &___TimePingInterval_20; }
	inline void set_TimePingInterval_20(int32_t value)
	{
		___TimePingInterval_20 = value;
	}

	inline static int32_t get_offset_of_DisconnectTimeout_21() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DisconnectTimeout_21)); }
	inline int32_t get_DisconnectTimeout_21() const { return ___DisconnectTimeout_21; }
	inline int32_t* get_address_of_DisconnectTimeout_21() { return &___DisconnectTimeout_21; }
	inline void set_DisconnectTimeout_21(int32_t value)
	{
		___DisconnectTimeout_21 = value;
	}

	inline static int32_t get_offset_of_U3CTransportProtocolU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTransportProtocolU3Ek__BackingField_22)); }
	inline uint8_t get_U3CTransportProtocolU3Ek__BackingField_22() const { return ___U3CTransportProtocolU3Ek__BackingField_22; }
	inline uint8_t* get_address_of_U3CTransportProtocolU3Ek__BackingField_22() { return &___U3CTransportProtocolU3Ek__BackingField_22; }
	inline void set_U3CTransportProtocolU3Ek__BackingField_22(uint8_t value)
	{
		___U3CTransportProtocolU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_mtu_24() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___mtu_24)); }
	inline int32_t get_mtu_24() const { return ___mtu_24; }
	inline int32_t* get_address_of_mtu_24() { return &___mtu_24; }
	inline void set_mtu_24(int32_t value)
	{
		___mtu_24 = value;
	}

	inline static int32_t get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CIsSendingOnlyAcksU3Ek__BackingField_25)); }
	inline bool get_U3CIsSendingOnlyAcksU3Ek__BackingField_25() const { return ___U3CIsSendingOnlyAcksU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CIsSendingOnlyAcksU3Ek__BackingField_25() { return &___U3CIsSendingOnlyAcksU3Ek__BackingField_25; }
	inline void set_U3CIsSendingOnlyAcksU3Ek__BackingField_25(bool value)
	{
		___U3CIsSendingOnlyAcksU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsIncomingU3Ek__BackingField_26)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsIncomingU3Ek__BackingField_26() const { return ___U3CTrafficStatsIncomingU3Ek__BackingField_26; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsIncomingU3Ek__BackingField_26() { return &___U3CTrafficStatsIncomingU3Ek__BackingField_26; }
	inline void set_U3CTrafficStatsIncomingU3Ek__BackingField_26(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsIncomingU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsIncomingU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsOutgoingU3Ek__BackingField_27)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsOutgoingU3Ek__BackingField_27() const { return ___U3CTrafficStatsOutgoingU3Ek__BackingField_27; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsOutgoingU3Ek__BackingField_27() { return &___U3CTrafficStatsOutgoingU3Ek__BackingField_27; }
	inline void set_U3CTrafficStatsOutgoingU3Ek__BackingField_27(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsOutgoingU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsOutgoingU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsGameLevelU3Ek__BackingField_28)); }
	inline TrafficStatsGameLevel_t4013908777 * get_U3CTrafficStatsGameLevelU3Ek__BackingField_28() const { return ___U3CTrafficStatsGameLevelU3Ek__BackingField_28; }
	inline TrafficStatsGameLevel_t4013908777 ** get_address_of_U3CTrafficStatsGameLevelU3Ek__BackingField_28() { return &___U3CTrafficStatsGameLevelU3Ek__BackingField_28; }
	inline void set_U3CTrafficStatsGameLevelU3Ek__BackingField_28(TrafficStatsGameLevel_t4013908777 * value)
	{
		___U3CTrafficStatsGameLevelU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsGameLevelU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_trafficStatsStopwatch_29() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsStopwatch_29)); }
	inline Stopwatch_t305734070 * get_trafficStatsStopwatch_29() const { return ___trafficStatsStopwatch_29; }
	inline Stopwatch_t305734070 ** get_address_of_trafficStatsStopwatch_29() { return &___trafficStatsStopwatch_29; }
	inline void set_trafficStatsStopwatch_29(Stopwatch_t305734070 * value)
	{
		___trafficStatsStopwatch_29 = value;
		Il2CppCodeGenWriteBarrier((&___trafficStatsStopwatch_29), value);
	}

	inline static int32_t get_offset_of_trafficStatsEnabled_30() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsEnabled_30)); }
	inline bool get_trafficStatsEnabled_30() const { return ___trafficStatsEnabled_30; }
	inline bool* get_address_of_trafficStatsEnabled_30() { return &___trafficStatsEnabled_30; }
	inline void set_trafficStatsEnabled_30(bool value)
	{
		___trafficStatsEnabled_30 = value;
	}

	inline static int32_t get_offset_of_peerBase_31() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___peerBase_31)); }
	inline PeerBase_t2956237011 * get_peerBase_31() const { return ___peerBase_31; }
	inline PeerBase_t2956237011 ** get_address_of_peerBase_31() { return &___peerBase_31; }
	inline void set_peerBase_31(PeerBase_t2956237011 * value)
	{
		___peerBase_31 = value;
		Il2CppCodeGenWriteBarrier((&___peerBase_31), value);
	}

	inline static int32_t get_offset_of_SendOutgoingLockObject_32() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SendOutgoingLockObject_32)); }
	inline RuntimeObject * get_SendOutgoingLockObject_32() const { return ___SendOutgoingLockObject_32; }
	inline RuntimeObject ** get_address_of_SendOutgoingLockObject_32() { return &___SendOutgoingLockObject_32; }
	inline void set_SendOutgoingLockObject_32(RuntimeObject * value)
	{
		___SendOutgoingLockObject_32 = value;
		Il2CppCodeGenWriteBarrier((&___SendOutgoingLockObject_32), value);
	}

	inline static int32_t get_offset_of_DispatchLockObject_33() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DispatchLockObject_33)); }
	inline RuntimeObject * get_DispatchLockObject_33() const { return ___DispatchLockObject_33; }
	inline RuntimeObject ** get_address_of_DispatchLockObject_33() { return &___DispatchLockObject_33; }
	inline void set_DispatchLockObject_33(RuntimeObject * value)
	{
		___DispatchLockObject_33 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchLockObject_33), value);
	}

	inline static int32_t get_offset_of_EnqueueLock_34() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___EnqueueLock_34)); }
	inline RuntimeObject * get_EnqueueLock_34() const { return ___EnqueueLock_34; }
	inline RuntimeObject ** get_address_of_EnqueueLock_34() { return &___EnqueueLock_34; }
	inline void set_EnqueueLock_34(RuntimeObject * value)
	{
		___EnqueueLock_34 = value;
		Il2CppCodeGenWriteBarrier((&___EnqueueLock_34), value);
	}

	inline static int32_t get_offset_of_PayloadEncryptionSecret_35() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___PayloadEncryptionSecret_35)); }
	inline ByteU5BU5D_t4116647657* get_PayloadEncryptionSecret_35() const { return ___PayloadEncryptionSecret_35; }
	inline ByteU5BU5D_t4116647657** get_address_of_PayloadEncryptionSecret_35() { return &___PayloadEncryptionSecret_35; }
	inline void set_PayloadEncryptionSecret_35(ByteU5BU5D_t4116647657* value)
	{
		___PayloadEncryptionSecret_35 = value;
		Il2CppCodeGenWriteBarrier((&___PayloadEncryptionSecret_35), value);
	}

	inline static int32_t get_offset_of_DgramEncryptor_36() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DgramEncryptor_36)); }
	inline Encryptor_t200327285 * get_DgramEncryptor_36() const { return ___DgramEncryptor_36; }
	inline Encryptor_t200327285 ** get_address_of_DgramEncryptor_36() { return &___DgramEncryptor_36; }
	inline void set_DgramEncryptor_36(Encryptor_t200327285 * value)
	{
		___DgramEncryptor_36 = value;
		Il2CppCodeGenWriteBarrier((&___DgramEncryptor_36), value);
	}

	inline static int32_t get_offset_of_DgramDecryptor_37() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DgramDecryptor_37)); }
	inline Decryptor_t2116099858 * get_DgramDecryptor_37() const { return ___DgramDecryptor_37; }
	inline Decryptor_t2116099858 ** get_address_of_DgramDecryptor_37() { return &___DgramDecryptor_37; }
	inline void set_DgramDecryptor_37(Decryptor_t2116099858 * value)
	{
		___DgramDecryptor_37 = value;
		Il2CppCodeGenWriteBarrier((&___DgramDecryptor_37), value);
	}

	inline static int32_t get_offset_of_randomizeSequenceNumbers_38() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___randomizeSequenceNumbers_38)); }
	inline bool get_randomizeSequenceNumbers_38() const { return ___randomizeSequenceNumbers_38; }
	inline bool* get_address_of_randomizeSequenceNumbers_38() { return &___randomizeSequenceNumbers_38; }
	inline void set_randomizeSequenceNumbers_38(bool value)
	{
		___randomizeSequenceNumbers_38 = value;
	}

	inline static int32_t get_offset_of_sequenceNumberSource_39() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___sequenceNumberSource_39)); }
	inline ByteU5BU5D_t4116647657* get_sequenceNumberSource_39() const { return ___sequenceNumberSource_39; }
	inline ByteU5BU5D_t4116647657** get_address_of_sequenceNumberSource_39() { return &___sequenceNumberSource_39; }
	inline void set_sequenceNumberSource_39(ByteU5BU5D_t4116647657* value)
	{
		___sequenceNumberSource_39 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceNumberSource_39), value);
	}
};

struct PhotonPeer_t1608153861_StaticFields
{
public:
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::AsyncKeyExchange
	bool ___AsyncKeyExchange_4;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::OutgoingStreamBufferSize
	int32_t ___OutgoingStreamBufferSize_23;

public:
	inline static int32_t get_offset_of_AsyncKeyExchange_4() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___AsyncKeyExchange_4)); }
	inline bool get_AsyncKeyExchange_4() const { return ___AsyncKeyExchange_4; }
	inline bool* get_address_of_AsyncKeyExchange_4() { return &___AsyncKeyExchange_4; }
	inline void set_AsyncKeyExchange_4(bool value)
	{
		___AsyncKeyExchange_4 = value;
	}

	inline static int32_t get_offset_of_OutgoingStreamBufferSize_23() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___OutgoingStreamBufferSize_23)); }
	inline int32_t get_OutgoingStreamBufferSize_23() const { return ___OutgoingStreamBufferSize_23; }
	inline int32_t* get_address_of_OutgoingStreamBufferSize_23() { return &___OutgoingStreamBufferSize_23; }
	inline void set_OutgoingStreamBufferSize_23(int32_t value)
	{
		___OutgoingStreamBufferSize_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPEER_T1608153861_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CHATPEER_T2186541770_H
#define CHATPEER_T2186541770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Chat.ChatPeer
struct  ChatPeer_t2186541770  : public PhotonPeer_t1608153861
{
public:

public:
};

struct ChatPeer_t2186541770_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32> ExitGames.Client.Photon.Chat.ChatPeer::ProtocolToNameServerPort
	Dictionary_2_t1720840067 * ___ProtocolToNameServerPort_42;

public:
	inline static int32_t get_offset_of_ProtocolToNameServerPort_42() { return static_cast<int32_t>(offsetof(ChatPeer_t2186541770_StaticFields, ___ProtocolToNameServerPort_42)); }
	inline Dictionary_2_t1720840067 * get_ProtocolToNameServerPort_42() const { return ___ProtocolToNameServerPort_42; }
	inline Dictionary_2_t1720840067 ** get_address_of_ProtocolToNameServerPort_42() { return &___ProtocolToNameServerPort_42; }
	inline void set_ProtocolToNameServerPort_42(Dictionary_2_t1720840067 * value)
	{
		___ProtocolToNameServerPort_42 = value;
		Il2CppCodeGenWriteBarrier((&___ProtocolToNameServerPort_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATPEER_T2186541770_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CONNECTINGPANEL_T2747570441_H
#define CONNECTINGPANEL_T2747570441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectingPanel
struct  ConnectingPanel_t2747570441  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTINGPANEL_T2747570441_H
#ifndef DECK_T2172403585_H
#define DECK_T2172403585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Deck
struct  Deck_t2172403585  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image Deck::deckImage
	Image_t2670269651 * ___deckImage_4;
	// UnityEngine.GameObject Deck::stonePrefab
	GameObject_t1113636619 * ___stonePrefab_5;
	// System.Boolean Deck::isFirst
	bool ___isFirst_6;
	// System.Int32 Deck::maxSubmit
	int32_t ___maxSubmit_7;
	// System.Int32 Deck::curSubmit
	int32_t ___curSubmit_8;
	// System.Int32 Deck::numOfStone
	int32_t ___numOfStone_9;
	// System.Int32[] Deck::stoneSubmit
	Int32U5BU5D_t385246372* ___stoneSubmit_10;
	// System.Int32[] Deck::stoneForPoint
	Int32U5BU5D_t385246372* ___stoneForPoint_11;
	// System.Int32[] Deck::stoneForNumber
	Int32U5BU5D_t385246372* ___stoneForNumber_12;
	// Stone[] Deck::stoneToDestory
	StoneU5BU5D_t1191912092* ___stoneToDestory_13;
	// UnityEngine.Sprite[] Deck::cloud
	SpriteU5BU5D_t2581906349* ___cloud_14;
	// UnityEngine.Sprite[] Deck::star
	SpriteU5BU5D_t2581906349* ___star_15;
	// UnityEngine.Sprite[] Deck::moon
	SpriteU5BU5D_t2581906349* ___moon_16;
	// UnityEngine.Sprite[] Deck::sun
	SpriteU5BU5D_t2581906349* ___sun_17;

public:
	inline static int32_t get_offset_of_deckImage_4() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___deckImage_4)); }
	inline Image_t2670269651 * get_deckImage_4() const { return ___deckImage_4; }
	inline Image_t2670269651 ** get_address_of_deckImage_4() { return &___deckImage_4; }
	inline void set_deckImage_4(Image_t2670269651 * value)
	{
		___deckImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___deckImage_4), value);
	}

	inline static int32_t get_offset_of_stonePrefab_5() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stonePrefab_5)); }
	inline GameObject_t1113636619 * get_stonePrefab_5() const { return ___stonePrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_stonePrefab_5() { return &___stonePrefab_5; }
	inline void set_stonePrefab_5(GameObject_t1113636619 * value)
	{
		___stonePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___stonePrefab_5), value);
	}

	inline static int32_t get_offset_of_isFirst_6() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___isFirst_6)); }
	inline bool get_isFirst_6() const { return ___isFirst_6; }
	inline bool* get_address_of_isFirst_6() { return &___isFirst_6; }
	inline void set_isFirst_6(bool value)
	{
		___isFirst_6 = value;
	}

	inline static int32_t get_offset_of_maxSubmit_7() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___maxSubmit_7)); }
	inline int32_t get_maxSubmit_7() const { return ___maxSubmit_7; }
	inline int32_t* get_address_of_maxSubmit_7() { return &___maxSubmit_7; }
	inline void set_maxSubmit_7(int32_t value)
	{
		___maxSubmit_7 = value;
	}

	inline static int32_t get_offset_of_curSubmit_8() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___curSubmit_8)); }
	inline int32_t get_curSubmit_8() const { return ___curSubmit_8; }
	inline int32_t* get_address_of_curSubmit_8() { return &___curSubmit_8; }
	inline void set_curSubmit_8(int32_t value)
	{
		___curSubmit_8 = value;
	}

	inline static int32_t get_offset_of_numOfStone_9() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___numOfStone_9)); }
	inline int32_t get_numOfStone_9() const { return ___numOfStone_9; }
	inline int32_t* get_address_of_numOfStone_9() { return &___numOfStone_9; }
	inline void set_numOfStone_9(int32_t value)
	{
		___numOfStone_9 = value;
	}

	inline static int32_t get_offset_of_stoneSubmit_10() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stoneSubmit_10)); }
	inline Int32U5BU5D_t385246372* get_stoneSubmit_10() const { return ___stoneSubmit_10; }
	inline Int32U5BU5D_t385246372** get_address_of_stoneSubmit_10() { return &___stoneSubmit_10; }
	inline void set_stoneSubmit_10(Int32U5BU5D_t385246372* value)
	{
		___stoneSubmit_10 = value;
		Il2CppCodeGenWriteBarrier((&___stoneSubmit_10), value);
	}

	inline static int32_t get_offset_of_stoneForPoint_11() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stoneForPoint_11)); }
	inline Int32U5BU5D_t385246372* get_stoneForPoint_11() const { return ___stoneForPoint_11; }
	inline Int32U5BU5D_t385246372** get_address_of_stoneForPoint_11() { return &___stoneForPoint_11; }
	inline void set_stoneForPoint_11(Int32U5BU5D_t385246372* value)
	{
		___stoneForPoint_11 = value;
		Il2CppCodeGenWriteBarrier((&___stoneForPoint_11), value);
	}

	inline static int32_t get_offset_of_stoneForNumber_12() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stoneForNumber_12)); }
	inline Int32U5BU5D_t385246372* get_stoneForNumber_12() const { return ___stoneForNumber_12; }
	inline Int32U5BU5D_t385246372** get_address_of_stoneForNumber_12() { return &___stoneForNumber_12; }
	inline void set_stoneForNumber_12(Int32U5BU5D_t385246372* value)
	{
		___stoneForNumber_12 = value;
		Il2CppCodeGenWriteBarrier((&___stoneForNumber_12), value);
	}

	inline static int32_t get_offset_of_stoneToDestory_13() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stoneToDestory_13)); }
	inline StoneU5BU5D_t1191912092* get_stoneToDestory_13() const { return ___stoneToDestory_13; }
	inline StoneU5BU5D_t1191912092** get_address_of_stoneToDestory_13() { return &___stoneToDestory_13; }
	inline void set_stoneToDestory_13(StoneU5BU5D_t1191912092* value)
	{
		___stoneToDestory_13 = value;
		Il2CppCodeGenWriteBarrier((&___stoneToDestory_13), value);
	}

	inline static int32_t get_offset_of_cloud_14() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___cloud_14)); }
	inline SpriteU5BU5D_t2581906349* get_cloud_14() const { return ___cloud_14; }
	inline SpriteU5BU5D_t2581906349** get_address_of_cloud_14() { return &___cloud_14; }
	inline void set_cloud_14(SpriteU5BU5D_t2581906349* value)
	{
		___cloud_14 = value;
		Il2CppCodeGenWriteBarrier((&___cloud_14), value);
	}

	inline static int32_t get_offset_of_star_15() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___star_15)); }
	inline SpriteU5BU5D_t2581906349* get_star_15() const { return ___star_15; }
	inline SpriteU5BU5D_t2581906349** get_address_of_star_15() { return &___star_15; }
	inline void set_star_15(SpriteU5BU5D_t2581906349* value)
	{
		___star_15 = value;
		Il2CppCodeGenWriteBarrier((&___star_15), value);
	}

	inline static int32_t get_offset_of_moon_16() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___moon_16)); }
	inline SpriteU5BU5D_t2581906349* get_moon_16() const { return ___moon_16; }
	inline SpriteU5BU5D_t2581906349** get_address_of_moon_16() { return &___moon_16; }
	inline void set_moon_16(SpriteU5BU5D_t2581906349* value)
	{
		___moon_16 = value;
		Il2CppCodeGenWriteBarrier((&___moon_16), value);
	}

	inline static int32_t get_offset_of_sun_17() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___sun_17)); }
	inline SpriteU5BU5D_t2581906349* get_sun_17() const { return ___sun_17; }
	inline SpriteU5BU5D_t2581906349** get_address_of_sun_17() { return &___sun_17; }
	inline void set_sun_17(SpriteU5BU5D_t2581906349* value)
	{
		___sun_17 = value;
		Il2CppCodeGenWriteBarrier((&___sun_17), value);
	}
};

struct Deck_t2172403585_StaticFields
{
public:
	// Deck Deck::<Instance>k__BackingField
	Deck_t2172403585 * ___U3CInstanceU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Deck_t2172403585_StaticFields, ___U3CInstanceU3Ek__BackingField_18)); }
	inline Deck_t2172403585 * get_U3CInstanceU3Ek__BackingField_18() const { return ___U3CInstanceU3Ek__BackingField_18; }
	inline Deck_t2172403585 ** get_address_of_U3CInstanceU3Ek__BackingField_18() { return &___U3CInstanceU3Ek__BackingField_18; }
	inline void set_U3CInstanceU3Ek__BackingField_18(Deck_t2172403585 * value)
	{
		___U3CInstanceU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECK_T2172403585_H
#ifndef DOORLOCK_T1141741040_H
#define DOORLOCK_T1141741040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoorLock
struct  DoorLock_t1141741040  : public MonoBehaviour_t3962482529
{
public:
	// System.String DoorLock::password
	String_t* ___password_4;
	// System.Boolean DoorLock::isActivce
	bool ___isActivce_5;
	// UnityEngine.UI.Text[] DoorLock::buttonTexts
	TextU5BU5D_t422084607* ___buttonTexts_6;
	// SceneTransition DoorLock::st
	SceneTransition_t1138091307 * ___st_7;

public:
	inline static int32_t get_offset_of_password_4() { return static_cast<int32_t>(offsetof(DoorLock_t1141741040, ___password_4)); }
	inline String_t* get_password_4() const { return ___password_4; }
	inline String_t** get_address_of_password_4() { return &___password_4; }
	inline void set_password_4(String_t* value)
	{
		___password_4 = value;
		Il2CppCodeGenWriteBarrier((&___password_4), value);
	}

	inline static int32_t get_offset_of_isActivce_5() { return static_cast<int32_t>(offsetof(DoorLock_t1141741040, ___isActivce_5)); }
	inline bool get_isActivce_5() const { return ___isActivce_5; }
	inline bool* get_address_of_isActivce_5() { return &___isActivce_5; }
	inline void set_isActivce_5(bool value)
	{
		___isActivce_5 = value;
	}

	inline static int32_t get_offset_of_buttonTexts_6() { return static_cast<int32_t>(offsetof(DoorLock_t1141741040, ___buttonTexts_6)); }
	inline TextU5BU5D_t422084607* get_buttonTexts_6() const { return ___buttonTexts_6; }
	inline TextU5BU5D_t422084607** get_address_of_buttonTexts_6() { return &___buttonTexts_6; }
	inline void set_buttonTexts_6(TextU5BU5D_t422084607* value)
	{
		___buttonTexts_6 = value;
		Il2CppCodeGenWriteBarrier((&___buttonTexts_6), value);
	}

	inline static int32_t get_offset_of_st_7() { return static_cast<int32_t>(offsetof(DoorLock_t1141741040, ___st_7)); }
	inline SceneTransition_t1138091307 * get_st_7() const { return ___st_7; }
	inline SceneTransition_t1138091307 ** get_address_of_st_7() { return &___st_7; }
	inline void set_st_7(SceneTransition_t1138091307 * value)
	{
		___st_7 = value;
		Il2CppCodeGenWriteBarrier((&___st_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOORLOCK_T1141741040_H
#ifndef EDITNAMEPANEL_T1647359677_H
#define EDITNAMEPANEL_T1647359677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditNamePanel
struct  EditNamePanel_t1647359677  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField EditNamePanel::input
	InputField_t3762917431 * ___input_4;

public:
	inline static int32_t get_offset_of_input_4() { return static_cast<int32_t>(offsetof(EditNamePanel_t1647359677, ___input_4)); }
	inline InputField_t3762917431 * get_input_4() const { return ___input_4; }
	inline InputField_t3762917431 ** get_address_of_input_4() { return &___input_4; }
	inline void set_input_4(InputField_t3762917431 * value)
	{
		___input_4 = value;
		Il2CppCodeGenWriteBarrier((&___input_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITNAMEPANEL_T1647359677_H
#ifndef BUTTONINSIDESCROLLLIST_T657350886_H
#define BUTTONINSIDESCROLLLIST_T657350886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.UtilityScripts.ButtonInsideScrollList
struct  ButtonInsideScrollList_t657350886  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.ScrollRect ExitGames.UtilityScripts.ButtonInsideScrollList::scrollRect
	ScrollRect_t4137855814 * ___scrollRect_4;

public:
	inline static int32_t get_offset_of_scrollRect_4() { return static_cast<int32_t>(offsetof(ButtonInsideScrollList_t657350886, ___scrollRect_4)); }
	inline ScrollRect_t4137855814 * get_scrollRect_4() const { return ___scrollRect_4; }
	inline ScrollRect_t4137855814 ** get_address_of_scrollRect_4() { return &___scrollRect_4; }
	inline void set_scrollRect_4(ScrollRect_t4137855814 * value)
	{
		___scrollRect_4 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINSIDESCROLLLIST_T657350886_H
#ifndef TEXTBUTTONTRANSITION_T3897064113_H
#define TEXTBUTTONTRANSITION_T3897064113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.UtilityScripts.TextButtonTransition
struct  TextButtonTransition_t3897064113  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ExitGames.UtilityScripts.TextButtonTransition::_text
	Text_t1901882714 * ____text_4;
	// UnityEngine.Color ExitGames.UtilityScripts.TextButtonTransition::NormalColor
	Color_t2555686324  ___NormalColor_5;
	// UnityEngine.Color ExitGames.UtilityScripts.TextButtonTransition::HoverColor
	Color_t2555686324  ___HoverColor_6;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(TextButtonTransition_t3897064113, ____text_4)); }
	inline Text_t1901882714 * get__text_4() const { return ____text_4; }
	inline Text_t1901882714 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_t1901882714 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((&____text_4), value);
	}

	inline static int32_t get_offset_of_NormalColor_5() { return static_cast<int32_t>(offsetof(TextButtonTransition_t3897064113, ___NormalColor_5)); }
	inline Color_t2555686324  get_NormalColor_5() const { return ___NormalColor_5; }
	inline Color_t2555686324 * get_address_of_NormalColor_5() { return &___NormalColor_5; }
	inline void set_NormalColor_5(Color_t2555686324  value)
	{
		___NormalColor_5 = value;
	}

	inline static int32_t get_offset_of_HoverColor_6() { return static_cast<int32_t>(offsetof(TextButtonTransition_t3897064113, ___HoverColor_6)); }
	inline Color_t2555686324  get_HoverColor_6() const { return ___HoverColor_6; }
	inline Color_t2555686324 * get_address_of_HoverColor_6() { return &___HoverColor_6; }
	inline void set_HoverColor_6(Color_t2555686324  value)
	{
		___HoverColor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTBUTTONTRANSITION_T3897064113_H
#ifndef TEXTTOGGLEISONTRANSITION_T1534099090_H
#define TEXTTOGGLEISONTRANSITION_T1534099090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.UtilityScripts.TextToggleIsOnTransition
struct  TextToggleIsOnTransition_t1534099090  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Toggle ExitGames.UtilityScripts.TextToggleIsOnTransition::toggle
	Toggle_t2735377061 * ___toggle_4;
	// UnityEngine.UI.Text ExitGames.UtilityScripts.TextToggleIsOnTransition::_text
	Text_t1901882714 * ____text_5;
	// UnityEngine.Color ExitGames.UtilityScripts.TextToggleIsOnTransition::NormalOnColor
	Color_t2555686324  ___NormalOnColor_6;
	// UnityEngine.Color ExitGames.UtilityScripts.TextToggleIsOnTransition::NormalOffColor
	Color_t2555686324  ___NormalOffColor_7;
	// UnityEngine.Color ExitGames.UtilityScripts.TextToggleIsOnTransition::HoverOnColor
	Color_t2555686324  ___HoverOnColor_8;
	// UnityEngine.Color ExitGames.UtilityScripts.TextToggleIsOnTransition::HoverOffColor
	Color_t2555686324  ___HoverOffColor_9;
	// System.Boolean ExitGames.UtilityScripts.TextToggleIsOnTransition::isHover
	bool ___isHover_10;

public:
	inline static int32_t get_offset_of_toggle_4() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___toggle_4)); }
	inline Toggle_t2735377061 * get_toggle_4() const { return ___toggle_4; }
	inline Toggle_t2735377061 ** get_address_of_toggle_4() { return &___toggle_4; }
	inline void set_toggle_4(Toggle_t2735377061 * value)
	{
		___toggle_4 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_4), value);
	}

	inline static int32_t get_offset_of__text_5() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ____text_5)); }
	inline Text_t1901882714 * get__text_5() const { return ____text_5; }
	inline Text_t1901882714 ** get_address_of__text_5() { return &____text_5; }
	inline void set__text_5(Text_t1901882714 * value)
	{
		____text_5 = value;
		Il2CppCodeGenWriteBarrier((&____text_5), value);
	}

	inline static int32_t get_offset_of_NormalOnColor_6() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___NormalOnColor_6)); }
	inline Color_t2555686324  get_NormalOnColor_6() const { return ___NormalOnColor_6; }
	inline Color_t2555686324 * get_address_of_NormalOnColor_6() { return &___NormalOnColor_6; }
	inline void set_NormalOnColor_6(Color_t2555686324  value)
	{
		___NormalOnColor_6 = value;
	}

	inline static int32_t get_offset_of_NormalOffColor_7() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___NormalOffColor_7)); }
	inline Color_t2555686324  get_NormalOffColor_7() const { return ___NormalOffColor_7; }
	inline Color_t2555686324 * get_address_of_NormalOffColor_7() { return &___NormalOffColor_7; }
	inline void set_NormalOffColor_7(Color_t2555686324  value)
	{
		___NormalOffColor_7 = value;
	}

	inline static int32_t get_offset_of_HoverOnColor_8() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___HoverOnColor_8)); }
	inline Color_t2555686324  get_HoverOnColor_8() const { return ___HoverOnColor_8; }
	inline Color_t2555686324 * get_address_of_HoverOnColor_8() { return &___HoverOnColor_8; }
	inline void set_HoverOnColor_8(Color_t2555686324  value)
	{
		___HoverOnColor_8 = value;
	}

	inline static int32_t get_offset_of_HoverOffColor_9() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___HoverOffColor_9)); }
	inline Color_t2555686324  get_HoverOffColor_9() const { return ___HoverOffColor_9; }
	inline Color_t2555686324 * get_address_of_HoverOffColor_9() { return &___HoverOffColor_9; }
	inline void set_HoverOffColor_9(Color_t2555686324  value)
	{
		___HoverOffColor_9 = value;
	}

	inline static int32_t get_offset_of_isHover_10() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t1534099090, ___isHover_10)); }
	inline bool get_isHover_10() const { return ___isHover_10; }
	inline bool* get_address_of_isHover_10() { return &___isHover_10; }
	inline void set_isHover_10(bool value)
	{
		___isHover_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOGGLEISONTRANSITION_T1534099090_H
#ifndef EXITPANEL_T1668634582_H
#define EXITPANEL_T1668634582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitPanel
struct  ExitPanel_t1668634582  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXITPANEL_T1668634582_H
#ifndef FIELD_T4115194983_H
#define FIELD_T4115194983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Field
struct  Field_t4115194983  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Field::stonePrefab
	GameObject_t1113636619 * ___stonePrefab_4;
	// UnityEngine.UI.Image Field::fieldImage
	Image_t2670269651 * ___fieldImage_5;
	// UnityEngine.UI.Text Field::t
	Text_t1901882714 * ___t_6;
	// System.Boolean Field::isFirstField
	bool ___isFirstField_7;
	// System.Int32 Field::prevPoint
	int32_t ___prevPoint_8;
	// System.Int32 Field::numOfStone
	int32_t ___numOfStone_9;

public:
	inline static int32_t get_offset_of_stonePrefab_4() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___stonePrefab_4)); }
	inline GameObject_t1113636619 * get_stonePrefab_4() const { return ___stonePrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_stonePrefab_4() { return &___stonePrefab_4; }
	inline void set_stonePrefab_4(GameObject_t1113636619 * value)
	{
		___stonePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___stonePrefab_4), value);
	}

	inline static int32_t get_offset_of_fieldImage_5() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___fieldImage_5)); }
	inline Image_t2670269651 * get_fieldImage_5() const { return ___fieldImage_5; }
	inline Image_t2670269651 ** get_address_of_fieldImage_5() { return &___fieldImage_5; }
	inline void set_fieldImage_5(Image_t2670269651 * value)
	{
		___fieldImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___fieldImage_5), value);
	}

	inline static int32_t get_offset_of_t_6() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___t_6)); }
	inline Text_t1901882714 * get_t_6() const { return ___t_6; }
	inline Text_t1901882714 ** get_address_of_t_6() { return &___t_6; }
	inline void set_t_6(Text_t1901882714 * value)
	{
		___t_6 = value;
		Il2CppCodeGenWriteBarrier((&___t_6), value);
	}

	inline static int32_t get_offset_of_isFirstField_7() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___isFirstField_7)); }
	inline bool get_isFirstField_7() const { return ___isFirstField_7; }
	inline bool* get_address_of_isFirstField_7() { return &___isFirstField_7; }
	inline void set_isFirstField_7(bool value)
	{
		___isFirstField_7 = value;
	}

	inline static int32_t get_offset_of_prevPoint_8() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___prevPoint_8)); }
	inline int32_t get_prevPoint_8() const { return ___prevPoint_8; }
	inline int32_t* get_address_of_prevPoint_8() { return &___prevPoint_8; }
	inline void set_prevPoint_8(int32_t value)
	{
		___prevPoint_8 = value;
	}

	inline static int32_t get_offset_of_numOfStone_9() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___numOfStone_9)); }
	inline int32_t get_numOfStone_9() const { return ___numOfStone_9; }
	inline int32_t* get_address_of_numOfStone_9() { return &___numOfStone_9; }
	inline void set_numOfStone_9(int32_t value)
	{
		___numOfStone_9 = value;
	}
};

struct Field_t4115194983_StaticFields
{
public:
	// Field Field::<Instance>k__BackingField
	Field_t4115194983 * ___U3CInstanceU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Field_t4115194983_StaticFields, ___U3CInstanceU3Ek__BackingField_10)); }
	inline Field_t4115194983 * get_U3CInstanceU3Ek__BackingField_10() const { return ___U3CInstanceU3Ek__BackingField_10; }
	inline Field_t4115194983 ** get_address_of_U3CInstanceU3Ek__BackingField_10() { return &___U3CInstanceU3Ek__BackingField_10; }
	inline void set_U3CInstanceU3Ek__BackingField_10(Field_t4115194983 * value)
	{
		___U3CInstanceU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_T4115194983_H
#ifndef HOWTOPANEL_T3065907647_H
#define HOWTOPANEL_T3065907647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HowToPanel
struct  HowToPanel_t3065907647  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOWTOPANEL_T3065907647_H
#ifndef IMOJIBUTTON_T701272134_H
#define IMOJIBUTTON_T701272134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImojiButton
struct  ImojiButton_t701272134  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] ImojiButton::imojis
	SpriteU5BU5D_t2581906349* ___imojis_4;
	// UnityEngine.GameObject ImojiButton::container
	GameObject_t1113636619 * ___container_5;
	// UnityEngine.GameObject ImojiButton::my
	GameObject_t1113636619 * ___my_6;
	// System.Boolean ImojiButton::isOn
	bool ___isOn_7;
	// System.Boolean ImojiButton::isImojing
	bool ___isImojing_8;

public:
	inline static int32_t get_offset_of_imojis_4() { return static_cast<int32_t>(offsetof(ImojiButton_t701272134, ___imojis_4)); }
	inline SpriteU5BU5D_t2581906349* get_imojis_4() const { return ___imojis_4; }
	inline SpriteU5BU5D_t2581906349** get_address_of_imojis_4() { return &___imojis_4; }
	inline void set_imojis_4(SpriteU5BU5D_t2581906349* value)
	{
		___imojis_4 = value;
		Il2CppCodeGenWriteBarrier((&___imojis_4), value);
	}

	inline static int32_t get_offset_of_container_5() { return static_cast<int32_t>(offsetof(ImojiButton_t701272134, ___container_5)); }
	inline GameObject_t1113636619 * get_container_5() const { return ___container_5; }
	inline GameObject_t1113636619 ** get_address_of_container_5() { return &___container_5; }
	inline void set_container_5(GameObject_t1113636619 * value)
	{
		___container_5 = value;
		Il2CppCodeGenWriteBarrier((&___container_5), value);
	}

	inline static int32_t get_offset_of_my_6() { return static_cast<int32_t>(offsetof(ImojiButton_t701272134, ___my_6)); }
	inline GameObject_t1113636619 * get_my_6() const { return ___my_6; }
	inline GameObject_t1113636619 ** get_address_of_my_6() { return &___my_6; }
	inline void set_my_6(GameObject_t1113636619 * value)
	{
		___my_6 = value;
		Il2CppCodeGenWriteBarrier((&___my_6), value);
	}

	inline static int32_t get_offset_of_isOn_7() { return static_cast<int32_t>(offsetof(ImojiButton_t701272134, ___isOn_7)); }
	inline bool get_isOn_7() const { return ___isOn_7; }
	inline bool* get_address_of_isOn_7() { return &___isOn_7; }
	inline void set_isOn_7(bool value)
	{
		___isOn_7 = value;
	}

	inline static int32_t get_offset_of_isImojing_8() { return static_cast<int32_t>(offsetof(ImojiButton_t701272134, ___isImojing_8)); }
	inline bool get_isImojing_8() const { return ___isImojing_8; }
	inline bool* get_address_of_isImojing_8() { return &___isImojing_8; }
	inline void set_isImojing_8(bool value)
	{
		___isImojing_8 = value;
	}
};

struct ImojiButton_t701272134_StaticFields
{
public:
	// ImojiButton ImojiButton::<Instance>k__BackingField
	ImojiButton_t701272134 * ___U3CInstanceU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ImojiButton_t701272134_StaticFields, ___U3CInstanceU3Ek__BackingField_9)); }
	inline ImojiButton_t701272134 * get_U3CInstanceU3Ek__BackingField_9() const { return ___U3CInstanceU3Ek__BackingField_9; }
	inline ImojiButton_t701272134 ** get_address_of_U3CInstanceU3Ek__BackingField_9() { return &___U3CInstanceU3Ek__BackingField_9; }
	inline void set_U3CInstanceU3Ek__BackingField_9(ImojiButton_t701272134 * value)
	{
		___U3CInstanceU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMOJIBUTTON_T701272134_H
#ifndef INGAMEEXITPANEL_T3158895127_H
#define INGAMEEXITPANEL_T3158895127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InGameExitPanel
struct  InGameExitPanel_t3158895127  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text InGameExitPanel::t
	Text_t1901882714 * ___t_4;

public:
	inline static int32_t get_offset_of_t_4() { return static_cast<int32_t>(offsetof(InGameExitPanel_t3158895127, ___t_4)); }
	inline Text_t1901882714 * get_t_4() const { return ___t_4; }
	inline Text_t1901882714 ** get_address_of_t_4() { return &___t_4; }
	inline void set_t_4(Text_t1901882714 * value)
	{
		___t_4 = value;
		Il2CppCodeGenWriteBarrier((&___t_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INGAMEEXITPANEL_T3158895127_H
#ifndef INGAMEPANEL_T2211285949_H
#define INGAMEPANEL_T2211285949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InGamePanel
struct  InGamePanel_t2211285949  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text InGamePanel::playerName
	Text_t1901882714 * ___playerName_4;
	// UnityEngine.UI.Text InGamePanel::chips
	Text_t1901882714 * ___chips_5;
	// UnityEngine.GameObject InGamePanel::exitPanel
	GameObject_t1113636619 * ___exitPanel_6;
	// System.Boolean InGamePanel::isExitOn
	bool ___isExitOn_7;
	// UnityEngine.GameObject InGamePanel::increaseMoney
	GameObject_t1113636619 * ___increaseMoney_8;
	// UnityEngine.UI.Image InGamePanel::rating
	Image_t2670269651 * ___rating_9;
	// UnityEngine.GameObject InGamePanel::playinfoPanel
	GameObject_t1113636619 * ___playinfoPanel_10;
	// System.Int32 InGamePanel::isReadPlayInfo
	int32_t ___isReadPlayInfo_11;

public:
	inline static int32_t get_offset_of_playerName_4() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___playerName_4)); }
	inline Text_t1901882714 * get_playerName_4() const { return ___playerName_4; }
	inline Text_t1901882714 ** get_address_of_playerName_4() { return &___playerName_4; }
	inline void set_playerName_4(Text_t1901882714 * value)
	{
		___playerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_4), value);
	}

	inline static int32_t get_offset_of_chips_5() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___chips_5)); }
	inline Text_t1901882714 * get_chips_5() const { return ___chips_5; }
	inline Text_t1901882714 ** get_address_of_chips_5() { return &___chips_5; }
	inline void set_chips_5(Text_t1901882714 * value)
	{
		___chips_5 = value;
		Il2CppCodeGenWriteBarrier((&___chips_5), value);
	}

	inline static int32_t get_offset_of_exitPanel_6() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___exitPanel_6)); }
	inline GameObject_t1113636619 * get_exitPanel_6() const { return ___exitPanel_6; }
	inline GameObject_t1113636619 ** get_address_of_exitPanel_6() { return &___exitPanel_6; }
	inline void set_exitPanel_6(GameObject_t1113636619 * value)
	{
		___exitPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___exitPanel_6), value);
	}

	inline static int32_t get_offset_of_isExitOn_7() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___isExitOn_7)); }
	inline bool get_isExitOn_7() const { return ___isExitOn_7; }
	inline bool* get_address_of_isExitOn_7() { return &___isExitOn_7; }
	inline void set_isExitOn_7(bool value)
	{
		___isExitOn_7 = value;
	}

	inline static int32_t get_offset_of_increaseMoney_8() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___increaseMoney_8)); }
	inline GameObject_t1113636619 * get_increaseMoney_8() const { return ___increaseMoney_8; }
	inline GameObject_t1113636619 ** get_address_of_increaseMoney_8() { return &___increaseMoney_8; }
	inline void set_increaseMoney_8(GameObject_t1113636619 * value)
	{
		___increaseMoney_8 = value;
		Il2CppCodeGenWriteBarrier((&___increaseMoney_8), value);
	}

	inline static int32_t get_offset_of_rating_9() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___rating_9)); }
	inline Image_t2670269651 * get_rating_9() const { return ___rating_9; }
	inline Image_t2670269651 ** get_address_of_rating_9() { return &___rating_9; }
	inline void set_rating_9(Image_t2670269651 * value)
	{
		___rating_9 = value;
		Il2CppCodeGenWriteBarrier((&___rating_9), value);
	}

	inline static int32_t get_offset_of_playinfoPanel_10() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___playinfoPanel_10)); }
	inline GameObject_t1113636619 * get_playinfoPanel_10() const { return ___playinfoPanel_10; }
	inline GameObject_t1113636619 ** get_address_of_playinfoPanel_10() { return &___playinfoPanel_10; }
	inline void set_playinfoPanel_10(GameObject_t1113636619 * value)
	{
		___playinfoPanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___playinfoPanel_10), value);
	}

	inline static int32_t get_offset_of_isReadPlayInfo_11() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___isReadPlayInfo_11)); }
	inline int32_t get_isReadPlayInfo_11() const { return ___isReadPlayInfo_11; }
	inline int32_t* get_address_of_isReadPlayInfo_11() { return &___isReadPlayInfo_11; }
	inline void set_isReadPlayInfo_11(int32_t value)
	{
		___isReadPlayInfo_11 = value;
	}
};

struct InGamePanel_t2211285949_StaticFields
{
public:
	// InGamePanel InGamePanel::<Instance>k__BackingField
	InGamePanel_t2211285949 * ___U3CInstanceU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949_StaticFields, ___U3CInstanceU3Ek__BackingField_12)); }
	inline InGamePanel_t2211285949 * get_U3CInstanceU3Ek__BackingField_12() const { return ___U3CInstanceU3Ek__BackingField_12; }
	inline InGamePanel_t2211285949 ** get_address_of_U3CInstanceU3Ek__BackingField_12() { return &___U3CInstanceU3Ek__BackingField_12; }
	inline void set_U3CInstanceU3Ek__BackingField_12(InGamePanel_t2211285949 * value)
	{
		___U3CInstanceU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INGAMEPANEL_T2211285949_H
#ifndef INCREASEEFFECT_T1728429428_H
#define INCREASEEFFECT_T1728429428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IncreaseEffect
struct  IncreaseEffect_t1728429428  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text IncreaseEffect::text
	Text_t1901882714 * ___text_4;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(IncreaseEffect_t1728429428, ___text_4)); }
	inline Text_t1901882714 * get_text_4() const { return ___text_4; }
	inline Text_t1901882714 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t1901882714 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREASEEFFECT_T1728429428_H
#ifndef INCREASEEFFECTPANEL_T3055077437_H
#define INCREASEEFFECTPANEL_T3055077437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IncreaseEffectPanel
struct  IncreaseEffectPanel_t3055077437  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject IncreaseEffectPanel::increaseMoney
	GameObject_t1113636619 * ___increaseMoney_4;
	// UnityEngine.GameObject IncreaseEffectPanel::posPrefab
	GameObject_t1113636619 * ___posPrefab_5;
	// UnityEngine.Transform[] IncreaseEffectPanel::pos
	TransformU5BU5D_t807237628* ___pos_6;

public:
	inline static int32_t get_offset_of_increaseMoney_4() { return static_cast<int32_t>(offsetof(IncreaseEffectPanel_t3055077437, ___increaseMoney_4)); }
	inline GameObject_t1113636619 * get_increaseMoney_4() const { return ___increaseMoney_4; }
	inline GameObject_t1113636619 ** get_address_of_increaseMoney_4() { return &___increaseMoney_4; }
	inline void set_increaseMoney_4(GameObject_t1113636619 * value)
	{
		___increaseMoney_4 = value;
		Il2CppCodeGenWriteBarrier((&___increaseMoney_4), value);
	}

	inline static int32_t get_offset_of_posPrefab_5() { return static_cast<int32_t>(offsetof(IncreaseEffectPanel_t3055077437, ___posPrefab_5)); }
	inline GameObject_t1113636619 * get_posPrefab_5() const { return ___posPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_posPrefab_5() { return &___posPrefab_5; }
	inline void set_posPrefab_5(GameObject_t1113636619 * value)
	{
		___posPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___posPrefab_5), value);
	}

	inline static int32_t get_offset_of_pos_6() { return static_cast<int32_t>(offsetof(IncreaseEffectPanel_t3055077437, ___pos_6)); }
	inline TransformU5BU5D_t807237628* get_pos_6() const { return ___pos_6; }
	inline TransformU5BU5D_t807237628** get_address_of_pos_6() { return &___pos_6; }
	inline void set_pos_6(TransformU5BU5D_t807237628* value)
	{
		___pos_6 = value;
		Il2CppCodeGenWriteBarrier((&___pos_6), value);
	}
};

struct IncreaseEffectPanel_t3055077437_StaticFields
{
public:
	// IncreaseEffectPanel IncreaseEffectPanel::<Instance>k__BackingField
	IncreaseEffectPanel_t3055077437 * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(IncreaseEffectPanel_t3055077437_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline IncreaseEffectPanel_t3055077437 * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline IncreaseEffectPanel_t3055077437 ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(IncreaseEffectPanel_t3055077437 * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREASEEFFECTPANEL_T3055077437_H
#ifndef LOBBYPANEL_T3775216716_H
#define LOBBYPANEL_T3775216716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyPanel
struct  LobbyPanel_t3775216716  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text LobbyPanel::playerName
	Text_t1901882714 * ___playerName_4;
	// UnityEngine.UI.Text LobbyPanel::chip
	Text_t1901882714 * ___chip_5;
	// UnityEngine.GameObject LobbyPanel::editNamePanel
	GameObject_t1113636619 * ___editNamePanel_6;
	// UnityEngine.GameObject LobbyPanel::exitPanel
	GameObject_t1113636619 * ___exitPanel_7;
	// UnityEngine.GameObject LobbyPanel::howtoPanel
	GameObject_t1113636619 * ___howtoPanel_8;
	// UnityEngine.UI.Image LobbyPanel::rating
	Image_t2670269651 * ___rating_9;
	// System.Boolean LobbyPanel::isEditOn
	bool ___isEditOn_10;
	// System.Boolean LobbyPanel::isExitOn
	bool ___isExitOn_11;

public:
	inline static int32_t get_offset_of_playerName_4() { return static_cast<int32_t>(offsetof(LobbyPanel_t3775216716, ___playerName_4)); }
	inline Text_t1901882714 * get_playerName_4() const { return ___playerName_4; }
	inline Text_t1901882714 ** get_address_of_playerName_4() { return &___playerName_4; }
	inline void set_playerName_4(Text_t1901882714 * value)
	{
		___playerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_4), value);
	}

	inline static int32_t get_offset_of_chip_5() { return static_cast<int32_t>(offsetof(LobbyPanel_t3775216716, ___chip_5)); }
	inline Text_t1901882714 * get_chip_5() const { return ___chip_5; }
	inline Text_t1901882714 ** get_address_of_chip_5() { return &___chip_5; }
	inline void set_chip_5(Text_t1901882714 * value)
	{
		___chip_5 = value;
		Il2CppCodeGenWriteBarrier((&___chip_5), value);
	}

	inline static int32_t get_offset_of_editNamePanel_6() { return static_cast<int32_t>(offsetof(LobbyPanel_t3775216716, ___editNamePanel_6)); }
	inline GameObject_t1113636619 * get_editNamePanel_6() const { return ___editNamePanel_6; }
	inline GameObject_t1113636619 ** get_address_of_editNamePanel_6() { return &___editNamePanel_6; }
	inline void set_editNamePanel_6(GameObject_t1113636619 * value)
	{
		___editNamePanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___editNamePanel_6), value);
	}

	inline static int32_t get_offset_of_exitPanel_7() { return static_cast<int32_t>(offsetof(LobbyPanel_t3775216716, ___exitPanel_7)); }
	inline GameObject_t1113636619 * get_exitPanel_7() const { return ___exitPanel_7; }
	inline GameObject_t1113636619 ** get_address_of_exitPanel_7() { return &___exitPanel_7; }
	inline void set_exitPanel_7(GameObject_t1113636619 * value)
	{
		___exitPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___exitPanel_7), value);
	}

	inline static int32_t get_offset_of_howtoPanel_8() { return static_cast<int32_t>(offsetof(LobbyPanel_t3775216716, ___howtoPanel_8)); }
	inline GameObject_t1113636619 * get_howtoPanel_8() const { return ___howtoPanel_8; }
	inline GameObject_t1113636619 ** get_address_of_howtoPanel_8() { return &___howtoPanel_8; }
	inline void set_howtoPanel_8(GameObject_t1113636619 * value)
	{
		___howtoPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___howtoPanel_8), value);
	}

	inline static int32_t get_offset_of_rating_9() { return static_cast<int32_t>(offsetof(LobbyPanel_t3775216716, ___rating_9)); }
	inline Image_t2670269651 * get_rating_9() const { return ___rating_9; }
	inline Image_t2670269651 ** get_address_of_rating_9() { return &___rating_9; }
	inline void set_rating_9(Image_t2670269651 * value)
	{
		___rating_9 = value;
		Il2CppCodeGenWriteBarrier((&___rating_9), value);
	}

	inline static int32_t get_offset_of_isEditOn_10() { return static_cast<int32_t>(offsetof(LobbyPanel_t3775216716, ___isEditOn_10)); }
	inline bool get_isEditOn_10() const { return ___isEditOn_10; }
	inline bool* get_address_of_isEditOn_10() { return &___isEditOn_10; }
	inline void set_isEditOn_10(bool value)
	{
		___isEditOn_10 = value;
	}

	inline static int32_t get_offset_of_isExitOn_11() { return static_cast<int32_t>(offsetof(LobbyPanel_t3775216716, ___isExitOn_11)); }
	inline bool get_isExitOn_11() const { return ___isExitOn_11; }
	inline bool* get_address_of_isExitOn_11() { return &___isExitOn_11; }
	inline void set_isExitOn_11(bool value)
	{
		___isExitOn_11 = value;
	}
};

struct LobbyPanel_t3775216716_StaticFields
{
public:
	// LobbyPanel LobbyPanel::<Instance>k__BackingField
	LobbyPanel_t3775216716 * ___U3CInstanceU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(LobbyPanel_t3775216716_StaticFields, ___U3CInstanceU3Ek__BackingField_12)); }
	inline LobbyPanel_t3775216716 * get_U3CInstanceU3Ek__BackingField_12() const { return ___U3CInstanceU3Ek__BackingField_12; }
	inline LobbyPanel_t3775216716 ** get_address_of_U3CInstanceU3Ek__BackingField_12() { return &___U3CInstanceU3Ek__BackingField_12; }
	inline void set_U3CInstanceU3Ek__BackingField_12(LobbyPanel_t3775216716 * value)
	{
		___U3CInstanceU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOBBYPANEL_T3775216716_H
#ifndef NEWROOMPANEL_T3215952434_H
#define NEWROOMPANEL_T3215952434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewRoomPanel
struct  NewRoomPanel_t3215952434  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField NewRoomPanel::input
	InputField_t3762917431 * ___input_4;

public:
	inline static int32_t get_offset_of_input_4() { return static_cast<int32_t>(offsetof(NewRoomPanel_t3215952434, ___input_4)); }
	inline InputField_t3762917431 * get_input_4() const { return ___input_4; }
	inline InputField_t3762917431 ** get_address_of_input_4() { return &___input_4; }
	inline void set_input_4(InputField_t3762917431 * value)
	{
		___input_4 = value;
		Il2CppCodeGenWriteBarrier((&___input_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWROOMPANEL_T3215952434_H
#ifndef PASSBUTTON_T4045823955_H
#define PASSBUTTON_T4045823955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PassButton
struct  PassButton_t4045823955  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button PassButton::b
	Button_t4055032469 * ___b_5;

public:
	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(PassButton_t4045823955, ___b_5)); }
	inline Button_t4055032469 * get_b_5() const { return ___b_5; }
	inline Button_t4055032469 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Button_t4055032469 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

struct PassButton_t4045823955_StaticFields
{
public:
	// PassButton PassButton::<Instance>k__BackingField
	PassButton_t4045823955 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PassButton_t4045823955_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline PassButton_t4045823955 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline PassButton_t4045823955 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(PassButton_t4045823955 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSBUTTON_T4045823955_H
#ifndef MONOBEHAVIOUR_T3225183318_H
#define MONOBEHAVIOUR_T3225183318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.MonoBehaviour
struct  MonoBehaviour_t3225183318  : public MonoBehaviour_t3962482529
{
public:
	// PhotonView Photon.MonoBehaviour::pvCache
	PhotonView_t2207721820 * ___pvCache_4;

public:
	inline static int32_t get_offset_of_pvCache_4() { return static_cast<int32_t>(offsetof(MonoBehaviour_t3225183318, ___pvCache_4)); }
	inline PhotonView_t2207721820 * get_pvCache_4() const { return ___pvCache_4; }
	inline PhotonView_t2207721820 ** get_address_of_pvCache_4() { return &___pvCache_4; }
	inline void set_pvCache_4(PhotonView_t2207721820 * value)
	{
		___pvCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3225183318_H
#ifndef PLAYINFOPANEL_T2099359151_H
#define PLAYINFOPANEL_T2099359151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayInfoPanel
struct  PlayInfoPanel_t2099359151  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYINFOPANEL_T2099359151_H
#ifndef PLAYERDATA_T220878115_H
#define PLAYERDATA_T220878115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerData
struct  PlayerData_t220878115  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 PlayerData::chips
	int32_t ___chips_4;
	// System.String PlayerData::playerName
	String_t* ___playerName_5;

public:
	inline static int32_t get_offset_of_chips_4() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___chips_4)); }
	inline int32_t get_chips_4() const { return ___chips_4; }
	inline int32_t* get_address_of_chips_4() { return &___chips_4; }
	inline void set_chips_4(int32_t value)
	{
		___chips_4 = value;
	}

	inline static int32_t get_offset_of_playerName_5() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___playerName_5)); }
	inline String_t* get_playerName_5() const { return ___playerName_5; }
	inline String_t** get_address_of_playerName_5() { return &___playerName_5; }
	inline void set_playerName_5(String_t* value)
	{
		___playerName_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_5), value);
	}
};

struct PlayerData_t220878115_StaticFields
{
public:
	// PlayerData PlayerData::<Instance>k__BackingField
	PlayerData_t220878115 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PlayerData_t220878115_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline PlayerData_t220878115 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline PlayerData_t220878115 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(PlayerData_t220878115 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDATA_T220878115_H
#ifndef PLAYERSTATUS_T402953766_H
#define PLAYERSTATUS_T402953766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerStatus
struct  PlayerStatus_t402953766  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image PlayerStatus::activeColor
	Image_t2670269651 * ___activeColor_4;
	// UnityEngine.UI.Image PlayerStatus::playerColor
	Image_t2670269651 * ___playerColor_5;
	// UnityEngine.UI.Image PlayerStatus::rating
	Image_t2670269651 * ___rating_6;
	// UnityEngine.UI.Text PlayerStatus::playerName
	Text_t1901882714 * ___playerName_7;
	// UnityEngine.UI.Text PlayerStatus::remainStone
	Text_t1901882714 * ___remainStone_8;
	// UnityEngine.UI.Text PlayerStatus::playerChips
	Text_t1901882714 * ___playerChips_9;
	// System.Int32 PlayerStatus::chips
	int32_t ___chips_10;
	// System.Int32 PlayerStatus::order
	int32_t ___order_11;
	// UnityEngine.UI.Slider PlayerStatus::slider
	Slider_t3903728902 * ___slider_12;
	// UnityEngine.UI.Image PlayerStatus::imojiBG
	Image_t2670269651 * ___imojiBG_13;
	// UnityEngine.UI.Image PlayerStatus::imoji
	Image_t2670269651 * ___imoji_14;

public:
	inline static int32_t get_offset_of_activeColor_4() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___activeColor_4)); }
	inline Image_t2670269651 * get_activeColor_4() const { return ___activeColor_4; }
	inline Image_t2670269651 ** get_address_of_activeColor_4() { return &___activeColor_4; }
	inline void set_activeColor_4(Image_t2670269651 * value)
	{
		___activeColor_4 = value;
		Il2CppCodeGenWriteBarrier((&___activeColor_4), value);
	}

	inline static int32_t get_offset_of_playerColor_5() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___playerColor_5)); }
	inline Image_t2670269651 * get_playerColor_5() const { return ___playerColor_5; }
	inline Image_t2670269651 ** get_address_of_playerColor_5() { return &___playerColor_5; }
	inline void set_playerColor_5(Image_t2670269651 * value)
	{
		___playerColor_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerColor_5), value);
	}

	inline static int32_t get_offset_of_rating_6() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___rating_6)); }
	inline Image_t2670269651 * get_rating_6() const { return ___rating_6; }
	inline Image_t2670269651 ** get_address_of_rating_6() { return &___rating_6; }
	inline void set_rating_6(Image_t2670269651 * value)
	{
		___rating_6 = value;
		Il2CppCodeGenWriteBarrier((&___rating_6), value);
	}

	inline static int32_t get_offset_of_playerName_7() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___playerName_7)); }
	inline Text_t1901882714 * get_playerName_7() const { return ___playerName_7; }
	inline Text_t1901882714 ** get_address_of_playerName_7() { return &___playerName_7; }
	inline void set_playerName_7(Text_t1901882714 * value)
	{
		___playerName_7 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_7), value);
	}

	inline static int32_t get_offset_of_remainStone_8() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___remainStone_8)); }
	inline Text_t1901882714 * get_remainStone_8() const { return ___remainStone_8; }
	inline Text_t1901882714 ** get_address_of_remainStone_8() { return &___remainStone_8; }
	inline void set_remainStone_8(Text_t1901882714 * value)
	{
		___remainStone_8 = value;
		Il2CppCodeGenWriteBarrier((&___remainStone_8), value);
	}

	inline static int32_t get_offset_of_playerChips_9() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___playerChips_9)); }
	inline Text_t1901882714 * get_playerChips_9() const { return ___playerChips_9; }
	inline Text_t1901882714 ** get_address_of_playerChips_9() { return &___playerChips_9; }
	inline void set_playerChips_9(Text_t1901882714 * value)
	{
		___playerChips_9 = value;
		Il2CppCodeGenWriteBarrier((&___playerChips_9), value);
	}

	inline static int32_t get_offset_of_chips_10() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___chips_10)); }
	inline int32_t get_chips_10() const { return ___chips_10; }
	inline int32_t* get_address_of_chips_10() { return &___chips_10; }
	inline void set_chips_10(int32_t value)
	{
		___chips_10 = value;
	}

	inline static int32_t get_offset_of_order_11() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___order_11)); }
	inline int32_t get_order_11() const { return ___order_11; }
	inline int32_t* get_address_of_order_11() { return &___order_11; }
	inline void set_order_11(int32_t value)
	{
		___order_11 = value;
	}

	inline static int32_t get_offset_of_slider_12() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___slider_12)); }
	inline Slider_t3903728902 * get_slider_12() const { return ___slider_12; }
	inline Slider_t3903728902 ** get_address_of_slider_12() { return &___slider_12; }
	inline void set_slider_12(Slider_t3903728902 * value)
	{
		___slider_12 = value;
		Il2CppCodeGenWriteBarrier((&___slider_12), value);
	}

	inline static int32_t get_offset_of_imojiBG_13() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___imojiBG_13)); }
	inline Image_t2670269651 * get_imojiBG_13() const { return ___imojiBG_13; }
	inline Image_t2670269651 ** get_address_of_imojiBG_13() { return &___imojiBG_13; }
	inline void set_imojiBG_13(Image_t2670269651 * value)
	{
		___imojiBG_13 = value;
		Il2CppCodeGenWriteBarrier((&___imojiBG_13), value);
	}

	inline static int32_t get_offset_of_imoji_14() { return static_cast<int32_t>(offsetof(PlayerStatus_t402953766, ___imoji_14)); }
	inline Image_t2670269651 * get_imoji_14() const { return ___imoji_14; }
	inline Image_t2670269651 ** get_address_of_imoji_14() { return &___imoji_14; }
	inline void set_imoji_14(Image_t2670269651 * value)
	{
		___imoji_14 = value;
		Il2CppCodeGenWriteBarrier((&___imoji_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSTATUS_T402953766_H
#ifndef PLAYERSTATUSPANEL_T1209538314_H
#define PLAYERSTATUSPANEL_T1209538314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerStatusPanel
struct  PlayerStatusPanel_t1209538314  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlayerStatusPanel::playerStatusPrefab
	GameObject_t1113636619 * ___playerStatusPrefab_4;
	// UnityEngine.GameObject PlayerStatusPanel::waitingPanel
	GameObject_t1113636619 * ___waitingPanel_5;
	// System.Boolean PlayerStatusPanel::isWaiting
	bool ___isWaiting_6;
	// System.Boolean[] PlayerStatusPanel::isExist
	BooleanU5BU5D_t2897418192* ___isExist_7;
	// System.Single PlayerStatusPanel::timer
	float ___timer_8;

public:
	inline static int32_t get_offset_of_playerStatusPrefab_4() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___playerStatusPrefab_4)); }
	inline GameObject_t1113636619 * get_playerStatusPrefab_4() const { return ___playerStatusPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_playerStatusPrefab_4() { return &___playerStatusPrefab_4; }
	inline void set_playerStatusPrefab_4(GameObject_t1113636619 * value)
	{
		___playerStatusPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerStatusPrefab_4), value);
	}

	inline static int32_t get_offset_of_waitingPanel_5() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___waitingPanel_5)); }
	inline GameObject_t1113636619 * get_waitingPanel_5() const { return ___waitingPanel_5; }
	inline GameObject_t1113636619 ** get_address_of_waitingPanel_5() { return &___waitingPanel_5; }
	inline void set_waitingPanel_5(GameObject_t1113636619 * value)
	{
		___waitingPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___waitingPanel_5), value);
	}

	inline static int32_t get_offset_of_isWaiting_6() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___isWaiting_6)); }
	inline bool get_isWaiting_6() const { return ___isWaiting_6; }
	inline bool* get_address_of_isWaiting_6() { return &___isWaiting_6; }
	inline void set_isWaiting_6(bool value)
	{
		___isWaiting_6 = value;
	}

	inline static int32_t get_offset_of_isExist_7() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___isExist_7)); }
	inline BooleanU5BU5D_t2897418192* get_isExist_7() const { return ___isExist_7; }
	inline BooleanU5BU5D_t2897418192** get_address_of_isExist_7() { return &___isExist_7; }
	inline void set_isExist_7(BooleanU5BU5D_t2897418192* value)
	{
		___isExist_7 = value;
		Il2CppCodeGenWriteBarrier((&___isExist_7), value);
	}

	inline static int32_t get_offset_of_timer_8() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___timer_8)); }
	inline float get_timer_8() const { return ___timer_8; }
	inline float* get_address_of_timer_8() { return &___timer_8; }
	inline void set_timer_8(float value)
	{
		___timer_8 = value;
	}
};

struct PlayerStatusPanel_t1209538314_StaticFields
{
public:
	// PlayerStatusPanel PlayerStatusPanel::<Instance>k__BackingField
	PlayerStatusPanel_t1209538314 * ___U3CInstanceU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314_StaticFields, ___U3CInstanceU3Ek__BackingField_9)); }
	inline PlayerStatusPanel_t1209538314 * get_U3CInstanceU3Ek__BackingField_9() const { return ___U3CInstanceU3Ek__BackingField_9; }
	inline PlayerStatusPanel_t1209538314 ** get_address_of_U3CInstanceU3Ek__BackingField_9() { return &___U3CInstanceU3Ek__BackingField_9; }
	inline void set_U3CInstanceU3Ek__BackingField_9(PlayerStatusPanel_t1209538314 * value)
	{
		___U3CInstanceU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSTATUSPANEL_T1209538314_H
#ifndef REMAINTIME_T3232366768_H
#define REMAINTIME_T3232366768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemainTime
struct  RemainTime_t3232366768  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text RemainTime::text
	Text_t1901882714 * ___text_4;
	// UnityEngine.UI.Slider RemainTime::slider
	Slider_t3903728902 * ___slider_5;
	// PlayerStatus RemainTime::current
	PlayerStatus_t402953766 * ___current_6;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(RemainTime_t3232366768, ___text_4)); }
	inline Text_t1901882714 * get_text_4() const { return ___text_4; }
	inline Text_t1901882714 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t1901882714 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_slider_5() { return static_cast<int32_t>(offsetof(RemainTime_t3232366768, ___slider_5)); }
	inline Slider_t3903728902 * get_slider_5() const { return ___slider_5; }
	inline Slider_t3903728902 ** get_address_of_slider_5() { return &___slider_5; }
	inline void set_slider_5(Slider_t3903728902 * value)
	{
		___slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___slider_5), value);
	}

	inline static int32_t get_offset_of_current_6() { return static_cast<int32_t>(offsetof(RemainTime_t3232366768, ___current_6)); }
	inline PlayerStatus_t402953766 * get_current_6() const { return ___current_6; }
	inline PlayerStatus_t402953766 ** get_address_of_current_6() { return &___current_6; }
	inline void set_current_6(PlayerStatus_t402953766 * value)
	{
		___current_6 = value;
		Il2CppCodeGenWriteBarrier((&___current_6), value);
	}
};

struct RemainTime_t3232366768_StaticFields
{
public:
	// RemainTime RemainTime::<Instance>k__BackingField
	RemainTime_t3232366768 * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RemainTime_t3232366768_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline RemainTime_t3232366768 * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline RemainTime_t3232366768 ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(RemainTime_t3232366768 * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMAINTIME_T3232366768_H
#ifndef ROOMDATA_T44449739_H
#define ROOMDATA_T44449739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomData
struct  RoomData_t44449739  : public MonoBehaviour_t3962482529
{
public:
	// System.String RoomData::gameName
	String_t* ___gameName_4;
	// System.Boolean RoomData::isPlaying
	bool ___isPlaying_5;
	// System.Int32 RoomData::playerNum
	int32_t ___playerNum_6;
	// UnityEngine.UI.Text RoomData::roomNumber
	Text_t1901882714 * ___roomNumber_7;
	// UnityEngine.UI.Text RoomData::roomName
	Text_t1901882714 * ___roomName_8;

public:
	inline static int32_t get_offset_of_gameName_4() { return static_cast<int32_t>(offsetof(RoomData_t44449739, ___gameName_4)); }
	inline String_t* get_gameName_4() const { return ___gameName_4; }
	inline String_t** get_address_of_gameName_4() { return &___gameName_4; }
	inline void set_gameName_4(String_t* value)
	{
		___gameName_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameName_4), value);
	}

	inline static int32_t get_offset_of_isPlaying_5() { return static_cast<int32_t>(offsetof(RoomData_t44449739, ___isPlaying_5)); }
	inline bool get_isPlaying_5() const { return ___isPlaying_5; }
	inline bool* get_address_of_isPlaying_5() { return &___isPlaying_5; }
	inline void set_isPlaying_5(bool value)
	{
		___isPlaying_5 = value;
	}

	inline static int32_t get_offset_of_playerNum_6() { return static_cast<int32_t>(offsetof(RoomData_t44449739, ___playerNum_6)); }
	inline int32_t get_playerNum_6() const { return ___playerNum_6; }
	inline int32_t* get_address_of_playerNum_6() { return &___playerNum_6; }
	inline void set_playerNum_6(int32_t value)
	{
		___playerNum_6 = value;
	}

	inline static int32_t get_offset_of_roomNumber_7() { return static_cast<int32_t>(offsetof(RoomData_t44449739, ___roomNumber_7)); }
	inline Text_t1901882714 * get_roomNumber_7() const { return ___roomNumber_7; }
	inline Text_t1901882714 ** get_address_of_roomNumber_7() { return &___roomNumber_7; }
	inline void set_roomNumber_7(Text_t1901882714 * value)
	{
		___roomNumber_7 = value;
		Il2CppCodeGenWriteBarrier((&___roomNumber_7), value);
	}

	inline static int32_t get_offset_of_roomName_8() { return static_cast<int32_t>(offsetof(RoomData_t44449739, ___roomName_8)); }
	inline Text_t1901882714 * get_roomName_8() const { return ___roomName_8; }
	inline Text_t1901882714 ** get_address_of_roomName_8() { return &___roomName_8; }
	inline void set_roomName_8(Text_t1901882714 * value)
	{
		___roomName_8 = value;
		Il2CppCodeGenWriteBarrier((&___roomName_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMDATA_T44449739_H
#ifndef ROOMLIST_T2314265074_H
#define ROOMLIST_T2314265074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomList
struct  RoomList_t2314265074  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject RoomList::roomButtonPrefab
	GameObject_t1113636619 * ___roomButtonPrefab_4;
	// UnityEngine.GameObject RoomList::roomPanel
	GameObject_t1113636619 * ___roomPanel_5;
	// UnityEngine.GameObject RoomList::createRoomPanel
	GameObject_t1113636619 * ___createRoomPanel_6;

public:
	inline static int32_t get_offset_of_roomButtonPrefab_4() { return static_cast<int32_t>(offsetof(RoomList_t2314265074, ___roomButtonPrefab_4)); }
	inline GameObject_t1113636619 * get_roomButtonPrefab_4() const { return ___roomButtonPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_roomButtonPrefab_4() { return &___roomButtonPrefab_4; }
	inline void set_roomButtonPrefab_4(GameObject_t1113636619 * value)
	{
		___roomButtonPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___roomButtonPrefab_4), value);
	}

	inline static int32_t get_offset_of_roomPanel_5() { return static_cast<int32_t>(offsetof(RoomList_t2314265074, ___roomPanel_5)); }
	inline GameObject_t1113636619 * get_roomPanel_5() const { return ___roomPanel_5; }
	inline GameObject_t1113636619 ** get_address_of_roomPanel_5() { return &___roomPanel_5; }
	inline void set_roomPanel_5(GameObject_t1113636619 * value)
	{
		___roomPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___roomPanel_5), value);
	}

	inline static int32_t get_offset_of_createRoomPanel_6() { return static_cast<int32_t>(offsetof(RoomList_t2314265074, ___createRoomPanel_6)); }
	inline GameObject_t1113636619 * get_createRoomPanel_6() const { return ___createRoomPanel_6; }
	inline GameObject_t1113636619 ** get_address_of_createRoomPanel_6() { return &___createRoomPanel_6; }
	inline void set_createRoomPanel_6(GameObject_t1113636619 * value)
	{
		___createRoomPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___createRoomPanel_6), value);
	}
};

struct RoomList_t2314265074_StaticFields
{
public:
	// RoomList RoomList::<Instance>k__BackingField
	RoomList_t2314265074 * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RoomList_t2314265074_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline RoomList_t2314265074 * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline RoomList_t2314265074 ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(RoomList_t2314265074 * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMLIST_T2314265074_H
#ifndef ROOMPANEL_T941367166_H
#define ROOMPANEL_T941367166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomPanel
struct  RoomPanel_t941367166  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text RoomPanel::gameName
	Text_t1901882714 * ___gameName_4;
	// UnityEngine.UI.Text RoomPanel::playerNum
	Text_t1901882714 * ___playerNum_5;
	// UnityEngine.UI.Text RoomPanel::roomState
	Text_t1901882714 * ___roomState_6;
	// UnityEngine.UI.Button RoomPanel::joinButton
	Button_t4055032469 * ___joinButton_7;

public:
	inline static int32_t get_offset_of_gameName_4() { return static_cast<int32_t>(offsetof(RoomPanel_t941367166, ___gameName_4)); }
	inline Text_t1901882714 * get_gameName_4() const { return ___gameName_4; }
	inline Text_t1901882714 ** get_address_of_gameName_4() { return &___gameName_4; }
	inline void set_gameName_4(Text_t1901882714 * value)
	{
		___gameName_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameName_4), value);
	}

	inline static int32_t get_offset_of_playerNum_5() { return static_cast<int32_t>(offsetof(RoomPanel_t941367166, ___playerNum_5)); }
	inline Text_t1901882714 * get_playerNum_5() const { return ___playerNum_5; }
	inline Text_t1901882714 ** get_address_of_playerNum_5() { return &___playerNum_5; }
	inline void set_playerNum_5(Text_t1901882714 * value)
	{
		___playerNum_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerNum_5), value);
	}

	inline static int32_t get_offset_of_roomState_6() { return static_cast<int32_t>(offsetof(RoomPanel_t941367166, ___roomState_6)); }
	inline Text_t1901882714 * get_roomState_6() const { return ___roomState_6; }
	inline Text_t1901882714 ** get_address_of_roomState_6() { return &___roomState_6; }
	inline void set_roomState_6(Text_t1901882714 * value)
	{
		___roomState_6 = value;
		Il2CppCodeGenWriteBarrier((&___roomState_6), value);
	}

	inline static int32_t get_offset_of_joinButton_7() { return static_cast<int32_t>(offsetof(RoomPanel_t941367166, ___joinButton_7)); }
	inline Button_t4055032469 * get_joinButton_7() const { return ___joinButton_7; }
	inline Button_t4055032469 ** get_address_of_joinButton_7() { return &___joinButton_7; }
	inline void set_joinButton_7(Button_t4055032469 * value)
	{
		___joinButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___joinButton_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMPANEL_T941367166_H
#ifndef SCENETRANSITION_T1138091307_H
#define SCENETRANSITION_T1138091307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneTransition
struct  SceneTransition_t1138091307  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SceneTransition::isTestMode
	bool ___isTestMode_4;
	// YccioNetworkManager SceneTransition::ynm
	YccioNetworkManager_t3600504787 * ___ynm_5;

public:
	inline static int32_t get_offset_of_isTestMode_4() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___isTestMode_4)); }
	inline bool get_isTestMode_4() const { return ___isTestMode_4; }
	inline bool* get_address_of_isTestMode_4() { return &___isTestMode_4; }
	inline void set_isTestMode_4(bool value)
	{
		___isTestMode_4 = value;
	}

	inline static int32_t get_offset_of_ynm_5() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___ynm_5)); }
	inline YccioNetworkManager_t3600504787 * get_ynm_5() const { return ___ynm_5; }
	inline YccioNetworkManager_t3600504787 ** get_address_of_ynm_5() { return &___ynm_5; }
	inline void set_ynm_5(YccioNetworkManager_t3600504787 * value)
	{
		___ynm_5 = value;
		Il2CppCodeGenWriteBarrier((&___ynm_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENETRANSITION_T1138091307_H
#ifndef SORTBYCOLOR_T1786351735_H
#define SORTBYCOLOR_T1786351735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SortByColor
struct  SortByColor_t1786351735  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button SortByColor::b
	Button_t4055032469 * ___b_4;

public:
	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(SortByColor_t1786351735, ___b_4)); }
	inline Button_t4055032469 * get_b_4() const { return ___b_4; }
	inline Button_t4055032469 ** get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(Button_t4055032469 * value)
	{
		___b_4 = value;
		Il2CppCodeGenWriteBarrier((&___b_4), value);
	}
};

struct SortByColor_t1786351735_StaticFields
{
public:
	// SortByColor SortByColor::<Instance>k__BackingField
	SortByColor_t1786351735 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SortByColor_t1786351735_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline SortByColor_t1786351735 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline SortByColor_t1786351735 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(SortByColor_t1786351735 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTBYCOLOR_T1786351735_H
#ifndef SORTBYNUMBER_T227753584_H
#define SORTBYNUMBER_T227753584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SortByNumber
struct  SortByNumber_t227753584  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button SortByNumber::b
	Button_t4055032469 * ___b_4;

public:
	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(SortByNumber_t227753584, ___b_4)); }
	inline Button_t4055032469 * get_b_4() const { return ___b_4; }
	inline Button_t4055032469 ** get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(Button_t4055032469 * value)
	{
		___b_4 = value;
		Il2CppCodeGenWriteBarrier((&___b_4), value);
	}
};

struct SortByNumber_t227753584_StaticFields
{
public:
	// SortByNumber SortByNumber::<Instance>k__BackingField
	SortByNumber_t227753584 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SortByNumber_t227753584_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline SortByNumber_t227753584 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline SortByNumber_t227753584 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(SortByNumber_t227753584 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTBYNUMBER_T227753584_H
#ifndef SOUNDMANAGER_T2102329059_H
#define SOUNDMANAGER_T2102329059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t2102329059  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip[] SoundManager::clips
	AudioClipU5BU5D_t143221404* ___clips_4;
	// UnityEngine.AudioSource SoundManager::source
	AudioSource_t3935305588 * ___source_5;

public:
	inline static int32_t get_offset_of_clips_4() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___clips_4)); }
	inline AudioClipU5BU5D_t143221404* get_clips_4() const { return ___clips_4; }
	inline AudioClipU5BU5D_t143221404** get_address_of_clips_4() { return &___clips_4; }
	inline void set_clips_4(AudioClipU5BU5D_t143221404* value)
	{
		___clips_4 = value;
		Il2CppCodeGenWriteBarrier((&___clips_4), value);
	}

	inline static int32_t get_offset_of_source_5() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___source_5)); }
	inline AudioSource_t3935305588 * get_source_5() const { return ___source_5; }
	inline AudioSource_t3935305588 ** get_address_of_source_5() { return &___source_5; }
	inline void set_source_5(AudioSource_t3935305588 * value)
	{
		___source_5 = value;
		Il2CppCodeGenWriteBarrier((&___source_5), value);
	}
};

struct SoundManager_t2102329059_StaticFields
{
public:
	// SoundManager SoundManager::<Instance>k__BackingField
	SoundManager_t2102329059 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline SoundManager_t2102329059 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline SoundManager_t2102329059 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(SoundManager_t2102329059 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T2102329059_H
#ifndef SPRITECONTAINER_T2500302117_H
#define SPRITECONTAINER_T2500302117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpriteContainer
struct  SpriteContainer_t2500302117  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] SpriteContainer::ratings
	SpriteU5BU5D_t2581906349* ___ratings_4;

public:
	inline static int32_t get_offset_of_ratings_4() { return static_cast<int32_t>(offsetof(SpriteContainer_t2500302117, ___ratings_4)); }
	inline SpriteU5BU5D_t2581906349* get_ratings_4() const { return ___ratings_4; }
	inline SpriteU5BU5D_t2581906349** get_address_of_ratings_4() { return &___ratings_4; }
	inline void set_ratings_4(SpriteU5BU5D_t2581906349* value)
	{
		___ratings_4 = value;
		Il2CppCodeGenWriteBarrier((&___ratings_4), value);
	}
};

struct SpriteContainer_t2500302117_StaticFields
{
public:
	// SpriteContainer SpriteContainer::<Instance>k__BackingField
	SpriteContainer_t2500302117 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SpriteContainer_t2500302117_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline SpriteContainer_t2500302117 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline SpriteContainer_t2500302117 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(SpriteContainer_t2500302117 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITECONTAINER_T2500302117_H
#ifndef STARTBUTTON_T1998348287_H
#define STARTBUTTON_T1998348287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartButton
struct  StartButton_t1998348287  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button StartButton::button
	Button_t4055032469 * ___button_4;
	// YccioNetworkManager StartButton::ynm
	YccioNetworkManager_t3600504787 * ___ynm_5;

public:
	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(StartButton_t1998348287, ___button_4)); }
	inline Button_t4055032469 * get_button_4() const { return ___button_4; }
	inline Button_t4055032469 ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(Button_t4055032469 * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier((&___button_4), value);
	}

	inline static int32_t get_offset_of_ynm_5() { return static_cast<int32_t>(offsetof(StartButton_t1998348287, ___ynm_5)); }
	inline YccioNetworkManager_t3600504787 * get_ynm_5() const { return ___ynm_5; }
	inline YccioNetworkManager_t3600504787 ** get_address_of_ynm_5() { return &___ynm_5; }
	inline void set_ynm_5(YccioNetworkManager_t3600504787 * value)
	{
		___ynm_5 = value;
		Il2CppCodeGenWriteBarrier((&___ynm_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTBUTTON_T1998348287_H
#ifndef STONE_T255273793_H
#define STONE_T255273793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stone
struct  Stone_t255273793  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Stone::stoneNumber
	int32_t ___stoneNumber_4;
	// System.Int32 Stone::number
	int32_t ___number_5;
	// StoneType Stone::type
	int32_t ___type_6;
	// UnityEngine.GameObject Stone::selectImage
	GameObject_t1113636619 * ___selectImage_7;
	// System.Boolean Stone::isSelected
	bool ___isSelected_8;

public:
	inline static int32_t get_offset_of_stoneNumber_4() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___stoneNumber_4)); }
	inline int32_t get_stoneNumber_4() const { return ___stoneNumber_4; }
	inline int32_t* get_address_of_stoneNumber_4() { return &___stoneNumber_4; }
	inline void set_stoneNumber_4(int32_t value)
	{
		___stoneNumber_4 = value;
	}

	inline static int32_t get_offset_of_number_5() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___number_5)); }
	inline int32_t get_number_5() const { return ___number_5; }
	inline int32_t* get_address_of_number_5() { return &___number_5; }
	inline void set_number_5(int32_t value)
	{
		___number_5 = value;
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of_selectImage_7() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___selectImage_7)); }
	inline GameObject_t1113636619 * get_selectImage_7() const { return ___selectImage_7; }
	inline GameObject_t1113636619 ** get_address_of_selectImage_7() { return &___selectImage_7; }
	inline void set_selectImage_7(GameObject_t1113636619 * value)
	{
		___selectImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___selectImage_7), value);
	}

	inline static int32_t get_offset_of_isSelected_8() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___isSelected_8)); }
	inline bool get_isSelected_8() const { return ___isSelected_8; }
	inline bool* get_address_of_isSelected_8() { return &___isSelected_8; }
	inline void set_isSelected_8(bool value)
	{
		___isSelected_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STONE_T255273793_H
#ifndef SUBMITBUTTON_T4135388028_H
#define SUBMITBUTTON_T4135388028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubmitButton
struct  SubmitButton_t4135388028  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button SubmitButton::b
	Button_t4055032469 * ___b_5;

public:
	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(SubmitButton_t4135388028, ___b_5)); }
	inline Button_t4055032469 * get_b_5() const { return ___b_5; }
	inline Button_t4055032469 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Button_t4055032469 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

struct SubmitButton_t4135388028_StaticFields
{
public:
	// SubmitButton SubmitButton::<Instance>k__BackingField
	SubmitButton_t4135388028 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SubmitButton_t4135388028_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline SubmitButton_t4135388028 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline SubmitButton_t4135388028 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(SubmitButton_t4135388028 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMITBUTTON_T4135388028_H
#ifndef SUPPORTLOGGER_T2840230211_H
#define SUPPORTLOGGER_T2840230211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SupportLogger
struct  SupportLogger_t2840230211  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SupportLogger::LogTrafficStats
	bool ___LogTrafficStats_4;

public:
	inline static int32_t get_offset_of_LogTrafficStats_4() { return static_cast<int32_t>(offsetof(SupportLogger_t2840230211, ___LogTrafficStats_4)); }
	inline bool get_LogTrafficStats_4() const { return ___LogTrafficStats_4; }
	inline bool* get_address_of_LogTrafficStats_4() { return &___LogTrafficStats_4; }
	inline void set_LogTrafficStats_4(bool value)
	{
		___LogTrafficStats_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTLOGGER_T2840230211_H
#ifndef SUPPORTLOGGING_T3610999087_H
#define SUPPORTLOGGING_T3610999087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SupportLogging
struct  SupportLogging_t3610999087  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SupportLogging::LogTrafficStats
	bool ___LogTrafficStats_4;

public:
	inline static int32_t get_offset_of_LogTrafficStats_4() { return static_cast<int32_t>(offsetof(SupportLogging_t3610999087, ___LogTrafficStats_4)); }
	inline bool get_LogTrafficStats_4() const { return ___LogTrafficStats_4; }
	inline bool* get_address_of_LogTrafficStats_4() { return &___LogTrafficStats_4; }
	inline void set_LogTrafficStats_4(bool value)
	{
		___LogTrafficStats_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTLOGGING_T3610999087_H
#ifndef WAITINGPANEL_T1716730091_H
#define WAITINGPANEL_T1716730091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaitingPanel
struct  WaitingPanel_t1716730091  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITINGPANEL_T1716730091_H
#ifndef WINLOSEPANEL_T3114026018_H
#define WINLOSEPANEL_T3114026018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinLosePanel
struct  WinLosePanel_t3114026018  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject WinLosePanel::win
	GameObject_t1113636619 * ___win_4;
	// UnityEngine.GameObject WinLosePanel::lose
	GameObject_t1113636619 * ___lose_5;
	// UnityEngine.GameObject WinLosePanel::particle
	GameObject_t1113636619 * ___particle_6;

public:
	inline static int32_t get_offset_of_win_4() { return static_cast<int32_t>(offsetof(WinLosePanel_t3114026018, ___win_4)); }
	inline GameObject_t1113636619 * get_win_4() const { return ___win_4; }
	inline GameObject_t1113636619 ** get_address_of_win_4() { return &___win_4; }
	inline void set_win_4(GameObject_t1113636619 * value)
	{
		___win_4 = value;
		Il2CppCodeGenWriteBarrier((&___win_4), value);
	}

	inline static int32_t get_offset_of_lose_5() { return static_cast<int32_t>(offsetof(WinLosePanel_t3114026018, ___lose_5)); }
	inline GameObject_t1113636619 * get_lose_5() const { return ___lose_5; }
	inline GameObject_t1113636619 ** get_address_of_lose_5() { return &___lose_5; }
	inline void set_lose_5(GameObject_t1113636619 * value)
	{
		___lose_5 = value;
		Il2CppCodeGenWriteBarrier((&___lose_5), value);
	}

	inline static int32_t get_offset_of_particle_6() { return static_cast<int32_t>(offsetof(WinLosePanel_t3114026018, ___particle_6)); }
	inline GameObject_t1113636619 * get_particle_6() const { return ___particle_6; }
	inline GameObject_t1113636619 ** get_address_of_particle_6() { return &___particle_6; }
	inline void set_particle_6(GameObject_t1113636619 * value)
	{
		___particle_6 = value;
		Il2CppCodeGenWriteBarrier((&___particle_6), value);
	}
};

struct WinLosePanel_t3114026018_StaticFields
{
public:
	// WinLosePanel WinLosePanel::<Instance>k__BackingField
	WinLosePanel_t3114026018 * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WinLosePanel_t3114026018_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline WinLosePanel_t3114026018 * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline WinLosePanel_t3114026018 ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(WinLosePanel_t3114026018 * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINLOSEPANEL_T3114026018_H
#ifndef WINLOSEPARTICLE_T1640804235_H
#define WINLOSEPARTICLE_T1640804235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinLoseParticle
struct  WinLoseParticle_t1640804235  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINLOSEPARTICLE_T1640804235_H
#ifndef PUNBEHAVIOUR_T987309092_H
#define PUNBEHAVIOUR_T987309092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.PunBehaviour
struct  PunBehaviour_t987309092  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNBEHAVIOUR_T987309092_H
#ifndef SMOOTHSYNCMOVEMENT_T1809568931_H
#define SMOOTHSYNCMOVEMENT_T1809568931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothSyncMovement
struct  SmoothSyncMovement_t1809568931  : public MonoBehaviour_t3225183318
{
public:
	// System.Single SmoothSyncMovement::SmoothingDelay
	float ___SmoothingDelay_5;
	// UnityEngine.Vector3 SmoothSyncMovement::correctPlayerPos
	Vector3_t3722313464  ___correctPlayerPos_6;
	// UnityEngine.Quaternion SmoothSyncMovement::correctPlayerRot
	Quaternion_t2301928331  ___correctPlayerRot_7;

public:
	inline static int32_t get_offset_of_SmoothingDelay_5() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t1809568931, ___SmoothingDelay_5)); }
	inline float get_SmoothingDelay_5() const { return ___SmoothingDelay_5; }
	inline float* get_address_of_SmoothingDelay_5() { return &___SmoothingDelay_5; }
	inline void set_SmoothingDelay_5(float value)
	{
		___SmoothingDelay_5 = value;
	}

	inline static int32_t get_offset_of_correctPlayerPos_6() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t1809568931, ___correctPlayerPos_6)); }
	inline Vector3_t3722313464  get_correctPlayerPos_6() const { return ___correctPlayerPos_6; }
	inline Vector3_t3722313464 * get_address_of_correctPlayerPos_6() { return &___correctPlayerPos_6; }
	inline void set_correctPlayerPos_6(Vector3_t3722313464  value)
	{
		___correctPlayerPos_6 = value;
	}

	inline static int32_t get_offset_of_correctPlayerRot_7() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t1809568931, ___correctPlayerRot_7)); }
	inline Quaternion_t2301928331  get_correctPlayerRot_7() const { return ___correctPlayerRot_7; }
	inline Quaternion_t2301928331 * get_address_of_correctPlayerRot_7() { return &___correctPlayerRot_7; }
	inline void set_correctPlayerRot_7(Quaternion_t2301928331  value)
	{
		___correctPlayerRot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHSYNCMOVEMENT_T1809568931_H
#ifndef YCCIONETWORKMANAGER_T3600504787_H
#define YCCIONETWORKMANAGER_T3600504787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YccioNetworkManager
struct  YccioNetworkManager_t3600504787  : public PunBehaviour_t987309092
{
public:
	// PunTurnManager YccioNetworkManager::turnManager
	PunTurnManager_t1223962931 * ___turnManager_5;
	// SceneTransition YccioNetworkManager::st
	SceneTransition_t1138091307 * ___st_6;
	// PlayerData YccioNetworkManager::pd
	PlayerData_t220878115 * ___pd_7;
	// System.String YccioNetworkManager::myRoomName
	String_t* ___myRoomName_8;
	// RoomList YccioNetworkManager::roomList
	RoomList_t2314265074 * ___roomList_9;
	// StartButton YccioNetworkManager::startButton
	StartButton_t1998348287 * ___startButton_10;
	// PlayerStatusPanel YccioNetworkManager::statusPanel
	PlayerStatusPanel_t1209538314 * ___statusPanel_11;
	// System.Int32 YccioNetworkManager::maxStone
	int32_t ___maxStone_12;
	// System.Int32 YccioNetworkManager::numStone
	int32_t ___numStone_13;
	// System.Int32 YccioNetworkManager::totalStone
	int32_t ___totalStone_14;
	// System.Int32 YccioNetworkManager::playOrder
	int32_t ___playOrder_15;
	// System.Int32 YccioNetworkManager::orderOffset
	int32_t ___orderOffset_16;
	// System.Int32 YccioNetworkManager::playerNumber
	int32_t ___playerNumber_17;
	// System.Boolean YccioNetworkManager::isMyTurn
	bool ___isMyTurn_18;
	// System.Int32 YccioNetworkManager::fieldOwner
	int32_t ___fieldOwner_19;
	// System.Int32 YccioNetworkManager::curOrder
	int32_t ___curOrder_20;
	// System.Boolean YccioNetworkManager::isNoCloud3
	bool ___isNoCloud3_21;
	// System.Boolean YccioNetworkManager::isWaiting
	bool ___isWaiting_22;
	// System.Int32[] YccioNetworkManager::remainStones
	Int32U5BU5D_t385246372* ___remainStones_23;
	// System.Boolean YccioNetworkManager::isWinner
	bool ___isWinner_24;

public:
	inline static int32_t get_offset_of_turnManager_5() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___turnManager_5)); }
	inline PunTurnManager_t1223962931 * get_turnManager_5() const { return ___turnManager_5; }
	inline PunTurnManager_t1223962931 ** get_address_of_turnManager_5() { return &___turnManager_5; }
	inline void set_turnManager_5(PunTurnManager_t1223962931 * value)
	{
		___turnManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___turnManager_5), value);
	}

	inline static int32_t get_offset_of_st_6() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___st_6)); }
	inline SceneTransition_t1138091307 * get_st_6() const { return ___st_6; }
	inline SceneTransition_t1138091307 ** get_address_of_st_6() { return &___st_6; }
	inline void set_st_6(SceneTransition_t1138091307 * value)
	{
		___st_6 = value;
		Il2CppCodeGenWriteBarrier((&___st_6), value);
	}

	inline static int32_t get_offset_of_pd_7() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___pd_7)); }
	inline PlayerData_t220878115 * get_pd_7() const { return ___pd_7; }
	inline PlayerData_t220878115 ** get_address_of_pd_7() { return &___pd_7; }
	inline void set_pd_7(PlayerData_t220878115 * value)
	{
		___pd_7 = value;
		Il2CppCodeGenWriteBarrier((&___pd_7), value);
	}

	inline static int32_t get_offset_of_myRoomName_8() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___myRoomName_8)); }
	inline String_t* get_myRoomName_8() const { return ___myRoomName_8; }
	inline String_t** get_address_of_myRoomName_8() { return &___myRoomName_8; }
	inline void set_myRoomName_8(String_t* value)
	{
		___myRoomName_8 = value;
		Il2CppCodeGenWriteBarrier((&___myRoomName_8), value);
	}

	inline static int32_t get_offset_of_roomList_9() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___roomList_9)); }
	inline RoomList_t2314265074 * get_roomList_9() const { return ___roomList_9; }
	inline RoomList_t2314265074 ** get_address_of_roomList_9() { return &___roomList_9; }
	inline void set_roomList_9(RoomList_t2314265074 * value)
	{
		___roomList_9 = value;
		Il2CppCodeGenWriteBarrier((&___roomList_9), value);
	}

	inline static int32_t get_offset_of_startButton_10() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___startButton_10)); }
	inline StartButton_t1998348287 * get_startButton_10() const { return ___startButton_10; }
	inline StartButton_t1998348287 ** get_address_of_startButton_10() { return &___startButton_10; }
	inline void set_startButton_10(StartButton_t1998348287 * value)
	{
		___startButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___startButton_10), value);
	}

	inline static int32_t get_offset_of_statusPanel_11() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___statusPanel_11)); }
	inline PlayerStatusPanel_t1209538314 * get_statusPanel_11() const { return ___statusPanel_11; }
	inline PlayerStatusPanel_t1209538314 ** get_address_of_statusPanel_11() { return &___statusPanel_11; }
	inline void set_statusPanel_11(PlayerStatusPanel_t1209538314 * value)
	{
		___statusPanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___statusPanel_11), value);
	}

	inline static int32_t get_offset_of_maxStone_12() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___maxStone_12)); }
	inline int32_t get_maxStone_12() const { return ___maxStone_12; }
	inline int32_t* get_address_of_maxStone_12() { return &___maxStone_12; }
	inline void set_maxStone_12(int32_t value)
	{
		___maxStone_12 = value;
	}

	inline static int32_t get_offset_of_numStone_13() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___numStone_13)); }
	inline int32_t get_numStone_13() const { return ___numStone_13; }
	inline int32_t* get_address_of_numStone_13() { return &___numStone_13; }
	inline void set_numStone_13(int32_t value)
	{
		___numStone_13 = value;
	}

	inline static int32_t get_offset_of_totalStone_14() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___totalStone_14)); }
	inline int32_t get_totalStone_14() const { return ___totalStone_14; }
	inline int32_t* get_address_of_totalStone_14() { return &___totalStone_14; }
	inline void set_totalStone_14(int32_t value)
	{
		___totalStone_14 = value;
	}

	inline static int32_t get_offset_of_playOrder_15() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___playOrder_15)); }
	inline int32_t get_playOrder_15() const { return ___playOrder_15; }
	inline int32_t* get_address_of_playOrder_15() { return &___playOrder_15; }
	inline void set_playOrder_15(int32_t value)
	{
		___playOrder_15 = value;
	}

	inline static int32_t get_offset_of_orderOffset_16() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___orderOffset_16)); }
	inline int32_t get_orderOffset_16() const { return ___orderOffset_16; }
	inline int32_t* get_address_of_orderOffset_16() { return &___orderOffset_16; }
	inline void set_orderOffset_16(int32_t value)
	{
		___orderOffset_16 = value;
	}

	inline static int32_t get_offset_of_playerNumber_17() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___playerNumber_17)); }
	inline int32_t get_playerNumber_17() const { return ___playerNumber_17; }
	inline int32_t* get_address_of_playerNumber_17() { return &___playerNumber_17; }
	inline void set_playerNumber_17(int32_t value)
	{
		___playerNumber_17 = value;
	}

	inline static int32_t get_offset_of_isMyTurn_18() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___isMyTurn_18)); }
	inline bool get_isMyTurn_18() const { return ___isMyTurn_18; }
	inline bool* get_address_of_isMyTurn_18() { return &___isMyTurn_18; }
	inline void set_isMyTurn_18(bool value)
	{
		___isMyTurn_18 = value;
	}

	inline static int32_t get_offset_of_fieldOwner_19() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___fieldOwner_19)); }
	inline int32_t get_fieldOwner_19() const { return ___fieldOwner_19; }
	inline int32_t* get_address_of_fieldOwner_19() { return &___fieldOwner_19; }
	inline void set_fieldOwner_19(int32_t value)
	{
		___fieldOwner_19 = value;
	}

	inline static int32_t get_offset_of_curOrder_20() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___curOrder_20)); }
	inline int32_t get_curOrder_20() const { return ___curOrder_20; }
	inline int32_t* get_address_of_curOrder_20() { return &___curOrder_20; }
	inline void set_curOrder_20(int32_t value)
	{
		___curOrder_20 = value;
	}

	inline static int32_t get_offset_of_isNoCloud3_21() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___isNoCloud3_21)); }
	inline bool get_isNoCloud3_21() const { return ___isNoCloud3_21; }
	inline bool* get_address_of_isNoCloud3_21() { return &___isNoCloud3_21; }
	inline void set_isNoCloud3_21(bool value)
	{
		___isNoCloud3_21 = value;
	}

	inline static int32_t get_offset_of_isWaiting_22() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___isWaiting_22)); }
	inline bool get_isWaiting_22() const { return ___isWaiting_22; }
	inline bool* get_address_of_isWaiting_22() { return &___isWaiting_22; }
	inline void set_isWaiting_22(bool value)
	{
		___isWaiting_22 = value;
	}

	inline static int32_t get_offset_of_remainStones_23() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___remainStones_23)); }
	inline Int32U5BU5D_t385246372* get_remainStones_23() const { return ___remainStones_23; }
	inline Int32U5BU5D_t385246372** get_address_of_remainStones_23() { return &___remainStones_23; }
	inline void set_remainStones_23(Int32U5BU5D_t385246372* value)
	{
		___remainStones_23 = value;
		Il2CppCodeGenWriteBarrier((&___remainStones_23), value);
	}

	inline static int32_t get_offset_of_isWinner_24() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___isWinner_24)); }
	inline bool get_isWinner_24() const { return ___isWinner_24; }
	inline bool* get_address_of_isWinner_24() { return &___isWinner_24; }
	inline void set_isWinner_24(bool value)
	{
		___isWinner_24 = value;
	}
};

struct YccioNetworkManager_t3600504787_StaticFields
{
public:
	// YccioNetworkManager YccioNetworkManager::<Instance>k__BackingField
	YccioNetworkManager_t3600504787 * ___U3CInstanceU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787_StaticFields, ___U3CInstanceU3Ek__BackingField_25)); }
	inline YccioNetworkManager_t3600504787 * get_U3CInstanceU3Ek__BackingField_25() const { return ___U3CInstanceU3Ek__BackingField_25; }
	inline YccioNetworkManager_t3600504787 ** get_address_of_U3CInstanceU3Ek__BackingField_25() { return &___U3CInstanceU3Ek__BackingField_25; }
	inline void set_U3CInstanceU3Ek__BackingField_25(YccioNetworkManager_t3600504787 * value)
	{
		___U3CInstanceU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YCCIONETWORKMANAGER_T3600504787_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (SmoothSyncMovement_t1809568931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[3] = 
{
	SmoothSyncMovement_t1809568931::get_offset_of_SmoothingDelay_5(),
	SmoothSyncMovement_t1809568931::get_offset_of_correctPlayerPos_6(),
	SmoothSyncMovement_t1809568931::get_offset_of_correctPlayerRot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (SupportLogger_t2840230211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[1] = 
{
	SupportLogger_t2840230211::get_offset_of_LogTrafficStats_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (SupportLogging_t3610999087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[1] = 
{
	SupportLogging_t3610999087::get_offset_of_LogTrafficStats_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (TimeKeeper_t3694205465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[4] = 
{
	TimeKeeper_t3694205465::get_offset_of_lastExecutionTime_0(),
	TimeKeeper_t3694205465::get_offset_of_shouldExecute_1(),
	TimeKeeper_t3694205465::get_offset_of_U3CIntervalU3Ek__BackingField_2(),
	TimeKeeper_t3694205465::get_offset_of_U3CIsEnabledU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (ButtonInsideScrollList_t657350886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	ButtonInsideScrollList_t657350886::get_offset_of_scrollRect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (TextButtonTransition_t3897064113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[3] = 
{
	TextButtonTransition_t3897064113::get_offset_of__text_4(),
	TextButtonTransition_t3897064113::get_offset_of_NormalColor_5(),
	TextButtonTransition_t3897064113::get_offset_of_HoverColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (TextToggleIsOnTransition_t1534099090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[7] = 
{
	TextToggleIsOnTransition_t1534099090::get_offset_of_toggle_4(),
	TextToggleIsOnTransition_t1534099090::get_offset_of__text_5(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_NormalOnColor_6(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_NormalOffColor_7(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_HoverOnColor_8(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_HoverOffColor_9(),
	TextToggleIsOnTransition_t1534099090::get_offset_of_isHover_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (ChatChannel_t3314309194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[5] = 
{
	ChatChannel_t3314309194::get_offset_of_Name_0(),
	ChatChannel_t3314309194::get_offset_of_Senders_1(),
	ChatChannel_t3314309194::get_offset_of_Messages_2(),
	ChatChannel_t3314309194::get_offset_of_MessageLimit_3(),
	ChatChannel_t3314309194::get_offset_of_U3CIsPrivateU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (ChatClient_t3322764984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[20] = 
{
	0,
	ChatClient_t3322764984::get_offset_of_U3CNameServerAddressU3Ek__BackingField_1(),
	ChatClient_t3322764984::get_offset_of_U3CFrontendAddressU3Ek__BackingField_2(),
	ChatClient_t3322764984::get_offset_of_chatRegion_3(),
	ChatClient_t3322764984::get_offset_of_U3CStateU3Ek__BackingField_4(),
	ChatClient_t3322764984::get_offset_of_U3CDisconnectedCauseU3Ek__BackingField_5(),
	ChatClient_t3322764984::get_offset_of_U3CAppVersionU3Ek__BackingField_6(),
	ChatClient_t3322764984::get_offset_of_U3CAppIdU3Ek__BackingField_7(),
	ChatClient_t3322764984::get_offset_of_U3CAuthValuesU3Ek__BackingField_8(),
	ChatClient_t3322764984::get_offset_of_MessageLimit_9(),
	ChatClient_t3322764984::get_offset_of_PublicChannels_10(),
	ChatClient_t3322764984::get_offset_of_PrivateChannels_11(),
	ChatClient_t3322764984::get_offset_of_PublicChannelsUnsubscribing_12(),
	ChatClient_t3322764984::get_offset_of_listener_13(),
	ChatClient_t3322764984::get_offset_of_chatPeer_14(),
	0,
	ChatClient_t3322764984::get_offset_of_didAuthenticate_16(),
	ChatClient_t3322764984::get_offset_of_msDeltaForServiceCalls_17(),
	ChatClient_t3322764984::get_offset_of_msTimestampOfLastServiceCall_18(),
	ChatClient_t3322764984::get_offset_of_U3CUseBackgroundWorkerForSendingU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (ChatDisconnectCause_t3824038781)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2509[12] = 
{
	ChatDisconnectCause_t3824038781::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (ChatEventCode_t3158930382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (ChatOperationCode_t2600569862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (ChatParameterCode_t1934080102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (ChatPeer_t2186541770), -1, sizeof(ChatPeer_t2186541770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2513[3] = 
{
	0,
	0,
	ChatPeer_t2186541770_StaticFields::get_offset_of_ProtocolToNameServerPort_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (CustomAuthenticationType_t3431816196)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2514[8] = 
{
	CustomAuthenticationType_t3431816196::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (AuthenticationValues_t187933346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[5] = 
{
	AuthenticationValues_t187933346::get_offset_of_authType_0(),
	AuthenticationValues_t187933346::get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1(),
	AuthenticationValues_t187933346::get_offset_of_U3CAuthPostDataU3Ek__BackingField_2(),
	AuthenticationValues_t187933346::get_offset_of_U3CTokenU3Ek__BackingField_3(),
	AuthenticationValues_t187933346::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (ParameterCode_t1075756884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (ErrorCode_t3704103031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (ChatState_t3709194201)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2518[13] = 
{
	ChatState_t3709194201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (ChatUserStatus_t1535268910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (ConnectingPanel_t2747570441), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (PointOffset_t2729089990)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2522[9] = 
{
	PointOffset_t2729089990::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (Deck_t2172403585), -1, sizeof(Deck_t2172403585_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2523[15] = 
{
	Deck_t2172403585::get_offset_of_deckImage_4(),
	Deck_t2172403585::get_offset_of_stonePrefab_5(),
	Deck_t2172403585::get_offset_of_isFirst_6(),
	Deck_t2172403585::get_offset_of_maxSubmit_7(),
	Deck_t2172403585::get_offset_of_curSubmit_8(),
	Deck_t2172403585::get_offset_of_numOfStone_9(),
	Deck_t2172403585::get_offset_of_stoneSubmit_10(),
	Deck_t2172403585::get_offset_of_stoneForPoint_11(),
	Deck_t2172403585::get_offset_of_stoneForNumber_12(),
	Deck_t2172403585::get_offset_of_stoneToDestory_13(),
	Deck_t2172403585::get_offset_of_cloud_14(),
	Deck_t2172403585::get_offset_of_star_15(),
	Deck_t2172403585::get_offset_of_moon_16(),
	Deck_t2172403585::get_offset_of_sun_17(),
	Deck_t2172403585_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (DoorLock_t1141741040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[4] = 
{
	DoorLock_t1141741040::get_offset_of_password_4(),
	DoorLock_t1141741040::get_offset_of_isActivce_5(),
	DoorLock_t1141741040::get_offset_of_buttonTexts_6(),
	DoorLock_t1141741040::get_offset_of_st_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (EditNamePanel_t1647359677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[1] = 
{
	EditNamePanel_t1647359677::get_offset_of_input_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (ExitPanel_t1668634582), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (Field_t4115194983), -1, sizeof(Field_t4115194983_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2527[7] = 
{
	Field_t4115194983::get_offset_of_stonePrefab_4(),
	Field_t4115194983::get_offset_of_fieldImage_5(),
	Field_t4115194983::get_offset_of_t_6(),
	Field_t4115194983::get_offset_of_isFirstField_7(),
	Field_t4115194983::get_offset_of_prevPoint_8(),
	Field_t4115194983::get_offset_of_numOfStone_9(),
	Field_t4115194983_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (HowToPanel_t3065907647), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (ImojiButton_t701272134), -1, sizeof(ImojiButton_t701272134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2529[6] = 
{
	ImojiButton_t701272134::get_offset_of_imojis_4(),
	ImojiButton_t701272134::get_offset_of_container_5(),
	ImojiButton_t701272134::get_offset_of_my_6(),
	ImojiButton_t701272134::get_offset_of_isOn_7(),
	ImojiButton_t701272134::get_offset_of_isImojing_8(),
	ImojiButton_t701272134_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[5] = 
{
	U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401::get_offset_of_U3CiU3E__1_0(),
	U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401::get_offset_of_U24this_1(),
	U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401::get_offset_of_U24current_2(),
	U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401::get_offset_of_U24disposing_3(),
	U3CCR_ImojiFadeU3Ec__Iterator0_t3182661401::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (IncreaseEffect_t1728429428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[1] = 
{
	IncreaseEffect_t1728429428::get_offset_of_text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (IncreaseEffectPanel_t3055077437), -1, sizeof(IncreaseEffectPanel_t3055077437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2532[4] = 
{
	IncreaseEffectPanel_t3055077437::get_offset_of_increaseMoney_4(),
	IncreaseEffectPanel_t3055077437::get_offset_of_posPrefab_5(),
	IncreaseEffectPanel_t3055077437::get_offset_of_pos_6(),
	IncreaseEffectPanel_t3055077437_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (InGameExitPanel_t3158895127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[1] = 
{
	InGameExitPanel_t3158895127::get_offset_of_t_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (InGamePanel_t2211285949), -1, sizeof(InGamePanel_t2211285949_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2534[9] = 
{
	InGamePanel_t2211285949::get_offset_of_playerName_4(),
	InGamePanel_t2211285949::get_offset_of_chips_5(),
	InGamePanel_t2211285949::get_offset_of_exitPanel_6(),
	InGamePanel_t2211285949::get_offset_of_isExitOn_7(),
	InGamePanel_t2211285949::get_offset_of_increaseMoney_8(),
	InGamePanel_t2211285949::get_offset_of_rating_9(),
	InGamePanel_t2211285949::get_offset_of_playinfoPanel_10(),
	InGamePanel_t2211285949::get_offset_of_isReadPlayInfo_11(),
	InGamePanel_t2211285949_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (LobbyPanel_t3775216716), -1, sizeof(LobbyPanel_t3775216716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2535[9] = 
{
	LobbyPanel_t3775216716::get_offset_of_playerName_4(),
	LobbyPanel_t3775216716::get_offset_of_chip_5(),
	LobbyPanel_t3775216716::get_offset_of_editNamePanel_6(),
	LobbyPanel_t3775216716::get_offset_of_exitPanel_7(),
	LobbyPanel_t3775216716::get_offset_of_howtoPanel_8(),
	LobbyPanel_t3775216716::get_offset_of_rating_9(),
	LobbyPanel_t3775216716::get_offset_of_isEditOn_10(),
	LobbyPanel_t3775216716::get_offset_of_isExitOn_11(),
	LobbyPanel_t3775216716_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (NewRoomPanel_t3215952434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[1] = 
{
	NewRoomPanel_t3215952434::get_offset_of_input_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (PassButton_t4045823955), -1, sizeof(PassButton_t4045823955_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2537[2] = 
{
	PassButton_t4045823955_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	PassButton_t4045823955::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (PlayerData_t220878115), -1, sizeof(PlayerData_t220878115_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2538[3] = 
{
	PlayerData_t220878115::get_offset_of_chips_4(),
	PlayerData_t220878115::get_offset_of_playerName_5(),
	PlayerData_t220878115_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (PlayerStatus_t402953766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[11] = 
{
	PlayerStatus_t402953766::get_offset_of_activeColor_4(),
	PlayerStatus_t402953766::get_offset_of_playerColor_5(),
	PlayerStatus_t402953766::get_offset_of_rating_6(),
	PlayerStatus_t402953766::get_offset_of_playerName_7(),
	PlayerStatus_t402953766::get_offset_of_remainStone_8(),
	PlayerStatus_t402953766::get_offset_of_playerChips_9(),
	PlayerStatus_t402953766::get_offset_of_chips_10(),
	PlayerStatus_t402953766::get_offset_of_order_11(),
	PlayerStatus_t402953766::get_offset_of_slider_12(),
	PlayerStatus_t402953766::get_offset_of_imojiBG_13(),
	PlayerStatus_t402953766::get_offset_of_imoji_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[5] = 
{
	U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569::get_offset_of_U3CiU3E__1_0(),
	U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569::get_offset_of_U24this_1(),
	U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569::get_offset_of_U24current_2(),
	U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569::get_offset_of_U24disposing_3(),
	U3CCR_ImojiFadeU3Ec__Iterator0_t4226668569::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (PlayerStatusPanel_t1209538314), -1, sizeof(PlayerStatusPanel_t1209538314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2541[6] = 
{
	PlayerStatusPanel_t1209538314::get_offset_of_playerStatusPrefab_4(),
	PlayerStatusPanel_t1209538314::get_offset_of_waitingPanel_5(),
	PlayerStatusPanel_t1209538314::get_offset_of_isWaiting_6(),
	PlayerStatusPanel_t1209538314::get_offset_of_isExist_7(),
	PlayerStatusPanel_t1209538314::get_offset_of_timer_8(),
	PlayerStatusPanel_t1209538314_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[7] = 
{
	U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058::get_offset_of_U3CstatusesU3E__0_0(),
	U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058::get_offset_of_activePlayOrder_1(),
	U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058::get_offset_of_remain_2(),
	U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058::get_offset_of_U24this_3(),
	U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058::get_offset_of_U24current_4(),
	U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058::get_offset_of_U24disposing_5(),
	U3CCR_UpdateRemainStoneU3Ec__Iterator0_t3674447058::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (PlayInfoPanel_t2099359151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (RemainTime_t3232366768), -1, sizeof(RemainTime_t3232366768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2544[4] = 
{
	RemainTime_t3232366768::get_offset_of_text_4(),
	RemainTime_t3232366768::get_offset_of_slider_5(),
	RemainTime_t3232366768::get_offset_of_current_6(),
	RemainTime_t3232366768_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (RoomData_t44449739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[5] = 
{
	RoomData_t44449739::get_offset_of_gameName_4(),
	RoomData_t44449739::get_offset_of_isPlaying_5(),
	RoomData_t44449739::get_offset_of_playerNum_6(),
	RoomData_t44449739::get_offset_of_roomNumber_7(),
	RoomData_t44449739::get_offset_of_roomName_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (RoomList_t2314265074), -1, sizeof(RoomList_t2314265074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2546[4] = 
{
	RoomList_t2314265074::get_offset_of_roomButtonPrefab_4(),
	RoomList_t2314265074::get_offset_of_roomPanel_5(),
	RoomList_t2314265074::get_offset_of_createRoomPanel_6(),
	RoomList_t2314265074_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (RoomPanel_t941367166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[4] = 
{
	RoomPanel_t941367166::get_offset_of_gameName_4(),
	RoomPanel_t941367166::get_offset_of_playerNum_5(),
	RoomPanel_t941367166::get_offset_of_roomState_6(),
	RoomPanel_t941367166::get_offset_of_joinButton_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (SceneTransition_t1138091307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[2] = 
{
	SceneTransition_t1138091307::get_offset_of_isTestMode_4(),
	SceneTransition_t1138091307::get_offset_of_ynm_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (SortByColor_t1786351735), -1, sizeof(SortByColor_t1786351735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2549[2] = 
{
	SortByColor_t1786351735::get_offset_of_b_4(),
	SortByColor_t1786351735_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (SortByNumber_t227753584), -1, sizeof(SortByNumber_t227753584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2550[2] = 
{
	SortByNumber_t227753584::get_offset_of_b_4(),
	SortByNumber_t227753584_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (SoundManager_t2102329059), -1, sizeof(SoundManager_t2102329059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2551[3] = 
{
	SoundManager_t2102329059::get_offset_of_clips_4(),
	SoundManager_t2102329059::get_offset_of_source_5(),
	SoundManager_t2102329059_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (SpriteContainer_t2500302117), -1, sizeof(SpriteContainer_t2500302117_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2552[2] = 
{
	SpriteContainer_t2500302117::get_offset_of_ratings_4(),
	SpriteContainer_t2500302117_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (StartButton_t1998348287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[2] = 
{
	StartButton_t1998348287::get_offset_of_button_4(),
	StartButton_t1998348287::get_offset_of_ynm_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (StoneType_t314252901)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2554[5] = 
{
	StoneType_t314252901::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (Stone_t255273793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[5] = 
{
	Stone_t255273793::get_offset_of_stoneNumber_4(),
	Stone_t255273793::get_offset_of_number_5(),
	Stone_t255273793::get_offset_of_type_6(),
	Stone_t255273793::get_offset_of_selectImage_7(),
	Stone_t255273793::get_offset_of_isSelected_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (SubmitButton_t4135388028), -1, sizeof(SubmitButton_t4135388028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2556[2] = 
{
	SubmitButton_t4135388028_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	SubmitButton_t4135388028::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (WaitingPanel_t1716730091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (WinLosePanel_t3114026018), -1, sizeof(WinLosePanel_t3114026018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2558[4] = 
{
	WinLosePanel_t3114026018::get_offset_of_win_4(),
	WinLosePanel_t3114026018::get_offset_of_lose_5(),
	WinLosePanel_t3114026018::get_offset_of_particle_6(),
	WinLosePanel_t3114026018_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (U3CC_OnWinU3Ec__Iterator0_t1703244554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[6] = 
{
	U3CC_OnWinU3Ec__Iterator0_t1703244554::get_offset_of_U3CtU3E__0_0(),
	U3CC_OnWinU3Ec__Iterator0_t1703244554::get_offset_of_U3CiU3E__1_1(),
	U3CC_OnWinU3Ec__Iterator0_t1703244554::get_offset_of_U24this_2(),
	U3CC_OnWinU3Ec__Iterator0_t1703244554::get_offset_of_U24current_3(),
	U3CC_OnWinU3Ec__Iterator0_t1703244554::get_offset_of_U24disposing_4(),
	U3CC_OnWinU3Ec__Iterator0_t1703244554::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (U3CC_OnLoseU3Ec__Iterator1_t1181911525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[6] = 
{
	U3CC_OnLoseU3Ec__Iterator1_t1181911525::get_offset_of_U3CtU3E__0_0(),
	U3CC_OnLoseU3Ec__Iterator1_t1181911525::get_offset_of_U3CiU3E__1_1(),
	U3CC_OnLoseU3Ec__Iterator1_t1181911525::get_offset_of_U24this_2(),
	U3CC_OnLoseU3Ec__Iterator1_t1181911525::get_offset_of_U24current_3(),
	U3CC_OnLoseU3Ec__Iterator1_t1181911525::get_offset_of_U24disposing_4(),
	U3CC_OnLoseU3Ec__Iterator1_t1181911525::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (WinLoseParticle_t1640804235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (PhotonEventCodes_t582153385)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2562[9] = 
{
	PhotonEventCodes_t582153385::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (YccioNetworkManager_t3600504787), -1, sizeof(YccioNetworkManager_t3600504787_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2563[21] = 
{
	YccioNetworkManager_t3600504787::get_offset_of_turnManager_5(),
	YccioNetworkManager_t3600504787::get_offset_of_st_6(),
	YccioNetworkManager_t3600504787::get_offset_of_pd_7(),
	YccioNetworkManager_t3600504787::get_offset_of_myRoomName_8(),
	YccioNetworkManager_t3600504787::get_offset_of_roomList_9(),
	YccioNetworkManager_t3600504787::get_offset_of_startButton_10(),
	YccioNetworkManager_t3600504787::get_offset_of_statusPanel_11(),
	YccioNetworkManager_t3600504787::get_offset_of_maxStone_12(),
	YccioNetworkManager_t3600504787::get_offset_of_numStone_13(),
	YccioNetworkManager_t3600504787::get_offset_of_totalStone_14(),
	YccioNetworkManager_t3600504787::get_offset_of_playOrder_15(),
	YccioNetworkManager_t3600504787::get_offset_of_orderOffset_16(),
	YccioNetworkManager_t3600504787::get_offset_of_playerNumber_17(),
	YccioNetworkManager_t3600504787::get_offset_of_isMyTurn_18(),
	YccioNetworkManager_t3600504787::get_offset_of_fieldOwner_19(),
	YccioNetworkManager_t3600504787::get_offset_of_curOrder_20(),
	YccioNetworkManager_t3600504787::get_offset_of_isNoCloud3_21(),
	YccioNetworkManager_t3600504787::get_offset_of_isWaiting_22(),
	YccioNetworkManager_t3600504787::get_offset_of_remainStones_23(),
	YccioNetworkManager_t3600504787::get_offset_of_isWinner_24(),
	YccioNetworkManager_t3600504787_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (U3CCR_BetSoundU3Ec__Iterator0_t1415787259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[5] = 
{
	U3CCR_BetSoundU3Ec__Iterator0_t1415787259::get_offset_of_U3CiU3E__1_0(),
	U3CCR_BetSoundU3Ec__Iterator0_t1415787259::get_offset_of_num_1(),
	U3CCR_BetSoundU3Ec__Iterator0_t1415787259::get_offset_of_U24current_2(),
	U3CCR_BetSoundU3Ec__Iterator0_t1415787259::get_offset_of_U24disposing_3(),
	U3CCR_BetSoundU3Ec__Iterator0_t1415787259::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (U3CCR_CalchipsU3Ec__Iterator1_t2144217578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[6] = 
{
	U3CCR_CalchipsU3Ec__Iterator1_t2144217578::get_offset_of_U3CmyU3E__0_0(),
	U3CCR_CalchipsU3Ec__Iterator1_t2144217578::get_offset_of_U3CiU3E__1_1(),
	U3CCR_CalchipsU3Ec__Iterator1_t2144217578::get_offset_of_U24this_2(),
	U3CCR_CalchipsU3Ec__Iterator1_t2144217578::get_offset_of_U24current_3(),
	U3CCR_CalchipsU3Ec__Iterator1_t2144217578::get_offset_of_U24disposing_4(),
	U3CCR_CalchipsU3Ec__Iterator1_t2144217578::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2566[6] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (U24ArrayTypeU3D16_t3253128244)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3253128244 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (U24ArrayTypeU3D32_t3651253610)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D32_t3651253610 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (U24ArrayTypeU3D48_t1336283963)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D48_t1336283963 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
