﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// AuthenticationValues
struct AuthenticationValues_t660572511;
// Deck
struct Deck_t2172403585;
// EnterRoomParams
struct EnterRoomParams_t3960472384;
// ExitGames.Client.Photon.EncryptorManaged.Decryptor
struct Decryptor_t2116099858;
// ExitGames.Client.Photon.EncryptorManaged.Encryptor
struct Encryptor_t200327285;
// ExitGames.Client.Photon.Hashtable
struct Hashtable_t1048209202;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t2581629031;
// ExitGames.Client.Photon.OperationResponse
struct OperationResponse_t423627973;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t2956237011;
// ExitGames.Client.Photon.PhotonPeer
struct PhotonPeer_t1608153861;
// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t1302902347;
// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t4013908777;
// Field
struct Field_t4115194983;
// IPunPrefabPool
struct IPunPrefabPool_t3054155687;
// IPunTurnManagerCallbacks
struct IPunTurnManagerCallbacks_t17903323;
// InGamePanel
struct InGamePanel_t2211285949;
// IncreaseEffectPanel
struct IncreaseEffectPanel_t3055077437;
// NetworkingPeer
struct NetworkingPeer_t264212356;
// PassButton
struct PassButton_t4045823955;
// Photon.MonoBehaviour
struct MonoBehaviour_t3225183318;
// Photon.PunBehaviour
struct PunBehaviour_t987309092;
// PhotonHandler
struct PhotonHandler_t2139970417;
// PhotonNetwork/EventCallback
struct EventCallback_t1220598991;
// PhotonPlayer
struct PhotonPlayer_t3305149557;
// PhotonPlayer[]
struct PhotonPlayerU5BU5D_t2880637464;
// PhotonStream
struct PhotonStream_t1003850889;
// PhotonView
struct PhotonView_t2207721820;
// PlayerData
struct PlayerData_t220878115;
// PlayerStatus
struct PlayerStatus_t402953766;
// PlayerStatusPanel
struct PlayerStatusPanel_t1209538314;
// PunTurnManager
struct PunTurnManager_t1223962931;
// RaiseEventOptions
struct RaiseEventOptions_t1229553678;
// RemainTime
struct RemainTime_t3232366768;
// Room
struct Room_t3759828263;
// RoomInfo
struct RoomInfo_t3170295620;
// RoomInfo[]
struct RoomInfoU5BU5D_t1491207981;
// RoomList
struct RoomList_t2314265074;
// RoomOptions
struct RoomOptions_t1787645948;
// RpsCore
struct RpsCore_t2154144697;
// RpsCore/<CycleRemoteHandCoroutine>c__Iterator1
struct U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456;
// RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0
struct U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197;
// SceneManagerHelper
struct SceneManagerHelper_t3665721098;
// SceneTransition
struct SceneTransition_t1138091307;
// ServerSettings
struct ServerSettings_t2755303613;
// ShowStatusWhenConnecting
struct ShowStatusWhenConnecting_t1063567576;
// SmoothSyncMovement
struct SmoothSyncMovement_t1809568931;
// SortByColor
struct SortByColor_t1786351735;
// SortByNumber
struct SortByNumber_t227753584;
// SoundManager
struct SoundManager_t2102329059;
// SpriteContainer
struct SpriteContainer_t2500302117;
// StartButton
struct StartButton_t1998348287;
// Stone
struct Stone_t255273793;
// Stone[]
struct StoneU5BU5D_t1191912092;
// SubmitButton
struct SubmitButton_t4135388028;
// SupportLogger
struct SupportLogger_t2840230211;
// SupportLogging
struct SupportLogging_t3610999087;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t819399007;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Byte,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t3835237792;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4209139644;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1694351041;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t1720840067;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type>
struct Dictionary_2_t1253839074;
// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct Dictionary_2_t1405253484;
// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.Hashtable>
struct Dictionary_2_t4231889829;
// System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer>
struct Dictionary_2_t2193862888;
// System.Collections.Generic.Dictionary`2<System.Int32,PhotonView>
struct Dictionary_2_t1096435151;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object[]>
struct Dictionary_2_t1732652656;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.String,RoomInfo>
struct Dictionary_2_t2955551919;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t3742157810;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t898892918;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>
struct Dictionary_2_t1499080758;
// System.Collections.Generic.Dictionary`2<UnityEngine.Component,System.Reflection.MethodInfo>
struct Dictionary_2_t3676033689;
// System.Collections.Generic.HashSet`1<PhotonPlayer>
struct HashSet_1_t1870099031;
// System.Collections.Generic.HashSet`1<System.Byte>
struct HashSet_1_t3994213146;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_t3973553389;
// System.Collections.Generic.IEqualityComparer`1<System.Byte>
struct IEqualityComparer_1_t3241628394;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t892470886;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Collections.Generic.List`1<FriendInfo>
struct List_1_t2005371586;
// System.Collections.Generic.List`1<Region>
struct List_1_t861332708;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<TypedLobbyInfo>
struct List_1_t3976582791;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3395709193;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2926365658;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// TypedLobby
struct TypedLobby_t3336582029;
// TypedLobbyInfo
struct TypedLobbyInfo_t2504508049;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1677636661;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1059417452;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Component[]
struct ComponentU5BU5D_t3294940482;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t811797299;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2510215842;
// UnityEngine.GUISettings
struct GUISettings_t1774757634;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t1143955295;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2383250302;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t48803504;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// WaitingPanel
struct WaitingPanel_t1716730091;
// WebRpcResponse
struct WebRpcResponse_t4177102182;
// WinLosePanel
struct WinLosePanel_t3114026018;
// WinLosePanel/<C_OnLose>c__Iterator1
struct U3CC_OnLoseU3Ec__Iterator1_t1181911525;
// WinLosePanel/<C_OnWin>c__Iterator0
struct U3CC_OnWinU3Ec__Iterator0_t1703244554;
// WinLoseParticle
struct WinLoseParticle_t1640804235;
// YccioNetworkManager
struct YccioNetworkManager_t3600504787;
// YccioNetworkManager/<CR_BetSound>c__Iterator0
struct U3CCR_BetSoundU3Ec__Iterator0_t1415787259;
// YccioNetworkManager/<CR_Calchips>c__Iterator1
struct U3CCR_CalchipsU3Ec__Iterator1_t2144217578;

extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern RuntimeClass* Byte_t1134296376_il2cpp_TypeInfo_var;
extern RuntimeClass* ClientState_t1348705391_il2cpp_TypeInfo_var;
extern RuntimeClass* CloudRegionCode_t1925019500_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2865362463_il2cpp_TypeInfo_var;
extern RuntimeClass* DisconnectCause_t501870387_il2cpp_TypeInfo_var;
extern RuntimeClass* EventCallback_t1220598991_il2cpp_TypeInfo_var;
extern RuntimeClass* GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var;
extern RuntimeClass* GUI_t1624858472_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* Hashtable_t1048209202_il2cpp_TypeInfo_var;
extern RuntimeClass* HostingOption_t2949276063_il2cpp_TypeInfo_var;
extern RuntimeClass* IndexOutOfRangeException_t1578797820_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32U5BU5D_t385246372_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern RuntimeClass* LobbyType_t3695323860_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* PhotonHandler_t2139970417_il2cpp_TypeInfo_var;
extern RuntimeClass* PhotonNetwork_t1610183659_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern RuntimeClass* RaiseEventOptions_t1229553678_il2cpp_TypeInfo_var;
extern RuntimeClass* RoomOptions_t1787645948_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern RuntimeClass* SortByColor_t1786351735_il2cpp_TypeInfo_var;
extern RuntimeClass* SortByNumber_t227753584_il2cpp_TypeInfo_var;
extern RuntimeClass* SoundManager_t2102329059_il2cpp_TypeInfo_var;
extern RuntimeClass* SpriteContainer_t2500302117_il2cpp_TypeInfo_var;
extern RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* SubmitButton_t4135388028_il2cpp_TypeInfo_var;
extern RuntimeClass* SupportClass_t2974952451_il2cpp_TypeInfo_var;
extern RuntimeClass* Team_t2865224648_il2cpp_TypeInfo_var;
extern RuntimeClass* TurnExtensions_t3150044944_il2cpp_TypeInfo_var;
extern RuntimeClass* TypedLobby_t3336582029_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CCR_BetSoundU3Ec__Iterator0_t1415787259_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CCR_CalchipsU3Ec__Iterator1_t2144217578_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CC_OnLoseU3Ec__Iterator1_t1181911525_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CC_OnWinU3Ec__Iterator0_t1703244554_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern RuntimeClass* WinLosePanel_t3114026018_il2cpp_TypeInfo_var;
extern RuntimeClass* YccioNetworkManager_t3600504787_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5_FieldInfo_var;
extern String_t* _stringLiteral1035202610;
extern String_t* _stringLiteral1193874114;
extern String_t* _stringLiteral1275403523;
extern String_t* _stringLiteral1309440680;
extern String_t* _stringLiteral1340198547;
extern String_t* _stringLiteral1475082161;
extern String_t* _stringLiteral1476776628;
extern String_t* _stringLiteral1512030231;
extern String_t* _stringLiteral1605126663;
extern String_t* _stringLiteral1688728708;
extern String_t* _stringLiteral1708225543;
extern String_t* _stringLiteral176793290;
extern String_t* _stringLiteral1837502969;
extern String_t* _stringLiteral2005346028;
extern String_t* _stringLiteral2085003243;
extern String_t* _stringLiteral2154486352;
extern String_t* _stringLiteral227414809;
extern String_t* _stringLiteral2277785952;
extern String_t* _stringLiteral2328556060;
extern String_t* _stringLiteral2353668337;
extern String_t* _stringLiteral2372802512;
extern String_t* _stringLiteral2500080693;
extern String_t* _stringLiteral2663913931;
extern String_t* _stringLiteral2668676434;
extern String_t* _stringLiteral2688095987;
extern String_t* _stringLiteral2745515193;
extern String_t* _stringLiteral2752530441;
extern String_t* _stringLiteral2815969589;
extern String_t* _stringLiteral2903119551;
extern String_t* _stringLiteral2918654047;
extern String_t* _stringLiteral2950688325;
extern String_t* _stringLiteral2979092519;
extern String_t* _stringLiteral3086498198;
extern String_t* _stringLiteral3219175999;
extern String_t* _stringLiteral3348952898;
extern String_t* _stringLiteral3428758596;
extern String_t* _stringLiteral3450648448;
extern String_t* _stringLiteral3450648455;
extern String_t* _stringLiteral3452614528;
extern String_t* _stringLiteral3452614529;
extern String_t* _stringLiteral3551321050;
extern String_t* _stringLiteral3596229116;
extern String_t* _stringLiteral3675857332;
extern String_t* _stringLiteral3786186961;
extern String_t* _stringLiteral3815572937;
extern String_t* _stringLiteral3917410033;
extern String_t* _stringLiteral3921725357;
extern String_t* _stringLiteral3928422333;
extern String_t* _stringLiteral4016121227;
extern String_t* _stringLiteral4274446839;
extern String_t* _stringLiteral4292058329;
extern String_t* _stringLiteral481352081;
extern String_t* _stringLiteral531699260;
extern String_t* _stringLiteral62725266;
extern String_t* _stringLiteral823592379;
extern String_t* _stringLiteral824334121;
extern String_t* _stringLiteral950452602;
extern String_t* _stringLiteral993523893;
extern const RuntimeMethod* Array_Sort_TisInt32_t2950945753_m2060455863_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisPlayerData_t220878115_m3167838379_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisPunTurnManager_t1223962931_m3266571455_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisSceneTransition_t1138091307_m3994718925_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisYccioNetworkManager_t3600504787_m4071155424_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisStone_t255273793_m1217350261_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m2387223709_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m2278349286_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1372101825_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m3280774074_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m4132484595_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4232616038_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1782500462_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisSupportLogging_t3610999087_m3906415171_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisRoomList_t2314265074_m2995111646_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisStartButton_t1998348287_m2462033924_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisText_t1901882714_m2114913816_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisWinLoseParticle_t1640804235_m3979570490_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisYccioNetworkManager_t3600504787_m610539862_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m4128318975_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3215236302_RuntimeMethod_var;
extern const RuntimeMethod* U3CCR_BetSoundU3Ec__Iterator0_Reset_m1012664378_RuntimeMethod_var;
extern const RuntimeMethod* U3CCR_CalchipsU3Ec__Iterator1_Reset_m1362035897_RuntimeMethod_var;
extern const RuntimeMethod* U3CC_OnLoseU3Ec__Iterator1_Reset_m3434917950_RuntimeMethod_var;
extern const RuntimeMethod* U3CC_OnWinU3Ec__Iterator0_Reset_m2251615767_RuntimeMethod_var;
extern const RuntimeMethod* U3CCycleRemoteHandCoroutineU3Ec__Iterator1_Reset_m3326500309_RuntimeMethod_var;
extern const RuntimeMethod* U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_Reset_m633430967_RuntimeMethod_var;
extern const RuntimeMethod* YccioNetworkManager_OnPhotonEvent_m152539995_RuntimeMethod_var;
extern const uint32_t SceneTransition_Awake_m1543823431_MetadataUsageId;
extern const uint32_t SceneTransition_GoToGameScene_m3230274153_MetadataUsageId;
extern const uint32_t SceneTransition_GoToLobbyScene_m984678998_MetadataUsageId;
extern const uint32_t ScoreExtensions_AddScore_m3409814160_MetadataUsageId;
extern const uint32_t ScoreExtensions_GetScore_m1181906475_MetadataUsageId;
extern const uint32_t ScoreExtensions_SetScore_m650688815_MetadataUsageId;
extern const uint32_t ServerSettings_IsAppId_m2900859773_MetadataUsageId;
extern const uint32_t ServerSettings_ResetBestRegionCodeInPreferences_m2677648169_MetadataUsageId;
extern const uint32_t ServerSettings_ToString_m2874290298_MetadataUsageId;
extern const uint32_t ServerSettings_UseMyServer_m2379333996_MetadataUsageId;
extern const uint32_t ServerSettings__ctor_m1448665227_MetadataUsageId;
extern const uint32_t ServerSettings_get_BestRegionCodeInPreferences_m2341611427_MetadataUsageId;
extern const uint32_t ShowStatusWhenConnecting_GetConnectingDots_m4281251292_MetadataUsageId;
extern const uint32_t ShowStatusWhenConnecting_OnGUI_m2526820743_MetadataUsageId;
extern const uint32_t SmoothSyncMovement_Awake_m593888716_MetadataUsageId;
extern const uint32_t SmoothSyncMovement_OnPhotonSerializeView_m1517933553_MetadataUsageId;
extern const uint32_t SmoothSyncMovement_Update_m340845157_MetadataUsageId;
extern const uint32_t SmoothSyncMovement__ctor_m1559543711_MetadataUsageId;
extern const uint32_t SortByColor_Awake_m119132563_MetadataUsageId;
extern const uint32_t SortByColor_OnClickSortByColor_m3417539386_MetadataUsageId;
extern const uint32_t SortByColor_get_Instance_m2199486831_MetadataUsageId;
extern const uint32_t SortByColor_set_Instance_m125504054_MetadataUsageId;
extern const uint32_t SortByNumber_Awake_m3263631240_MetadataUsageId;
extern const uint32_t SortByNumber_OnClickSortByNum_m1575780689_MetadataUsageId;
extern const uint32_t SortByNumber_get_Instance_m183889917_MetadataUsageId;
extern const uint32_t SortByNumber_set_Instance_m357922602_MetadataUsageId;
extern const uint32_t SoundManager_Awake_m2554457172_MetadataUsageId;
extern const uint32_t SoundManager_get_Instance_m3963388714_MetadataUsageId;
extern const uint32_t SoundManager_set_Instance_m1537600706_MetadataUsageId;
extern const uint32_t SpriteContainer_Awake_m410995551_MetadataUsageId;
extern const uint32_t SpriteContainer_get_Instance_m3971019802_MetadataUsageId;
extern const uint32_t SpriteContainer_set_Instance_m610192580_MetadataUsageId;
extern const uint32_t StartButton_Start_m1217349344_MetadataUsageId;
extern const uint32_t Stone_InitStone_m1584423283_MetadataUsageId;
extern const uint32_t Stone_OnStoneClick_m4120694212_MetadataUsageId;
extern const uint32_t SubmitButton_Awake_m2577497336_MetadataUsageId;
extern const uint32_t SubmitButton_get_Instance_m3888875242_MetadataUsageId;
extern const uint32_t SubmitButton_set_Instance_m853857809_MetadataUsageId;
extern const uint32_t SupportLogger_Start_m4228716924_MetadataUsageId;
extern const uint32_t SupportLogging_LogBasics_m2059416039_MetadataUsageId;
extern const uint32_t SupportLogging_LogStats_m357709987_MetadataUsageId;
extern const uint32_t SupportLogging_OnApplicationPause_m1998044826_MetadataUsageId;
extern const uint32_t SupportLogging_OnConnectedToPhoton_m4286413539_MetadataUsageId;
extern const uint32_t SupportLogging_OnCreatedRoom_m909910432_MetadataUsageId;
extern const uint32_t SupportLogging_OnDisconnectedFromPhoton_m3427121297_MetadataUsageId;
extern const uint32_t SupportLogging_OnFailedToConnectToPhoton_m1253602912_MetadataUsageId;
extern const uint32_t SupportLogging_OnJoinedLobby_m1882645112_MetadataUsageId;
extern const uint32_t SupportLogging_OnJoinedRoom_m1699194386_MetadataUsageId;
extern const uint32_t SupportLogging_OnLeftRoom_m1053673117_MetadataUsageId;
extern const uint32_t SupportLogging_Start_m615266166_MetadataUsageId;
extern const uint32_t TeamExtensions_GetTeam_m369910217_MetadataUsageId;
extern const uint32_t TeamExtensions_SetTeam_m2985934832_MetadataUsageId;
extern const uint32_t TurnExtensions_GetFinishedTurn_m96341076_MetadataUsageId;
extern const uint32_t TurnExtensions_GetTurnStart_m3702491223_MetadataUsageId;
extern const uint32_t TurnExtensions_GetTurn_m4131414938_MetadataUsageId;
extern const uint32_t TurnExtensions_SetFinishedTurn_m862379605_MetadataUsageId;
extern const uint32_t TurnExtensions_SetTurn_m2738922425_MetadataUsageId;
extern const uint32_t TurnExtensions__cctor_m1640371730_MetadataUsageId;
extern const uint32_t TypedLobbyInfo_ToString_m241642779_MetadataUsageId;
extern const uint32_t TypedLobbyInfo__ctor_m701921937_MetadataUsageId;
extern const uint32_t TypedLobby_ToString_m2822648706_MetadataUsageId;
extern const uint32_t TypedLobby__cctor_m1081382309_MetadataUsageId;
extern const uint32_t TypedLobby__ctor_m815421660_MetadataUsageId;
extern const uint32_t TypedLobby_get_IsDefault_m342755869_MetadataUsageId;
extern const uint32_t U3CCR_BetSoundU3Ec__Iterator0_MoveNext_m3891831477_MetadataUsageId;
extern const uint32_t U3CCR_BetSoundU3Ec__Iterator0_Reset_m1012664378_MetadataUsageId;
extern const uint32_t U3CCR_CalchipsU3Ec__Iterator1_MoveNext_m3873318284_MetadataUsageId;
extern const uint32_t U3CCR_CalchipsU3Ec__Iterator1_Reset_m1362035897_MetadataUsageId;
extern const uint32_t U3CC_OnLoseU3Ec__Iterator1_MoveNext_m1702681965_MetadataUsageId;
extern const uint32_t U3CC_OnLoseU3Ec__Iterator1_Reset_m3434917950_MetadataUsageId;
extern const uint32_t U3CC_OnWinU3Ec__Iterator0_MoveNext_m296220714_MetadataUsageId;
extern const uint32_t U3CC_OnWinU3Ec__Iterator0_Reset_m2251615767_MetadataUsageId;
extern const uint32_t U3CCycleRemoteHandCoroutineU3Ec__Iterator1_MoveNext_m4258493291_MetadataUsageId;
extern const uint32_t U3CCycleRemoteHandCoroutineU3Ec__Iterator1_Reset_m3326500309_MetadataUsageId;
extern const uint32_t U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_MoveNext_m2597396364_MetadataUsageId;
extern const uint32_t U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_Reset_m633430967_MetadataUsageId;
extern const uint32_t WebRpcResponse_ToStringFull_m3199251698_MetadataUsageId;
extern const uint32_t WebRpcResponse__ctor_m4000971832_MetadataUsageId;
extern const uint32_t WinLosePanel_Awake_m994853271_MetadataUsageId;
extern const uint32_t WinLosePanel_C_OnLose_m3097372575_MetadataUsageId;
extern const uint32_t WinLosePanel_C_OnWin_m1621631832_MetadataUsageId;
extern const uint32_t WinLosePanel_Update_m505985956_MetadataUsageId;
extern const uint32_t WinLosePanel_get_Instance_m3091037315_MetadataUsageId;
extern const uint32_t WinLosePanel_set_Instance_m3033113386_MetadataUsageId;
extern const uint32_t WinLoseParticle_Init_m4007159944_MetadataUsageId;
extern const uint32_t YccioNetworkManager_Awake_m4169508252_MetadataUsageId;
extern const uint32_t YccioNetworkManager_CR_BetSound_m2763905888_MetadataUsageId;
extern const uint32_t YccioNetworkManager_CR_Calchips_m80184455_MetadataUsageId;
extern const uint32_t YccioNetworkManager_DistributeStatus_m2345907618_MetadataUsageId;
extern const uint32_t YccioNetworkManager_EndGame_m4061492958_MetadataUsageId;
extern const uint32_t YccioNetworkManager_EnterLobby_m3924952965_MetadataUsageId;
extern const uint32_t YccioNetworkManager_GetGameList_m3683534293_MetadataUsageId;
extern const uint32_t YccioNetworkManager_JoinGame_m723493021_MetadataUsageId;
extern const uint32_t YccioNetworkManager_LeaveGame_m4146386290_MetadataUsageId;
extern const uint32_t YccioNetworkManager_MyTurn_m3314630788_MetadataUsageId;
extern const uint32_t YccioNetworkManager_NewGame_m858465561_MetadataUsageId;
extern const uint32_t YccioNetworkManager_NotMyTurn_m3026768657_MetadataUsageId;
extern const uint32_t YccioNetworkManager_OnDisable_m3549428848_MetadataUsageId;
extern const uint32_t YccioNetworkManager_OnEnable_m3274924364_MetadataUsageId;
extern const uint32_t YccioNetworkManager_OnJoinedRoom_m1409881903_MetadataUsageId;
extern const uint32_t YccioNetworkManager_OnPhotonCreateRoomFailed_m1280733107_MetadataUsageId;
extern const uint32_t YccioNetworkManager_OnPhotonEvent_m152539995_MetadataUsageId;
extern const uint32_t YccioNetworkManager_OnPhotonPlayerConnected_m2687873397_MetadataUsageId;
extern const uint32_t YccioNetworkManager_OnPhotonPlayerDisconnected_m1767781834_MetadataUsageId;
extern const uint32_t YccioNetworkManager_OnPlayerFinished_m2557697077_MetadataUsageId;
extern const uint32_t YccioNetworkManager_OnTurnTimeEnds_m369754152_MetadataUsageId;
extern const uint32_t YccioNetworkManager_RequestDistribute_m2698563940_MetadataUsageId;
extern const uint32_t YccioNetworkManager_SendImoji_m1285368662_MetadataUsageId;
extern const uint32_t YccioNetworkManager_SendRemainStones_m1163236977_MetadataUsageId;
extern const uint32_t YccioNetworkManager_SetFirstOrder_m516433491_MetadataUsageId;
extern const uint32_t YccioNetworkManager_SetPlayOrder_m745047584_MetadataUsageId;
extern const uint32_t YccioNetworkManager_SetStartButton_m3526135598_MetadataUsageId;
extern const uint32_t YccioNetworkManager_ShuffleSetting_m970800901_MetadataUsageId;
extern const uint32_t YccioNetworkManager_Shuffle_m1267737554_MetadataUsageId;
extern const uint32_t YccioNetworkManager_StartGame_m3707574726_MetadataUsageId;
extern const uint32_t YccioNetworkManager_UpdatePlayersInfo_m337863159_MetadataUsageId;
extern const uint32_t YccioNetworkManager_UpdateRemainStone_m4039245266_MetadataUsageId;
extern const uint32_t YccioNetworkManager_UpdateRoom_m2402611726_MetadataUsageId;
extern const uint32_t YccioNetworkManager_UpdateStatus_m2094236186_MetadataUsageId;
extern const uint32_t YccioNetworkManager__ctor_m1879442940_MetadataUsageId;
extern const uint32_t YccioNetworkManager_get_Instance_m1769066715_MetadataUsageId;
extern const uint32_t YccioNetworkManager_set_Instance_m1649774145_MetadataUsageId;
struct GUIStyleState_t1397964415_marshaled_com;
struct GUIStyleState_t1397964415_marshaled_pinvoke;
struct RectOffset_t1369453676_marshaled_com;

struct PhotonPlayerU5BU5D_t2880637464;
struct RoomInfoU5BU5D_t1491207981;
struct StoneU5BU5D_t1191912092;
struct Int32U5BU5D_t385246372;
struct ObjectU5BU5D_t2843939325;
struct AudioClipU5BU5D_t143221404;
struct GUILayoutOptionU5BU5D_t2510215842;
struct GUIStyleU5BU5D_t2383250302;
struct SpriteU5BU5D_t2581906349;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OPERATIONRESPONSE_T423627973_H
#define OPERATIONRESPONSE_T423627973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.OperationResponse
struct  OperationResponse_t423627973  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.OperationResponse::OperationCode
	uint8_t ___OperationCode_0;
	// System.Int16 ExitGames.Client.Photon.OperationResponse::ReturnCode
	int16_t ___ReturnCode_1;
	// System.String ExitGames.Client.Photon.OperationResponse::DebugMessage
	String_t* ___DebugMessage_2;
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.OperationResponse::Parameters
	Dictionary_2_t1405253484 * ___Parameters_3;

public:
	inline static int32_t get_offset_of_OperationCode_0() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___OperationCode_0)); }
	inline uint8_t get_OperationCode_0() const { return ___OperationCode_0; }
	inline uint8_t* get_address_of_OperationCode_0() { return &___OperationCode_0; }
	inline void set_OperationCode_0(uint8_t value)
	{
		___OperationCode_0 = value;
	}

	inline static int32_t get_offset_of_ReturnCode_1() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___ReturnCode_1)); }
	inline int16_t get_ReturnCode_1() const { return ___ReturnCode_1; }
	inline int16_t* get_address_of_ReturnCode_1() { return &___ReturnCode_1; }
	inline void set_ReturnCode_1(int16_t value)
	{
		___ReturnCode_1 = value;
	}

	inline static int32_t get_offset_of_DebugMessage_2() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___DebugMessage_2)); }
	inline String_t* get_DebugMessage_2() const { return ___DebugMessage_2; }
	inline String_t** get_address_of_DebugMessage_2() { return &___DebugMessage_2; }
	inline void set_DebugMessage_2(String_t* value)
	{
		___DebugMessage_2 = value;
		Il2CppCodeGenWriteBarrier((&___DebugMessage_2), value);
	}

	inline static int32_t get_offset_of_Parameters_3() { return static_cast<int32_t>(offsetof(OperationResponse_t423627973, ___Parameters_3)); }
	inline Dictionary_2_t1405253484 * get_Parameters_3() const { return ___Parameters_3; }
	inline Dictionary_2_t1405253484 ** get_address_of_Parameters_3() { return &___Parameters_3; }
	inline void set_Parameters_3(Dictionary_2_t1405253484 * value)
	{
		___Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATIONRESPONSE_T423627973_H
#ifndef PHOTONPLAYER_T3305149557_H
#define PHOTONPLAYER_T3305149557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonPlayer
struct  PhotonPlayer_t3305149557  : public RuntimeObject
{
public:
	// System.Int32 PhotonPlayer::actorID
	int32_t ___actorID_0;
	// System.String PhotonPlayer::nameField
	String_t* ___nameField_1;
	// System.String PhotonPlayer::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_2;
	// System.Boolean PhotonPlayer::IsLocal
	bool ___IsLocal_3;
	// System.Boolean PhotonPlayer::<IsInactive>k__BackingField
	bool ___U3CIsInactiveU3Ek__BackingField_4;
	// ExitGames.Client.Photon.Hashtable PhotonPlayer::<CustomProperties>k__BackingField
	Hashtable_t1048209202 * ___U3CCustomPropertiesU3Ek__BackingField_5;
	// System.Object PhotonPlayer::TagObject
	RuntimeObject * ___TagObject_6;

public:
	inline static int32_t get_offset_of_actorID_0() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___actorID_0)); }
	inline int32_t get_actorID_0() const { return ___actorID_0; }
	inline int32_t* get_address_of_actorID_0() { return &___actorID_0; }
	inline void set_actorID_0(int32_t value)
	{
		___actorID_0 = value;
	}

	inline static int32_t get_offset_of_nameField_1() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___nameField_1)); }
	inline String_t* get_nameField_1() const { return ___nameField_1; }
	inline String_t** get_address_of_nameField_1() { return &___nameField_1; }
	inline void set_nameField_1(String_t* value)
	{
		___nameField_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameField_1), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___U3CUserIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_2() const { return ___U3CUserIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_2() { return &___U3CUserIdU3Ek__BackingField_2; }
	inline void set_U3CUserIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_IsLocal_3() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___IsLocal_3)); }
	inline bool get_IsLocal_3() const { return ___IsLocal_3; }
	inline bool* get_address_of_IsLocal_3() { return &___IsLocal_3; }
	inline void set_IsLocal_3(bool value)
	{
		___IsLocal_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsInactiveU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___U3CIsInactiveU3Ek__BackingField_4)); }
	inline bool get_U3CIsInactiveU3Ek__BackingField_4() const { return ___U3CIsInactiveU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsInactiveU3Ek__BackingField_4() { return &___U3CIsInactiveU3Ek__BackingField_4; }
	inline void set_U3CIsInactiveU3Ek__BackingField_4(bool value)
	{
		___U3CIsInactiveU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCustomPropertiesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___U3CCustomPropertiesU3Ek__BackingField_5)); }
	inline Hashtable_t1048209202 * get_U3CCustomPropertiesU3Ek__BackingField_5() const { return ___U3CCustomPropertiesU3Ek__BackingField_5; }
	inline Hashtable_t1048209202 ** get_address_of_U3CCustomPropertiesU3Ek__BackingField_5() { return &___U3CCustomPropertiesU3Ek__BackingField_5; }
	inline void set_U3CCustomPropertiesU3Ek__BackingField_5(Hashtable_t1048209202 * value)
	{
		___U3CCustomPropertiesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomPropertiesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_TagObject_6() { return static_cast<int32_t>(offsetof(PhotonPlayer_t3305149557, ___TagObject_6)); }
	inline RuntimeObject * get_TagObject_6() const { return ___TagObject_6; }
	inline RuntimeObject ** get_address_of_TagObject_6() { return &___TagObject_6; }
	inline void set_TagObject_6(RuntimeObject * value)
	{
		___TagObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___TagObject_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPLAYER_T3305149557_H
#ifndef PHOTONSTREAM_T1003850889_H
#define PHOTONSTREAM_T1003850889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonStream
struct  PhotonStream_t1003850889  : public RuntimeObject
{
public:
	// System.Boolean PhotonStream::write
	bool ___write_0;
	// System.Collections.Generic.Queue`1<System.Object> PhotonStream::writeData
	Queue_1_t2926365658 * ___writeData_1;
	// System.Object[] PhotonStream::readData
	ObjectU5BU5D_t2843939325* ___readData_2;
	// System.Byte PhotonStream::currentItem
	uint8_t ___currentItem_3;

public:
	inline static int32_t get_offset_of_write_0() { return static_cast<int32_t>(offsetof(PhotonStream_t1003850889, ___write_0)); }
	inline bool get_write_0() const { return ___write_0; }
	inline bool* get_address_of_write_0() { return &___write_0; }
	inline void set_write_0(bool value)
	{
		___write_0 = value;
	}

	inline static int32_t get_offset_of_writeData_1() { return static_cast<int32_t>(offsetof(PhotonStream_t1003850889, ___writeData_1)); }
	inline Queue_1_t2926365658 * get_writeData_1() const { return ___writeData_1; }
	inline Queue_1_t2926365658 ** get_address_of_writeData_1() { return &___writeData_1; }
	inline void set_writeData_1(Queue_1_t2926365658 * value)
	{
		___writeData_1 = value;
		Il2CppCodeGenWriteBarrier((&___writeData_1), value);
	}

	inline static int32_t get_offset_of_readData_2() { return static_cast<int32_t>(offsetof(PhotonStream_t1003850889, ___readData_2)); }
	inline ObjectU5BU5D_t2843939325* get_readData_2() const { return ___readData_2; }
	inline ObjectU5BU5D_t2843939325** get_address_of_readData_2() { return &___readData_2; }
	inline void set_readData_2(ObjectU5BU5D_t2843939325* value)
	{
		___readData_2 = value;
		Il2CppCodeGenWriteBarrier((&___readData_2), value);
	}

	inline static int32_t get_offset_of_currentItem_3() { return static_cast<int32_t>(offsetof(PhotonStream_t1003850889, ___currentItem_3)); }
	inline uint8_t get_currentItem_3() const { return ___currentItem_3; }
	inline uint8_t* get_address_of_currentItem_3() { return &___currentItem_3; }
	inline void set_currentItem_3(uint8_t value)
	{
		___currentItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONSTREAM_T1003850889_H
#ifndef ROOMINFO_T3170295620_H
#define ROOMINFO_T3170295620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomInfo
struct  RoomInfo_t3170295620  : public RuntimeObject
{
public:
	// System.Boolean RoomInfo::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_0;
	// System.Boolean RoomInfo::<removedFromList>k__BackingField
	bool ___U3CremovedFromListU3Ek__BackingField_1;
	// ExitGames.Client.Photon.Hashtable RoomInfo::customPropertiesField
	Hashtable_t1048209202 * ___customPropertiesField_2;
	// System.Byte RoomInfo::maxPlayersField
	uint8_t ___maxPlayersField_3;
	// System.Int32 RoomInfo::emptyRoomTtlField
	int32_t ___emptyRoomTtlField_4;
	// System.Int32 RoomInfo::playerTtlField
	int32_t ___playerTtlField_5;
	// System.String[] RoomInfo::expectedUsersField
	StringU5BU5D_t1281789340* ___expectedUsersField_6;
	// System.Boolean RoomInfo::openField
	bool ___openField_7;
	// System.Boolean RoomInfo::visibleField
	bool ___visibleField_8;
	// System.Boolean RoomInfo::autoCleanUpField
	bool ___autoCleanUpField_9;
	// System.String RoomInfo::nameField
	String_t* ___nameField_10;
	// System.Int32 RoomInfo::masterClientIdField
	int32_t ___masterClientIdField_11;
	// System.Boolean RoomInfo::<serverSideMasterClient>k__BackingField
	bool ___U3CserverSideMasterClientU3Ek__BackingField_12;
	// System.Int32 RoomInfo::<PlayerCount>k__BackingField
	int32_t ___U3CPlayerCountU3Ek__BackingField_13;
	// System.Boolean RoomInfo::<IsLocalClientInside>k__BackingField
	bool ___U3CIsLocalClientInsideU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___U3CIsPlayingU3Ek__BackingField_0)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_0() const { return ___U3CIsPlayingU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_0() { return &___U3CIsPlayingU3Ek__BackingField_0; }
	inline void set_U3CIsPlayingU3Ek__BackingField_0(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CremovedFromListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___U3CremovedFromListU3Ek__BackingField_1)); }
	inline bool get_U3CremovedFromListU3Ek__BackingField_1() const { return ___U3CremovedFromListU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CremovedFromListU3Ek__BackingField_1() { return &___U3CremovedFromListU3Ek__BackingField_1; }
	inline void set_U3CremovedFromListU3Ek__BackingField_1(bool value)
	{
		___U3CremovedFromListU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_customPropertiesField_2() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___customPropertiesField_2)); }
	inline Hashtable_t1048209202 * get_customPropertiesField_2() const { return ___customPropertiesField_2; }
	inline Hashtable_t1048209202 ** get_address_of_customPropertiesField_2() { return &___customPropertiesField_2; }
	inline void set_customPropertiesField_2(Hashtable_t1048209202 * value)
	{
		___customPropertiesField_2 = value;
		Il2CppCodeGenWriteBarrier((&___customPropertiesField_2), value);
	}

	inline static int32_t get_offset_of_maxPlayersField_3() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___maxPlayersField_3)); }
	inline uint8_t get_maxPlayersField_3() const { return ___maxPlayersField_3; }
	inline uint8_t* get_address_of_maxPlayersField_3() { return &___maxPlayersField_3; }
	inline void set_maxPlayersField_3(uint8_t value)
	{
		___maxPlayersField_3 = value;
	}

	inline static int32_t get_offset_of_emptyRoomTtlField_4() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___emptyRoomTtlField_4)); }
	inline int32_t get_emptyRoomTtlField_4() const { return ___emptyRoomTtlField_4; }
	inline int32_t* get_address_of_emptyRoomTtlField_4() { return &___emptyRoomTtlField_4; }
	inline void set_emptyRoomTtlField_4(int32_t value)
	{
		___emptyRoomTtlField_4 = value;
	}

	inline static int32_t get_offset_of_playerTtlField_5() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___playerTtlField_5)); }
	inline int32_t get_playerTtlField_5() const { return ___playerTtlField_5; }
	inline int32_t* get_address_of_playerTtlField_5() { return &___playerTtlField_5; }
	inline void set_playerTtlField_5(int32_t value)
	{
		___playerTtlField_5 = value;
	}

	inline static int32_t get_offset_of_expectedUsersField_6() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___expectedUsersField_6)); }
	inline StringU5BU5D_t1281789340* get_expectedUsersField_6() const { return ___expectedUsersField_6; }
	inline StringU5BU5D_t1281789340** get_address_of_expectedUsersField_6() { return &___expectedUsersField_6; }
	inline void set_expectedUsersField_6(StringU5BU5D_t1281789340* value)
	{
		___expectedUsersField_6 = value;
		Il2CppCodeGenWriteBarrier((&___expectedUsersField_6), value);
	}

	inline static int32_t get_offset_of_openField_7() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___openField_7)); }
	inline bool get_openField_7() const { return ___openField_7; }
	inline bool* get_address_of_openField_7() { return &___openField_7; }
	inline void set_openField_7(bool value)
	{
		___openField_7 = value;
	}

	inline static int32_t get_offset_of_visibleField_8() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___visibleField_8)); }
	inline bool get_visibleField_8() const { return ___visibleField_8; }
	inline bool* get_address_of_visibleField_8() { return &___visibleField_8; }
	inline void set_visibleField_8(bool value)
	{
		___visibleField_8 = value;
	}

	inline static int32_t get_offset_of_autoCleanUpField_9() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___autoCleanUpField_9)); }
	inline bool get_autoCleanUpField_9() const { return ___autoCleanUpField_9; }
	inline bool* get_address_of_autoCleanUpField_9() { return &___autoCleanUpField_9; }
	inline void set_autoCleanUpField_9(bool value)
	{
		___autoCleanUpField_9 = value;
	}

	inline static int32_t get_offset_of_nameField_10() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___nameField_10)); }
	inline String_t* get_nameField_10() const { return ___nameField_10; }
	inline String_t** get_address_of_nameField_10() { return &___nameField_10; }
	inline void set_nameField_10(String_t* value)
	{
		___nameField_10 = value;
		Il2CppCodeGenWriteBarrier((&___nameField_10), value);
	}

	inline static int32_t get_offset_of_masterClientIdField_11() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___masterClientIdField_11)); }
	inline int32_t get_masterClientIdField_11() const { return ___masterClientIdField_11; }
	inline int32_t* get_address_of_masterClientIdField_11() { return &___masterClientIdField_11; }
	inline void set_masterClientIdField_11(int32_t value)
	{
		___masterClientIdField_11 = value;
	}

	inline static int32_t get_offset_of_U3CserverSideMasterClientU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___U3CserverSideMasterClientU3Ek__BackingField_12)); }
	inline bool get_U3CserverSideMasterClientU3Ek__BackingField_12() const { return ___U3CserverSideMasterClientU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CserverSideMasterClientU3Ek__BackingField_12() { return &___U3CserverSideMasterClientU3Ek__BackingField_12; }
	inline void set_U3CserverSideMasterClientU3Ek__BackingField_12(bool value)
	{
		___U3CserverSideMasterClientU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CPlayerCountU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___U3CPlayerCountU3Ek__BackingField_13)); }
	inline int32_t get_U3CPlayerCountU3Ek__BackingField_13() const { return ___U3CPlayerCountU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CPlayerCountU3Ek__BackingField_13() { return &___U3CPlayerCountU3Ek__BackingField_13; }
	inline void set_U3CPlayerCountU3Ek__BackingField_13(int32_t value)
	{
		___U3CPlayerCountU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CIsLocalClientInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(RoomInfo_t3170295620, ___U3CIsLocalClientInsideU3Ek__BackingField_14)); }
	inline bool get_U3CIsLocalClientInsideU3Ek__BackingField_14() const { return ___U3CIsLocalClientInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsLocalClientInsideU3Ek__BackingField_14() { return &___U3CIsLocalClientInsideU3Ek__BackingField_14; }
	inline void set_U3CIsLocalClientInsideU3Ek__BackingField_14(bool value)
	{
		___U3CIsLocalClientInsideU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMINFO_T3170295620_H
#ifndef ROOMOPTIONS_T1787645948_H
#define ROOMOPTIONS_T1787645948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomOptions
struct  RoomOptions_t1787645948  : public RuntimeObject
{
public:
	// System.Boolean RoomOptions::isVisibleField
	bool ___isVisibleField_0;
	// System.Boolean RoomOptions::isOpenField
	bool ___isOpenField_1;
	// System.Byte RoomOptions::MaxPlayers
	uint8_t ___MaxPlayers_2;
	// System.Int32 RoomOptions::PlayerTtl
	int32_t ___PlayerTtl_3;
	// System.Int32 RoomOptions::EmptyRoomTtl
	int32_t ___EmptyRoomTtl_4;
	// System.Boolean RoomOptions::cleanupCacheOnLeaveField
	bool ___cleanupCacheOnLeaveField_5;
	// ExitGames.Client.Photon.Hashtable RoomOptions::CustomRoomProperties
	Hashtable_t1048209202 * ___CustomRoomProperties_6;
	// System.String[] RoomOptions::CustomRoomPropertiesForLobby
	StringU5BU5D_t1281789340* ___CustomRoomPropertiesForLobby_7;
	// System.String[] RoomOptions::Plugins
	StringU5BU5D_t1281789340* ___Plugins_8;
	// System.Boolean RoomOptions::suppressRoomEventsField
	bool ___suppressRoomEventsField_9;
	// System.Boolean RoomOptions::publishUserIdField
	bool ___publishUserIdField_10;
	// System.Boolean RoomOptions::deleteNullPropertiesField
	bool ___deleteNullPropertiesField_11;

public:
	inline static int32_t get_offset_of_isVisibleField_0() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___isVisibleField_0)); }
	inline bool get_isVisibleField_0() const { return ___isVisibleField_0; }
	inline bool* get_address_of_isVisibleField_0() { return &___isVisibleField_0; }
	inline void set_isVisibleField_0(bool value)
	{
		___isVisibleField_0 = value;
	}

	inline static int32_t get_offset_of_isOpenField_1() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___isOpenField_1)); }
	inline bool get_isOpenField_1() const { return ___isOpenField_1; }
	inline bool* get_address_of_isOpenField_1() { return &___isOpenField_1; }
	inline void set_isOpenField_1(bool value)
	{
		___isOpenField_1 = value;
	}

	inline static int32_t get_offset_of_MaxPlayers_2() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___MaxPlayers_2)); }
	inline uint8_t get_MaxPlayers_2() const { return ___MaxPlayers_2; }
	inline uint8_t* get_address_of_MaxPlayers_2() { return &___MaxPlayers_2; }
	inline void set_MaxPlayers_2(uint8_t value)
	{
		___MaxPlayers_2 = value;
	}

	inline static int32_t get_offset_of_PlayerTtl_3() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___PlayerTtl_3)); }
	inline int32_t get_PlayerTtl_3() const { return ___PlayerTtl_3; }
	inline int32_t* get_address_of_PlayerTtl_3() { return &___PlayerTtl_3; }
	inline void set_PlayerTtl_3(int32_t value)
	{
		___PlayerTtl_3 = value;
	}

	inline static int32_t get_offset_of_EmptyRoomTtl_4() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___EmptyRoomTtl_4)); }
	inline int32_t get_EmptyRoomTtl_4() const { return ___EmptyRoomTtl_4; }
	inline int32_t* get_address_of_EmptyRoomTtl_4() { return &___EmptyRoomTtl_4; }
	inline void set_EmptyRoomTtl_4(int32_t value)
	{
		___EmptyRoomTtl_4 = value;
	}

	inline static int32_t get_offset_of_cleanupCacheOnLeaveField_5() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___cleanupCacheOnLeaveField_5)); }
	inline bool get_cleanupCacheOnLeaveField_5() const { return ___cleanupCacheOnLeaveField_5; }
	inline bool* get_address_of_cleanupCacheOnLeaveField_5() { return &___cleanupCacheOnLeaveField_5; }
	inline void set_cleanupCacheOnLeaveField_5(bool value)
	{
		___cleanupCacheOnLeaveField_5 = value;
	}

	inline static int32_t get_offset_of_CustomRoomProperties_6() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___CustomRoomProperties_6)); }
	inline Hashtable_t1048209202 * get_CustomRoomProperties_6() const { return ___CustomRoomProperties_6; }
	inline Hashtable_t1048209202 ** get_address_of_CustomRoomProperties_6() { return &___CustomRoomProperties_6; }
	inline void set_CustomRoomProperties_6(Hashtable_t1048209202 * value)
	{
		___CustomRoomProperties_6 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRoomProperties_6), value);
	}

	inline static int32_t get_offset_of_CustomRoomPropertiesForLobby_7() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___CustomRoomPropertiesForLobby_7)); }
	inline StringU5BU5D_t1281789340* get_CustomRoomPropertiesForLobby_7() const { return ___CustomRoomPropertiesForLobby_7; }
	inline StringU5BU5D_t1281789340** get_address_of_CustomRoomPropertiesForLobby_7() { return &___CustomRoomPropertiesForLobby_7; }
	inline void set_CustomRoomPropertiesForLobby_7(StringU5BU5D_t1281789340* value)
	{
		___CustomRoomPropertiesForLobby_7 = value;
		Il2CppCodeGenWriteBarrier((&___CustomRoomPropertiesForLobby_7), value);
	}

	inline static int32_t get_offset_of_Plugins_8() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___Plugins_8)); }
	inline StringU5BU5D_t1281789340* get_Plugins_8() const { return ___Plugins_8; }
	inline StringU5BU5D_t1281789340** get_address_of_Plugins_8() { return &___Plugins_8; }
	inline void set_Plugins_8(StringU5BU5D_t1281789340* value)
	{
		___Plugins_8 = value;
		Il2CppCodeGenWriteBarrier((&___Plugins_8), value);
	}

	inline static int32_t get_offset_of_suppressRoomEventsField_9() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___suppressRoomEventsField_9)); }
	inline bool get_suppressRoomEventsField_9() const { return ___suppressRoomEventsField_9; }
	inline bool* get_address_of_suppressRoomEventsField_9() { return &___suppressRoomEventsField_9; }
	inline void set_suppressRoomEventsField_9(bool value)
	{
		___suppressRoomEventsField_9 = value;
	}

	inline static int32_t get_offset_of_publishUserIdField_10() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___publishUserIdField_10)); }
	inline bool get_publishUserIdField_10() const { return ___publishUserIdField_10; }
	inline bool* get_address_of_publishUserIdField_10() { return &___publishUserIdField_10; }
	inline void set_publishUserIdField_10(bool value)
	{
		___publishUserIdField_10 = value;
	}

	inline static int32_t get_offset_of_deleteNullPropertiesField_11() { return static_cast<int32_t>(offsetof(RoomOptions_t1787645948, ___deleteNullPropertiesField_11)); }
	inline bool get_deleteNullPropertiesField_11() const { return ___deleteNullPropertiesField_11; }
	inline bool* get_address_of_deleteNullPropertiesField_11() { return &___deleteNullPropertiesField_11; }
	inline void set_deleteNullPropertiesField_11(bool value)
	{
		___deleteNullPropertiesField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMOPTIONS_T1787645948_H
#ifndef U3CCYCLEREMOTEHANDCOROUTINEU3EC__ITERATOR1_T811054456_H
#define U3CCYCLEREMOTEHANDCOROUTINEU3EC__ITERATOR1_T811054456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore/<CycleRemoteHandCoroutine>c__Iterator1
struct  U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456  : public RuntimeObject
{
public:
	// RpsCore RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::$this
	RpsCore_t2154144697 * ___U24this_0;
	// System.Object RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456, ___U24this_0)); }
	inline RpsCore_t2154144697 * get_U24this_0() const { return ___U24this_0; }
	inline RpsCore_t2154144697 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RpsCore_t2154144697 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCYCLEREMOTEHANDCOROUTINEU3EC__ITERATOR1_T811054456_H
#ifndef U3CSHOWRESULTSBEGINNEXTTURNCOROUTINEU3EC__ITERATOR0_T2002201197_H
#define U3CSHOWRESULTSBEGINNEXTTURNCOROUTINEU3EC__ITERATOR0_T2002201197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0
struct  U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197  : public RuntimeObject
{
public:
	// RpsCore RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::$this
	RpsCore_t2154144697 * ___U24this_0;
	// System.Object RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197, ___U24this_0)); }
	inline RpsCore_t2154144697 * get_U24this_0() const { return ___U24this_0; }
	inline RpsCore_t2154144697 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RpsCore_t2154144697 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWRESULTSBEGINNEXTTURNCOROUTINEU3EC__ITERATOR0_T2002201197_H
#ifndef SCENEMANAGERHELPER_T3665721098_H
#define SCENEMANAGERHELPER_T3665721098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneManagerHelper
struct  SceneManagerHelper_t3665721098  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMANAGERHELPER_T3665721098_H
#ifndef SCOREEXTENSIONS_T185655756_H
#define SCOREEXTENSIONS_T185655756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreExtensions
struct  ScoreExtensions_t185655756  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOREEXTENSIONS_T185655756_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_T1405253484_H
#define DICTIONARY_2_T1405253484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Byte,System.Object>
struct  Dictionary_2_t1405253484  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	ByteU5BU5D_t4116647657* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___keySlots_6)); }
	inline ByteU5BU5D_t4116647657* get_keySlots_6() const { return ___keySlots_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(ByteU5BU5D_t4116647657* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1405253484_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3835237792 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1405253484_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3835237792 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3835237792 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3835237792 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1405253484_H
#ifndef DICTIONARY_2_T132545152_H
#define DICTIONARY_2_T132545152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct  Dictionary_2_t132545152  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	ObjectU5BU5D_t2843939325* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___keySlots_6)); }
	inline ObjectU5BU5D_t2843939325* get_keySlots_6() const { return ___keySlots_6; }
	inline ObjectU5BU5D_t2843939325** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(ObjectU5BU5D_t2843939325* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t132545152_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4209139644 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4209139644 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4209139644 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4209139644 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T132545152_H
#ifndef DICTIONARY_2_T2865362463_H
#define DICTIONARY_2_T2865362463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t2865362463  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2865362463_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1694351041 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1694351041 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1694351041 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1694351041 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2865362463_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1281789340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1281789340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1281789340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef LIST_1_T3395709193_H
#define LIST_1_T3395709193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Component>
struct  List_1_t3395709193  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ComponentU5BU5D_t3294940482* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3395709193, ____items_1)); }
	inline ComponentU5BU5D_t3294940482* get__items_1() const { return ____items_1; }
	inline ComponentU5BU5D_t3294940482** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ComponentU5BU5D_t3294940482* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3395709193, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3395709193, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3395709193_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ComponentU5BU5D_t3294940482* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3395709193_StaticFields, ___EmptyArray_4)); }
	inline ComponentU5BU5D_t3294940482* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ComponentU5BU5D_t3294940482** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ComponentU5BU5D_t3294940482* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3395709193_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STRINGBUILDER_T_H
#define STRINGBUILDER_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TEAMEXTENSIONS_T2346265574_H
#define TEAMEXTENSIONS_T2346265574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeamExtensions
struct  TeamExtensions_t2346265574  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMEXTENSIONS_T2346265574_H
#ifndef TURNEXTENSIONS_T3150044944_H
#define TURNEXTENSIONS_T3150044944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnExtensions
struct  TurnExtensions_t3150044944  : public RuntimeObject
{
public:

public:
};

struct TurnExtensions_t3150044944_StaticFields
{
public:
	// System.String TurnExtensions::TurnPropKey
	String_t* ___TurnPropKey_0;
	// System.String TurnExtensions::TurnStartPropKey
	String_t* ___TurnStartPropKey_1;
	// System.String TurnExtensions::FinishedTurnPropKey
	String_t* ___FinishedTurnPropKey_2;

public:
	inline static int32_t get_offset_of_TurnPropKey_0() { return static_cast<int32_t>(offsetof(TurnExtensions_t3150044944_StaticFields, ___TurnPropKey_0)); }
	inline String_t* get_TurnPropKey_0() const { return ___TurnPropKey_0; }
	inline String_t** get_address_of_TurnPropKey_0() { return &___TurnPropKey_0; }
	inline void set_TurnPropKey_0(String_t* value)
	{
		___TurnPropKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___TurnPropKey_0), value);
	}

	inline static int32_t get_offset_of_TurnStartPropKey_1() { return static_cast<int32_t>(offsetof(TurnExtensions_t3150044944_StaticFields, ___TurnStartPropKey_1)); }
	inline String_t* get_TurnStartPropKey_1() const { return ___TurnStartPropKey_1; }
	inline String_t** get_address_of_TurnStartPropKey_1() { return &___TurnStartPropKey_1; }
	inline void set_TurnStartPropKey_1(String_t* value)
	{
		___TurnStartPropKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___TurnStartPropKey_1), value);
	}

	inline static int32_t get_offset_of_FinishedTurnPropKey_2() { return static_cast<int32_t>(offsetof(TurnExtensions_t3150044944_StaticFields, ___FinishedTurnPropKey_2)); }
	inline String_t* get_FinishedTurnPropKey_2() const { return ___FinishedTurnPropKey_2; }
	inline String_t** get_address_of_FinishedTurnPropKey_2() { return &___FinishedTurnPropKey_2; }
	inline void set_FinishedTurnPropKey_2(String_t* value)
	{
		___FinishedTurnPropKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___FinishedTurnPropKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNEXTENSIONS_T3150044944_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef WEBRPCRESPONSE_T4177102182_H
#define WEBRPCRESPONSE_T4177102182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebRpcResponse
struct  WebRpcResponse_t4177102182  : public RuntimeObject
{
public:
	// System.String WebRpcResponse::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 WebRpcResponse::<ReturnCode>k__BackingField
	int32_t ___U3CReturnCodeU3Ek__BackingField_1;
	// System.String WebRpcResponse::<DebugMessage>k__BackingField
	String_t* ___U3CDebugMessageU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> WebRpcResponse::<Parameters>k__BackingField
	Dictionary_2_t2865362463 * ___U3CParametersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebRpcResponse_t4177102182, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CReturnCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebRpcResponse_t4177102182, ___U3CReturnCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CReturnCodeU3Ek__BackingField_1() const { return ___U3CReturnCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CReturnCodeU3Ek__BackingField_1() { return &___U3CReturnCodeU3Ek__BackingField_1; }
	inline void set_U3CReturnCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CReturnCodeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDebugMessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebRpcResponse_t4177102182, ___U3CDebugMessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CDebugMessageU3Ek__BackingField_2() const { return ___U3CDebugMessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CDebugMessageU3Ek__BackingField_2() { return &___U3CDebugMessageU3Ek__BackingField_2; }
	inline void set_U3CDebugMessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CDebugMessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDebugMessageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebRpcResponse_t4177102182, ___U3CParametersU3Ek__BackingField_3)); }
	inline Dictionary_2_t2865362463 * get_U3CParametersU3Ek__BackingField_3() const { return ___U3CParametersU3Ek__BackingField_3; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CParametersU3Ek__BackingField_3() { return &___U3CParametersU3Ek__BackingField_3; }
	inline void set_U3CParametersU3Ek__BackingField_3(Dictionary_2_t2865362463 * value)
	{
		___U3CParametersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRPCRESPONSE_T4177102182_H
#ifndef U3CC_ONLOSEU3EC__ITERATOR1_T1181911525_H
#define U3CC_ONLOSEU3EC__ITERATOR1_T1181911525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinLosePanel/<C_OnLose>c__Iterator1
struct  U3CC_OnLoseU3Ec__Iterator1_t1181911525  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text WinLosePanel/<C_OnLose>c__Iterator1::<t>__0
	Text_t1901882714 * ___U3CtU3E__0_0;
	// System.Int32 WinLosePanel/<C_OnLose>c__Iterator1::<i>__1
	int32_t ___U3CiU3E__1_1;
	// WinLosePanel WinLosePanel/<C_OnLose>c__Iterator1::$this
	WinLosePanel_t3114026018 * ___U24this_2;
	// System.Object WinLosePanel/<C_OnLose>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WinLosePanel/<C_OnLose>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 WinLosePanel/<C_OnLose>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U3CtU3E__0_0)); }
	inline Text_t1901882714 * get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline Text_t1901882714 ** get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(Text_t1901882714 * value)
	{
		___U3CtU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U24this_2)); }
	inline WinLosePanel_t3114026018 * get_U24this_2() const { return ___U24this_2; }
	inline WinLosePanel_t3114026018 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WinLosePanel_t3114026018 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CC_OnLoseU3Ec__Iterator1_t1181911525, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CC_ONLOSEU3EC__ITERATOR1_T1181911525_H
#ifndef U3CC_ONWINU3EC__ITERATOR0_T1703244554_H
#define U3CC_ONWINU3EC__ITERATOR0_T1703244554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinLosePanel/<C_OnWin>c__Iterator0
struct  U3CC_OnWinU3Ec__Iterator0_t1703244554  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text WinLosePanel/<C_OnWin>c__Iterator0::<t>__0
	Text_t1901882714 * ___U3CtU3E__0_0;
	// System.Int32 WinLosePanel/<C_OnWin>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_1;
	// WinLosePanel WinLosePanel/<C_OnWin>c__Iterator0::$this
	WinLosePanel_t3114026018 * ___U24this_2;
	// System.Object WinLosePanel/<C_OnWin>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WinLosePanel/<C_OnWin>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 WinLosePanel/<C_OnWin>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U3CtU3E__0_0)); }
	inline Text_t1901882714 * get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline Text_t1901882714 ** get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(Text_t1901882714 * value)
	{
		___U3CtU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U24this_2)); }
	inline WinLosePanel_t3114026018 * get_U24this_2() const { return ___U24this_2; }
	inline WinLosePanel_t3114026018 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WinLosePanel_t3114026018 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CC_OnWinU3Ec__Iterator0_t1703244554, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CC_ONWINU3EC__ITERATOR0_T1703244554_H
#ifndef U3CCR_BETSOUNDU3EC__ITERATOR0_T1415787259_H
#define U3CCR_BETSOUNDU3EC__ITERATOR0_T1415787259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YccioNetworkManager/<CR_BetSound>c__Iterator0
struct  U3CCR_BetSoundU3Ec__Iterator0_t1415787259  : public RuntimeObject
{
public:
	// System.Int32 YccioNetworkManager/<CR_BetSound>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.Int32 YccioNetworkManager/<CR_BetSound>c__Iterator0::num
	int32_t ___num_1;
	// System.Object YccioNetworkManager/<CR_BetSound>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean YccioNetworkManager/<CR_BetSound>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 YccioNetworkManager/<CR_BetSound>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_num_1() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___num_1)); }
	inline int32_t get_num_1() const { return ___num_1; }
	inline int32_t* get_address_of_num_1() { return &___num_1; }
	inline void set_num_1(int32_t value)
	{
		___num_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCR_BetSoundU3Ec__Iterator0_t1415787259, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCR_BETSOUNDU3EC__ITERATOR0_T1415787259_H
#ifndef U3CCR_CALCHIPSU3EC__ITERATOR1_T2144217578_H
#define U3CCR_CALCHIPSU3EC__ITERATOR1_T2144217578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YccioNetworkManager/<CR_Calchips>c__Iterator1
struct  U3CCR_CalchipsU3Ec__Iterator1_t2144217578  : public RuntimeObject
{
public:
	// System.Int32 YccioNetworkManager/<CR_Calchips>c__Iterator1::<my>__0
	int32_t ___U3CmyU3E__0_0;
	// System.Int32 YccioNetworkManager/<CR_Calchips>c__Iterator1::<i>__1
	int32_t ___U3CiU3E__1_1;
	// YccioNetworkManager YccioNetworkManager/<CR_Calchips>c__Iterator1::$this
	YccioNetworkManager_t3600504787 * ___U24this_2;
	// System.Object YccioNetworkManager/<CR_Calchips>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean YccioNetworkManager/<CR_Calchips>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 YccioNetworkManager/<CR_Calchips>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CmyU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U3CmyU3E__0_0)); }
	inline int32_t get_U3CmyU3E__0_0() const { return ___U3CmyU3E__0_0; }
	inline int32_t* get_address_of_U3CmyU3E__0_0() { return &___U3CmyU3E__0_0; }
	inline void set_U3CmyU3E__0_0(int32_t value)
	{
		___U3CmyU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U24this_2)); }
	inline YccioNetworkManager_t3600504787 * get_U24this_2() const { return ___U24this_2; }
	inline YccioNetworkManager_t3600504787 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(YccioNetworkManager_t3600504787 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCR_CalchipsU3Ec__Iterator1_t2144217578, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCR_CALCHIPSU3EC__ITERATOR1_T2144217578_H
#ifndef U24ARRAYTYPEU3D16_T3253128244_H
#define U24ARRAYTYPEU3D16_T3253128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128244 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128244__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128244_H
#ifndef U24ARRAYTYPEU3D32_T3651253610_H
#define U24ARRAYTYPEU3D32_T3651253610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=32
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D32_t3651253610 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D32_t3651253610__padding[32];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D32_T3651253610_H
#ifndef U24ARRAYTYPEU3D48_T1336283963_H
#define U24ARRAYTYPEU3D48_T1336283963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=48
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D48_t1336283963 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D48_t1336283963__padding[48];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D48_T1336283963_H
#ifndef HASHTABLE_T1048209202_H
#define HASHTABLE_T1048209202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.Hashtable
struct  Hashtable_t1048209202  : public Dictionary_2_t132545152
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHTABLE_T1048209202_H
#ifndef PHOTONMESSAGEINFO_T3855471533_H
#define PHOTONMESSAGEINFO_T3855471533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonMessageInfo
struct  PhotonMessageInfo_t3855471533 
{
public:
	// System.Int32 PhotonMessageInfo::timeInt
	int32_t ___timeInt_0;
	// PhotonPlayer PhotonMessageInfo::sender
	PhotonPlayer_t3305149557 * ___sender_1;
	// PhotonView PhotonMessageInfo::photonView
	PhotonView_t2207721820 * ___photonView_2;

public:
	inline static int32_t get_offset_of_timeInt_0() { return static_cast<int32_t>(offsetof(PhotonMessageInfo_t3855471533, ___timeInt_0)); }
	inline int32_t get_timeInt_0() const { return ___timeInt_0; }
	inline int32_t* get_address_of_timeInt_0() { return &___timeInt_0; }
	inline void set_timeInt_0(int32_t value)
	{
		___timeInt_0 = value;
	}

	inline static int32_t get_offset_of_sender_1() { return static_cast<int32_t>(offsetof(PhotonMessageInfo_t3855471533, ___sender_1)); }
	inline PhotonPlayer_t3305149557 * get_sender_1() const { return ___sender_1; }
	inline PhotonPlayer_t3305149557 ** get_address_of_sender_1() { return &___sender_1; }
	inline void set_sender_1(PhotonPlayer_t3305149557 * value)
	{
		___sender_1 = value;
		Il2CppCodeGenWriteBarrier((&___sender_1), value);
	}

	inline static int32_t get_offset_of_photonView_2() { return static_cast<int32_t>(offsetof(PhotonMessageInfo_t3855471533, ___photonView_2)); }
	inline PhotonView_t2207721820 * get_photonView_2() const { return ___photonView_2; }
	inline PhotonView_t2207721820 ** get_address_of_photonView_2() { return &___photonView_2; }
	inline void set_photonView_2(PhotonView_t2207721820 * value)
	{
		___photonView_2 = value;
		Il2CppCodeGenWriteBarrier((&___photonView_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PhotonMessageInfo
struct PhotonMessageInfo_t3855471533_marshaled_pinvoke
{
	int32_t ___timeInt_0;
	PhotonPlayer_t3305149557 * ___sender_1;
	PhotonView_t2207721820 * ___photonView_2;
};
// Native definition for COM marshalling of PhotonMessageInfo
struct PhotonMessageInfo_t3855471533_marshaled_com
{
	int32_t ___timeInt_0;
	PhotonPlayer_t3305149557 * ___sender_1;
	PhotonView_t2207721820 * ___photonView_2;
};
#endif // PHOTONMESSAGEINFO_T3855471533_H
#ifndef ROOM_T3759828263_H
#define ROOM_T3759828263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Room
struct  Room_t3759828263  : public RoomInfo_t3170295620
{
public:
	// System.String[] Room::<PropertiesListedInLobby>k__BackingField
	StringU5BU5D_t1281789340* ___U3CPropertiesListedInLobbyU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CPropertiesListedInLobbyU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Room_t3759828263, ___U3CPropertiesListedInLobbyU3Ek__BackingField_15)); }
	inline StringU5BU5D_t1281789340* get_U3CPropertiesListedInLobbyU3Ek__BackingField_15() const { return ___U3CPropertiesListedInLobbyU3Ek__BackingField_15; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CPropertiesListedInLobbyU3Ek__BackingField_15() { return &___U3CPropertiesListedInLobbyU3Ek__BackingField_15; }
	inline void set_U3CPropertiesListedInLobbyU3Ek__BackingField_15(StringU5BU5D_t1281789340* value)
	{
		___U3CPropertiesListedInLobbyU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesListedInLobbyU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOM_T3759828263_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUMERATOR_T989985774_H
#define ENUMERATOR_T989985774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>
struct  Enumerator_t989985774 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3395709193 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Component_t1923634451 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t989985774, ___l_0)); }
	inline List_1_t3395709193 * get_l_0() const { return ___l_0; }
	inline List_1_t3395709193 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3395709193 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t989985774, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t989985774, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t989985774, ___current_3)); }
	inline Component_t1923634451 * get_current_3() const { return ___current_3; }
	inline Component_t1923634451 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Component_t1923634451 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T989985774_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-7A078F136EEAACFF264DA2E9994AF7D3D3536738
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-8658990BAD6546E619D8A5C4F90BCF3F089E0953
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-739C505E9F0985CE1E08892BC46BE5E839FF061A
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-35FDBB6669F521B572D4AD71DD77E77F43C1B71B
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-641FED42B144F98E4D3581E150975A5B388B5358
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-9E5175008751D08F361488C9927086B276B965FA
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5;

public:
	inline static int32_t get_offset_of_U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0() const { return ___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0() { return &___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0; }
	inline void set_U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2D7A078F136EEAACFF264DA2E9994AF7D3D3536738_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1() const { return ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1() { return &___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1; }
	inline void set_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2() const { return ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2() { return &___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2; }
	inline void set_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3() const { return ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3() { return &___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3; }
	inline void set_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4() const { return ___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4() { return &___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4; }
	inline void set_U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D641FED42B144F98E4D3581E150975A5B388B5358_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5() const { return ___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5() { return &___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5; }
	inline void set_U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef AUTHMODEOPTION_T1305270560_H
#define AUTHMODEOPTION_T1305270560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AuthModeOption
struct  AuthModeOption_t1305270560 
{
public:
	// System.Int32 AuthModeOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthModeOption_t1305270560, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHMODEOPTION_T1305270560_H
#ifndef CLIENTSTATE_T1348705391_H
#define CLIENTSTATE_T1348705391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClientState
struct  ClientState_t1348705391 
{
public:
	// System.Int32 ClientState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClientState_t1348705391, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSTATE_T1348705391_H
#ifndef CLOUDREGIONCODE_T1925019500_H
#define CLOUDREGIONCODE_T1925019500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRegionCode
struct  CloudRegionCode_t1925019500 
{
public:
	// System.Int32 CloudRegionCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudRegionCode_t1925019500, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDREGIONCODE_T1925019500_H
#ifndef CLOUDREGIONFLAG_T3756941471_H
#define CLOUDREGIONFLAG_T3756941471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudRegionFlag
struct  CloudRegionFlag_t3756941471 
{
public:
	// System.Int32 CloudRegionFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudRegionFlag_t3756941471, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDREGIONFLAG_T3756941471_H
#ifndef DISCONNECTCAUSE_T501870387_H
#define DISCONNECTCAUSE_T501870387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisconnectCause
struct  DisconnectCause_t501870387 
{
public:
	// System.Int32 DisconnectCause::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DisconnectCause_t501870387, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCONNECTCAUSE_T501870387_H
#ifndef ENCRYPTIONMODE_T4213192103_H
#define ENCRYPTIONMODE_T4213192103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EncryptionMode
struct  EncryptionMode_t4213192103 
{
public:
	// System.Int32 EncryptionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EncryptionMode_t4213192103, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTIONMODE_T4213192103_H
#ifndef EVENTCACHING_T168509732_H
#define EVENTCACHING_T168509732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventCaching
struct  EventCaching_t168509732 
{
public:
	// System.Byte EventCaching::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventCaching_t168509732, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTCACHING_T168509732_H
#ifndef CONNECTIONPROTOCOL_T2586603950_H
#define CONNECTIONPROTOCOL_T2586603950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.ConnectionProtocol
struct  ConnectionProtocol_t2586603950 
{
public:
	// System.Byte ExitGames.Client.Photon.ConnectionProtocol::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionProtocol_t2586603950, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONPROTOCOL_T2586603950_H
#ifndef DEBUGLEVEL_T3671880145_H
#define DEBUGLEVEL_T3671880145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.DebugLevel
struct  DebugLevel_t3671880145 
{
public:
	// System.Byte ExitGames.Client.Photon.DebugLevel::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugLevel_t3671880145, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVEL_T3671880145_H
#ifndef SERIALIZATIONPROTOCOL_T4091957412_H
#define SERIALIZATIONPROTOCOL_T4091957412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializationProtocol
struct  SerializationProtocol_t4091957412 
{
public:
	// System.Int32 ExitGames.Client.Photon.SerializationProtocol::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SerializationProtocol_t4091957412, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONPROTOCOL_T4091957412_H
#ifndef JOINTYPE_T3510207077_H
#define JOINTYPE_T3510207077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoinType
struct  JoinType_t3510207077 
{
public:
	// System.Int32 JoinType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoinType_t3510207077, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTYPE_T3510207077_H
#ifndef LOBBYTYPE_T3695323860_H
#define LOBBYTYPE_T3695323860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyType
struct  LobbyType_t3695323860 
{
public:
	// System.Byte LobbyType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LobbyType_t3695323860, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOBBYTYPE_T3695323860_H
#ifndef ONSERIALIZERIGIDBODY_T385167779_H
#define ONSERIALIZERIGIDBODY_T385167779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnSerializeRigidBody
struct  OnSerializeRigidBody_t385167779 
{
public:
	// System.Int32 OnSerializeRigidBody::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OnSerializeRigidBody_t385167779, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSERIALIZERIGIDBODY_T385167779_H
#ifndef ONSERIALIZETRANSFORM_T1364648257_H
#define ONSERIALIZETRANSFORM_T1364648257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnSerializeTransform
struct  OnSerializeTransform_t1364648257 
{
public:
	// System.Int32 OnSerializeTransform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OnSerializeTransform_t1364648257, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSERIALIZETRANSFORM_T1364648257_H
#ifndef OWNERSHIPOPTION_T37885007_H
#define OWNERSHIPOPTION_T37885007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OwnershipOption
struct  OwnershipOption_t37885007 
{
public:
	// System.Int32 OwnershipOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OwnershipOption_t37885007, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OWNERSHIPOPTION_T37885007_H
#ifndef PHOTONEVENTCODES_T582153385_H
#define PHOTONEVENTCODES_T582153385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonEventCodes
struct  PhotonEventCodes_t582153385 
{
public:
	// System.Int32 PhotonEventCodes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonEventCodes_t582153385, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONEVENTCODES_T582153385_H
#ifndef PHOTONLOGLEVEL_T4226222036_H
#define PHOTONLOGLEVEL_T4226222036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonLogLevel
struct  PhotonLogLevel_t4226222036 
{
public:
	// System.Int32 PhotonLogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PhotonLogLevel_t4226222036, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONLOGLEVEL_T4226222036_H
#ifndef TEAM_T2865224648_H
#define TEAM_T2865224648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PunTeams/Team
struct  Team_t2865224648 
{
public:
	// System.Byte PunTeams/Team::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Team_t2865224648, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAM_T2865224648_H
#ifndef RECEIVERGROUP_T3206452792_H
#define RECEIVERGROUP_T3206452792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReceiverGroup
struct  ReceiverGroup_t3206452792 
{
public:
	// System.Byte ReceiverGroup::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReceiverGroup_t3206452792, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIVERGROUP_T3206452792_H
#ifndef HAND_T2784208099_H
#define HAND_T2784208099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore/Hand
struct  Hand_t2784208099 
{
public:
	// System.Int32 RpsCore/Hand::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Hand_t2784208099, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HAND_T2784208099_H
#ifndef RESULTTYPE_T940288093_H
#define RESULTTYPE_T940288093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore/ResultType
struct  ResultType_t940288093 
{
public:
	// System.Int32 RpsCore/ResultType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ResultType_t940288093, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTTYPE_T940288093_H
#ifndef SERVERCONNECTION_T867335480_H
#define SERVERCONNECTION_T867335480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerConnection
struct  ServerConnection_t867335480 
{
public:
	// System.Int32 ServerConnection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ServerConnection_t867335480, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCONNECTION_T867335480_H
#ifndef HOSTINGOPTION_T2949276063_H
#define HOSTINGOPTION_T2949276063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerSettings/HostingOption
struct  HostingOption_t2949276063 
{
public:
	// System.Int32 ServerSettings/HostingOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HostingOption_t2949276063, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTINGOPTION_T2949276063_H
#ifndef STONETYPE_T314252901_H
#define STONETYPE_T314252901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StoneType
struct  StoneType_t314252901 
{
public:
	// System.Int32 StoneType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StoneType_t314252901, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STONETYPE_T314252901_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T1578797820_H
#define INDEXOUTOFRANGEEXCEPTION_T1578797820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t1578797820  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T1578797820_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef RUNTIMEFIELDHANDLE_T1871169219_H
#define RUNTIMEFIELDHANDLE_T1871169219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t1871169219 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t1871169219, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T1871169219_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef FORCEMODE2D_T255358695_H
#define FORCEMODE2D_T255358695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode2D
struct  ForceMode2D_t255358695 
{
public:
	// System.Int32 UnityEngine.ForceMode2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode2D_t255358695, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE2D_T255358695_H
#ifndef TYPE_T3858932131_H
#define TYPE_T3858932131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayoutOption/Type
struct  Type_t3858932131 
{
public:
	// System.Int32 UnityEngine.GUILayoutOption/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t3858932131, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3858932131_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RECTOFFSET_T1369453676_H
#define RECTOFFSET_T1369453676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t1369453676  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T1369453676_H
#ifndef LOADSCENEMODE_T3251202195_H
#define LOADSCENEMODE_T3251202195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t3251202195 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadSceneMode_t3251202195, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T3251202195_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef VIEWSYNCHRONIZATION_T3183556584_H
#define VIEWSYNCHRONIZATION_T3183556584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewSynchronization
struct  ViewSynchronization_t3183556584 
{
public:
	// System.Int32 ViewSynchronization::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ViewSynchronization_t3183556584, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWSYNCHRONIZATION_T3183556584_H
#ifndef PHOTONPEER_T1608153861_H
#define PHOTONPEER_T1608153861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonPeer
struct  PhotonPeer_t1608153861  : public RuntimeObject
{
public:
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ClientSdkId
	uint8_t ___ClientSdkId_3;
	// System.String ExitGames.Client.Photon.PhotonPeer::clientVersion
	String_t* ___clientVersion_5;
	// ExitGames.Client.Photon.SerializationProtocol ExitGames.Client.Photon.PhotonPeer::<SerializationProtocolType>k__BackingField
	int32_t ___U3CSerializationProtocolTypeU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type> ExitGames.Client.Photon.PhotonPeer::SocketImplementationConfig
	Dictionary_2_t1253839074 * ___SocketImplementationConfig_7;
	// System.Type ExitGames.Client.Photon.PhotonPeer::<SocketImplementation>k__BackingField
	Type_t * ___U3CSocketImplementationU3Ek__BackingField_8;
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PhotonPeer::DebugOut
	uint8_t ___DebugOut_9;
	// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::<Listener>k__BackingField
	RuntimeObject* ___U3CListenerU3Ek__BackingField_10;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<EnableServerTracing>k__BackingField
	bool ___U3CEnableServerTracingU3Ek__BackingField_11;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::quickResendAttempts
	uint8_t ___quickResendAttempts_12;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMinConnections
	int32_t ___RhttpMinConnections_13;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMaxConnections
	int32_t ___RhttpMaxConnections_14;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::<LimitOfUnreliableCommands>k__BackingField
	int32_t ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ChannelCount
	uint8_t ___ChannelCount_16;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::crcEnabled
	bool ___crcEnabled_17;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::WarningSize
	int32_t ___WarningSize_18;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::SentCountAllowance
	int32_t ___SentCountAllowance_19;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::TimePingInterval
	int32_t ___TimePingInterval_20;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::DisconnectTimeout
	int32_t ___DisconnectTimeout_21;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::<TransportProtocol>k__BackingField
	uint8_t ___U3CTransportProtocolU3Ek__BackingField_22;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::mtu
	int32_t ___mtu_24;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<IsSendingOnlyAcks>k__BackingField
	bool ___U3CIsSendingOnlyAcksU3Ek__BackingField_25;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsIncoming>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsIncomingU3Ek__BackingField_26;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsOutgoing>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsOutgoingU3Ek__BackingField_27;
	// ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::<TrafficStatsGameLevel>k__BackingField
	TrafficStatsGameLevel_t4013908777 * ___U3CTrafficStatsGameLevelU3Ek__BackingField_28;
	// System.Diagnostics.Stopwatch ExitGames.Client.Photon.PhotonPeer::trafficStatsStopwatch
	Stopwatch_t305734070 * ___trafficStatsStopwatch_29;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::trafficStatsEnabled
	bool ___trafficStatsEnabled_30;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PhotonPeer::peerBase
	PeerBase_t2956237011 * ___peerBase_31;
	// System.Object ExitGames.Client.Photon.PhotonPeer::SendOutgoingLockObject
	RuntimeObject * ___SendOutgoingLockObject_32;
	// System.Object ExitGames.Client.Photon.PhotonPeer::DispatchLockObject
	RuntimeObject * ___DispatchLockObject_33;
	// System.Object ExitGames.Client.Photon.PhotonPeer::EnqueueLock
	RuntimeObject * ___EnqueueLock_34;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::PayloadEncryptionSecret
	ByteU5BU5D_t4116647657* ___PayloadEncryptionSecret_35;
	// ExitGames.Client.Photon.EncryptorManaged.Encryptor ExitGames.Client.Photon.PhotonPeer::DgramEncryptor
	Encryptor_t200327285 * ___DgramEncryptor_36;
	// ExitGames.Client.Photon.EncryptorManaged.Decryptor ExitGames.Client.Photon.PhotonPeer::DgramDecryptor
	Decryptor_t2116099858 * ___DgramDecryptor_37;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::randomizeSequenceNumbers
	bool ___randomizeSequenceNumbers_38;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::sequenceNumberSource
	ByteU5BU5D_t4116647657* ___sequenceNumberSource_39;

public:
	inline static int32_t get_offset_of_ClientSdkId_3() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ClientSdkId_3)); }
	inline uint8_t get_ClientSdkId_3() const { return ___ClientSdkId_3; }
	inline uint8_t* get_address_of_ClientSdkId_3() { return &___ClientSdkId_3; }
	inline void set_ClientSdkId_3(uint8_t value)
	{
		___ClientSdkId_3 = value;
	}

	inline static int32_t get_offset_of_clientVersion_5() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___clientVersion_5)); }
	inline String_t* get_clientVersion_5() const { return ___clientVersion_5; }
	inline String_t** get_address_of_clientVersion_5() { return &___clientVersion_5; }
	inline void set_clientVersion_5(String_t* value)
	{
		___clientVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___clientVersion_5), value);
	}

	inline static int32_t get_offset_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSerializationProtocolTypeU3Ek__BackingField_6)); }
	inline int32_t get_U3CSerializationProtocolTypeU3Ek__BackingField_6() const { return ___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CSerializationProtocolTypeU3Ek__BackingField_6() { return &___U3CSerializationProtocolTypeU3Ek__BackingField_6; }
	inline void set_U3CSerializationProtocolTypeU3Ek__BackingField_6(int32_t value)
	{
		___U3CSerializationProtocolTypeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_SocketImplementationConfig_7() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SocketImplementationConfig_7)); }
	inline Dictionary_2_t1253839074 * get_SocketImplementationConfig_7() const { return ___SocketImplementationConfig_7; }
	inline Dictionary_2_t1253839074 ** get_address_of_SocketImplementationConfig_7() { return &___SocketImplementationConfig_7; }
	inline void set_SocketImplementationConfig_7(Dictionary_2_t1253839074 * value)
	{
		___SocketImplementationConfig_7 = value;
		Il2CppCodeGenWriteBarrier((&___SocketImplementationConfig_7), value);
	}

	inline static int32_t get_offset_of_U3CSocketImplementationU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSocketImplementationU3Ek__BackingField_8)); }
	inline Type_t * get_U3CSocketImplementationU3Ek__BackingField_8() const { return ___U3CSocketImplementationU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CSocketImplementationU3Ek__BackingField_8() { return &___U3CSocketImplementationU3Ek__BackingField_8; }
	inline void set_U3CSocketImplementationU3Ek__BackingField_8(Type_t * value)
	{
		___U3CSocketImplementationU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketImplementationU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_DebugOut_9() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DebugOut_9)); }
	inline uint8_t get_DebugOut_9() const { return ___DebugOut_9; }
	inline uint8_t* get_address_of_DebugOut_9() { return &___DebugOut_9; }
	inline void set_DebugOut_9(uint8_t value)
	{
		___DebugOut_9 = value;
	}

	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CListenerU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CListenerU3Ek__BackingField_10() const { return ___U3CListenerU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CListenerU3Ek__BackingField_10() { return &___U3CListenerU3Ek__BackingField_10; }
	inline void set_U3CListenerU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CListenerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListenerU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CEnableServerTracingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CEnableServerTracingU3Ek__BackingField_11)); }
	inline bool get_U3CEnableServerTracingU3Ek__BackingField_11() const { return ___U3CEnableServerTracingU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CEnableServerTracingU3Ek__BackingField_11() { return &___U3CEnableServerTracingU3Ek__BackingField_11; }
	inline void set_U3CEnableServerTracingU3Ek__BackingField_11(bool value)
	{
		___U3CEnableServerTracingU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_quickResendAttempts_12() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___quickResendAttempts_12)); }
	inline uint8_t get_quickResendAttempts_12() const { return ___quickResendAttempts_12; }
	inline uint8_t* get_address_of_quickResendAttempts_12() { return &___quickResendAttempts_12; }
	inline void set_quickResendAttempts_12(uint8_t value)
	{
		___quickResendAttempts_12 = value;
	}

	inline static int32_t get_offset_of_RhttpMinConnections_13() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMinConnections_13)); }
	inline int32_t get_RhttpMinConnections_13() const { return ___RhttpMinConnections_13; }
	inline int32_t* get_address_of_RhttpMinConnections_13() { return &___RhttpMinConnections_13; }
	inline void set_RhttpMinConnections_13(int32_t value)
	{
		___RhttpMinConnections_13 = value;
	}

	inline static int32_t get_offset_of_RhttpMaxConnections_14() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMaxConnections_14)); }
	inline int32_t get_RhttpMaxConnections_14() const { return ___RhttpMaxConnections_14; }
	inline int32_t* get_address_of_RhttpMaxConnections_14() { return &___RhttpMaxConnections_14; }
	inline void set_RhttpMaxConnections_14(int32_t value)
	{
		___RhttpMaxConnections_14 = value;
	}

	inline static int32_t get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15)); }
	inline int32_t get_U3CLimitOfUnreliableCommandsU3Ek__BackingField_15() const { return ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_15() { return &___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15; }
	inline void set_U3CLimitOfUnreliableCommandsU3Ek__BackingField_15(int32_t value)
	{
		___U3CLimitOfUnreliableCommandsU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_ChannelCount_16() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ChannelCount_16)); }
	inline uint8_t get_ChannelCount_16() const { return ___ChannelCount_16; }
	inline uint8_t* get_address_of_ChannelCount_16() { return &___ChannelCount_16; }
	inline void set_ChannelCount_16(uint8_t value)
	{
		___ChannelCount_16 = value;
	}

	inline static int32_t get_offset_of_crcEnabled_17() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___crcEnabled_17)); }
	inline bool get_crcEnabled_17() const { return ___crcEnabled_17; }
	inline bool* get_address_of_crcEnabled_17() { return &___crcEnabled_17; }
	inline void set_crcEnabled_17(bool value)
	{
		___crcEnabled_17 = value;
	}

	inline static int32_t get_offset_of_WarningSize_18() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___WarningSize_18)); }
	inline int32_t get_WarningSize_18() const { return ___WarningSize_18; }
	inline int32_t* get_address_of_WarningSize_18() { return &___WarningSize_18; }
	inline void set_WarningSize_18(int32_t value)
	{
		___WarningSize_18 = value;
	}

	inline static int32_t get_offset_of_SentCountAllowance_19() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SentCountAllowance_19)); }
	inline int32_t get_SentCountAllowance_19() const { return ___SentCountAllowance_19; }
	inline int32_t* get_address_of_SentCountAllowance_19() { return &___SentCountAllowance_19; }
	inline void set_SentCountAllowance_19(int32_t value)
	{
		___SentCountAllowance_19 = value;
	}

	inline static int32_t get_offset_of_TimePingInterval_20() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___TimePingInterval_20)); }
	inline int32_t get_TimePingInterval_20() const { return ___TimePingInterval_20; }
	inline int32_t* get_address_of_TimePingInterval_20() { return &___TimePingInterval_20; }
	inline void set_TimePingInterval_20(int32_t value)
	{
		___TimePingInterval_20 = value;
	}

	inline static int32_t get_offset_of_DisconnectTimeout_21() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DisconnectTimeout_21)); }
	inline int32_t get_DisconnectTimeout_21() const { return ___DisconnectTimeout_21; }
	inline int32_t* get_address_of_DisconnectTimeout_21() { return &___DisconnectTimeout_21; }
	inline void set_DisconnectTimeout_21(int32_t value)
	{
		___DisconnectTimeout_21 = value;
	}

	inline static int32_t get_offset_of_U3CTransportProtocolU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTransportProtocolU3Ek__BackingField_22)); }
	inline uint8_t get_U3CTransportProtocolU3Ek__BackingField_22() const { return ___U3CTransportProtocolU3Ek__BackingField_22; }
	inline uint8_t* get_address_of_U3CTransportProtocolU3Ek__BackingField_22() { return &___U3CTransportProtocolU3Ek__BackingField_22; }
	inline void set_U3CTransportProtocolU3Ek__BackingField_22(uint8_t value)
	{
		___U3CTransportProtocolU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_mtu_24() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___mtu_24)); }
	inline int32_t get_mtu_24() const { return ___mtu_24; }
	inline int32_t* get_address_of_mtu_24() { return &___mtu_24; }
	inline void set_mtu_24(int32_t value)
	{
		___mtu_24 = value;
	}

	inline static int32_t get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CIsSendingOnlyAcksU3Ek__BackingField_25)); }
	inline bool get_U3CIsSendingOnlyAcksU3Ek__BackingField_25() const { return ___U3CIsSendingOnlyAcksU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CIsSendingOnlyAcksU3Ek__BackingField_25() { return &___U3CIsSendingOnlyAcksU3Ek__BackingField_25; }
	inline void set_U3CIsSendingOnlyAcksU3Ek__BackingField_25(bool value)
	{
		___U3CIsSendingOnlyAcksU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsIncomingU3Ek__BackingField_26)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsIncomingU3Ek__BackingField_26() const { return ___U3CTrafficStatsIncomingU3Ek__BackingField_26; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsIncomingU3Ek__BackingField_26() { return &___U3CTrafficStatsIncomingU3Ek__BackingField_26; }
	inline void set_U3CTrafficStatsIncomingU3Ek__BackingField_26(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsIncomingU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsIncomingU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsOutgoingU3Ek__BackingField_27)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsOutgoingU3Ek__BackingField_27() const { return ___U3CTrafficStatsOutgoingU3Ek__BackingField_27; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsOutgoingU3Ek__BackingField_27() { return &___U3CTrafficStatsOutgoingU3Ek__BackingField_27; }
	inline void set_U3CTrafficStatsOutgoingU3Ek__BackingField_27(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsOutgoingU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsOutgoingU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsGameLevelU3Ek__BackingField_28)); }
	inline TrafficStatsGameLevel_t4013908777 * get_U3CTrafficStatsGameLevelU3Ek__BackingField_28() const { return ___U3CTrafficStatsGameLevelU3Ek__BackingField_28; }
	inline TrafficStatsGameLevel_t4013908777 ** get_address_of_U3CTrafficStatsGameLevelU3Ek__BackingField_28() { return &___U3CTrafficStatsGameLevelU3Ek__BackingField_28; }
	inline void set_U3CTrafficStatsGameLevelU3Ek__BackingField_28(TrafficStatsGameLevel_t4013908777 * value)
	{
		___U3CTrafficStatsGameLevelU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsGameLevelU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_trafficStatsStopwatch_29() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsStopwatch_29)); }
	inline Stopwatch_t305734070 * get_trafficStatsStopwatch_29() const { return ___trafficStatsStopwatch_29; }
	inline Stopwatch_t305734070 ** get_address_of_trafficStatsStopwatch_29() { return &___trafficStatsStopwatch_29; }
	inline void set_trafficStatsStopwatch_29(Stopwatch_t305734070 * value)
	{
		___trafficStatsStopwatch_29 = value;
		Il2CppCodeGenWriteBarrier((&___trafficStatsStopwatch_29), value);
	}

	inline static int32_t get_offset_of_trafficStatsEnabled_30() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsEnabled_30)); }
	inline bool get_trafficStatsEnabled_30() const { return ___trafficStatsEnabled_30; }
	inline bool* get_address_of_trafficStatsEnabled_30() { return &___trafficStatsEnabled_30; }
	inline void set_trafficStatsEnabled_30(bool value)
	{
		___trafficStatsEnabled_30 = value;
	}

	inline static int32_t get_offset_of_peerBase_31() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___peerBase_31)); }
	inline PeerBase_t2956237011 * get_peerBase_31() const { return ___peerBase_31; }
	inline PeerBase_t2956237011 ** get_address_of_peerBase_31() { return &___peerBase_31; }
	inline void set_peerBase_31(PeerBase_t2956237011 * value)
	{
		___peerBase_31 = value;
		Il2CppCodeGenWriteBarrier((&___peerBase_31), value);
	}

	inline static int32_t get_offset_of_SendOutgoingLockObject_32() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SendOutgoingLockObject_32)); }
	inline RuntimeObject * get_SendOutgoingLockObject_32() const { return ___SendOutgoingLockObject_32; }
	inline RuntimeObject ** get_address_of_SendOutgoingLockObject_32() { return &___SendOutgoingLockObject_32; }
	inline void set_SendOutgoingLockObject_32(RuntimeObject * value)
	{
		___SendOutgoingLockObject_32 = value;
		Il2CppCodeGenWriteBarrier((&___SendOutgoingLockObject_32), value);
	}

	inline static int32_t get_offset_of_DispatchLockObject_33() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DispatchLockObject_33)); }
	inline RuntimeObject * get_DispatchLockObject_33() const { return ___DispatchLockObject_33; }
	inline RuntimeObject ** get_address_of_DispatchLockObject_33() { return &___DispatchLockObject_33; }
	inline void set_DispatchLockObject_33(RuntimeObject * value)
	{
		___DispatchLockObject_33 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchLockObject_33), value);
	}

	inline static int32_t get_offset_of_EnqueueLock_34() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___EnqueueLock_34)); }
	inline RuntimeObject * get_EnqueueLock_34() const { return ___EnqueueLock_34; }
	inline RuntimeObject ** get_address_of_EnqueueLock_34() { return &___EnqueueLock_34; }
	inline void set_EnqueueLock_34(RuntimeObject * value)
	{
		___EnqueueLock_34 = value;
		Il2CppCodeGenWriteBarrier((&___EnqueueLock_34), value);
	}

	inline static int32_t get_offset_of_PayloadEncryptionSecret_35() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___PayloadEncryptionSecret_35)); }
	inline ByteU5BU5D_t4116647657* get_PayloadEncryptionSecret_35() const { return ___PayloadEncryptionSecret_35; }
	inline ByteU5BU5D_t4116647657** get_address_of_PayloadEncryptionSecret_35() { return &___PayloadEncryptionSecret_35; }
	inline void set_PayloadEncryptionSecret_35(ByteU5BU5D_t4116647657* value)
	{
		___PayloadEncryptionSecret_35 = value;
		Il2CppCodeGenWriteBarrier((&___PayloadEncryptionSecret_35), value);
	}

	inline static int32_t get_offset_of_DgramEncryptor_36() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DgramEncryptor_36)); }
	inline Encryptor_t200327285 * get_DgramEncryptor_36() const { return ___DgramEncryptor_36; }
	inline Encryptor_t200327285 ** get_address_of_DgramEncryptor_36() { return &___DgramEncryptor_36; }
	inline void set_DgramEncryptor_36(Encryptor_t200327285 * value)
	{
		___DgramEncryptor_36 = value;
		Il2CppCodeGenWriteBarrier((&___DgramEncryptor_36), value);
	}

	inline static int32_t get_offset_of_DgramDecryptor_37() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DgramDecryptor_37)); }
	inline Decryptor_t2116099858 * get_DgramDecryptor_37() const { return ___DgramDecryptor_37; }
	inline Decryptor_t2116099858 ** get_address_of_DgramDecryptor_37() { return &___DgramDecryptor_37; }
	inline void set_DgramDecryptor_37(Decryptor_t2116099858 * value)
	{
		___DgramDecryptor_37 = value;
		Il2CppCodeGenWriteBarrier((&___DgramDecryptor_37), value);
	}

	inline static int32_t get_offset_of_randomizeSequenceNumbers_38() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___randomizeSequenceNumbers_38)); }
	inline bool get_randomizeSequenceNumbers_38() const { return ___randomizeSequenceNumbers_38; }
	inline bool* get_address_of_randomizeSequenceNumbers_38() { return &___randomizeSequenceNumbers_38; }
	inline void set_randomizeSequenceNumbers_38(bool value)
	{
		___randomizeSequenceNumbers_38 = value;
	}

	inline static int32_t get_offset_of_sequenceNumberSource_39() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___sequenceNumberSource_39)); }
	inline ByteU5BU5D_t4116647657* get_sequenceNumberSource_39() const { return ___sequenceNumberSource_39; }
	inline ByteU5BU5D_t4116647657** get_address_of_sequenceNumberSource_39() { return &___sequenceNumberSource_39; }
	inline void set_sequenceNumberSource_39(ByteU5BU5D_t4116647657* value)
	{
		___sequenceNumberSource_39 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceNumberSource_39), value);
	}
};

struct PhotonPeer_t1608153861_StaticFields
{
public:
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::AsyncKeyExchange
	bool ___AsyncKeyExchange_4;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::OutgoingStreamBufferSize
	int32_t ___OutgoingStreamBufferSize_23;

public:
	inline static int32_t get_offset_of_AsyncKeyExchange_4() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___AsyncKeyExchange_4)); }
	inline bool get_AsyncKeyExchange_4() const { return ___AsyncKeyExchange_4; }
	inline bool* get_address_of_AsyncKeyExchange_4() { return &___AsyncKeyExchange_4; }
	inline void set_AsyncKeyExchange_4(bool value)
	{
		___AsyncKeyExchange_4 = value;
	}

	inline static int32_t get_offset_of_OutgoingStreamBufferSize_23() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___OutgoingStreamBufferSize_23)); }
	inline int32_t get_OutgoingStreamBufferSize_23() const { return ___OutgoingStreamBufferSize_23; }
	inline int32_t* get_address_of_OutgoingStreamBufferSize_23() { return &___OutgoingStreamBufferSize_23; }
	inline void set_OutgoingStreamBufferSize_23(int32_t value)
	{
		___OutgoingStreamBufferSize_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPEER_T1608153861_H
#ifndef PHOTONNETWORK_T1610183659_H
#define PHOTONNETWORK_T1610183659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonNetwork
struct  PhotonNetwork_t1610183659  : public RuntimeObject
{
public:

public:
};

struct PhotonNetwork_t1610183659_StaticFields
{
public:
	// System.String PhotonNetwork::<gameVersion>k__BackingField
	String_t* ___U3CgameVersionU3Ek__BackingField_1;
	// PhotonHandler PhotonNetwork::photonMono
	PhotonHandler_t2139970417 * ___photonMono_2;
	// NetworkingPeer PhotonNetwork::networkingPeer
	NetworkingPeer_t264212356 * ___networkingPeer_3;
	// System.Int32 PhotonNetwork::MAX_VIEW_IDS
	int32_t ___MAX_VIEW_IDS_4;
	// ServerSettings PhotonNetwork::PhotonServerSettings
	ServerSettings_t2755303613 * ___PhotonServerSettings_6;
	// System.Boolean PhotonNetwork::InstantiateInRoomOnly
	bool ___InstantiateInRoomOnly_7;
	// PhotonLogLevel PhotonNetwork::logLevel
	int32_t ___logLevel_8;
	// System.Collections.Generic.List`1<FriendInfo> PhotonNetwork::<Friends>k__BackingField
	List_1_t2005371586 * ___U3CFriendsU3Ek__BackingField_9;
	// System.Single PhotonNetwork::precisionForVectorSynchronization
	float ___precisionForVectorSynchronization_10;
	// System.Single PhotonNetwork::precisionForQuaternionSynchronization
	float ___precisionForQuaternionSynchronization_11;
	// System.Single PhotonNetwork::precisionForFloatSynchronization
	float ___precisionForFloatSynchronization_12;
	// System.Boolean PhotonNetwork::UseRpcMonoBehaviourCache
	bool ___UseRpcMonoBehaviourCache_13;
	// System.Boolean PhotonNetwork::UsePrefabCache
	bool ___UsePrefabCache_14;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> PhotonNetwork::PrefabCache
	Dictionary_2_t898892918 * ___PrefabCache_15;
	// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> PhotonNetwork::SendMonoMessageTargets
	HashSet_1_t3973553389 * ___SendMonoMessageTargets_16;
	// System.Type PhotonNetwork::SendMonoMessageTargetType
	Type_t * ___SendMonoMessageTargetType_17;
	// System.Boolean PhotonNetwork::StartRpcsAsCoroutine
	bool ___StartRpcsAsCoroutine_18;
	// System.Boolean PhotonNetwork::isOfflineMode
	bool ___isOfflineMode_19;
	// Room PhotonNetwork::offlineModeRoom
	Room_t3759828263 * ___offlineModeRoom_20;
	// System.Int32 PhotonNetwork::maxConnections
	int32_t ___maxConnections_21;
	// System.Boolean PhotonNetwork::_mAutomaticallySyncScene
	bool ____mAutomaticallySyncScene_22;
	// System.Boolean PhotonNetwork::m_autoCleanUpPlayerObjects
	bool ___m_autoCleanUpPlayerObjects_23;
	// System.Int32 PhotonNetwork::sendInterval
	int32_t ___sendInterval_24;
	// System.Int32 PhotonNetwork::sendIntervalOnSerialize
	int32_t ___sendIntervalOnSerialize_25;
	// System.Boolean PhotonNetwork::m_isMessageQueueRunning
	bool ___m_isMessageQueueRunning_26;
	// System.Diagnostics.Stopwatch PhotonNetwork::startupStopwatch
	Stopwatch_t305734070 * ___startupStopwatch_27;
	// System.Single PhotonNetwork::BackgroundTimeout
	float ___BackgroundTimeout_28;
	// System.Boolean PhotonNetwork::<UseAlternativeUdpPorts>k__BackingField
	bool ___U3CUseAlternativeUdpPortsU3Ek__BackingField_29;
	// PhotonNetwork/EventCallback PhotonNetwork::OnEventCall
	EventCallback_t1220598991 * ___OnEventCall_30;
	// System.Int32 PhotonNetwork::lastUsedViewSubId
	int32_t ___lastUsedViewSubId_31;
	// System.Int32 PhotonNetwork::lastUsedViewSubIdStatic
	int32_t ___lastUsedViewSubIdStatic_32;
	// System.Collections.Generic.List`1<System.Int32> PhotonNetwork::manuallyAllocatedViewIds
	List_1_t128053199 * ___manuallyAllocatedViewIds_33;

public:
	inline static int32_t get_offset_of_U3CgameVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___U3CgameVersionU3Ek__BackingField_1)); }
	inline String_t* get_U3CgameVersionU3Ek__BackingField_1() const { return ___U3CgameVersionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CgameVersionU3Ek__BackingField_1() { return &___U3CgameVersionU3Ek__BackingField_1; }
	inline void set_U3CgameVersionU3Ek__BackingField_1(String_t* value)
	{
		___U3CgameVersionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameVersionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_photonMono_2() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___photonMono_2)); }
	inline PhotonHandler_t2139970417 * get_photonMono_2() const { return ___photonMono_2; }
	inline PhotonHandler_t2139970417 ** get_address_of_photonMono_2() { return &___photonMono_2; }
	inline void set_photonMono_2(PhotonHandler_t2139970417 * value)
	{
		___photonMono_2 = value;
		Il2CppCodeGenWriteBarrier((&___photonMono_2), value);
	}

	inline static int32_t get_offset_of_networkingPeer_3() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___networkingPeer_3)); }
	inline NetworkingPeer_t264212356 * get_networkingPeer_3() const { return ___networkingPeer_3; }
	inline NetworkingPeer_t264212356 ** get_address_of_networkingPeer_3() { return &___networkingPeer_3; }
	inline void set_networkingPeer_3(NetworkingPeer_t264212356 * value)
	{
		___networkingPeer_3 = value;
		Il2CppCodeGenWriteBarrier((&___networkingPeer_3), value);
	}

	inline static int32_t get_offset_of_MAX_VIEW_IDS_4() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___MAX_VIEW_IDS_4)); }
	inline int32_t get_MAX_VIEW_IDS_4() const { return ___MAX_VIEW_IDS_4; }
	inline int32_t* get_address_of_MAX_VIEW_IDS_4() { return &___MAX_VIEW_IDS_4; }
	inline void set_MAX_VIEW_IDS_4(int32_t value)
	{
		___MAX_VIEW_IDS_4 = value;
	}

	inline static int32_t get_offset_of_PhotonServerSettings_6() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___PhotonServerSettings_6)); }
	inline ServerSettings_t2755303613 * get_PhotonServerSettings_6() const { return ___PhotonServerSettings_6; }
	inline ServerSettings_t2755303613 ** get_address_of_PhotonServerSettings_6() { return &___PhotonServerSettings_6; }
	inline void set_PhotonServerSettings_6(ServerSettings_t2755303613 * value)
	{
		___PhotonServerSettings_6 = value;
		Il2CppCodeGenWriteBarrier((&___PhotonServerSettings_6), value);
	}

	inline static int32_t get_offset_of_InstantiateInRoomOnly_7() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___InstantiateInRoomOnly_7)); }
	inline bool get_InstantiateInRoomOnly_7() const { return ___InstantiateInRoomOnly_7; }
	inline bool* get_address_of_InstantiateInRoomOnly_7() { return &___InstantiateInRoomOnly_7; }
	inline void set_InstantiateInRoomOnly_7(bool value)
	{
		___InstantiateInRoomOnly_7 = value;
	}

	inline static int32_t get_offset_of_logLevel_8() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___logLevel_8)); }
	inline int32_t get_logLevel_8() const { return ___logLevel_8; }
	inline int32_t* get_address_of_logLevel_8() { return &___logLevel_8; }
	inline void set_logLevel_8(int32_t value)
	{
		___logLevel_8 = value;
	}

	inline static int32_t get_offset_of_U3CFriendsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___U3CFriendsU3Ek__BackingField_9)); }
	inline List_1_t2005371586 * get_U3CFriendsU3Ek__BackingField_9() const { return ___U3CFriendsU3Ek__BackingField_9; }
	inline List_1_t2005371586 ** get_address_of_U3CFriendsU3Ek__BackingField_9() { return &___U3CFriendsU3Ek__BackingField_9; }
	inline void set_U3CFriendsU3Ek__BackingField_9(List_1_t2005371586 * value)
	{
		___U3CFriendsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFriendsU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_precisionForVectorSynchronization_10() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___precisionForVectorSynchronization_10)); }
	inline float get_precisionForVectorSynchronization_10() const { return ___precisionForVectorSynchronization_10; }
	inline float* get_address_of_precisionForVectorSynchronization_10() { return &___precisionForVectorSynchronization_10; }
	inline void set_precisionForVectorSynchronization_10(float value)
	{
		___precisionForVectorSynchronization_10 = value;
	}

	inline static int32_t get_offset_of_precisionForQuaternionSynchronization_11() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___precisionForQuaternionSynchronization_11)); }
	inline float get_precisionForQuaternionSynchronization_11() const { return ___precisionForQuaternionSynchronization_11; }
	inline float* get_address_of_precisionForQuaternionSynchronization_11() { return &___precisionForQuaternionSynchronization_11; }
	inline void set_precisionForQuaternionSynchronization_11(float value)
	{
		___precisionForQuaternionSynchronization_11 = value;
	}

	inline static int32_t get_offset_of_precisionForFloatSynchronization_12() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___precisionForFloatSynchronization_12)); }
	inline float get_precisionForFloatSynchronization_12() const { return ___precisionForFloatSynchronization_12; }
	inline float* get_address_of_precisionForFloatSynchronization_12() { return &___precisionForFloatSynchronization_12; }
	inline void set_precisionForFloatSynchronization_12(float value)
	{
		___precisionForFloatSynchronization_12 = value;
	}

	inline static int32_t get_offset_of_UseRpcMonoBehaviourCache_13() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___UseRpcMonoBehaviourCache_13)); }
	inline bool get_UseRpcMonoBehaviourCache_13() const { return ___UseRpcMonoBehaviourCache_13; }
	inline bool* get_address_of_UseRpcMonoBehaviourCache_13() { return &___UseRpcMonoBehaviourCache_13; }
	inline void set_UseRpcMonoBehaviourCache_13(bool value)
	{
		___UseRpcMonoBehaviourCache_13 = value;
	}

	inline static int32_t get_offset_of_UsePrefabCache_14() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___UsePrefabCache_14)); }
	inline bool get_UsePrefabCache_14() const { return ___UsePrefabCache_14; }
	inline bool* get_address_of_UsePrefabCache_14() { return &___UsePrefabCache_14; }
	inline void set_UsePrefabCache_14(bool value)
	{
		___UsePrefabCache_14 = value;
	}

	inline static int32_t get_offset_of_PrefabCache_15() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___PrefabCache_15)); }
	inline Dictionary_2_t898892918 * get_PrefabCache_15() const { return ___PrefabCache_15; }
	inline Dictionary_2_t898892918 ** get_address_of_PrefabCache_15() { return &___PrefabCache_15; }
	inline void set_PrefabCache_15(Dictionary_2_t898892918 * value)
	{
		___PrefabCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabCache_15), value);
	}

	inline static int32_t get_offset_of_SendMonoMessageTargets_16() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___SendMonoMessageTargets_16)); }
	inline HashSet_1_t3973553389 * get_SendMonoMessageTargets_16() const { return ___SendMonoMessageTargets_16; }
	inline HashSet_1_t3973553389 ** get_address_of_SendMonoMessageTargets_16() { return &___SendMonoMessageTargets_16; }
	inline void set_SendMonoMessageTargets_16(HashSet_1_t3973553389 * value)
	{
		___SendMonoMessageTargets_16 = value;
		Il2CppCodeGenWriteBarrier((&___SendMonoMessageTargets_16), value);
	}

	inline static int32_t get_offset_of_SendMonoMessageTargetType_17() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___SendMonoMessageTargetType_17)); }
	inline Type_t * get_SendMonoMessageTargetType_17() const { return ___SendMonoMessageTargetType_17; }
	inline Type_t ** get_address_of_SendMonoMessageTargetType_17() { return &___SendMonoMessageTargetType_17; }
	inline void set_SendMonoMessageTargetType_17(Type_t * value)
	{
		___SendMonoMessageTargetType_17 = value;
		Il2CppCodeGenWriteBarrier((&___SendMonoMessageTargetType_17), value);
	}

	inline static int32_t get_offset_of_StartRpcsAsCoroutine_18() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___StartRpcsAsCoroutine_18)); }
	inline bool get_StartRpcsAsCoroutine_18() const { return ___StartRpcsAsCoroutine_18; }
	inline bool* get_address_of_StartRpcsAsCoroutine_18() { return &___StartRpcsAsCoroutine_18; }
	inline void set_StartRpcsAsCoroutine_18(bool value)
	{
		___StartRpcsAsCoroutine_18 = value;
	}

	inline static int32_t get_offset_of_isOfflineMode_19() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___isOfflineMode_19)); }
	inline bool get_isOfflineMode_19() const { return ___isOfflineMode_19; }
	inline bool* get_address_of_isOfflineMode_19() { return &___isOfflineMode_19; }
	inline void set_isOfflineMode_19(bool value)
	{
		___isOfflineMode_19 = value;
	}

	inline static int32_t get_offset_of_offlineModeRoom_20() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___offlineModeRoom_20)); }
	inline Room_t3759828263 * get_offlineModeRoom_20() const { return ___offlineModeRoom_20; }
	inline Room_t3759828263 ** get_address_of_offlineModeRoom_20() { return &___offlineModeRoom_20; }
	inline void set_offlineModeRoom_20(Room_t3759828263 * value)
	{
		___offlineModeRoom_20 = value;
		Il2CppCodeGenWriteBarrier((&___offlineModeRoom_20), value);
	}

	inline static int32_t get_offset_of_maxConnections_21() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___maxConnections_21)); }
	inline int32_t get_maxConnections_21() const { return ___maxConnections_21; }
	inline int32_t* get_address_of_maxConnections_21() { return &___maxConnections_21; }
	inline void set_maxConnections_21(int32_t value)
	{
		___maxConnections_21 = value;
	}

	inline static int32_t get_offset_of__mAutomaticallySyncScene_22() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ____mAutomaticallySyncScene_22)); }
	inline bool get__mAutomaticallySyncScene_22() const { return ____mAutomaticallySyncScene_22; }
	inline bool* get_address_of__mAutomaticallySyncScene_22() { return &____mAutomaticallySyncScene_22; }
	inline void set__mAutomaticallySyncScene_22(bool value)
	{
		____mAutomaticallySyncScene_22 = value;
	}

	inline static int32_t get_offset_of_m_autoCleanUpPlayerObjects_23() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___m_autoCleanUpPlayerObjects_23)); }
	inline bool get_m_autoCleanUpPlayerObjects_23() const { return ___m_autoCleanUpPlayerObjects_23; }
	inline bool* get_address_of_m_autoCleanUpPlayerObjects_23() { return &___m_autoCleanUpPlayerObjects_23; }
	inline void set_m_autoCleanUpPlayerObjects_23(bool value)
	{
		___m_autoCleanUpPlayerObjects_23 = value;
	}

	inline static int32_t get_offset_of_sendInterval_24() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___sendInterval_24)); }
	inline int32_t get_sendInterval_24() const { return ___sendInterval_24; }
	inline int32_t* get_address_of_sendInterval_24() { return &___sendInterval_24; }
	inline void set_sendInterval_24(int32_t value)
	{
		___sendInterval_24 = value;
	}

	inline static int32_t get_offset_of_sendIntervalOnSerialize_25() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___sendIntervalOnSerialize_25)); }
	inline int32_t get_sendIntervalOnSerialize_25() const { return ___sendIntervalOnSerialize_25; }
	inline int32_t* get_address_of_sendIntervalOnSerialize_25() { return &___sendIntervalOnSerialize_25; }
	inline void set_sendIntervalOnSerialize_25(int32_t value)
	{
		___sendIntervalOnSerialize_25 = value;
	}

	inline static int32_t get_offset_of_m_isMessageQueueRunning_26() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___m_isMessageQueueRunning_26)); }
	inline bool get_m_isMessageQueueRunning_26() const { return ___m_isMessageQueueRunning_26; }
	inline bool* get_address_of_m_isMessageQueueRunning_26() { return &___m_isMessageQueueRunning_26; }
	inline void set_m_isMessageQueueRunning_26(bool value)
	{
		___m_isMessageQueueRunning_26 = value;
	}

	inline static int32_t get_offset_of_startupStopwatch_27() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___startupStopwatch_27)); }
	inline Stopwatch_t305734070 * get_startupStopwatch_27() const { return ___startupStopwatch_27; }
	inline Stopwatch_t305734070 ** get_address_of_startupStopwatch_27() { return &___startupStopwatch_27; }
	inline void set_startupStopwatch_27(Stopwatch_t305734070 * value)
	{
		___startupStopwatch_27 = value;
		Il2CppCodeGenWriteBarrier((&___startupStopwatch_27), value);
	}

	inline static int32_t get_offset_of_BackgroundTimeout_28() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___BackgroundTimeout_28)); }
	inline float get_BackgroundTimeout_28() const { return ___BackgroundTimeout_28; }
	inline float* get_address_of_BackgroundTimeout_28() { return &___BackgroundTimeout_28; }
	inline void set_BackgroundTimeout_28(float value)
	{
		___BackgroundTimeout_28 = value;
	}

	inline static int32_t get_offset_of_U3CUseAlternativeUdpPortsU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___U3CUseAlternativeUdpPortsU3Ek__BackingField_29)); }
	inline bool get_U3CUseAlternativeUdpPortsU3Ek__BackingField_29() const { return ___U3CUseAlternativeUdpPortsU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CUseAlternativeUdpPortsU3Ek__BackingField_29() { return &___U3CUseAlternativeUdpPortsU3Ek__BackingField_29; }
	inline void set_U3CUseAlternativeUdpPortsU3Ek__BackingField_29(bool value)
	{
		___U3CUseAlternativeUdpPortsU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_OnEventCall_30() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___OnEventCall_30)); }
	inline EventCallback_t1220598991 * get_OnEventCall_30() const { return ___OnEventCall_30; }
	inline EventCallback_t1220598991 ** get_address_of_OnEventCall_30() { return &___OnEventCall_30; }
	inline void set_OnEventCall_30(EventCallback_t1220598991 * value)
	{
		___OnEventCall_30 = value;
		Il2CppCodeGenWriteBarrier((&___OnEventCall_30), value);
	}

	inline static int32_t get_offset_of_lastUsedViewSubId_31() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___lastUsedViewSubId_31)); }
	inline int32_t get_lastUsedViewSubId_31() const { return ___lastUsedViewSubId_31; }
	inline int32_t* get_address_of_lastUsedViewSubId_31() { return &___lastUsedViewSubId_31; }
	inline void set_lastUsedViewSubId_31(int32_t value)
	{
		___lastUsedViewSubId_31 = value;
	}

	inline static int32_t get_offset_of_lastUsedViewSubIdStatic_32() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___lastUsedViewSubIdStatic_32)); }
	inline int32_t get_lastUsedViewSubIdStatic_32() const { return ___lastUsedViewSubIdStatic_32; }
	inline int32_t* get_address_of_lastUsedViewSubIdStatic_32() { return &___lastUsedViewSubIdStatic_32; }
	inline void set_lastUsedViewSubIdStatic_32(int32_t value)
	{
		___lastUsedViewSubIdStatic_32 = value;
	}

	inline static int32_t get_offset_of_manuallyAllocatedViewIds_33() { return static_cast<int32_t>(offsetof(PhotonNetwork_t1610183659_StaticFields, ___manuallyAllocatedViewIds_33)); }
	inline List_1_t128053199 * get_manuallyAllocatedViewIds_33() const { return ___manuallyAllocatedViewIds_33; }
	inline List_1_t128053199 ** get_address_of_manuallyAllocatedViewIds_33() { return &___manuallyAllocatedViewIds_33; }
	inline void set_manuallyAllocatedViewIds_33(List_1_t128053199 * value)
	{
		___manuallyAllocatedViewIds_33 = value;
		Il2CppCodeGenWriteBarrier((&___manuallyAllocatedViewIds_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONNETWORK_T1610183659_H
#ifndef RAISEEVENTOPTIONS_T1229553678_H
#define RAISEEVENTOPTIONS_T1229553678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RaiseEventOptions
struct  RaiseEventOptions_t1229553678  : public RuntimeObject
{
public:
	// EventCaching RaiseEventOptions::CachingOption
	uint8_t ___CachingOption_1;
	// System.Byte RaiseEventOptions::InterestGroup
	uint8_t ___InterestGroup_2;
	// System.Int32[] RaiseEventOptions::TargetActors
	Int32U5BU5D_t385246372* ___TargetActors_3;
	// ReceiverGroup RaiseEventOptions::Receivers
	uint8_t ___Receivers_4;
	// System.Byte RaiseEventOptions::SequenceChannel
	uint8_t ___SequenceChannel_5;
	// System.Boolean RaiseEventOptions::ForwardToWebhook
	bool ___ForwardToWebhook_6;
	// System.Boolean RaiseEventOptions::Encrypt
	bool ___Encrypt_7;

public:
	inline static int32_t get_offset_of_CachingOption_1() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___CachingOption_1)); }
	inline uint8_t get_CachingOption_1() const { return ___CachingOption_1; }
	inline uint8_t* get_address_of_CachingOption_1() { return &___CachingOption_1; }
	inline void set_CachingOption_1(uint8_t value)
	{
		___CachingOption_1 = value;
	}

	inline static int32_t get_offset_of_InterestGroup_2() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___InterestGroup_2)); }
	inline uint8_t get_InterestGroup_2() const { return ___InterestGroup_2; }
	inline uint8_t* get_address_of_InterestGroup_2() { return &___InterestGroup_2; }
	inline void set_InterestGroup_2(uint8_t value)
	{
		___InterestGroup_2 = value;
	}

	inline static int32_t get_offset_of_TargetActors_3() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___TargetActors_3)); }
	inline Int32U5BU5D_t385246372* get_TargetActors_3() const { return ___TargetActors_3; }
	inline Int32U5BU5D_t385246372** get_address_of_TargetActors_3() { return &___TargetActors_3; }
	inline void set_TargetActors_3(Int32U5BU5D_t385246372* value)
	{
		___TargetActors_3 = value;
		Il2CppCodeGenWriteBarrier((&___TargetActors_3), value);
	}

	inline static int32_t get_offset_of_Receivers_4() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___Receivers_4)); }
	inline uint8_t get_Receivers_4() const { return ___Receivers_4; }
	inline uint8_t* get_address_of_Receivers_4() { return &___Receivers_4; }
	inline void set_Receivers_4(uint8_t value)
	{
		___Receivers_4 = value;
	}

	inline static int32_t get_offset_of_SequenceChannel_5() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___SequenceChannel_5)); }
	inline uint8_t get_SequenceChannel_5() const { return ___SequenceChannel_5; }
	inline uint8_t* get_address_of_SequenceChannel_5() { return &___SequenceChannel_5; }
	inline void set_SequenceChannel_5(uint8_t value)
	{
		___SequenceChannel_5 = value;
	}

	inline static int32_t get_offset_of_ForwardToWebhook_6() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___ForwardToWebhook_6)); }
	inline bool get_ForwardToWebhook_6() const { return ___ForwardToWebhook_6; }
	inline bool* get_address_of_ForwardToWebhook_6() { return &___ForwardToWebhook_6; }
	inline void set_ForwardToWebhook_6(bool value)
	{
		___ForwardToWebhook_6 = value;
	}

	inline static int32_t get_offset_of_Encrypt_7() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678, ___Encrypt_7)); }
	inline bool get_Encrypt_7() const { return ___Encrypt_7; }
	inline bool* get_address_of_Encrypt_7() { return &___Encrypt_7; }
	inline void set_Encrypt_7(bool value)
	{
		___Encrypt_7 = value;
	}
};

struct RaiseEventOptions_t1229553678_StaticFields
{
public:
	// RaiseEventOptions RaiseEventOptions::Default
	RaiseEventOptions_t1229553678 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(RaiseEventOptions_t1229553678_StaticFields, ___Default_0)); }
	inline RaiseEventOptions_t1229553678 * get_Default_0() const { return ___Default_0; }
	inline RaiseEventOptions_t1229553678 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(RaiseEventOptions_t1229553678 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAISEEVENTOPTIONS_T1229553678_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPEDLOBBY_T3336582029_H
#define TYPEDLOBBY_T3336582029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TypedLobby
struct  TypedLobby_t3336582029  : public RuntimeObject
{
public:
	// System.String TypedLobby::Name
	String_t* ___Name_0;
	// LobbyType TypedLobby::Type
	uint8_t ___Type_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TypedLobby_t3336582029, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(TypedLobby_t3336582029, ___Type_1)); }
	inline uint8_t get_Type_1() const { return ___Type_1; }
	inline uint8_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(uint8_t value)
	{
		___Type_1 = value;
	}
};

struct TypedLobby_t3336582029_StaticFields
{
public:
	// TypedLobby TypedLobby::Default
	TypedLobby_t3336582029 * ___Default_2;

public:
	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(TypedLobby_t3336582029_StaticFields, ___Default_2)); }
	inline TypedLobby_t3336582029 * get_Default_2() const { return ___Default_2; }
	inline TypedLobby_t3336582029 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(TypedLobby_t3336582029 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDLOBBY_T3336582029_H
#ifndef AUDIOCLIP_T3680889665_H
#define AUDIOCLIP_T3680889665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_t3680889665  : public Object_t631007953
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t1677636661 * ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t1059417452 * ___m_PCMSetPositionCallback_5;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_4() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMReaderCallback_4)); }
	inline PCMReaderCallback_t1677636661 * get_m_PCMReaderCallback_4() const { return ___m_PCMReaderCallback_4; }
	inline PCMReaderCallback_t1677636661 ** get_address_of_m_PCMReaderCallback_4() { return &___m_PCMReaderCallback_4; }
	inline void set_m_PCMReaderCallback_4(PCMReaderCallback_t1677636661 * value)
	{
		___m_PCMReaderCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_4), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_5() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMSetPositionCallback_5)); }
	inline PCMSetPositionCallback_t1059417452 * get_m_PCMSetPositionCallback_5() const { return ___m_PCMSetPositionCallback_5; }
	inline PCMSetPositionCallback_t1059417452 ** get_address_of_m_PCMSetPositionCallback_5() { return &___m_PCMSetPositionCallback_5; }
	inline void set_m_PCMSetPositionCallback_5(PCMSetPositionCallback_t1059417452 * value)
	{
		___m_PCMSetPositionCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_T3680889665_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GUILAYOUTOPTION_T811797299_H
#define GUILAYOUTOPTION_T811797299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayoutOption
struct  GUILayoutOption_t811797299  : public RuntimeObject
{
public:
	// UnityEngine.GUILayoutOption/Type UnityEngine.GUILayoutOption::type
	int32_t ___type_0;
	// System.Object UnityEngine.GUILayoutOption::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(GUILayoutOption_t811797299, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(GUILayoutOption_t811797299, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTOPTION_T811797299_H
#ifndef GUISTYLE_T3956901511_H
#define GUISTYLE_T3956901511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t3956901511  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t1397964415 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t1397964415 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t1397964415 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t1397964415 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t1397964415 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t1397964415 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t1397964415 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t1397964415 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t1369453676 * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t1369453676 * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t1369453676 * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t1369453676 * ___m_Overflow_12;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Normal_1)); }
	inline GUIStyleState_t1397964415 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t1397964415 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Hover_2)); }
	inline GUIStyleState_t1397964415 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t1397964415 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Active_3)); }
	inline GUIStyleState_t1397964415 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t1397964415 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Focused_4)); }
	inline GUIStyleState_t1397964415 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t1397964415 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnNormal_5)); }
	inline GUIStyleState_t1397964415 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t1397964415 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnHover_6)); }
	inline GUIStyleState_t1397964415 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t1397964415 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnActive_7)); }
	inline GUIStyleState_t1397964415 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t1397964415 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnFocused_8)); }
	inline GUIStyleState_t1397964415 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t1397964415 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Border_9)); }
	inline RectOffset_t1369453676 * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_t1369453676 ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_t1369453676 * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Padding_10)); }
	inline RectOffset_t1369453676 * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_t1369453676 * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Margin_11)); }
	inline RectOffset_t1369453676 * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_t1369453676 ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_t1369453676 * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Overflow_12)); }
	inline RectOffset_t1369453676 * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_t1369453676 ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_t1369453676 * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}
};

struct GUIStyle_t3956901511_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_13;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t3956901511 * ___s_None_14;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_13() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___showKeyboardFocus_13)); }
	inline bool get_showKeyboardFocus_13() const { return ___showKeyboardFocus_13; }
	inline bool* get_address_of_showKeyboardFocus_13() { return &___showKeyboardFocus_13; }
	inline void set_showKeyboardFocus_13(bool value)
	{
		___showKeyboardFocus_13 = value;
	}

	inline static int32_t get_offset_of_s_None_14() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___s_None_14)); }
	inline GUIStyle_t3956901511 * get_s_None_14() const { return ___s_None_14; }
	inline GUIStyle_t3956901511 ** get_address_of_s_None_14() { return &___s_None_14; }
	inline void set_s_None_14(GUIStyle_t3956901511 * value)
	{
		___s_None_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Border_9;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Overflow_12;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_com* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_com* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_com* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_com* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_com* ___m_Border_9;
	RectOffset_t1369453676_marshaled_com* ___m_Padding_10;
	RectOffset_t1369453676_marshaled_com* ___m_Margin_11;
	RectOffset_t1369453676_marshaled_com* ___m_Overflow_12;
};
#endif // GUISTYLE_T3956901511_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef LOADBALANCINGPEER_T3218467959_H
#define LOADBALANCINGPEER_T3218467959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadBalancingPeer
struct  LoadBalancingPeer_t3218467959  : public PhotonPeer_t1608153861
{
public:
	// System.Collections.Generic.Dictionary`2<System.Byte,System.Object> LoadBalancingPeer::opParameters
	Dictionary_2_t1405253484 * ___opParameters_40;

public:
	inline static int32_t get_offset_of_opParameters_40() { return static_cast<int32_t>(offsetof(LoadBalancingPeer_t3218467959, ___opParameters_40)); }
	inline Dictionary_2_t1405253484 * get_opParameters_40() const { return ___opParameters_40; }
	inline Dictionary_2_t1405253484 ** get_address_of_opParameters_40() { return &___opParameters_40; }
	inline void set_opParameters_40(Dictionary_2_t1405253484 * value)
	{
		___opParameters_40 = value;
		Il2CppCodeGenWriteBarrier((&___opParameters_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADBALANCINGPEER_T3218467959_H
#ifndef EVENTCALLBACK_T1220598991_H
#define EVENTCALLBACK_T1220598991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonNetwork/EventCallback
struct  EventCallback_t1220598991  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTCALLBACK_T1220598991_H
#ifndef SERVERSETTINGS_T2755303613_H
#define SERVERSETTINGS_T2755303613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerSettings
struct  ServerSettings_t2755303613  : public ScriptableObject_t2528358522
{
public:
	// System.String ServerSettings::AppID
	String_t* ___AppID_4;
	// System.String ServerSettings::VoiceAppID
	String_t* ___VoiceAppID_5;
	// System.String ServerSettings::ChatAppID
	String_t* ___ChatAppID_6;
	// ServerSettings/HostingOption ServerSettings::HostType
	int32_t ___HostType_7;
	// CloudRegionCode ServerSettings::PreferredRegion
	int32_t ___PreferredRegion_8;
	// CloudRegionFlag ServerSettings::EnabledRegions
	int32_t ___EnabledRegions_9;
	// ExitGames.Client.Photon.ConnectionProtocol ServerSettings::Protocol
	uint8_t ___Protocol_10;
	// System.String ServerSettings::ServerAddress
	String_t* ___ServerAddress_11;
	// System.Int32 ServerSettings::ServerPort
	int32_t ___ServerPort_12;
	// System.Int32 ServerSettings::VoiceServerPort
	int32_t ___VoiceServerPort_13;
	// System.Boolean ServerSettings::JoinLobby
	bool ___JoinLobby_14;
	// System.Boolean ServerSettings::EnableLobbyStatistics
	bool ___EnableLobbyStatistics_15;
	// PhotonLogLevel ServerSettings::PunLogging
	int32_t ___PunLogging_16;
	// ExitGames.Client.Photon.DebugLevel ServerSettings::NetworkLogging
	uint8_t ___NetworkLogging_17;
	// System.Boolean ServerSettings::RunInBackground
	bool ___RunInBackground_18;
	// System.Collections.Generic.List`1<System.String> ServerSettings::RpcList
	List_1_t3319525431 * ___RpcList_19;
	// System.Boolean ServerSettings::DisableAutoOpenWizard
	bool ___DisableAutoOpenWizard_20;

public:
	inline static int32_t get_offset_of_AppID_4() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___AppID_4)); }
	inline String_t* get_AppID_4() const { return ___AppID_4; }
	inline String_t** get_address_of_AppID_4() { return &___AppID_4; }
	inline void set_AppID_4(String_t* value)
	{
		___AppID_4 = value;
		Il2CppCodeGenWriteBarrier((&___AppID_4), value);
	}

	inline static int32_t get_offset_of_VoiceAppID_5() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___VoiceAppID_5)); }
	inline String_t* get_VoiceAppID_5() const { return ___VoiceAppID_5; }
	inline String_t** get_address_of_VoiceAppID_5() { return &___VoiceAppID_5; }
	inline void set_VoiceAppID_5(String_t* value)
	{
		___VoiceAppID_5 = value;
		Il2CppCodeGenWriteBarrier((&___VoiceAppID_5), value);
	}

	inline static int32_t get_offset_of_ChatAppID_6() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___ChatAppID_6)); }
	inline String_t* get_ChatAppID_6() const { return ___ChatAppID_6; }
	inline String_t** get_address_of_ChatAppID_6() { return &___ChatAppID_6; }
	inline void set_ChatAppID_6(String_t* value)
	{
		___ChatAppID_6 = value;
		Il2CppCodeGenWriteBarrier((&___ChatAppID_6), value);
	}

	inline static int32_t get_offset_of_HostType_7() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___HostType_7)); }
	inline int32_t get_HostType_7() const { return ___HostType_7; }
	inline int32_t* get_address_of_HostType_7() { return &___HostType_7; }
	inline void set_HostType_7(int32_t value)
	{
		___HostType_7 = value;
	}

	inline static int32_t get_offset_of_PreferredRegion_8() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___PreferredRegion_8)); }
	inline int32_t get_PreferredRegion_8() const { return ___PreferredRegion_8; }
	inline int32_t* get_address_of_PreferredRegion_8() { return &___PreferredRegion_8; }
	inline void set_PreferredRegion_8(int32_t value)
	{
		___PreferredRegion_8 = value;
	}

	inline static int32_t get_offset_of_EnabledRegions_9() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___EnabledRegions_9)); }
	inline int32_t get_EnabledRegions_9() const { return ___EnabledRegions_9; }
	inline int32_t* get_address_of_EnabledRegions_9() { return &___EnabledRegions_9; }
	inline void set_EnabledRegions_9(int32_t value)
	{
		___EnabledRegions_9 = value;
	}

	inline static int32_t get_offset_of_Protocol_10() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___Protocol_10)); }
	inline uint8_t get_Protocol_10() const { return ___Protocol_10; }
	inline uint8_t* get_address_of_Protocol_10() { return &___Protocol_10; }
	inline void set_Protocol_10(uint8_t value)
	{
		___Protocol_10 = value;
	}

	inline static int32_t get_offset_of_ServerAddress_11() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___ServerAddress_11)); }
	inline String_t* get_ServerAddress_11() const { return ___ServerAddress_11; }
	inline String_t** get_address_of_ServerAddress_11() { return &___ServerAddress_11; }
	inline void set_ServerAddress_11(String_t* value)
	{
		___ServerAddress_11 = value;
		Il2CppCodeGenWriteBarrier((&___ServerAddress_11), value);
	}

	inline static int32_t get_offset_of_ServerPort_12() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___ServerPort_12)); }
	inline int32_t get_ServerPort_12() const { return ___ServerPort_12; }
	inline int32_t* get_address_of_ServerPort_12() { return &___ServerPort_12; }
	inline void set_ServerPort_12(int32_t value)
	{
		___ServerPort_12 = value;
	}

	inline static int32_t get_offset_of_VoiceServerPort_13() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___VoiceServerPort_13)); }
	inline int32_t get_VoiceServerPort_13() const { return ___VoiceServerPort_13; }
	inline int32_t* get_address_of_VoiceServerPort_13() { return &___VoiceServerPort_13; }
	inline void set_VoiceServerPort_13(int32_t value)
	{
		___VoiceServerPort_13 = value;
	}

	inline static int32_t get_offset_of_JoinLobby_14() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___JoinLobby_14)); }
	inline bool get_JoinLobby_14() const { return ___JoinLobby_14; }
	inline bool* get_address_of_JoinLobby_14() { return &___JoinLobby_14; }
	inline void set_JoinLobby_14(bool value)
	{
		___JoinLobby_14 = value;
	}

	inline static int32_t get_offset_of_EnableLobbyStatistics_15() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___EnableLobbyStatistics_15)); }
	inline bool get_EnableLobbyStatistics_15() const { return ___EnableLobbyStatistics_15; }
	inline bool* get_address_of_EnableLobbyStatistics_15() { return &___EnableLobbyStatistics_15; }
	inline void set_EnableLobbyStatistics_15(bool value)
	{
		___EnableLobbyStatistics_15 = value;
	}

	inline static int32_t get_offset_of_PunLogging_16() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___PunLogging_16)); }
	inline int32_t get_PunLogging_16() const { return ___PunLogging_16; }
	inline int32_t* get_address_of_PunLogging_16() { return &___PunLogging_16; }
	inline void set_PunLogging_16(int32_t value)
	{
		___PunLogging_16 = value;
	}

	inline static int32_t get_offset_of_NetworkLogging_17() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___NetworkLogging_17)); }
	inline uint8_t get_NetworkLogging_17() const { return ___NetworkLogging_17; }
	inline uint8_t* get_address_of_NetworkLogging_17() { return &___NetworkLogging_17; }
	inline void set_NetworkLogging_17(uint8_t value)
	{
		___NetworkLogging_17 = value;
	}

	inline static int32_t get_offset_of_RunInBackground_18() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___RunInBackground_18)); }
	inline bool get_RunInBackground_18() const { return ___RunInBackground_18; }
	inline bool* get_address_of_RunInBackground_18() { return &___RunInBackground_18; }
	inline void set_RunInBackground_18(bool value)
	{
		___RunInBackground_18 = value;
	}

	inline static int32_t get_offset_of_RpcList_19() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___RpcList_19)); }
	inline List_1_t3319525431 * get_RpcList_19() const { return ___RpcList_19; }
	inline List_1_t3319525431 ** get_address_of_RpcList_19() { return &___RpcList_19; }
	inline void set_RpcList_19(List_1_t3319525431 * value)
	{
		___RpcList_19 = value;
		Il2CppCodeGenWriteBarrier((&___RpcList_19), value);
	}

	inline static int32_t get_offset_of_DisableAutoOpenWizard_20() { return static_cast<int32_t>(offsetof(ServerSettings_t2755303613, ___DisableAutoOpenWizard_20)); }
	inline bool get_DisableAutoOpenWizard_20() const { return ___DisableAutoOpenWizard_20; }
	inline bool* get_address_of_DisableAutoOpenWizard_20() { return &___DisableAutoOpenWizard_20; }
	inline void set_DisableAutoOpenWizard_20(bool value)
	{
		___DisableAutoOpenWizard_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSETTINGS_T2755303613_H
#ifndef TYPEDLOBBYINFO_T2504508049_H
#define TYPEDLOBBYINFO_T2504508049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TypedLobbyInfo
struct  TypedLobbyInfo_t2504508049  : public TypedLobby_t3336582029
{
public:
	// System.Int32 TypedLobbyInfo::PlayerCount
	int32_t ___PlayerCount_3;
	// System.Int32 TypedLobbyInfo::RoomCount
	int32_t ___RoomCount_4;

public:
	inline static int32_t get_offset_of_PlayerCount_3() { return static_cast<int32_t>(offsetof(TypedLobbyInfo_t2504508049, ___PlayerCount_3)); }
	inline int32_t get_PlayerCount_3() const { return ___PlayerCount_3; }
	inline int32_t* get_address_of_PlayerCount_3() { return &___PlayerCount_3; }
	inline void set_PlayerCount_3(int32_t value)
	{
		___PlayerCount_3 = value;
	}

	inline static int32_t get_offset_of_RoomCount_4() { return static_cast<int32_t>(offsetof(TypedLobbyInfo_t2504508049, ___RoomCount_4)); }
	inline int32_t get_RoomCount_4() const { return ___RoomCount_4; }
	inline int32_t* get_address_of_RoomCount_4() { return &___RoomCount_4; }
	inline void set_RoomCount_4(int32_t value)
	{
		___RoomCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDLOBBYINFO_T2504508049_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef CANVASGROUP_T4083511760_H
#define CANVASGROUP_T4083511760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_t4083511760  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_T4083511760_H
#ifndef GUISKIN_T1244372282_H
#define GUISKIN_T1244372282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUISkin
struct  GUISkin_t1244372282  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_t1956802104 * ___m_Font_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t3956901511 * ___m_box_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t3956901511 * ___m_button_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t3956901511 * ___m_toggle_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t3956901511 * ___m_label_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t3956901511 * ___m_textField_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t3956901511 * ___m_textArea_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t3956901511 * ___m_window_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t3956901511 * ___m_horizontalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t3956901511 * ___m_horizontalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t3956901511 * ___m_verticalSlider_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t3956901511 * ___m_verticalSliderThumb_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t3956901511 * ___m_horizontalScrollbar_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t3956901511 * ___m_horizontalScrollbarThumb_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarLeftButton_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarRightButton_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t3956901511 * ___m_verticalScrollbar_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t3956901511 * ___m_verticalScrollbarThumb_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarUpButton_22;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarDownButton_23;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t3956901511 * ___m_ScrollView_24;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t2383250302* ___m_CustomStyles_25;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_t1774757634 * ___m_Settings_26;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_t3742157810 * ___m_Styles_28;

public:
	inline static int32_t get_offset_of_m_Font_4() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Font_4)); }
	inline Font_t1956802104 * get_m_Font_4() const { return ___m_Font_4; }
	inline Font_t1956802104 ** get_address_of_m_Font_4() { return &___m_Font_4; }
	inline void set_m_Font_4(Font_t1956802104 * value)
	{
		___m_Font_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Font_4), value);
	}

	inline static int32_t get_offset_of_m_box_5() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_box_5)); }
	inline GUIStyle_t3956901511 * get_m_box_5() const { return ___m_box_5; }
	inline GUIStyle_t3956901511 ** get_address_of_m_box_5() { return &___m_box_5; }
	inline void set_m_box_5(GUIStyle_t3956901511 * value)
	{
		___m_box_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_box_5), value);
	}

	inline static int32_t get_offset_of_m_button_6() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_button_6)); }
	inline GUIStyle_t3956901511 * get_m_button_6() const { return ___m_button_6; }
	inline GUIStyle_t3956901511 ** get_address_of_m_button_6() { return &___m_button_6; }
	inline void set_m_button_6(GUIStyle_t3956901511 * value)
	{
		___m_button_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_6), value);
	}

	inline static int32_t get_offset_of_m_toggle_7() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_toggle_7)); }
	inline GUIStyle_t3956901511 * get_m_toggle_7() const { return ___m_toggle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_m_toggle_7() { return &___m_toggle_7; }
	inline void set_m_toggle_7(GUIStyle_t3956901511 * value)
	{
		___m_toggle_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggle_7), value);
	}

	inline static int32_t get_offset_of_m_label_8() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_label_8)); }
	inline GUIStyle_t3956901511 * get_m_label_8() const { return ___m_label_8; }
	inline GUIStyle_t3956901511 ** get_address_of_m_label_8() { return &___m_label_8; }
	inline void set_m_label_8(GUIStyle_t3956901511 * value)
	{
		___m_label_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_label_8), value);
	}

	inline static int32_t get_offset_of_m_textField_9() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textField_9)); }
	inline GUIStyle_t3956901511 * get_m_textField_9() const { return ___m_textField_9; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textField_9() { return &___m_textField_9; }
	inline void set_m_textField_9(GUIStyle_t3956901511 * value)
	{
		___m_textField_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textField_9), value);
	}

	inline static int32_t get_offset_of_m_textArea_10() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textArea_10)); }
	inline GUIStyle_t3956901511 * get_m_textArea_10() const { return ___m_textArea_10; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textArea_10() { return &___m_textArea_10; }
	inline void set_m_textArea_10(GUIStyle_t3956901511 * value)
	{
		___m_textArea_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_textArea_10), value);
	}

	inline static int32_t get_offset_of_m_window_11() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_window_11)); }
	inline GUIStyle_t3956901511 * get_m_window_11() const { return ___m_window_11; }
	inline GUIStyle_t3956901511 ** get_address_of_m_window_11() { return &___m_window_11; }
	inline void set_m_window_11(GUIStyle_t3956901511 * value)
	{
		___m_window_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_window_11), value);
	}

	inline static int32_t get_offset_of_m_horizontalSlider_12() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSlider_12)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSlider_12() const { return ___m_horizontalSlider_12; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSlider_12() { return &___m_horizontalSlider_12; }
	inline void set_m_horizontalSlider_12(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSlider_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSlider_12), value);
	}

	inline static int32_t get_offset_of_m_horizontalSliderThumb_13() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSliderThumb_13)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSliderThumb_13() const { return ___m_horizontalSliderThumb_13; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSliderThumb_13() { return &___m_horizontalSliderThumb_13; }
	inline void set_m_horizontalSliderThumb_13(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSliderThumb_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSliderThumb_13), value);
	}

	inline static int32_t get_offset_of_m_verticalSlider_14() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSlider_14)); }
	inline GUIStyle_t3956901511 * get_m_verticalSlider_14() const { return ___m_verticalSlider_14; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSlider_14() { return &___m_verticalSlider_14; }
	inline void set_m_verticalSlider_14(GUIStyle_t3956901511 * value)
	{
		___m_verticalSlider_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSlider_14), value);
	}

	inline static int32_t get_offset_of_m_verticalSliderThumb_15() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSliderThumb_15)); }
	inline GUIStyle_t3956901511 * get_m_verticalSliderThumb_15() const { return ___m_verticalSliderThumb_15; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSliderThumb_15() { return &___m_verticalSliderThumb_15; }
	inline void set_m_verticalSliderThumb_15(GUIStyle_t3956901511 * value)
	{
		___m_verticalSliderThumb_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSliderThumb_15), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbar_16() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbar_16)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbar_16() const { return ___m_horizontalScrollbar_16; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbar_16() { return &___m_horizontalScrollbar_16; }
	inline void set_m_horizontalScrollbar_16(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbar_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbar_16), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarThumb_17() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarThumb_17)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarThumb_17() const { return ___m_horizontalScrollbarThumb_17; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarThumb_17() { return &___m_horizontalScrollbarThumb_17; }
	inline void set_m_horizontalScrollbarThumb_17(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarThumb_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarThumb_17), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarLeftButton_18() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarLeftButton_18)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarLeftButton_18() const { return ___m_horizontalScrollbarLeftButton_18; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarLeftButton_18() { return &___m_horizontalScrollbarLeftButton_18; }
	inline void set_m_horizontalScrollbarLeftButton_18(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarLeftButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarLeftButton_18), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarRightButton_19() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarRightButton_19)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarRightButton_19() const { return ___m_horizontalScrollbarRightButton_19; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarRightButton_19() { return &___m_horizontalScrollbarRightButton_19; }
	inline void set_m_horizontalScrollbarRightButton_19(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarRightButton_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarRightButton_19), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbar_20() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbar_20)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbar_20() const { return ___m_verticalScrollbar_20; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbar_20() { return &___m_verticalScrollbar_20; }
	inline void set_m_verticalScrollbar_20(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbar_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbar_20), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarThumb_21() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarThumb_21)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarThumb_21() const { return ___m_verticalScrollbarThumb_21; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarThumb_21() { return &___m_verticalScrollbarThumb_21; }
	inline void set_m_verticalScrollbarThumb_21(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarThumb_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarThumb_21), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarUpButton_22() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarUpButton_22)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarUpButton_22() const { return ___m_verticalScrollbarUpButton_22; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarUpButton_22() { return &___m_verticalScrollbarUpButton_22; }
	inline void set_m_verticalScrollbarUpButton_22(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarUpButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarUpButton_22), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarDownButton_23() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarDownButton_23)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarDownButton_23() const { return ___m_verticalScrollbarDownButton_23; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarDownButton_23() { return &___m_verticalScrollbarDownButton_23; }
	inline void set_m_verticalScrollbarDownButton_23(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarDownButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarDownButton_23), value);
	}

	inline static int32_t get_offset_of_m_ScrollView_24() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_ScrollView_24)); }
	inline GUIStyle_t3956901511 * get_m_ScrollView_24() const { return ___m_ScrollView_24; }
	inline GUIStyle_t3956901511 ** get_address_of_m_ScrollView_24() { return &___m_ScrollView_24; }
	inline void set_m_ScrollView_24(GUIStyle_t3956901511 * value)
	{
		___m_ScrollView_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScrollView_24), value);
	}

	inline static int32_t get_offset_of_m_CustomStyles_25() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_CustomStyles_25)); }
	inline GUIStyleU5BU5D_t2383250302* get_m_CustomStyles_25() const { return ___m_CustomStyles_25; }
	inline GUIStyleU5BU5D_t2383250302** get_address_of_m_CustomStyles_25() { return &___m_CustomStyles_25; }
	inline void set_m_CustomStyles_25(GUIStyleU5BU5D_t2383250302* value)
	{
		___m_CustomStyles_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomStyles_25), value);
	}

	inline static int32_t get_offset_of_m_Settings_26() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Settings_26)); }
	inline GUISettings_t1774757634 * get_m_Settings_26() const { return ___m_Settings_26; }
	inline GUISettings_t1774757634 ** get_address_of_m_Settings_26() { return &___m_Settings_26; }
	inline void set_m_Settings_26(GUISettings_t1774757634 * value)
	{
		___m_Settings_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_26), value);
	}

	inline static int32_t get_offset_of_m_Styles_28() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Styles_28)); }
	inline Dictionary_2_t3742157810 * get_m_Styles_28() const { return ___m_Styles_28; }
	inline Dictionary_2_t3742157810 ** get_address_of_m_Styles_28() { return &___m_Styles_28; }
	inline void set_m_Styles_28(Dictionary_2_t3742157810 * value)
	{
		___m_Styles_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Styles_28), value);
	}
};

struct GUISkin_t1244372282_StaticFields
{
public:
	// UnityEngine.GUIStyle UnityEngine.GUISkin::ms_Error
	GUIStyle_t3956901511 * ___ms_Error_27;
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_t1143955295 * ___m_SkinChanged_29;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t1244372282 * ___current_30;

public:
	inline static int32_t get_offset_of_ms_Error_27() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___ms_Error_27)); }
	inline GUIStyle_t3956901511 * get_ms_Error_27() const { return ___ms_Error_27; }
	inline GUIStyle_t3956901511 ** get_address_of_ms_Error_27() { return &___ms_Error_27; }
	inline void set_ms_Error_27(GUIStyle_t3956901511 * value)
	{
		___ms_Error_27 = value;
		Il2CppCodeGenWriteBarrier((&___ms_Error_27), value);
	}

	inline static int32_t get_offset_of_m_SkinChanged_29() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___m_SkinChanged_29)); }
	inline SkinChangedDelegate_t1143955295 * get_m_SkinChanged_29() const { return ___m_SkinChanged_29; }
	inline SkinChangedDelegate_t1143955295 ** get_address_of_m_SkinChanged_29() { return &___m_SkinChanged_29; }
	inline void set_m_SkinChanged_29(SkinChangedDelegate_t1143955295 * value)
	{
		___m_SkinChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkinChanged_29), value);
	}

	inline static int32_t get_offset_of_current_30() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___current_30)); }
	inline GUISkin_t1244372282 * get_current_30() const { return ___current_30; }
	inline GUISkin_t1244372282 ** get_address_of_current_30() { return &___current_30; }
	inline void set_current_30(GUISkin_t1244372282 * value)
	{
		___current_30 = value;
		Il2CppCodeGenWriteBarrier((&___current_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISKIN_T1244372282_H
#ifndef RIGIDBODY2D_T939494601_H
#define RIGIDBODY2D_T939494601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t939494601  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T939494601_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef NETWORKINGPEER_T264212356_H
#define NETWORKINGPEER_T264212356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkingPeer
struct  NetworkingPeer_t264212356  : public LoadBalancingPeer_t3218467959
{
public:
	// System.String NetworkingPeer::AppId
	String_t* ___AppId_41;
	// AuthenticationValues NetworkingPeer::<AuthValues>k__BackingField
	AuthenticationValues_t660572511 * ___U3CAuthValuesU3Ek__BackingField_42;
	// System.String NetworkingPeer::tokenCache
	String_t* ___tokenCache_43;
	// AuthModeOption NetworkingPeer::AuthMode
	int32_t ___AuthMode_44;
	// EncryptionMode NetworkingPeer::EncryptionMode
	int32_t ___EncryptionMode_45;
	// System.Boolean NetworkingPeer::<IsUsingNameServer>k__BackingField
	bool ___U3CIsUsingNameServerU3Ek__BackingField_46;
	// System.String NetworkingPeer::<MasterServerAddress>k__BackingField
	String_t* ___U3CMasterServerAddressU3Ek__BackingField_50;
	// System.String NetworkingPeer::<GameServerAddress>k__BackingField
	String_t* ___U3CGameServerAddressU3Ek__BackingField_51;
	// ServerConnection NetworkingPeer::<Server>k__BackingField
	int32_t ___U3CServerU3Ek__BackingField_52;
	// ClientState NetworkingPeer::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_53;
	// System.Boolean NetworkingPeer::IsInitialConnect
	bool ___IsInitialConnect_54;
	// System.Boolean NetworkingPeer::insideLobby
	bool ___insideLobby_55;
	// TypedLobby NetworkingPeer::<lobby>k__BackingField
	TypedLobby_t3336582029 * ___U3ClobbyU3Ek__BackingField_56;
	// System.Collections.Generic.List`1<TypedLobbyInfo> NetworkingPeer::LobbyStatistics
	List_1_t3976582791 * ___LobbyStatistics_57;
	// System.Collections.Generic.Dictionary`2<System.String,RoomInfo> NetworkingPeer::mGameList
	Dictionary_2_t2955551919 * ___mGameList_58;
	// RoomInfo[] NetworkingPeer::mGameListCopy
	RoomInfoU5BU5D_t1491207981* ___mGameListCopy_59;
	// System.String NetworkingPeer::playername
	String_t* ___playername_60;
	// System.Boolean NetworkingPeer::mPlayernameHasToBeUpdated
	bool ___mPlayernameHasToBeUpdated_61;
	// Room NetworkingPeer::currentRoom
	Room_t3759828263 * ___currentRoom_62;
	// PhotonPlayer NetworkingPeer::<LocalPlayer>k__BackingField
	PhotonPlayer_t3305149557 * ___U3CLocalPlayerU3Ek__BackingField_63;
	// System.Int32 NetworkingPeer::<PlayersOnMasterCount>k__BackingField
	int32_t ___U3CPlayersOnMasterCountU3Ek__BackingField_64;
	// System.Int32 NetworkingPeer::<PlayersInRoomsCount>k__BackingField
	int32_t ___U3CPlayersInRoomsCountU3Ek__BackingField_65;
	// System.Int32 NetworkingPeer::<RoomsCount>k__BackingField
	int32_t ___U3CRoomsCountU3Ek__BackingField_66;
	// JoinType NetworkingPeer::lastJoinType
	int32_t ___lastJoinType_67;
	// EnterRoomParams NetworkingPeer::enterRoomParamsCache
	EnterRoomParams_t3960472384 * ___enterRoomParamsCache_68;
	// System.Boolean NetworkingPeer::didAuthenticate
	bool ___didAuthenticate_69;
	// System.String[] NetworkingPeer::friendListRequested
	StringU5BU5D_t1281789340* ___friendListRequested_70;
	// System.Int32 NetworkingPeer::friendListTimestamp
	int32_t ___friendListTimestamp_71;
	// System.Boolean NetworkingPeer::isFetchingFriendList
	bool ___isFetchingFriendList_72;
	// System.Collections.Generic.List`1<Region> NetworkingPeer::<AvailableRegions>k__BackingField
	List_1_t861332708 * ___U3CAvailableRegionsU3Ek__BackingField_73;
	// CloudRegionCode NetworkingPeer::<CloudRegion>k__BackingField
	int32_t ___U3CCloudRegionU3Ek__BackingField_74;
	// System.Collections.Generic.Dictionary`2<System.Int32,PhotonPlayer> NetworkingPeer::mActors
	Dictionary_2_t2193862888 * ___mActors_75;
	// PhotonPlayer[] NetworkingPeer::mOtherPlayerListCopy
	PhotonPlayerU5BU5D_t2880637464* ___mOtherPlayerListCopy_76;
	// PhotonPlayer[] NetworkingPeer::mPlayerListCopy
	PhotonPlayerU5BU5D_t2880637464* ___mPlayerListCopy_77;
	// System.Boolean NetworkingPeer::hasSwitchedMC
	bool ___hasSwitchedMC_78;
	// System.Collections.Generic.HashSet`1<System.Byte> NetworkingPeer::allowedReceivingGroups
	HashSet_1_t3994213146 * ___allowedReceivingGroups_79;
	// System.Collections.Generic.HashSet`1<System.Byte> NetworkingPeer::blockSendingGroups
	HashSet_1_t3994213146 * ___blockSendingGroups_80;
	// System.Collections.Generic.Dictionary`2<System.Int32,PhotonView> NetworkingPeer::photonViewList
	Dictionary_2_t1096435151 * ___photonViewList_81;
	// PhotonStream NetworkingPeer::readStream
	PhotonStream_t1003850889 * ___readStream_82;
	// PhotonStream NetworkingPeer::pStream
	PhotonStream_t1003850889 * ___pStream_83;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.Hashtable> NetworkingPeer::dataPerGroupReliable
	Dictionary_2_t4231889829 * ___dataPerGroupReliable_84;
	// System.Collections.Generic.Dictionary`2<System.Int32,ExitGames.Client.Photon.Hashtable> NetworkingPeer::dataPerGroupUnreliable
	Dictionary_2_t4231889829 * ___dataPerGroupUnreliable_85;
	// System.Int16 NetworkingPeer::currentLevelPrefix
	int16_t ___currentLevelPrefix_86;
	// System.Boolean NetworkingPeer::loadingLevelAndPausedNetwork
	bool ___loadingLevelAndPausedNetwork_87;
	// IPunPrefabPool NetworkingPeer::ObjectPool
	RuntimeObject* ___ObjectPool_91;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>> NetworkingPeer::monoRPCMethodsCache
	Dictionary_2_t1499080758 * ___monoRPCMethodsCache_93;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> NetworkingPeer::rpcShortcuts
	Dictionary_2_t2736202052 * ___rpcShortcuts_94;
	// System.String NetworkingPeer::cachedServerAddress
	String_t* ___cachedServerAddress_96;
	// System.String NetworkingPeer::cachedApplicationName
	String_t* ___cachedApplicationName_97;
	// ServerConnection NetworkingPeer::cachedServerType
	int32_t ___cachedServerType_98;
	// UnityEngine.AsyncOperation NetworkingPeer::_AsyncLevelLoadingOperation
	AsyncOperation_t1445031843 * ____AsyncLevelLoadingOperation_99;
	// RaiseEventOptions NetworkingPeer::_levelReloadEventOptions
	RaiseEventOptions_t1229553678 * ____levelReloadEventOptions_100;
	// System.Boolean NetworkingPeer::_isReconnecting
	bool ____isReconnecting_101;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Object[]> NetworkingPeer::tempInstantiationData
	Dictionary_2_t1732652656 * ___tempInstantiationData_102;
	// RaiseEventOptions NetworkingPeer::options
	RaiseEventOptions_t1229553678 * ___options_104;
	// System.Boolean NetworkingPeer::IsReloadingLevel
	bool ___IsReloadingLevel_109;
	// System.Boolean NetworkingPeer::AsynchLevelLoadCall
	bool ___AsynchLevelLoadCall_110;

public:
	inline static int32_t get_offset_of_AppId_41() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___AppId_41)); }
	inline String_t* get_AppId_41() const { return ___AppId_41; }
	inline String_t** get_address_of_AppId_41() { return &___AppId_41; }
	inline void set_AppId_41(String_t* value)
	{
		___AppId_41 = value;
		Il2CppCodeGenWriteBarrier((&___AppId_41), value);
	}

	inline static int32_t get_offset_of_U3CAuthValuesU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CAuthValuesU3Ek__BackingField_42)); }
	inline AuthenticationValues_t660572511 * get_U3CAuthValuesU3Ek__BackingField_42() const { return ___U3CAuthValuesU3Ek__BackingField_42; }
	inline AuthenticationValues_t660572511 ** get_address_of_U3CAuthValuesU3Ek__BackingField_42() { return &___U3CAuthValuesU3Ek__BackingField_42; }
	inline void set_U3CAuthValuesU3Ek__BackingField_42(AuthenticationValues_t660572511 * value)
	{
		___U3CAuthValuesU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthValuesU3Ek__BackingField_42), value);
	}

	inline static int32_t get_offset_of_tokenCache_43() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___tokenCache_43)); }
	inline String_t* get_tokenCache_43() const { return ___tokenCache_43; }
	inline String_t** get_address_of_tokenCache_43() { return &___tokenCache_43; }
	inline void set_tokenCache_43(String_t* value)
	{
		___tokenCache_43 = value;
		Il2CppCodeGenWriteBarrier((&___tokenCache_43), value);
	}

	inline static int32_t get_offset_of_AuthMode_44() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___AuthMode_44)); }
	inline int32_t get_AuthMode_44() const { return ___AuthMode_44; }
	inline int32_t* get_address_of_AuthMode_44() { return &___AuthMode_44; }
	inline void set_AuthMode_44(int32_t value)
	{
		___AuthMode_44 = value;
	}

	inline static int32_t get_offset_of_EncryptionMode_45() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___EncryptionMode_45)); }
	inline int32_t get_EncryptionMode_45() const { return ___EncryptionMode_45; }
	inline int32_t* get_address_of_EncryptionMode_45() { return &___EncryptionMode_45; }
	inline void set_EncryptionMode_45(int32_t value)
	{
		___EncryptionMode_45 = value;
	}

	inline static int32_t get_offset_of_U3CIsUsingNameServerU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CIsUsingNameServerU3Ek__BackingField_46)); }
	inline bool get_U3CIsUsingNameServerU3Ek__BackingField_46() const { return ___U3CIsUsingNameServerU3Ek__BackingField_46; }
	inline bool* get_address_of_U3CIsUsingNameServerU3Ek__BackingField_46() { return &___U3CIsUsingNameServerU3Ek__BackingField_46; }
	inline void set_U3CIsUsingNameServerU3Ek__BackingField_46(bool value)
	{
		___U3CIsUsingNameServerU3Ek__BackingField_46 = value;
	}

	inline static int32_t get_offset_of_U3CMasterServerAddressU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CMasterServerAddressU3Ek__BackingField_50)); }
	inline String_t* get_U3CMasterServerAddressU3Ek__BackingField_50() const { return ___U3CMasterServerAddressU3Ek__BackingField_50; }
	inline String_t** get_address_of_U3CMasterServerAddressU3Ek__BackingField_50() { return &___U3CMasterServerAddressU3Ek__BackingField_50; }
	inline void set_U3CMasterServerAddressU3Ek__BackingField_50(String_t* value)
	{
		___U3CMasterServerAddressU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMasterServerAddressU3Ek__BackingField_50), value);
	}

	inline static int32_t get_offset_of_U3CGameServerAddressU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CGameServerAddressU3Ek__BackingField_51)); }
	inline String_t* get_U3CGameServerAddressU3Ek__BackingField_51() const { return ___U3CGameServerAddressU3Ek__BackingField_51; }
	inline String_t** get_address_of_U3CGameServerAddressU3Ek__BackingField_51() { return &___U3CGameServerAddressU3Ek__BackingField_51; }
	inline void set_U3CGameServerAddressU3Ek__BackingField_51(String_t* value)
	{
		___U3CGameServerAddressU3Ek__BackingField_51 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameServerAddressU3Ek__BackingField_51), value);
	}

	inline static int32_t get_offset_of_U3CServerU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CServerU3Ek__BackingField_52)); }
	inline int32_t get_U3CServerU3Ek__BackingField_52() const { return ___U3CServerU3Ek__BackingField_52; }
	inline int32_t* get_address_of_U3CServerU3Ek__BackingField_52() { return &___U3CServerU3Ek__BackingField_52; }
	inline void set_U3CServerU3Ek__BackingField_52(int32_t value)
	{
		___U3CServerU3Ek__BackingField_52 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CStateU3Ek__BackingField_53)); }
	inline int32_t get_U3CStateU3Ek__BackingField_53() const { return ___U3CStateU3Ek__BackingField_53; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_53() { return &___U3CStateU3Ek__BackingField_53; }
	inline void set_U3CStateU3Ek__BackingField_53(int32_t value)
	{
		___U3CStateU3Ek__BackingField_53 = value;
	}

	inline static int32_t get_offset_of_IsInitialConnect_54() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___IsInitialConnect_54)); }
	inline bool get_IsInitialConnect_54() const { return ___IsInitialConnect_54; }
	inline bool* get_address_of_IsInitialConnect_54() { return &___IsInitialConnect_54; }
	inline void set_IsInitialConnect_54(bool value)
	{
		___IsInitialConnect_54 = value;
	}

	inline static int32_t get_offset_of_insideLobby_55() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___insideLobby_55)); }
	inline bool get_insideLobby_55() const { return ___insideLobby_55; }
	inline bool* get_address_of_insideLobby_55() { return &___insideLobby_55; }
	inline void set_insideLobby_55(bool value)
	{
		___insideLobby_55 = value;
	}

	inline static int32_t get_offset_of_U3ClobbyU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3ClobbyU3Ek__BackingField_56)); }
	inline TypedLobby_t3336582029 * get_U3ClobbyU3Ek__BackingField_56() const { return ___U3ClobbyU3Ek__BackingField_56; }
	inline TypedLobby_t3336582029 ** get_address_of_U3ClobbyU3Ek__BackingField_56() { return &___U3ClobbyU3Ek__BackingField_56; }
	inline void set_U3ClobbyU3Ek__BackingField_56(TypedLobby_t3336582029 * value)
	{
		___U3ClobbyU3Ek__BackingField_56 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClobbyU3Ek__BackingField_56), value);
	}

	inline static int32_t get_offset_of_LobbyStatistics_57() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___LobbyStatistics_57)); }
	inline List_1_t3976582791 * get_LobbyStatistics_57() const { return ___LobbyStatistics_57; }
	inline List_1_t3976582791 ** get_address_of_LobbyStatistics_57() { return &___LobbyStatistics_57; }
	inline void set_LobbyStatistics_57(List_1_t3976582791 * value)
	{
		___LobbyStatistics_57 = value;
		Il2CppCodeGenWriteBarrier((&___LobbyStatistics_57), value);
	}

	inline static int32_t get_offset_of_mGameList_58() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mGameList_58)); }
	inline Dictionary_2_t2955551919 * get_mGameList_58() const { return ___mGameList_58; }
	inline Dictionary_2_t2955551919 ** get_address_of_mGameList_58() { return &___mGameList_58; }
	inline void set_mGameList_58(Dictionary_2_t2955551919 * value)
	{
		___mGameList_58 = value;
		Il2CppCodeGenWriteBarrier((&___mGameList_58), value);
	}

	inline static int32_t get_offset_of_mGameListCopy_59() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mGameListCopy_59)); }
	inline RoomInfoU5BU5D_t1491207981* get_mGameListCopy_59() const { return ___mGameListCopy_59; }
	inline RoomInfoU5BU5D_t1491207981** get_address_of_mGameListCopy_59() { return &___mGameListCopy_59; }
	inline void set_mGameListCopy_59(RoomInfoU5BU5D_t1491207981* value)
	{
		___mGameListCopy_59 = value;
		Il2CppCodeGenWriteBarrier((&___mGameListCopy_59), value);
	}

	inline static int32_t get_offset_of_playername_60() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___playername_60)); }
	inline String_t* get_playername_60() const { return ___playername_60; }
	inline String_t** get_address_of_playername_60() { return &___playername_60; }
	inline void set_playername_60(String_t* value)
	{
		___playername_60 = value;
		Il2CppCodeGenWriteBarrier((&___playername_60), value);
	}

	inline static int32_t get_offset_of_mPlayernameHasToBeUpdated_61() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mPlayernameHasToBeUpdated_61)); }
	inline bool get_mPlayernameHasToBeUpdated_61() const { return ___mPlayernameHasToBeUpdated_61; }
	inline bool* get_address_of_mPlayernameHasToBeUpdated_61() { return &___mPlayernameHasToBeUpdated_61; }
	inline void set_mPlayernameHasToBeUpdated_61(bool value)
	{
		___mPlayernameHasToBeUpdated_61 = value;
	}

	inline static int32_t get_offset_of_currentRoom_62() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___currentRoom_62)); }
	inline Room_t3759828263 * get_currentRoom_62() const { return ___currentRoom_62; }
	inline Room_t3759828263 ** get_address_of_currentRoom_62() { return &___currentRoom_62; }
	inline void set_currentRoom_62(Room_t3759828263 * value)
	{
		___currentRoom_62 = value;
		Il2CppCodeGenWriteBarrier((&___currentRoom_62), value);
	}

	inline static int32_t get_offset_of_U3CLocalPlayerU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CLocalPlayerU3Ek__BackingField_63)); }
	inline PhotonPlayer_t3305149557 * get_U3CLocalPlayerU3Ek__BackingField_63() const { return ___U3CLocalPlayerU3Ek__BackingField_63; }
	inline PhotonPlayer_t3305149557 ** get_address_of_U3CLocalPlayerU3Ek__BackingField_63() { return &___U3CLocalPlayerU3Ek__BackingField_63; }
	inline void set_U3CLocalPlayerU3Ek__BackingField_63(PhotonPlayer_t3305149557 * value)
	{
		___U3CLocalPlayerU3Ek__BackingField_63 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLocalPlayerU3Ek__BackingField_63), value);
	}

	inline static int32_t get_offset_of_U3CPlayersOnMasterCountU3Ek__BackingField_64() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CPlayersOnMasterCountU3Ek__BackingField_64)); }
	inline int32_t get_U3CPlayersOnMasterCountU3Ek__BackingField_64() const { return ___U3CPlayersOnMasterCountU3Ek__BackingField_64; }
	inline int32_t* get_address_of_U3CPlayersOnMasterCountU3Ek__BackingField_64() { return &___U3CPlayersOnMasterCountU3Ek__BackingField_64; }
	inline void set_U3CPlayersOnMasterCountU3Ek__BackingField_64(int32_t value)
	{
		___U3CPlayersOnMasterCountU3Ek__BackingField_64 = value;
	}

	inline static int32_t get_offset_of_U3CPlayersInRoomsCountU3Ek__BackingField_65() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CPlayersInRoomsCountU3Ek__BackingField_65)); }
	inline int32_t get_U3CPlayersInRoomsCountU3Ek__BackingField_65() const { return ___U3CPlayersInRoomsCountU3Ek__BackingField_65; }
	inline int32_t* get_address_of_U3CPlayersInRoomsCountU3Ek__BackingField_65() { return &___U3CPlayersInRoomsCountU3Ek__BackingField_65; }
	inline void set_U3CPlayersInRoomsCountU3Ek__BackingField_65(int32_t value)
	{
		___U3CPlayersInRoomsCountU3Ek__BackingField_65 = value;
	}

	inline static int32_t get_offset_of_U3CRoomsCountU3Ek__BackingField_66() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CRoomsCountU3Ek__BackingField_66)); }
	inline int32_t get_U3CRoomsCountU3Ek__BackingField_66() const { return ___U3CRoomsCountU3Ek__BackingField_66; }
	inline int32_t* get_address_of_U3CRoomsCountU3Ek__BackingField_66() { return &___U3CRoomsCountU3Ek__BackingField_66; }
	inline void set_U3CRoomsCountU3Ek__BackingField_66(int32_t value)
	{
		___U3CRoomsCountU3Ek__BackingField_66 = value;
	}

	inline static int32_t get_offset_of_lastJoinType_67() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___lastJoinType_67)); }
	inline int32_t get_lastJoinType_67() const { return ___lastJoinType_67; }
	inline int32_t* get_address_of_lastJoinType_67() { return &___lastJoinType_67; }
	inline void set_lastJoinType_67(int32_t value)
	{
		___lastJoinType_67 = value;
	}

	inline static int32_t get_offset_of_enterRoomParamsCache_68() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___enterRoomParamsCache_68)); }
	inline EnterRoomParams_t3960472384 * get_enterRoomParamsCache_68() const { return ___enterRoomParamsCache_68; }
	inline EnterRoomParams_t3960472384 ** get_address_of_enterRoomParamsCache_68() { return &___enterRoomParamsCache_68; }
	inline void set_enterRoomParamsCache_68(EnterRoomParams_t3960472384 * value)
	{
		___enterRoomParamsCache_68 = value;
		Il2CppCodeGenWriteBarrier((&___enterRoomParamsCache_68), value);
	}

	inline static int32_t get_offset_of_didAuthenticate_69() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___didAuthenticate_69)); }
	inline bool get_didAuthenticate_69() const { return ___didAuthenticate_69; }
	inline bool* get_address_of_didAuthenticate_69() { return &___didAuthenticate_69; }
	inline void set_didAuthenticate_69(bool value)
	{
		___didAuthenticate_69 = value;
	}

	inline static int32_t get_offset_of_friendListRequested_70() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___friendListRequested_70)); }
	inline StringU5BU5D_t1281789340* get_friendListRequested_70() const { return ___friendListRequested_70; }
	inline StringU5BU5D_t1281789340** get_address_of_friendListRequested_70() { return &___friendListRequested_70; }
	inline void set_friendListRequested_70(StringU5BU5D_t1281789340* value)
	{
		___friendListRequested_70 = value;
		Il2CppCodeGenWriteBarrier((&___friendListRequested_70), value);
	}

	inline static int32_t get_offset_of_friendListTimestamp_71() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___friendListTimestamp_71)); }
	inline int32_t get_friendListTimestamp_71() const { return ___friendListTimestamp_71; }
	inline int32_t* get_address_of_friendListTimestamp_71() { return &___friendListTimestamp_71; }
	inline void set_friendListTimestamp_71(int32_t value)
	{
		___friendListTimestamp_71 = value;
	}

	inline static int32_t get_offset_of_isFetchingFriendList_72() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___isFetchingFriendList_72)); }
	inline bool get_isFetchingFriendList_72() const { return ___isFetchingFriendList_72; }
	inline bool* get_address_of_isFetchingFriendList_72() { return &___isFetchingFriendList_72; }
	inline void set_isFetchingFriendList_72(bool value)
	{
		___isFetchingFriendList_72 = value;
	}

	inline static int32_t get_offset_of_U3CAvailableRegionsU3Ek__BackingField_73() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CAvailableRegionsU3Ek__BackingField_73)); }
	inline List_1_t861332708 * get_U3CAvailableRegionsU3Ek__BackingField_73() const { return ___U3CAvailableRegionsU3Ek__BackingField_73; }
	inline List_1_t861332708 ** get_address_of_U3CAvailableRegionsU3Ek__BackingField_73() { return &___U3CAvailableRegionsU3Ek__BackingField_73; }
	inline void set_U3CAvailableRegionsU3Ek__BackingField_73(List_1_t861332708 * value)
	{
		___U3CAvailableRegionsU3Ek__BackingField_73 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAvailableRegionsU3Ek__BackingField_73), value);
	}

	inline static int32_t get_offset_of_U3CCloudRegionU3Ek__BackingField_74() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___U3CCloudRegionU3Ek__BackingField_74)); }
	inline int32_t get_U3CCloudRegionU3Ek__BackingField_74() const { return ___U3CCloudRegionU3Ek__BackingField_74; }
	inline int32_t* get_address_of_U3CCloudRegionU3Ek__BackingField_74() { return &___U3CCloudRegionU3Ek__BackingField_74; }
	inline void set_U3CCloudRegionU3Ek__BackingField_74(int32_t value)
	{
		___U3CCloudRegionU3Ek__BackingField_74 = value;
	}

	inline static int32_t get_offset_of_mActors_75() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mActors_75)); }
	inline Dictionary_2_t2193862888 * get_mActors_75() const { return ___mActors_75; }
	inline Dictionary_2_t2193862888 ** get_address_of_mActors_75() { return &___mActors_75; }
	inline void set_mActors_75(Dictionary_2_t2193862888 * value)
	{
		___mActors_75 = value;
		Il2CppCodeGenWriteBarrier((&___mActors_75), value);
	}

	inline static int32_t get_offset_of_mOtherPlayerListCopy_76() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mOtherPlayerListCopy_76)); }
	inline PhotonPlayerU5BU5D_t2880637464* get_mOtherPlayerListCopy_76() const { return ___mOtherPlayerListCopy_76; }
	inline PhotonPlayerU5BU5D_t2880637464** get_address_of_mOtherPlayerListCopy_76() { return &___mOtherPlayerListCopy_76; }
	inline void set_mOtherPlayerListCopy_76(PhotonPlayerU5BU5D_t2880637464* value)
	{
		___mOtherPlayerListCopy_76 = value;
		Il2CppCodeGenWriteBarrier((&___mOtherPlayerListCopy_76), value);
	}

	inline static int32_t get_offset_of_mPlayerListCopy_77() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___mPlayerListCopy_77)); }
	inline PhotonPlayerU5BU5D_t2880637464* get_mPlayerListCopy_77() const { return ___mPlayerListCopy_77; }
	inline PhotonPlayerU5BU5D_t2880637464** get_address_of_mPlayerListCopy_77() { return &___mPlayerListCopy_77; }
	inline void set_mPlayerListCopy_77(PhotonPlayerU5BU5D_t2880637464* value)
	{
		___mPlayerListCopy_77 = value;
		Il2CppCodeGenWriteBarrier((&___mPlayerListCopy_77), value);
	}

	inline static int32_t get_offset_of_hasSwitchedMC_78() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___hasSwitchedMC_78)); }
	inline bool get_hasSwitchedMC_78() const { return ___hasSwitchedMC_78; }
	inline bool* get_address_of_hasSwitchedMC_78() { return &___hasSwitchedMC_78; }
	inline void set_hasSwitchedMC_78(bool value)
	{
		___hasSwitchedMC_78 = value;
	}

	inline static int32_t get_offset_of_allowedReceivingGroups_79() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___allowedReceivingGroups_79)); }
	inline HashSet_1_t3994213146 * get_allowedReceivingGroups_79() const { return ___allowedReceivingGroups_79; }
	inline HashSet_1_t3994213146 ** get_address_of_allowedReceivingGroups_79() { return &___allowedReceivingGroups_79; }
	inline void set_allowedReceivingGroups_79(HashSet_1_t3994213146 * value)
	{
		___allowedReceivingGroups_79 = value;
		Il2CppCodeGenWriteBarrier((&___allowedReceivingGroups_79), value);
	}

	inline static int32_t get_offset_of_blockSendingGroups_80() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___blockSendingGroups_80)); }
	inline HashSet_1_t3994213146 * get_blockSendingGroups_80() const { return ___blockSendingGroups_80; }
	inline HashSet_1_t3994213146 ** get_address_of_blockSendingGroups_80() { return &___blockSendingGroups_80; }
	inline void set_blockSendingGroups_80(HashSet_1_t3994213146 * value)
	{
		___blockSendingGroups_80 = value;
		Il2CppCodeGenWriteBarrier((&___blockSendingGroups_80), value);
	}

	inline static int32_t get_offset_of_photonViewList_81() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___photonViewList_81)); }
	inline Dictionary_2_t1096435151 * get_photonViewList_81() const { return ___photonViewList_81; }
	inline Dictionary_2_t1096435151 ** get_address_of_photonViewList_81() { return &___photonViewList_81; }
	inline void set_photonViewList_81(Dictionary_2_t1096435151 * value)
	{
		___photonViewList_81 = value;
		Il2CppCodeGenWriteBarrier((&___photonViewList_81), value);
	}

	inline static int32_t get_offset_of_readStream_82() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___readStream_82)); }
	inline PhotonStream_t1003850889 * get_readStream_82() const { return ___readStream_82; }
	inline PhotonStream_t1003850889 ** get_address_of_readStream_82() { return &___readStream_82; }
	inline void set_readStream_82(PhotonStream_t1003850889 * value)
	{
		___readStream_82 = value;
		Il2CppCodeGenWriteBarrier((&___readStream_82), value);
	}

	inline static int32_t get_offset_of_pStream_83() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___pStream_83)); }
	inline PhotonStream_t1003850889 * get_pStream_83() const { return ___pStream_83; }
	inline PhotonStream_t1003850889 ** get_address_of_pStream_83() { return &___pStream_83; }
	inline void set_pStream_83(PhotonStream_t1003850889 * value)
	{
		___pStream_83 = value;
		Il2CppCodeGenWriteBarrier((&___pStream_83), value);
	}

	inline static int32_t get_offset_of_dataPerGroupReliable_84() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___dataPerGroupReliable_84)); }
	inline Dictionary_2_t4231889829 * get_dataPerGroupReliable_84() const { return ___dataPerGroupReliable_84; }
	inline Dictionary_2_t4231889829 ** get_address_of_dataPerGroupReliable_84() { return &___dataPerGroupReliable_84; }
	inline void set_dataPerGroupReliable_84(Dictionary_2_t4231889829 * value)
	{
		___dataPerGroupReliable_84 = value;
		Il2CppCodeGenWriteBarrier((&___dataPerGroupReliable_84), value);
	}

	inline static int32_t get_offset_of_dataPerGroupUnreliable_85() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___dataPerGroupUnreliable_85)); }
	inline Dictionary_2_t4231889829 * get_dataPerGroupUnreliable_85() const { return ___dataPerGroupUnreliable_85; }
	inline Dictionary_2_t4231889829 ** get_address_of_dataPerGroupUnreliable_85() { return &___dataPerGroupUnreliable_85; }
	inline void set_dataPerGroupUnreliable_85(Dictionary_2_t4231889829 * value)
	{
		___dataPerGroupUnreliable_85 = value;
		Il2CppCodeGenWriteBarrier((&___dataPerGroupUnreliable_85), value);
	}

	inline static int32_t get_offset_of_currentLevelPrefix_86() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___currentLevelPrefix_86)); }
	inline int16_t get_currentLevelPrefix_86() const { return ___currentLevelPrefix_86; }
	inline int16_t* get_address_of_currentLevelPrefix_86() { return &___currentLevelPrefix_86; }
	inline void set_currentLevelPrefix_86(int16_t value)
	{
		___currentLevelPrefix_86 = value;
	}

	inline static int32_t get_offset_of_loadingLevelAndPausedNetwork_87() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___loadingLevelAndPausedNetwork_87)); }
	inline bool get_loadingLevelAndPausedNetwork_87() const { return ___loadingLevelAndPausedNetwork_87; }
	inline bool* get_address_of_loadingLevelAndPausedNetwork_87() { return &___loadingLevelAndPausedNetwork_87; }
	inline void set_loadingLevelAndPausedNetwork_87(bool value)
	{
		___loadingLevelAndPausedNetwork_87 = value;
	}

	inline static int32_t get_offset_of_ObjectPool_91() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___ObjectPool_91)); }
	inline RuntimeObject* get_ObjectPool_91() const { return ___ObjectPool_91; }
	inline RuntimeObject** get_address_of_ObjectPool_91() { return &___ObjectPool_91; }
	inline void set_ObjectPool_91(RuntimeObject* value)
	{
		___ObjectPool_91 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectPool_91), value);
	}

	inline static int32_t get_offset_of_monoRPCMethodsCache_93() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___monoRPCMethodsCache_93)); }
	inline Dictionary_2_t1499080758 * get_monoRPCMethodsCache_93() const { return ___monoRPCMethodsCache_93; }
	inline Dictionary_2_t1499080758 ** get_address_of_monoRPCMethodsCache_93() { return &___monoRPCMethodsCache_93; }
	inline void set_monoRPCMethodsCache_93(Dictionary_2_t1499080758 * value)
	{
		___monoRPCMethodsCache_93 = value;
		Il2CppCodeGenWriteBarrier((&___monoRPCMethodsCache_93), value);
	}

	inline static int32_t get_offset_of_rpcShortcuts_94() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___rpcShortcuts_94)); }
	inline Dictionary_2_t2736202052 * get_rpcShortcuts_94() const { return ___rpcShortcuts_94; }
	inline Dictionary_2_t2736202052 ** get_address_of_rpcShortcuts_94() { return &___rpcShortcuts_94; }
	inline void set_rpcShortcuts_94(Dictionary_2_t2736202052 * value)
	{
		___rpcShortcuts_94 = value;
		Il2CppCodeGenWriteBarrier((&___rpcShortcuts_94), value);
	}

	inline static int32_t get_offset_of_cachedServerAddress_96() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___cachedServerAddress_96)); }
	inline String_t* get_cachedServerAddress_96() const { return ___cachedServerAddress_96; }
	inline String_t** get_address_of_cachedServerAddress_96() { return &___cachedServerAddress_96; }
	inline void set_cachedServerAddress_96(String_t* value)
	{
		___cachedServerAddress_96 = value;
		Il2CppCodeGenWriteBarrier((&___cachedServerAddress_96), value);
	}

	inline static int32_t get_offset_of_cachedApplicationName_97() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___cachedApplicationName_97)); }
	inline String_t* get_cachedApplicationName_97() const { return ___cachedApplicationName_97; }
	inline String_t** get_address_of_cachedApplicationName_97() { return &___cachedApplicationName_97; }
	inline void set_cachedApplicationName_97(String_t* value)
	{
		___cachedApplicationName_97 = value;
		Il2CppCodeGenWriteBarrier((&___cachedApplicationName_97), value);
	}

	inline static int32_t get_offset_of_cachedServerType_98() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___cachedServerType_98)); }
	inline int32_t get_cachedServerType_98() const { return ___cachedServerType_98; }
	inline int32_t* get_address_of_cachedServerType_98() { return &___cachedServerType_98; }
	inline void set_cachedServerType_98(int32_t value)
	{
		___cachedServerType_98 = value;
	}

	inline static int32_t get_offset_of__AsyncLevelLoadingOperation_99() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ____AsyncLevelLoadingOperation_99)); }
	inline AsyncOperation_t1445031843 * get__AsyncLevelLoadingOperation_99() const { return ____AsyncLevelLoadingOperation_99; }
	inline AsyncOperation_t1445031843 ** get_address_of__AsyncLevelLoadingOperation_99() { return &____AsyncLevelLoadingOperation_99; }
	inline void set__AsyncLevelLoadingOperation_99(AsyncOperation_t1445031843 * value)
	{
		____AsyncLevelLoadingOperation_99 = value;
		Il2CppCodeGenWriteBarrier((&____AsyncLevelLoadingOperation_99), value);
	}

	inline static int32_t get_offset_of__levelReloadEventOptions_100() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ____levelReloadEventOptions_100)); }
	inline RaiseEventOptions_t1229553678 * get__levelReloadEventOptions_100() const { return ____levelReloadEventOptions_100; }
	inline RaiseEventOptions_t1229553678 ** get_address_of__levelReloadEventOptions_100() { return &____levelReloadEventOptions_100; }
	inline void set__levelReloadEventOptions_100(RaiseEventOptions_t1229553678 * value)
	{
		____levelReloadEventOptions_100 = value;
		Il2CppCodeGenWriteBarrier((&____levelReloadEventOptions_100), value);
	}

	inline static int32_t get_offset_of__isReconnecting_101() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ____isReconnecting_101)); }
	inline bool get__isReconnecting_101() const { return ____isReconnecting_101; }
	inline bool* get_address_of__isReconnecting_101() { return &____isReconnecting_101; }
	inline void set__isReconnecting_101(bool value)
	{
		____isReconnecting_101 = value;
	}

	inline static int32_t get_offset_of_tempInstantiationData_102() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___tempInstantiationData_102)); }
	inline Dictionary_2_t1732652656 * get_tempInstantiationData_102() const { return ___tempInstantiationData_102; }
	inline Dictionary_2_t1732652656 ** get_address_of_tempInstantiationData_102() { return &___tempInstantiationData_102; }
	inline void set_tempInstantiationData_102(Dictionary_2_t1732652656 * value)
	{
		___tempInstantiationData_102 = value;
		Il2CppCodeGenWriteBarrier((&___tempInstantiationData_102), value);
	}

	inline static int32_t get_offset_of_options_104() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___options_104)); }
	inline RaiseEventOptions_t1229553678 * get_options_104() const { return ___options_104; }
	inline RaiseEventOptions_t1229553678 ** get_address_of_options_104() { return &___options_104; }
	inline void set_options_104(RaiseEventOptions_t1229553678 * value)
	{
		___options_104 = value;
		Il2CppCodeGenWriteBarrier((&___options_104), value);
	}

	inline static int32_t get_offset_of_IsReloadingLevel_109() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___IsReloadingLevel_109)); }
	inline bool get_IsReloadingLevel_109() const { return ___IsReloadingLevel_109; }
	inline bool* get_address_of_IsReloadingLevel_109() { return &___IsReloadingLevel_109; }
	inline void set_IsReloadingLevel_109(bool value)
	{
		___IsReloadingLevel_109 = value;
	}

	inline static int32_t get_offset_of_AsynchLevelLoadCall_110() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356, ___AsynchLevelLoadCall_110)); }
	inline bool get_AsynchLevelLoadCall_110() const { return ___AsynchLevelLoadCall_110; }
	inline bool* get_address_of_AsynchLevelLoadCall_110() { return &___AsynchLevelLoadCall_110; }
	inline void set_AsynchLevelLoadCall_110(bool value)
	{
		___AsynchLevelLoadCall_110 = value;
	}
};

struct NetworkingPeer_t264212356_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32> NetworkingPeer::ProtocolToNameServerPort
	Dictionary_2_t1720840067 * ___ProtocolToNameServerPort_49;
	// System.Boolean NetworkingPeer::UsePrefabCache
	bool ___UsePrefabCache_90;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> NetworkingPeer::PrefabCache
	Dictionary_2_t898892918 * ___PrefabCache_92;
	// System.String NetworkingPeer::OnPhotonInstantiateString
	String_t* ___OnPhotonInstantiateString_95;
	// System.Int32 NetworkingPeer::ObjectsInOneUpdate
	int32_t ___ObjectsInOneUpdate_103;

public:
	inline static int32_t get_offset_of_ProtocolToNameServerPort_49() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___ProtocolToNameServerPort_49)); }
	inline Dictionary_2_t1720840067 * get_ProtocolToNameServerPort_49() const { return ___ProtocolToNameServerPort_49; }
	inline Dictionary_2_t1720840067 ** get_address_of_ProtocolToNameServerPort_49() { return &___ProtocolToNameServerPort_49; }
	inline void set_ProtocolToNameServerPort_49(Dictionary_2_t1720840067 * value)
	{
		___ProtocolToNameServerPort_49 = value;
		Il2CppCodeGenWriteBarrier((&___ProtocolToNameServerPort_49), value);
	}

	inline static int32_t get_offset_of_UsePrefabCache_90() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___UsePrefabCache_90)); }
	inline bool get_UsePrefabCache_90() const { return ___UsePrefabCache_90; }
	inline bool* get_address_of_UsePrefabCache_90() { return &___UsePrefabCache_90; }
	inline void set_UsePrefabCache_90(bool value)
	{
		___UsePrefabCache_90 = value;
	}

	inline static int32_t get_offset_of_PrefabCache_92() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___PrefabCache_92)); }
	inline Dictionary_2_t898892918 * get_PrefabCache_92() const { return ___PrefabCache_92; }
	inline Dictionary_2_t898892918 ** get_address_of_PrefabCache_92() { return &___PrefabCache_92; }
	inline void set_PrefabCache_92(Dictionary_2_t898892918 * value)
	{
		___PrefabCache_92 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabCache_92), value);
	}

	inline static int32_t get_offset_of_OnPhotonInstantiateString_95() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___OnPhotonInstantiateString_95)); }
	inline String_t* get_OnPhotonInstantiateString_95() const { return ___OnPhotonInstantiateString_95; }
	inline String_t** get_address_of_OnPhotonInstantiateString_95() { return &___OnPhotonInstantiateString_95; }
	inline void set_OnPhotonInstantiateString_95(String_t* value)
	{
		___OnPhotonInstantiateString_95 = value;
		Il2CppCodeGenWriteBarrier((&___OnPhotonInstantiateString_95), value);
	}

	inline static int32_t get_offset_of_ObjectsInOneUpdate_103() { return static_cast<int32_t>(offsetof(NetworkingPeer_t264212356_StaticFields, ___ObjectsInOneUpdate_103)); }
	inline int32_t get_ObjectsInOneUpdate_103() const { return ___ObjectsInOneUpdate_103; }
	inline int32_t* get_address_of_ObjectsInOneUpdate_103() { return &___ObjectsInOneUpdate_103; }
	inline void set_ObjectsInOneUpdate_103(int32_t value)
	{
		___ObjectsInOneUpdate_103 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKINGPEER_T264212356_H
#ifndef AUDIOBEHAVIOUR_T2879336574_H
#define AUDIOBEHAVIOUR_T2879336574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioBehaviour
struct  AudioBehaviour_t2879336574  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBEHAVIOUR_T2879336574_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DECK_T2172403585_H
#define DECK_T2172403585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Deck
struct  Deck_t2172403585  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image Deck::deckImage
	Image_t2670269651 * ___deckImage_4;
	// UnityEngine.GameObject Deck::stonePrefab
	GameObject_t1113636619 * ___stonePrefab_5;
	// System.Boolean Deck::isFirst
	bool ___isFirst_6;
	// System.Int32 Deck::maxSubmit
	int32_t ___maxSubmit_7;
	// System.Int32 Deck::curSubmit
	int32_t ___curSubmit_8;
	// System.Int32 Deck::numOfStone
	int32_t ___numOfStone_9;
	// System.Int32[] Deck::stoneSubmit
	Int32U5BU5D_t385246372* ___stoneSubmit_10;
	// System.Int32[] Deck::stoneForPoint
	Int32U5BU5D_t385246372* ___stoneForPoint_11;
	// System.Int32[] Deck::stoneForNumber
	Int32U5BU5D_t385246372* ___stoneForNumber_12;
	// Stone[] Deck::stoneToDestory
	StoneU5BU5D_t1191912092* ___stoneToDestory_13;
	// UnityEngine.Sprite[] Deck::cloud
	SpriteU5BU5D_t2581906349* ___cloud_14;
	// UnityEngine.Sprite[] Deck::star
	SpriteU5BU5D_t2581906349* ___star_15;
	// UnityEngine.Sprite[] Deck::moon
	SpriteU5BU5D_t2581906349* ___moon_16;
	// UnityEngine.Sprite[] Deck::sun
	SpriteU5BU5D_t2581906349* ___sun_17;

public:
	inline static int32_t get_offset_of_deckImage_4() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___deckImage_4)); }
	inline Image_t2670269651 * get_deckImage_4() const { return ___deckImage_4; }
	inline Image_t2670269651 ** get_address_of_deckImage_4() { return &___deckImage_4; }
	inline void set_deckImage_4(Image_t2670269651 * value)
	{
		___deckImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___deckImage_4), value);
	}

	inline static int32_t get_offset_of_stonePrefab_5() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stonePrefab_5)); }
	inline GameObject_t1113636619 * get_stonePrefab_5() const { return ___stonePrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_stonePrefab_5() { return &___stonePrefab_5; }
	inline void set_stonePrefab_5(GameObject_t1113636619 * value)
	{
		___stonePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___stonePrefab_5), value);
	}

	inline static int32_t get_offset_of_isFirst_6() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___isFirst_6)); }
	inline bool get_isFirst_6() const { return ___isFirst_6; }
	inline bool* get_address_of_isFirst_6() { return &___isFirst_6; }
	inline void set_isFirst_6(bool value)
	{
		___isFirst_6 = value;
	}

	inline static int32_t get_offset_of_maxSubmit_7() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___maxSubmit_7)); }
	inline int32_t get_maxSubmit_7() const { return ___maxSubmit_7; }
	inline int32_t* get_address_of_maxSubmit_7() { return &___maxSubmit_7; }
	inline void set_maxSubmit_7(int32_t value)
	{
		___maxSubmit_7 = value;
	}

	inline static int32_t get_offset_of_curSubmit_8() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___curSubmit_8)); }
	inline int32_t get_curSubmit_8() const { return ___curSubmit_8; }
	inline int32_t* get_address_of_curSubmit_8() { return &___curSubmit_8; }
	inline void set_curSubmit_8(int32_t value)
	{
		___curSubmit_8 = value;
	}

	inline static int32_t get_offset_of_numOfStone_9() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___numOfStone_9)); }
	inline int32_t get_numOfStone_9() const { return ___numOfStone_9; }
	inline int32_t* get_address_of_numOfStone_9() { return &___numOfStone_9; }
	inline void set_numOfStone_9(int32_t value)
	{
		___numOfStone_9 = value;
	}

	inline static int32_t get_offset_of_stoneSubmit_10() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stoneSubmit_10)); }
	inline Int32U5BU5D_t385246372* get_stoneSubmit_10() const { return ___stoneSubmit_10; }
	inline Int32U5BU5D_t385246372** get_address_of_stoneSubmit_10() { return &___stoneSubmit_10; }
	inline void set_stoneSubmit_10(Int32U5BU5D_t385246372* value)
	{
		___stoneSubmit_10 = value;
		Il2CppCodeGenWriteBarrier((&___stoneSubmit_10), value);
	}

	inline static int32_t get_offset_of_stoneForPoint_11() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stoneForPoint_11)); }
	inline Int32U5BU5D_t385246372* get_stoneForPoint_11() const { return ___stoneForPoint_11; }
	inline Int32U5BU5D_t385246372** get_address_of_stoneForPoint_11() { return &___stoneForPoint_11; }
	inline void set_stoneForPoint_11(Int32U5BU5D_t385246372* value)
	{
		___stoneForPoint_11 = value;
		Il2CppCodeGenWriteBarrier((&___stoneForPoint_11), value);
	}

	inline static int32_t get_offset_of_stoneForNumber_12() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stoneForNumber_12)); }
	inline Int32U5BU5D_t385246372* get_stoneForNumber_12() const { return ___stoneForNumber_12; }
	inline Int32U5BU5D_t385246372** get_address_of_stoneForNumber_12() { return &___stoneForNumber_12; }
	inline void set_stoneForNumber_12(Int32U5BU5D_t385246372* value)
	{
		___stoneForNumber_12 = value;
		Il2CppCodeGenWriteBarrier((&___stoneForNumber_12), value);
	}

	inline static int32_t get_offset_of_stoneToDestory_13() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___stoneToDestory_13)); }
	inline StoneU5BU5D_t1191912092* get_stoneToDestory_13() const { return ___stoneToDestory_13; }
	inline StoneU5BU5D_t1191912092** get_address_of_stoneToDestory_13() { return &___stoneToDestory_13; }
	inline void set_stoneToDestory_13(StoneU5BU5D_t1191912092* value)
	{
		___stoneToDestory_13 = value;
		Il2CppCodeGenWriteBarrier((&___stoneToDestory_13), value);
	}

	inline static int32_t get_offset_of_cloud_14() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___cloud_14)); }
	inline SpriteU5BU5D_t2581906349* get_cloud_14() const { return ___cloud_14; }
	inline SpriteU5BU5D_t2581906349** get_address_of_cloud_14() { return &___cloud_14; }
	inline void set_cloud_14(SpriteU5BU5D_t2581906349* value)
	{
		___cloud_14 = value;
		Il2CppCodeGenWriteBarrier((&___cloud_14), value);
	}

	inline static int32_t get_offset_of_star_15() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___star_15)); }
	inline SpriteU5BU5D_t2581906349* get_star_15() const { return ___star_15; }
	inline SpriteU5BU5D_t2581906349** get_address_of_star_15() { return &___star_15; }
	inline void set_star_15(SpriteU5BU5D_t2581906349* value)
	{
		___star_15 = value;
		Il2CppCodeGenWriteBarrier((&___star_15), value);
	}

	inline static int32_t get_offset_of_moon_16() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___moon_16)); }
	inline SpriteU5BU5D_t2581906349* get_moon_16() const { return ___moon_16; }
	inline SpriteU5BU5D_t2581906349** get_address_of_moon_16() { return &___moon_16; }
	inline void set_moon_16(SpriteU5BU5D_t2581906349* value)
	{
		___moon_16 = value;
		Il2CppCodeGenWriteBarrier((&___moon_16), value);
	}

	inline static int32_t get_offset_of_sun_17() { return static_cast<int32_t>(offsetof(Deck_t2172403585, ___sun_17)); }
	inline SpriteU5BU5D_t2581906349* get_sun_17() const { return ___sun_17; }
	inline SpriteU5BU5D_t2581906349** get_address_of_sun_17() { return &___sun_17; }
	inline void set_sun_17(SpriteU5BU5D_t2581906349* value)
	{
		___sun_17 = value;
		Il2CppCodeGenWriteBarrier((&___sun_17), value);
	}
};

struct Deck_t2172403585_StaticFields
{
public:
	// Deck Deck::<Instance>k__BackingField
	Deck_t2172403585 * ___U3CInstanceU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Deck_t2172403585_StaticFields, ___U3CInstanceU3Ek__BackingField_18)); }
	inline Deck_t2172403585 * get_U3CInstanceU3Ek__BackingField_18() const { return ___U3CInstanceU3Ek__BackingField_18; }
	inline Deck_t2172403585 ** get_address_of_U3CInstanceU3Ek__BackingField_18() { return &___U3CInstanceU3Ek__BackingField_18; }
	inline void set_U3CInstanceU3Ek__BackingField_18(Deck_t2172403585 * value)
	{
		___U3CInstanceU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECK_T2172403585_H
#ifndef FIELD_T4115194983_H
#define FIELD_T4115194983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Field
struct  Field_t4115194983  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Field::stonePrefab
	GameObject_t1113636619 * ___stonePrefab_4;
	// UnityEngine.UI.Image Field::fieldImage
	Image_t2670269651 * ___fieldImage_5;
	// UnityEngine.UI.Text Field::t
	Text_t1901882714 * ___t_6;
	// System.Boolean Field::isFirstField
	bool ___isFirstField_7;
	// System.Int32 Field::prevPoint
	int32_t ___prevPoint_8;
	// System.Int32 Field::numOfStone
	int32_t ___numOfStone_9;

public:
	inline static int32_t get_offset_of_stonePrefab_4() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___stonePrefab_4)); }
	inline GameObject_t1113636619 * get_stonePrefab_4() const { return ___stonePrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_stonePrefab_4() { return &___stonePrefab_4; }
	inline void set_stonePrefab_4(GameObject_t1113636619 * value)
	{
		___stonePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___stonePrefab_4), value);
	}

	inline static int32_t get_offset_of_fieldImage_5() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___fieldImage_5)); }
	inline Image_t2670269651 * get_fieldImage_5() const { return ___fieldImage_5; }
	inline Image_t2670269651 ** get_address_of_fieldImage_5() { return &___fieldImage_5; }
	inline void set_fieldImage_5(Image_t2670269651 * value)
	{
		___fieldImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___fieldImage_5), value);
	}

	inline static int32_t get_offset_of_t_6() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___t_6)); }
	inline Text_t1901882714 * get_t_6() const { return ___t_6; }
	inline Text_t1901882714 ** get_address_of_t_6() { return &___t_6; }
	inline void set_t_6(Text_t1901882714 * value)
	{
		___t_6 = value;
		Il2CppCodeGenWriteBarrier((&___t_6), value);
	}

	inline static int32_t get_offset_of_isFirstField_7() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___isFirstField_7)); }
	inline bool get_isFirstField_7() const { return ___isFirstField_7; }
	inline bool* get_address_of_isFirstField_7() { return &___isFirstField_7; }
	inline void set_isFirstField_7(bool value)
	{
		___isFirstField_7 = value;
	}

	inline static int32_t get_offset_of_prevPoint_8() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___prevPoint_8)); }
	inline int32_t get_prevPoint_8() const { return ___prevPoint_8; }
	inline int32_t* get_address_of_prevPoint_8() { return &___prevPoint_8; }
	inline void set_prevPoint_8(int32_t value)
	{
		___prevPoint_8 = value;
	}

	inline static int32_t get_offset_of_numOfStone_9() { return static_cast<int32_t>(offsetof(Field_t4115194983, ___numOfStone_9)); }
	inline int32_t get_numOfStone_9() const { return ___numOfStone_9; }
	inline int32_t* get_address_of_numOfStone_9() { return &___numOfStone_9; }
	inline void set_numOfStone_9(int32_t value)
	{
		___numOfStone_9 = value;
	}
};

struct Field_t4115194983_StaticFields
{
public:
	// Field Field::<Instance>k__BackingField
	Field_t4115194983 * ___U3CInstanceU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Field_t4115194983_StaticFields, ___U3CInstanceU3Ek__BackingField_10)); }
	inline Field_t4115194983 * get_U3CInstanceU3Ek__BackingField_10() const { return ___U3CInstanceU3Ek__BackingField_10; }
	inline Field_t4115194983 ** get_address_of_U3CInstanceU3Ek__BackingField_10() { return &___U3CInstanceU3Ek__BackingField_10; }
	inline void set_U3CInstanceU3Ek__BackingField_10(Field_t4115194983 * value)
	{
		___U3CInstanceU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_T4115194983_H
#ifndef INGAMEPANEL_T2211285949_H
#define INGAMEPANEL_T2211285949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InGamePanel
struct  InGamePanel_t2211285949  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text InGamePanel::playerName
	Text_t1901882714 * ___playerName_4;
	// UnityEngine.UI.Text InGamePanel::chips
	Text_t1901882714 * ___chips_5;
	// UnityEngine.GameObject InGamePanel::exitPanel
	GameObject_t1113636619 * ___exitPanel_6;
	// System.Boolean InGamePanel::isExitOn
	bool ___isExitOn_7;
	// UnityEngine.GameObject InGamePanel::increaseMoney
	GameObject_t1113636619 * ___increaseMoney_8;
	// UnityEngine.UI.Image InGamePanel::rating
	Image_t2670269651 * ___rating_9;
	// UnityEngine.GameObject InGamePanel::playinfoPanel
	GameObject_t1113636619 * ___playinfoPanel_10;
	// System.Int32 InGamePanel::isReadPlayInfo
	int32_t ___isReadPlayInfo_11;

public:
	inline static int32_t get_offset_of_playerName_4() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___playerName_4)); }
	inline Text_t1901882714 * get_playerName_4() const { return ___playerName_4; }
	inline Text_t1901882714 ** get_address_of_playerName_4() { return &___playerName_4; }
	inline void set_playerName_4(Text_t1901882714 * value)
	{
		___playerName_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_4), value);
	}

	inline static int32_t get_offset_of_chips_5() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___chips_5)); }
	inline Text_t1901882714 * get_chips_5() const { return ___chips_5; }
	inline Text_t1901882714 ** get_address_of_chips_5() { return &___chips_5; }
	inline void set_chips_5(Text_t1901882714 * value)
	{
		___chips_5 = value;
		Il2CppCodeGenWriteBarrier((&___chips_5), value);
	}

	inline static int32_t get_offset_of_exitPanel_6() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___exitPanel_6)); }
	inline GameObject_t1113636619 * get_exitPanel_6() const { return ___exitPanel_6; }
	inline GameObject_t1113636619 ** get_address_of_exitPanel_6() { return &___exitPanel_6; }
	inline void set_exitPanel_6(GameObject_t1113636619 * value)
	{
		___exitPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___exitPanel_6), value);
	}

	inline static int32_t get_offset_of_isExitOn_7() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___isExitOn_7)); }
	inline bool get_isExitOn_7() const { return ___isExitOn_7; }
	inline bool* get_address_of_isExitOn_7() { return &___isExitOn_7; }
	inline void set_isExitOn_7(bool value)
	{
		___isExitOn_7 = value;
	}

	inline static int32_t get_offset_of_increaseMoney_8() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___increaseMoney_8)); }
	inline GameObject_t1113636619 * get_increaseMoney_8() const { return ___increaseMoney_8; }
	inline GameObject_t1113636619 ** get_address_of_increaseMoney_8() { return &___increaseMoney_8; }
	inline void set_increaseMoney_8(GameObject_t1113636619 * value)
	{
		___increaseMoney_8 = value;
		Il2CppCodeGenWriteBarrier((&___increaseMoney_8), value);
	}

	inline static int32_t get_offset_of_rating_9() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___rating_9)); }
	inline Image_t2670269651 * get_rating_9() const { return ___rating_9; }
	inline Image_t2670269651 ** get_address_of_rating_9() { return &___rating_9; }
	inline void set_rating_9(Image_t2670269651 * value)
	{
		___rating_9 = value;
		Il2CppCodeGenWriteBarrier((&___rating_9), value);
	}

	inline static int32_t get_offset_of_playinfoPanel_10() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___playinfoPanel_10)); }
	inline GameObject_t1113636619 * get_playinfoPanel_10() const { return ___playinfoPanel_10; }
	inline GameObject_t1113636619 ** get_address_of_playinfoPanel_10() { return &___playinfoPanel_10; }
	inline void set_playinfoPanel_10(GameObject_t1113636619 * value)
	{
		___playinfoPanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___playinfoPanel_10), value);
	}

	inline static int32_t get_offset_of_isReadPlayInfo_11() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949, ___isReadPlayInfo_11)); }
	inline int32_t get_isReadPlayInfo_11() const { return ___isReadPlayInfo_11; }
	inline int32_t* get_address_of_isReadPlayInfo_11() { return &___isReadPlayInfo_11; }
	inline void set_isReadPlayInfo_11(int32_t value)
	{
		___isReadPlayInfo_11 = value;
	}
};

struct InGamePanel_t2211285949_StaticFields
{
public:
	// InGamePanel InGamePanel::<Instance>k__BackingField
	InGamePanel_t2211285949 * ___U3CInstanceU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(InGamePanel_t2211285949_StaticFields, ___U3CInstanceU3Ek__BackingField_12)); }
	inline InGamePanel_t2211285949 * get_U3CInstanceU3Ek__BackingField_12() const { return ___U3CInstanceU3Ek__BackingField_12; }
	inline InGamePanel_t2211285949 ** get_address_of_U3CInstanceU3Ek__BackingField_12() { return &___U3CInstanceU3Ek__BackingField_12; }
	inline void set_U3CInstanceU3Ek__BackingField_12(InGamePanel_t2211285949 * value)
	{
		___U3CInstanceU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INGAMEPANEL_T2211285949_H
#ifndef INCREASEEFFECTPANEL_T3055077437_H
#define INCREASEEFFECTPANEL_T3055077437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IncreaseEffectPanel
struct  IncreaseEffectPanel_t3055077437  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject IncreaseEffectPanel::increaseMoney
	GameObject_t1113636619 * ___increaseMoney_4;
	// UnityEngine.GameObject IncreaseEffectPanel::posPrefab
	GameObject_t1113636619 * ___posPrefab_5;
	// UnityEngine.Transform[] IncreaseEffectPanel::pos
	TransformU5BU5D_t807237628* ___pos_6;

public:
	inline static int32_t get_offset_of_increaseMoney_4() { return static_cast<int32_t>(offsetof(IncreaseEffectPanel_t3055077437, ___increaseMoney_4)); }
	inline GameObject_t1113636619 * get_increaseMoney_4() const { return ___increaseMoney_4; }
	inline GameObject_t1113636619 ** get_address_of_increaseMoney_4() { return &___increaseMoney_4; }
	inline void set_increaseMoney_4(GameObject_t1113636619 * value)
	{
		___increaseMoney_4 = value;
		Il2CppCodeGenWriteBarrier((&___increaseMoney_4), value);
	}

	inline static int32_t get_offset_of_posPrefab_5() { return static_cast<int32_t>(offsetof(IncreaseEffectPanel_t3055077437, ___posPrefab_5)); }
	inline GameObject_t1113636619 * get_posPrefab_5() const { return ___posPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_posPrefab_5() { return &___posPrefab_5; }
	inline void set_posPrefab_5(GameObject_t1113636619 * value)
	{
		___posPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___posPrefab_5), value);
	}

	inline static int32_t get_offset_of_pos_6() { return static_cast<int32_t>(offsetof(IncreaseEffectPanel_t3055077437, ___pos_6)); }
	inline TransformU5BU5D_t807237628* get_pos_6() const { return ___pos_6; }
	inline TransformU5BU5D_t807237628** get_address_of_pos_6() { return &___pos_6; }
	inline void set_pos_6(TransformU5BU5D_t807237628* value)
	{
		___pos_6 = value;
		Il2CppCodeGenWriteBarrier((&___pos_6), value);
	}
};

struct IncreaseEffectPanel_t3055077437_StaticFields
{
public:
	// IncreaseEffectPanel IncreaseEffectPanel::<Instance>k__BackingField
	IncreaseEffectPanel_t3055077437 * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(IncreaseEffectPanel_t3055077437_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline IncreaseEffectPanel_t3055077437 * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline IncreaseEffectPanel_t3055077437 ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(IncreaseEffectPanel_t3055077437 * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREASEEFFECTPANEL_T3055077437_H
#ifndef PASSBUTTON_T4045823955_H
#define PASSBUTTON_T4045823955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PassButton
struct  PassButton_t4045823955  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button PassButton::b
	Button_t4055032469 * ___b_5;

public:
	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(PassButton_t4045823955, ___b_5)); }
	inline Button_t4055032469 * get_b_5() const { return ___b_5; }
	inline Button_t4055032469 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Button_t4055032469 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

struct PassButton_t4045823955_StaticFields
{
public:
	// PassButton PassButton::<Instance>k__BackingField
	PassButton_t4045823955 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PassButton_t4045823955_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline PassButton_t4045823955 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline PassButton_t4045823955 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(PassButton_t4045823955 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSBUTTON_T4045823955_H
#ifndef MONOBEHAVIOUR_T3225183318_H
#define MONOBEHAVIOUR_T3225183318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.MonoBehaviour
struct  MonoBehaviour_t3225183318  : public MonoBehaviour_t3962482529
{
public:
	// PhotonView Photon.MonoBehaviour::pvCache
	PhotonView_t2207721820 * ___pvCache_4;

public:
	inline static int32_t get_offset_of_pvCache_4() { return static_cast<int32_t>(offsetof(MonoBehaviour_t3225183318, ___pvCache_4)); }
	inline PhotonView_t2207721820 * get_pvCache_4() const { return ___pvCache_4; }
	inline PhotonView_t2207721820 ** get_address_of_pvCache_4() { return &___pvCache_4; }
	inline void set_pvCache_4(PhotonView_t2207721820 * value)
	{
		___pvCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3225183318_H
#ifndef PLAYERDATA_T220878115_H
#define PLAYERDATA_T220878115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerData
struct  PlayerData_t220878115  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 PlayerData::chips
	int32_t ___chips_4;
	// System.String PlayerData::playerName
	String_t* ___playerName_5;

public:
	inline static int32_t get_offset_of_chips_4() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___chips_4)); }
	inline int32_t get_chips_4() const { return ___chips_4; }
	inline int32_t* get_address_of_chips_4() { return &___chips_4; }
	inline void set_chips_4(int32_t value)
	{
		___chips_4 = value;
	}

	inline static int32_t get_offset_of_playerName_5() { return static_cast<int32_t>(offsetof(PlayerData_t220878115, ___playerName_5)); }
	inline String_t* get_playerName_5() const { return ___playerName_5; }
	inline String_t** get_address_of_playerName_5() { return &___playerName_5; }
	inline void set_playerName_5(String_t* value)
	{
		___playerName_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerName_5), value);
	}
};

struct PlayerData_t220878115_StaticFields
{
public:
	// PlayerData PlayerData::<Instance>k__BackingField
	PlayerData_t220878115 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PlayerData_t220878115_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline PlayerData_t220878115 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline PlayerData_t220878115 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(PlayerData_t220878115 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDATA_T220878115_H
#ifndef PLAYERSTATUSPANEL_T1209538314_H
#define PLAYERSTATUSPANEL_T1209538314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerStatusPanel
struct  PlayerStatusPanel_t1209538314  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlayerStatusPanel::playerStatusPrefab
	GameObject_t1113636619 * ___playerStatusPrefab_4;
	// UnityEngine.GameObject PlayerStatusPanel::waitingPanel
	GameObject_t1113636619 * ___waitingPanel_5;
	// System.Boolean PlayerStatusPanel::isWaiting
	bool ___isWaiting_6;
	// System.Boolean[] PlayerStatusPanel::isExist
	BooleanU5BU5D_t2897418192* ___isExist_7;
	// System.Single PlayerStatusPanel::timer
	float ___timer_8;

public:
	inline static int32_t get_offset_of_playerStatusPrefab_4() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___playerStatusPrefab_4)); }
	inline GameObject_t1113636619 * get_playerStatusPrefab_4() const { return ___playerStatusPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_playerStatusPrefab_4() { return &___playerStatusPrefab_4; }
	inline void set_playerStatusPrefab_4(GameObject_t1113636619 * value)
	{
		___playerStatusPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerStatusPrefab_4), value);
	}

	inline static int32_t get_offset_of_waitingPanel_5() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___waitingPanel_5)); }
	inline GameObject_t1113636619 * get_waitingPanel_5() const { return ___waitingPanel_5; }
	inline GameObject_t1113636619 ** get_address_of_waitingPanel_5() { return &___waitingPanel_5; }
	inline void set_waitingPanel_5(GameObject_t1113636619 * value)
	{
		___waitingPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___waitingPanel_5), value);
	}

	inline static int32_t get_offset_of_isWaiting_6() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___isWaiting_6)); }
	inline bool get_isWaiting_6() const { return ___isWaiting_6; }
	inline bool* get_address_of_isWaiting_6() { return &___isWaiting_6; }
	inline void set_isWaiting_6(bool value)
	{
		___isWaiting_6 = value;
	}

	inline static int32_t get_offset_of_isExist_7() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___isExist_7)); }
	inline BooleanU5BU5D_t2897418192* get_isExist_7() const { return ___isExist_7; }
	inline BooleanU5BU5D_t2897418192** get_address_of_isExist_7() { return &___isExist_7; }
	inline void set_isExist_7(BooleanU5BU5D_t2897418192* value)
	{
		___isExist_7 = value;
		Il2CppCodeGenWriteBarrier((&___isExist_7), value);
	}

	inline static int32_t get_offset_of_timer_8() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314, ___timer_8)); }
	inline float get_timer_8() const { return ___timer_8; }
	inline float* get_address_of_timer_8() { return &___timer_8; }
	inline void set_timer_8(float value)
	{
		___timer_8 = value;
	}
};

struct PlayerStatusPanel_t1209538314_StaticFields
{
public:
	// PlayerStatusPanel PlayerStatusPanel::<Instance>k__BackingField
	PlayerStatusPanel_t1209538314 * ___U3CInstanceU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PlayerStatusPanel_t1209538314_StaticFields, ___U3CInstanceU3Ek__BackingField_9)); }
	inline PlayerStatusPanel_t1209538314 * get_U3CInstanceU3Ek__BackingField_9() const { return ___U3CInstanceU3Ek__BackingField_9; }
	inline PlayerStatusPanel_t1209538314 ** get_address_of_U3CInstanceU3Ek__BackingField_9() { return &___U3CInstanceU3Ek__BackingField_9; }
	inline void set_U3CInstanceU3Ek__BackingField_9(PlayerStatusPanel_t1209538314 * value)
	{
		___U3CInstanceU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSTATUSPANEL_T1209538314_H
#ifndef REMAINTIME_T3232366768_H
#define REMAINTIME_T3232366768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemainTime
struct  RemainTime_t3232366768  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text RemainTime::text
	Text_t1901882714 * ___text_4;
	// UnityEngine.UI.Slider RemainTime::slider
	Slider_t3903728902 * ___slider_5;
	// PlayerStatus RemainTime::current
	PlayerStatus_t402953766 * ___current_6;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(RemainTime_t3232366768, ___text_4)); }
	inline Text_t1901882714 * get_text_4() const { return ___text_4; }
	inline Text_t1901882714 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t1901882714 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_slider_5() { return static_cast<int32_t>(offsetof(RemainTime_t3232366768, ___slider_5)); }
	inline Slider_t3903728902 * get_slider_5() const { return ___slider_5; }
	inline Slider_t3903728902 ** get_address_of_slider_5() { return &___slider_5; }
	inline void set_slider_5(Slider_t3903728902 * value)
	{
		___slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___slider_5), value);
	}

	inline static int32_t get_offset_of_current_6() { return static_cast<int32_t>(offsetof(RemainTime_t3232366768, ___current_6)); }
	inline PlayerStatus_t402953766 * get_current_6() const { return ___current_6; }
	inline PlayerStatus_t402953766 ** get_address_of_current_6() { return &___current_6; }
	inline void set_current_6(PlayerStatus_t402953766 * value)
	{
		___current_6 = value;
		Il2CppCodeGenWriteBarrier((&___current_6), value);
	}
};

struct RemainTime_t3232366768_StaticFields
{
public:
	// RemainTime RemainTime::<Instance>k__BackingField
	RemainTime_t3232366768 * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RemainTime_t3232366768_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline RemainTime_t3232366768 * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline RemainTime_t3232366768 ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(RemainTime_t3232366768 * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMAINTIME_T3232366768_H
#ifndef ROOMLIST_T2314265074_H
#define ROOMLIST_T2314265074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomList
struct  RoomList_t2314265074  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject RoomList::roomButtonPrefab
	GameObject_t1113636619 * ___roomButtonPrefab_4;
	// UnityEngine.GameObject RoomList::roomPanel
	GameObject_t1113636619 * ___roomPanel_5;
	// UnityEngine.GameObject RoomList::createRoomPanel
	GameObject_t1113636619 * ___createRoomPanel_6;

public:
	inline static int32_t get_offset_of_roomButtonPrefab_4() { return static_cast<int32_t>(offsetof(RoomList_t2314265074, ___roomButtonPrefab_4)); }
	inline GameObject_t1113636619 * get_roomButtonPrefab_4() const { return ___roomButtonPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_roomButtonPrefab_4() { return &___roomButtonPrefab_4; }
	inline void set_roomButtonPrefab_4(GameObject_t1113636619 * value)
	{
		___roomButtonPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___roomButtonPrefab_4), value);
	}

	inline static int32_t get_offset_of_roomPanel_5() { return static_cast<int32_t>(offsetof(RoomList_t2314265074, ___roomPanel_5)); }
	inline GameObject_t1113636619 * get_roomPanel_5() const { return ___roomPanel_5; }
	inline GameObject_t1113636619 ** get_address_of_roomPanel_5() { return &___roomPanel_5; }
	inline void set_roomPanel_5(GameObject_t1113636619 * value)
	{
		___roomPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___roomPanel_5), value);
	}

	inline static int32_t get_offset_of_createRoomPanel_6() { return static_cast<int32_t>(offsetof(RoomList_t2314265074, ___createRoomPanel_6)); }
	inline GameObject_t1113636619 * get_createRoomPanel_6() const { return ___createRoomPanel_6; }
	inline GameObject_t1113636619 ** get_address_of_createRoomPanel_6() { return &___createRoomPanel_6; }
	inline void set_createRoomPanel_6(GameObject_t1113636619 * value)
	{
		___createRoomPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___createRoomPanel_6), value);
	}
};

struct RoomList_t2314265074_StaticFields
{
public:
	// RoomList RoomList::<Instance>k__BackingField
	RoomList_t2314265074 * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RoomList_t2314265074_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline RoomList_t2314265074 * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline RoomList_t2314265074 ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(RoomList_t2314265074 * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMLIST_T2314265074_H
#ifndef SCENETRANSITION_T1138091307_H
#define SCENETRANSITION_T1138091307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneTransition
struct  SceneTransition_t1138091307  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SceneTransition::isTestMode
	bool ___isTestMode_4;
	// YccioNetworkManager SceneTransition::ynm
	YccioNetworkManager_t3600504787 * ___ynm_5;

public:
	inline static int32_t get_offset_of_isTestMode_4() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___isTestMode_4)); }
	inline bool get_isTestMode_4() const { return ___isTestMode_4; }
	inline bool* get_address_of_isTestMode_4() { return &___isTestMode_4; }
	inline void set_isTestMode_4(bool value)
	{
		___isTestMode_4 = value;
	}

	inline static int32_t get_offset_of_ynm_5() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___ynm_5)); }
	inline YccioNetworkManager_t3600504787 * get_ynm_5() const { return ___ynm_5; }
	inline YccioNetworkManager_t3600504787 ** get_address_of_ynm_5() { return &___ynm_5; }
	inline void set_ynm_5(YccioNetworkManager_t3600504787 * value)
	{
		___ynm_5 = value;
		Il2CppCodeGenWriteBarrier((&___ynm_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENETRANSITION_T1138091307_H
#ifndef SHOWSTATUSWHENCONNECTING_T1063567576_H
#define SHOWSTATUSWHENCONNECTING_T1063567576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowStatusWhenConnecting
struct  ShowStatusWhenConnecting_t1063567576  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUISkin ShowStatusWhenConnecting::Skin
	GUISkin_t1244372282 * ___Skin_4;

public:
	inline static int32_t get_offset_of_Skin_4() { return static_cast<int32_t>(offsetof(ShowStatusWhenConnecting_t1063567576, ___Skin_4)); }
	inline GUISkin_t1244372282 * get_Skin_4() const { return ___Skin_4; }
	inline GUISkin_t1244372282 ** get_address_of_Skin_4() { return &___Skin_4; }
	inline void set_Skin_4(GUISkin_t1244372282 * value)
	{
		___Skin_4 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWSTATUSWHENCONNECTING_T1063567576_H
#ifndef SORTBYCOLOR_T1786351735_H
#define SORTBYCOLOR_T1786351735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SortByColor
struct  SortByColor_t1786351735  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button SortByColor::b
	Button_t4055032469 * ___b_4;

public:
	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(SortByColor_t1786351735, ___b_4)); }
	inline Button_t4055032469 * get_b_4() const { return ___b_4; }
	inline Button_t4055032469 ** get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(Button_t4055032469 * value)
	{
		___b_4 = value;
		Il2CppCodeGenWriteBarrier((&___b_4), value);
	}
};

struct SortByColor_t1786351735_StaticFields
{
public:
	// SortByColor SortByColor::<Instance>k__BackingField
	SortByColor_t1786351735 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SortByColor_t1786351735_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline SortByColor_t1786351735 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline SortByColor_t1786351735 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(SortByColor_t1786351735 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTBYCOLOR_T1786351735_H
#ifndef SORTBYNUMBER_T227753584_H
#define SORTBYNUMBER_T227753584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SortByNumber
struct  SortByNumber_t227753584  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button SortByNumber::b
	Button_t4055032469 * ___b_4;

public:
	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(SortByNumber_t227753584, ___b_4)); }
	inline Button_t4055032469 * get_b_4() const { return ___b_4; }
	inline Button_t4055032469 ** get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(Button_t4055032469 * value)
	{
		___b_4 = value;
		Il2CppCodeGenWriteBarrier((&___b_4), value);
	}
};

struct SortByNumber_t227753584_StaticFields
{
public:
	// SortByNumber SortByNumber::<Instance>k__BackingField
	SortByNumber_t227753584 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SortByNumber_t227753584_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline SortByNumber_t227753584 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline SortByNumber_t227753584 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(SortByNumber_t227753584 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTBYNUMBER_T227753584_H
#ifndef SOUNDMANAGER_T2102329059_H
#define SOUNDMANAGER_T2102329059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t2102329059  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip[] SoundManager::clips
	AudioClipU5BU5D_t143221404* ___clips_4;
	// UnityEngine.AudioSource SoundManager::source
	AudioSource_t3935305588 * ___source_5;

public:
	inline static int32_t get_offset_of_clips_4() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___clips_4)); }
	inline AudioClipU5BU5D_t143221404* get_clips_4() const { return ___clips_4; }
	inline AudioClipU5BU5D_t143221404** get_address_of_clips_4() { return &___clips_4; }
	inline void set_clips_4(AudioClipU5BU5D_t143221404* value)
	{
		___clips_4 = value;
		Il2CppCodeGenWriteBarrier((&___clips_4), value);
	}

	inline static int32_t get_offset_of_source_5() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___source_5)); }
	inline AudioSource_t3935305588 * get_source_5() const { return ___source_5; }
	inline AudioSource_t3935305588 ** get_address_of_source_5() { return &___source_5; }
	inline void set_source_5(AudioSource_t3935305588 * value)
	{
		___source_5 = value;
		Il2CppCodeGenWriteBarrier((&___source_5), value);
	}
};

struct SoundManager_t2102329059_StaticFields
{
public:
	// SoundManager SoundManager::<Instance>k__BackingField
	SoundManager_t2102329059 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline SoundManager_t2102329059 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline SoundManager_t2102329059 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(SoundManager_t2102329059 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T2102329059_H
#ifndef SPRITECONTAINER_T2500302117_H
#define SPRITECONTAINER_T2500302117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpriteContainer
struct  SpriteContainer_t2500302117  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] SpriteContainer::ratings
	SpriteU5BU5D_t2581906349* ___ratings_4;

public:
	inline static int32_t get_offset_of_ratings_4() { return static_cast<int32_t>(offsetof(SpriteContainer_t2500302117, ___ratings_4)); }
	inline SpriteU5BU5D_t2581906349* get_ratings_4() const { return ___ratings_4; }
	inline SpriteU5BU5D_t2581906349** get_address_of_ratings_4() { return &___ratings_4; }
	inline void set_ratings_4(SpriteU5BU5D_t2581906349* value)
	{
		___ratings_4 = value;
		Il2CppCodeGenWriteBarrier((&___ratings_4), value);
	}
};

struct SpriteContainer_t2500302117_StaticFields
{
public:
	// SpriteContainer SpriteContainer::<Instance>k__BackingField
	SpriteContainer_t2500302117 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SpriteContainer_t2500302117_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline SpriteContainer_t2500302117 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline SpriteContainer_t2500302117 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(SpriteContainer_t2500302117 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITECONTAINER_T2500302117_H
#ifndef STARTBUTTON_T1998348287_H
#define STARTBUTTON_T1998348287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartButton
struct  StartButton_t1998348287  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button StartButton::button
	Button_t4055032469 * ___button_4;
	// YccioNetworkManager StartButton::ynm
	YccioNetworkManager_t3600504787 * ___ynm_5;

public:
	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(StartButton_t1998348287, ___button_4)); }
	inline Button_t4055032469 * get_button_4() const { return ___button_4; }
	inline Button_t4055032469 ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(Button_t4055032469 * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier((&___button_4), value);
	}

	inline static int32_t get_offset_of_ynm_5() { return static_cast<int32_t>(offsetof(StartButton_t1998348287, ___ynm_5)); }
	inline YccioNetworkManager_t3600504787 * get_ynm_5() const { return ___ynm_5; }
	inline YccioNetworkManager_t3600504787 ** get_address_of_ynm_5() { return &___ynm_5; }
	inline void set_ynm_5(YccioNetworkManager_t3600504787 * value)
	{
		___ynm_5 = value;
		Il2CppCodeGenWriteBarrier((&___ynm_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTBUTTON_T1998348287_H
#ifndef STONE_T255273793_H
#define STONE_T255273793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stone
struct  Stone_t255273793  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Stone::stoneNumber
	int32_t ___stoneNumber_4;
	// System.Int32 Stone::number
	int32_t ___number_5;
	// StoneType Stone::type
	int32_t ___type_6;
	// UnityEngine.GameObject Stone::selectImage
	GameObject_t1113636619 * ___selectImage_7;
	// System.Boolean Stone::isSelected
	bool ___isSelected_8;

public:
	inline static int32_t get_offset_of_stoneNumber_4() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___stoneNumber_4)); }
	inline int32_t get_stoneNumber_4() const { return ___stoneNumber_4; }
	inline int32_t* get_address_of_stoneNumber_4() { return &___stoneNumber_4; }
	inline void set_stoneNumber_4(int32_t value)
	{
		___stoneNumber_4 = value;
	}

	inline static int32_t get_offset_of_number_5() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___number_5)); }
	inline int32_t get_number_5() const { return ___number_5; }
	inline int32_t* get_address_of_number_5() { return &___number_5; }
	inline void set_number_5(int32_t value)
	{
		___number_5 = value;
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of_selectImage_7() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___selectImage_7)); }
	inline GameObject_t1113636619 * get_selectImage_7() const { return ___selectImage_7; }
	inline GameObject_t1113636619 ** get_address_of_selectImage_7() { return &___selectImage_7; }
	inline void set_selectImage_7(GameObject_t1113636619 * value)
	{
		___selectImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___selectImage_7), value);
	}

	inline static int32_t get_offset_of_isSelected_8() { return static_cast<int32_t>(offsetof(Stone_t255273793, ___isSelected_8)); }
	inline bool get_isSelected_8() const { return ___isSelected_8; }
	inline bool* get_address_of_isSelected_8() { return &___isSelected_8; }
	inline void set_isSelected_8(bool value)
	{
		___isSelected_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STONE_T255273793_H
#ifndef SUBMITBUTTON_T4135388028_H
#define SUBMITBUTTON_T4135388028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubmitButton
struct  SubmitButton_t4135388028  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button SubmitButton::b
	Button_t4055032469 * ___b_5;

public:
	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(SubmitButton_t4135388028, ___b_5)); }
	inline Button_t4055032469 * get_b_5() const { return ___b_5; }
	inline Button_t4055032469 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Button_t4055032469 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

struct SubmitButton_t4135388028_StaticFields
{
public:
	// SubmitButton SubmitButton::<Instance>k__BackingField
	SubmitButton_t4135388028 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SubmitButton_t4135388028_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline SubmitButton_t4135388028 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline SubmitButton_t4135388028 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(SubmitButton_t4135388028 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMITBUTTON_T4135388028_H
#ifndef SUPPORTLOGGER_T2840230211_H
#define SUPPORTLOGGER_T2840230211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SupportLogger
struct  SupportLogger_t2840230211  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SupportLogger::LogTrafficStats
	bool ___LogTrafficStats_4;

public:
	inline static int32_t get_offset_of_LogTrafficStats_4() { return static_cast<int32_t>(offsetof(SupportLogger_t2840230211, ___LogTrafficStats_4)); }
	inline bool get_LogTrafficStats_4() const { return ___LogTrafficStats_4; }
	inline bool* get_address_of_LogTrafficStats_4() { return &___LogTrafficStats_4; }
	inline void set_LogTrafficStats_4(bool value)
	{
		___LogTrafficStats_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTLOGGER_T2840230211_H
#ifndef SUPPORTLOGGING_T3610999087_H
#define SUPPORTLOGGING_T3610999087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SupportLogging
struct  SupportLogging_t3610999087  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SupportLogging::LogTrafficStats
	bool ___LogTrafficStats_4;

public:
	inline static int32_t get_offset_of_LogTrafficStats_4() { return static_cast<int32_t>(offsetof(SupportLogging_t3610999087, ___LogTrafficStats_4)); }
	inline bool get_LogTrafficStats_4() const { return ___LogTrafficStats_4; }
	inline bool* get_address_of_LogTrafficStats_4() { return &___LogTrafficStats_4; }
	inline void set_LogTrafficStats_4(bool value)
	{
		___LogTrafficStats_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTLOGGING_T3610999087_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public AudioBehaviour_t2879336574
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_4;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_5;

public:
	inline static int32_t get_offset_of_spatializerExtension_4() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_4)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_4() const { return ___spatializerExtension_4; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_4() { return &___spatializerExtension_4; }
	inline void set_spatializerExtension_4(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_4 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_4), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_5() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_5)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_5() const { return ___ambisonicExtension_5; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_5() { return &___ambisonicExtension_5; }
	inline void set_ambisonicExtension_5(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_5 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef WAITINGPANEL_T1716730091_H
#define WAITINGPANEL_T1716730091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaitingPanel
struct  WaitingPanel_t1716730091  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITINGPANEL_T1716730091_H
#ifndef WINLOSEPANEL_T3114026018_H
#define WINLOSEPANEL_T3114026018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinLosePanel
struct  WinLosePanel_t3114026018  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject WinLosePanel::win
	GameObject_t1113636619 * ___win_4;
	// UnityEngine.GameObject WinLosePanel::lose
	GameObject_t1113636619 * ___lose_5;
	// UnityEngine.GameObject WinLosePanel::particle
	GameObject_t1113636619 * ___particle_6;

public:
	inline static int32_t get_offset_of_win_4() { return static_cast<int32_t>(offsetof(WinLosePanel_t3114026018, ___win_4)); }
	inline GameObject_t1113636619 * get_win_4() const { return ___win_4; }
	inline GameObject_t1113636619 ** get_address_of_win_4() { return &___win_4; }
	inline void set_win_4(GameObject_t1113636619 * value)
	{
		___win_4 = value;
		Il2CppCodeGenWriteBarrier((&___win_4), value);
	}

	inline static int32_t get_offset_of_lose_5() { return static_cast<int32_t>(offsetof(WinLosePanel_t3114026018, ___lose_5)); }
	inline GameObject_t1113636619 * get_lose_5() const { return ___lose_5; }
	inline GameObject_t1113636619 ** get_address_of_lose_5() { return &___lose_5; }
	inline void set_lose_5(GameObject_t1113636619 * value)
	{
		___lose_5 = value;
		Il2CppCodeGenWriteBarrier((&___lose_5), value);
	}

	inline static int32_t get_offset_of_particle_6() { return static_cast<int32_t>(offsetof(WinLosePanel_t3114026018, ___particle_6)); }
	inline GameObject_t1113636619 * get_particle_6() const { return ___particle_6; }
	inline GameObject_t1113636619 ** get_address_of_particle_6() { return &___particle_6; }
	inline void set_particle_6(GameObject_t1113636619 * value)
	{
		___particle_6 = value;
		Il2CppCodeGenWriteBarrier((&___particle_6), value);
	}
};

struct WinLosePanel_t3114026018_StaticFields
{
public:
	// WinLosePanel WinLosePanel::<Instance>k__BackingField
	WinLosePanel_t3114026018 * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WinLosePanel_t3114026018_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline WinLosePanel_t3114026018 * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline WinLosePanel_t3114026018 ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(WinLosePanel_t3114026018 * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINLOSEPANEL_T3114026018_H
#ifndef WINLOSEPARTICLE_T1640804235_H
#define WINLOSEPARTICLE_T1640804235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinLoseParticle
struct  WinLoseParticle_t1640804235  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINLOSEPARTICLE_T1640804235_H
#ifndef PUNBEHAVIOUR_T987309092_H
#define PUNBEHAVIOUR_T987309092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.PunBehaviour
struct  PunBehaviour_t987309092  : public MonoBehaviour_t3225183318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNBEHAVIOUR_T987309092_H
#ifndef PHOTONVIEW_T2207721820_H
#define PHOTONVIEW_T2207721820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonView
struct  PhotonView_t2207721820  : public MonoBehaviour_t3225183318
{
public:
	// System.Int32 PhotonView::ownerId
	int32_t ___ownerId_5;
	// System.Byte PhotonView::group
	uint8_t ___group_6;
	// System.Boolean PhotonView::mixedModeIsReliable
	bool ___mixedModeIsReliable_7;
	// System.Boolean PhotonView::OwnerShipWasTransfered
	bool ___OwnerShipWasTransfered_8;
	// System.Int32 PhotonView::prefixBackup
	int32_t ___prefixBackup_9;
	// System.Object[] PhotonView::instantiationDataField
	ObjectU5BU5D_t2843939325* ___instantiationDataField_10;
	// System.Object[] PhotonView::lastOnSerializeDataSent
	ObjectU5BU5D_t2843939325* ___lastOnSerializeDataSent_11;
	// System.Object[] PhotonView::lastOnSerializeDataReceived
	ObjectU5BU5D_t2843939325* ___lastOnSerializeDataReceived_12;
	// ViewSynchronization PhotonView::synchronization
	int32_t ___synchronization_13;
	// OnSerializeTransform PhotonView::onSerializeTransformOption
	int32_t ___onSerializeTransformOption_14;
	// OnSerializeRigidBody PhotonView::onSerializeRigidBodyOption
	int32_t ___onSerializeRigidBodyOption_15;
	// OwnershipOption PhotonView::ownershipTransfer
	int32_t ___ownershipTransfer_16;
	// System.Collections.Generic.List`1<UnityEngine.Component> PhotonView::ObservedComponents
	List_1_t3395709193 * ___ObservedComponents_17;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Component,System.Reflection.MethodInfo> PhotonView::m_OnSerializeMethodInfos
	Dictionary_2_t3676033689 * ___m_OnSerializeMethodInfos_18;
	// System.Int32 PhotonView::viewIdField
	int32_t ___viewIdField_19;
	// System.Int32 PhotonView::instantiationId
	int32_t ___instantiationId_20;
	// System.Int32 PhotonView::currentMasterID
	int32_t ___currentMasterID_21;
	// System.Boolean PhotonView::didAwake
	bool ___didAwake_22;
	// System.Boolean PhotonView::isRuntimeInstantiated
	bool ___isRuntimeInstantiated_23;
	// System.Boolean PhotonView::removedFromLocalViewList
	bool ___removedFromLocalViewList_24;
	// UnityEngine.MonoBehaviour[] PhotonView::RpcMonoBehaviours
	MonoBehaviourU5BU5D_t2007329276* ___RpcMonoBehaviours_25;
	// System.Reflection.MethodInfo PhotonView::OnSerializeMethodInfo
	MethodInfo_t * ___OnSerializeMethodInfo_26;
	// System.Boolean PhotonView::failedToFindOnSerialize
	bool ___failedToFindOnSerialize_27;

public:
	inline static int32_t get_offset_of_ownerId_5() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___ownerId_5)); }
	inline int32_t get_ownerId_5() const { return ___ownerId_5; }
	inline int32_t* get_address_of_ownerId_5() { return &___ownerId_5; }
	inline void set_ownerId_5(int32_t value)
	{
		___ownerId_5 = value;
	}

	inline static int32_t get_offset_of_group_6() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___group_6)); }
	inline uint8_t get_group_6() const { return ___group_6; }
	inline uint8_t* get_address_of_group_6() { return &___group_6; }
	inline void set_group_6(uint8_t value)
	{
		___group_6 = value;
	}

	inline static int32_t get_offset_of_mixedModeIsReliable_7() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___mixedModeIsReliable_7)); }
	inline bool get_mixedModeIsReliable_7() const { return ___mixedModeIsReliable_7; }
	inline bool* get_address_of_mixedModeIsReliable_7() { return &___mixedModeIsReliable_7; }
	inline void set_mixedModeIsReliable_7(bool value)
	{
		___mixedModeIsReliable_7 = value;
	}

	inline static int32_t get_offset_of_OwnerShipWasTransfered_8() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___OwnerShipWasTransfered_8)); }
	inline bool get_OwnerShipWasTransfered_8() const { return ___OwnerShipWasTransfered_8; }
	inline bool* get_address_of_OwnerShipWasTransfered_8() { return &___OwnerShipWasTransfered_8; }
	inline void set_OwnerShipWasTransfered_8(bool value)
	{
		___OwnerShipWasTransfered_8 = value;
	}

	inline static int32_t get_offset_of_prefixBackup_9() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___prefixBackup_9)); }
	inline int32_t get_prefixBackup_9() const { return ___prefixBackup_9; }
	inline int32_t* get_address_of_prefixBackup_9() { return &___prefixBackup_9; }
	inline void set_prefixBackup_9(int32_t value)
	{
		___prefixBackup_9 = value;
	}

	inline static int32_t get_offset_of_instantiationDataField_10() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___instantiationDataField_10)); }
	inline ObjectU5BU5D_t2843939325* get_instantiationDataField_10() const { return ___instantiationDataField_10; }
	inline ObjectU5BU5D_t2843939325** get_address_of_instantiationDataField_10() { return &___instantiationDataField_10; }
	inline void set_instantiationDataField_10(ObjectU5BU5D_t2843939325* value)
	{
		___instantiationDataField_10 = value;
		Il2CppCodeGenWriteBarrier((&___instantiationDataField_10), value);
	}

	inline static int32_t get_offset_of_lastOnSerializeDataSent_11() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___lastOnSerializeDataSent_11)); }
	inline ObjectU5BU5D_t2843939325* get_lastOnSerializeDataSent_11() const { return ___lastOnSerializeDataSent_11; }
	inline ObjectU5BU5D_t2843939325** get_address_of_lastOnSerializeDataSent_11() { return &___lastOnSerializeDataSent_11; }
	inline void set_lastOnSerializeDataSent_11(ObjectU5BU5D_t2843939325* value)
	{
		___lastOnSerializeDataSent_11 = value;
		Il2CppCodeGenWriteBarrier((&___lastOnSerializeDataSent_11), value);
	}

	inline static int32_t get_offset_of_lastOnSerializeDataReceived_12() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___lastOnSerializeDataReceived_12)); }
	inline ObjectU5BU5D_t2843939325* get_lastOnSerializeDataReceived_12() const { return ___lastOnSerializeDataReceived_12; }
	inline ObjectU5BU5D_t2843939325** get_address_of_lastOnSerializeDataReceived_12() { return &___lastOnSerializeDataReceived_12; }
	inline void set_lastOnSerializeDataReceived_12(ObjectU5BU5D_t2843939325* value)
	{
		___lastOnSerializeDataReceived_12 = value;
		Il2CppCodeGenWriteBarrier((&___lastOnSerializeDataReceived_12), value);
	}

	inline static int32_t get_offset_of_synchronization_13() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___synchronization_13)); }
	inline int32_t get_synchronization_13() const { return ___synchronization_13; }
	inline int32_t* get_address_of_synchronization_13() { return &___synchronization_13; }
	inline void set_synchronization_13(int32_t value)
	{
		___synchronization_13 = value;
	}

	inline static int32_t get_offset_of_onSerializeTransformOption_14() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___onSerializeTransformOption_14)); }
	inline int32_t get_onSerializeTransformOption_14() const { return ___onSerializeTransformOption_14; }
	inline int32_t* get_address_of_onSerializeTransformOption_14() { return &___onSerializeTransformOption_14; }
	inline void set_onSerializeTransformOption_14(int32_t value)
	{
		___onSerializeTransformOption_14 = value;
	}

	inline static int32_t get_offset_of_onSerializeRigidBodyOption_15() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___onSerializeRigidBodyOption_15)); }
	inline int32_t get_onSerializeRigidBodyOption_15() const { return ___onSerializeRigidBodyOption_15; }
	inline int32_t* get_address_of_onSerializeRigidBodyOption_15() { return &___onSerializeRigidBodyOption_15; }
	inline void set_onSerializeRigidBodyOption_15(int32_t value)
	{
		___onSerializeRigidBodyOption_15 = value;
	}

	inline static int32_t get_offset_of_ownershipTransfer_16() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___ownershipTransfer_16)); }
	inline int32_t get_ownershipTransfer_16() const { return ___ownershipTransfer_16; }
	inline int32_t* get_address_of_ownershipTransfer_16() { return &___ownershipTransfer_16; }
	inline void set_ownershipTransfer_16(int32_t value)
	{
		___ownershipTransfer_16 = value;
	}

	inline static int32_t get_offset_of_ObservedComponents_17() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___ObservedComponents_17)); }
	inline List_1_t3395709193 * get_ObservedComponents_17() const { return ___ObservedComponents_17; }
	inline List_1_t3395709193 ** get_address_of_ObservedComponents_17() { return &___ObservedComponents_17; }
	inline void set_ObservedComponents_17(List_1_t3395709193 * value)
	{
		___ObservedComponents_17 = value;
		Il2CppCodeGenWriteBarrier((&___ObservedComponents_17), value);
	}

	inline static int32_t get_offset_of_m_OnSerializeMethodInfos_18() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___m_OnSerializeMethodInfos_18)); }
	inline Dictionary_2_t3676033689 * get_m_OnSerializeMethodInfos_18() const { return ___m_OnSerializeMethodInfos_18; }
	inline Dictionary_2_t3676033689 ** get_address_of_m_OnSerializeMethodInfos_18() { return &___m_OnSerializeMethodInfos_18; }
	inline void set_m_OnSerializeMethodInfos_18(Dictionary_2_t3676033689 * value)
	{
		___m_OnSerializeMethodInfos_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSerializeMethodInfos_18), value);
	}

	inline static int32_t get_offset_of_viewIdField_19() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___viewIdField_19)); }
	inline int32_t get_viewIdField_19() const { return ___viewIdField_19; }
	inline int32_t* get_address_of_viewIdField_19() { return &___viewIdField_19; }
	inline void set_viewIdField_19(int32_t value)
	{
		___viewIdField_19 = value;
	}

	inline static int32_t get_offset_of_instantiationId_20() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___instantiationId_20)); }
	inline int32_t get_instantiationId_20() const { return ___instantiationId_20; }
	inline int32_t* get_address_of_instantiationId_20() { return &___instantiationId_20; }
	inline void set_instantiationId_20(int32_t value)
	{
		___instantiationId_20 = value;
	}

	inline static int32_t get_offset_of_currentMasterID_21() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___currentMasterID_21)); }
	inline int32_t get_currentMasterID_21() const { return ___currentMasterID_21; }
	inline int32_t* get_address_of_currentMasterID_21() { return &___currentMasterID_21; }
	inline void set_currentMasterID_21(int32_t value)
	{
		___currentMasterID_21 = value;
	}

	inline static int32_t get_offset_of_didAwake_22() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___didAwake_22)); }
	inline bool get_didAwake_22() const { return ___didAwake_22; }
	inline bool* get_address_of_didAwake_22() { return &___didAwake_22; }
	inline void set_didAwake_22(bool value)
	{
		___didAwake_22 = value;
	}

	inline static int32_t get_offset_of_isRuntimeInstantiated_23() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___isRuntimeInstantiated_23)); }
	inline bool get_isRuntimeInstantiated_23() const { return ___isRuntimeInstantiated_23; }
	inline bool* get_address_of_isRuntimeInstantiated_23() { return &___isRuntimeInstantiated_23; }
	inline void set_isRuntimeInstantiated_23(bool value)
	{
		___isRuntimeInstantiated_23 = value;
	}

	inline static int32_t get_offset_of_removedFromLocalViewList_24() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___removedFromLocalViewList_24)); }
	inline bool get_removedFromLocalViewList_24() const { return ___removedFromLocalViewList_24; }
	inline bool* get_address_of_removedFromLocalViewList_24() { return &___removedFromLocalViewList_24; }
	inline void set_removedFromLocalViewList_24(bool value)
	{
		___removedFromLocalViewList_24 = value;
	}

	inline static int32_t get_offset_of_RpcMonoBehaviours_25() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___RpcMonoBehaviours_25)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_RpcMonoBehaviours_25() const { return ___RpcMonoBehaviours_25; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_RpcMonoBehaviours_25() { return &___RpcMonoBehaviours_25; }
	inline void set_RpcMonoBehaviours_25(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___RpcMonoBehaviours_25 = value;
		Il2CppCodeGenWriteBarrier((&___RpcMonoBehaviours_25), value);
	}

	inline static int32_t get_offset_of_OnSerializeMethodInfo_26() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___OnSerializeMethodInfo_26)); }
	inline MethodInfo_t * get_OnSerializeMethodInfo_26() const { return ___OnSerializeMethodInfo_26; }
	inline MethodInfo_t ** get_address_of_OnSerializeMethodInfo_26() { return &___OnSerializeMethodInfo_26; }
	inline void set_OnSerializeMethodInfo_26(MethodInfo_t * value)
	{
		___OnSerializeMethodInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnSerializeMethodInfo_26), value);
	}

	inline static int32_t get_offset_of_failedToFindOnSerialize_27() { return static_cast<int32_t>(offsetof(PhotonView_t2207721820, ___failedToFindOnSerialize_27)); }
	inline bool get_failedToFindOnSerialize_27() const { return ___failedToFindOnSerialize_27; }
	inline bool* get_address_of_failedToFindOnSerialize_27() { return &___failedToFindOnSerialize_27; }
	inline void set_failedToFindOnSerialize_27(bool value)
	{
		___failedToFindOnSerialize_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONVIEW_T2207721820_H
#ifndef SMOOTHSYNCMOVEMENT_T1809568931_H
#define SMOOTHSYNCMOVEMENT_T1809568931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothSyncMovement
struct  SmoothSyncMovement_t1809568931  : public MonoBehaviour_t3225183318
{
public:
	// System.Single SmoothSyncMovement::SmoothingDelay
	float ___SmoothingDelay_5;
	// UnityEngine.Vector3 SmoothSyncMovement::correctPlayerPos
	Vector3_t3722313464  ___correctPlayerPos_6;
	// UnityEngine.Quaternion SmoothSyncMovement::correctPlayerRot
	Quaternion_t2301928331  ___correctPlayerRot_7;

public:
	inline static int32_t get_offset_of_SmoothingDelay_5() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t1809568931, ___SmoothingDelay_5)); }
	inline float get_SmoothingDelay_5() const { return ___SmoothingDelay_5; }
	inline float* get_address_of_SmoothingDelay_5() { return &___SmoothingDelay_5; }
	inline void set_SmoothingDelay_5(float value)
	{
		___SmoothingDelay_5 = value;
	}

	inline static int32_t get_offset_of_correctPlayerPos_6() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t1809568931, ___correctPlayerPos_6)); }
	inline Vector3_t3722313464  get_correctPlayerPos_6() const { return ___correctPlayerPos_6; }
	inline Vector3_t3722313464 * get_address_of_correctPlayerPos_6() { return &___correctPlayerPos_6; }
	inline void set_correctPlayerPos_6(Vector3_t3722313464  value)
	{
		___correctPlayerPos_6 = value;
	}

	inline static int32_t get_offset_of_correctPlayerRot_7() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t1809568931, ___correctPlayerRot_7)); }
	inline Quaternion_t2301928331  get_correctPlayerRot_7() const { return ___correctPlayerRot_7; }
	inline Quaternion_t2301928331 * get_address_of_correctPlayerRot_7() { return &___correctPlayerRot_7; }
	inline void set_correctPlayerRot_7(Quaternion_t2301928331  value)
	{
		___correctPlayerRot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHSYNCMOVEMENT_T1809568931_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef PUNTURNMANAGER_T1223962931_H
#define PUNTURNMANAGER_T1223962931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PunTurnManager
struct  PunTurnManager_t1223962931  : public PunBehaviour_t987309092
{
public:
	// System.Single PunTurnManager::TurnDuration
	float ___TurnDuration_5;
	// IPunTurnManagerCallbacks PunTurnManager::TurnManagerListener
	RuntimeObject* ___TurnManagerListener_6;
	// System.Collections.Generic.HashSet`1<PhotonPlayer> PunTurnManager::finishedPlayers
	HashSet_1_t1870099031 * ___finishedPlayers_7;
	// System.Boolean PunTurnManager::_isOverCallProcessed
	bool ____isOverCallProcessed_11;

public:
	inline static int32_t get_offset_of_TurnDuration_5() { return static_cast<int32_t>(offsetof(PunTurnManager_t1223962931, ___TurnDuration_5)); }
	inline float get_TurnDuration_5() const { return ___TurnDuration_5; }
	inline float* get_address_of_TurnDuration_5() { return &___TurnDuration_5; }
	inline void set_TurnDuration_5(float value)
	{
		___TurnDuration_5 = value;
	}

	inline static int32_t get_offset_of_TurnManagerListener_6() { return static_cast<int32_t>(offsetof(PunTurnManager_t1223962931, ___TurnManagerListener_6)); }
	inline RuntimeObject* get_TurnManagerListener_6() const { return ___TurnManagerListener_6; }
	inline RuntimeObject** get_address_of_TurnManagerListener_6() { return &___TurnManagerListener_6; }
	inline void set_TurnManagerListener_6(RuntimeObject* value)
	{
		___TurnManagerListener_6 = value;
		Il2CppCodeGenWriteBarrier((&___TurnManagerListener_6), value);
	}

	inline static int32_t get_offset_of_finishedPlayers_7() { return static_cast<int32_t>(offsetof(PunTurnManager_t1223962931, ___finishedPlayers_7)); }
	inline HashSet_1_t1870099031 * get_finishedPlayers_7() const { return ___finishedPlayers_7; }
	inline HashSet_1_t1870099031 ** get_address_of_finishedPlayers_7() { return &___finishedPlayers_7; }
	inline void set_finishedPlayers_7(HashSet_1_t1870099031 * value)
	{
		___finishedPlayers_7 = value;
		Il2CppCodeGenWriteBarrier((&___finishedPlayers_7), value);
	}

	inline static int32_t get_offset_of__isOverCallProcessed_11() { return static_cast<int32_t>(offsetof(PunTurnManager_t1223962931, ____isOverCallProcessed_11)); }
	inline bool get__isOverCallProcessed_11() const { return ____isOverCallProcessed_11; }
	inline bool* get_address_of__isOverCallProcessed_11() { return &____isOverCallProcessed_11; }
	inline void set__isOverCallProcessed_11(bool value)
	{
		____isOverCallProcessed_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNTURNMANAGER_T1223962931_H
#ifndef RPSCORE_T2154144697_H
#define RPSCORE_T2154144697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RpsCore
struct  RpsCore_t2154144697  : public PunBehaviour_t987309092
{
public:
	// UnityEngine.RectTransform RpsCore::ConnectUiView
	RectTransform_t3704657025 * ___ConnectUiView_5;
	// UnityEngine.RectTransform RpsCore::GameUiView
	RectTransform_t3704657025 * ___GameUiView_6;
	// UnityEngine.CanvasGroup RpsCore::ButtonCanvasGroup
	CanvasGroup_t4083511760 * ___ButtonCanvasGroup_7;
	// UnityEngine.RectTransform RpsCore::TimerFillImage
	RectTransform_t3704657025 * ___TimerFillImage_8;
	// UnityEngine.UI.Text RpsCore::TurnText
	Text_t1901882714 * ___TurnText_9;
	// UnityEngine.UI.Text RpsCore::TimeText
	Text_t1901882714 * ___TimeText_10;
	// UnityEngine.UI.Text RpsCore::RemotePlayerText
	Text_t1901882714 * ___RemotePlayerText_11;
	// UnityEngine.UI.Text RpsCore::LocalPlayerText
	Text_t1901882714 * ___LocalPlayerText_12;
	// UnityEngine.UI.Image RpsCore::WinOrLossImage
	Image_t2670269651 * ___WinOrLossImage_13;
	// UnityEngine.UI.Image RpsCore::localSelectionImage
	Image_t2670269651 * ___localSelectionImage_14;
	// RpsCore/Hand RpsCore::localSelection
	int32_t ___localSelection_15;
	// UnityEngine.UI.Image RpsCore::remoteSelectionImage
	Image_t2670269651 * ___remoteSelectionImage_16;
	// RpsCore/Hand RpsCore::remoteSelection
	int32_t ___remoteSelection_17;
	// UnityEngine.Sprite RpsCore::SelectedRock
	Sprite_t280657092 * ___SelectedRock_18;
	// UnityEngine.Sprite RpsCore::SelectedPaper
	Sprite_t280657092 * ___SelectedPaper_19;
	// UnityEngine.Sprite RpsCore::SelectedScissors
	Sprite_t280657092 * ___SelectedScissors_20;
	// UnityEngine.Sprite RpsCore::SpriteWin
	Sprite_t280657092 * ___SpriteWin_21;
	// UnityEngine.Sprite RpsCore::SpriteLose
	Sprite_t280657092 * ___SpriteLose_22;
	// UnityEngine.Sprite RpsCore::SpriteDraw
	Sprite_t280657092 * ___SpriteDraw_23;
	// UnityEngine.RectTransform RpsCore::DisconnectedPanel
	RectTransform_t3704657025 * ___DisconnectedPanel_24;
	// RpsCore/ResultType RpsCore::result
	int32_t ___result_25;
	// PunTurnManager RpsCore::turnManager
	PunTurnManager_t1223962931 * ___turnManager_26;
	// RpsCore/Hand RpsCore::randomHand
	int32_t ___randomHand_27;
	// System.Boolean RpsCore::IsShowingResults
	bool ___IsShowingResults_28;

public:
	inline static int32_t get_offset_of_ConnectUiView_5() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___ConnectUiView_5)); }
	inline RectTransform_t3704657025 * get_ConnectUiView_5() const { return ___ConnectUiView_5; }
	inline RectTransform_t3704657025 ** get_address_of_ConnectUiView_5() { return &___ConnectUiView_5; }
	inline void set_ConnectUiView_5(RectTransform_t3704657025 * value)
	{
		___ConnectUiView_5 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectUiView_5), value);
	}

	inline static int32_t get_offset_of_GameUiView_6() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___GameUiView_6)); }
	inline RectTransform_t3704657025 * get_GameUiView_6() const { return ___GameUiView_6; }
	inline RectTransform_t3704657025 ** get_address_of_GameUiView_6() { return &___GameUiView_6; }
	inline void set_GameUiView_6(RectTransform_t3704657025 * value)
	{
		___GameUiView_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameUiView_6), value);
	}

	inline static int32_t get_offset_of_ButtonCanvasGroup_7() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___ButtonCanvasGroup_7)); }
	inline CanvasGroup_t4083511760 * get_ButtonCanvasGroup_7() const { return ___ButtonCanvasGroup_7; }
	inline CanvasGroup_t4083511760 ** get_address_of_ButtonCanvasGroup_7() { return &___ButtonCanvasGroup_7; }
	inline void set_ButtonCanvasGroup_7(CanvasGroup_t4083511760 * value)
	{
		___ButtonCanvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCanvasGroup_7), value);
	}

	inline static int32_t get_offset_of_TimerFillImage_8() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___TimerFillImage_8)); }
	inline RectTransform_t3704657025 * get_TimerFillImage_8() const { return ___TimerFillImage_8; }
	inline RectTransform_t3704657025 ** get_address_of_TimerFillImage_8() { return &___TimerFillImage_8; }
	inline void set_TimerFillImage_8(RectTransform_t3704657025 * value)
	{
		___TimerFillImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___TimerFillImage_8), value);
	}

	inline static int32_t get_offset_of_TurnText_9() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___TurnText_9)); }
	inline Text_t1901882714 * get_TurnText_9() const { return ___TurnText_9; }
	inline Text_t1901882714 ** get_address_of_TurnText_9() { return &___TurnText_9; }
	inline void set_TurnText_9(Text_t1901882714 * value)
	{
		___TurnText_9 = value;
		Il2CppCodeGenWriteBarrier((&___TurnText_9), value);
	}

	inline static int32_t get_offset_of_TimeText_10() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___TimeText_10)); }
	inline Text_t1901882714 * get_TimeText_10() const { return ___TimeText_10; }
	inline Text_t1901882714 ** get_address_of_TimeText_10() { return &___TimeText_10; }
	inline void set_TimeText_10(Text_t1901882714 * value)
	{
		___TimeText_10 = value;
		Il2CppCodeGenWriteBarrier((&___TimeText_10), value);
	}

	inline static int32_t get_offset_of_RemotePlayerText_11() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___RemotePlayerText_11)); }
	inline Text_t1901882714 * get_RemotePlayerText_11() const { return ___RemotePlayerText_11; }
	inline Text_t1901882714 ** get_address_of_RemotePlayerText_11() { return &___RemotePlayerText_11; }
	inline void set_RemotePlayerText_11(Text_t1901882714 * value)
	{
		___RemotePlayerText_11 = value;
		Il2CppCodeGenWriteBarrier((&___RemotePlayerText_11), value);
	}

	inline static int32_t get_offset_of_LocalPlayerText_12() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___LocalPlayerText_12)); }
	inline Text_t1901882714 * get_LocalPlayerText_12() const { return ___LocalPlayerText_12; }
	inline Text_t1901882714 ** get_address_of_LocalPlayerText_12() { return &___LocalPlayerText_12; }
	inline void set_LocalPlayerText_12(Text_t1901882714 * value)
	{
		___LocalPlayerText_12 = value;
		Il2CppCodeGenWriteBarrier((&___LocalPlayerText_12), value);
	}

	inline static int32_t get_offset_of_WinOrLossImage_13() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___WinOrLossImage_13)); }
	inline Image_t2670269651 * get_WinOrLossImage_13() const { return ___WinOrLossImage_13; }
	inline Image_t2670269651 ** get_address_of_WinOrLossImage_13() { return &___WinOrLossImage_13; }
	inline void set_WinOrLossImage_13(Image_t2670269651 * value)
	{
		___WinOrLossImage_13 = value;
		Il2CppCodeGenWriteBarrier((&___WinOrLossImage_13), value);
	}

	inline static int32_t get_offset_of_localSelectionImage_14() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___localSelectionImage_14)); }
	inline Image_t2670269651 * get_localSelectionImage_14() const { return ___localSelectionImage_14; }
	inline Image_t2670269651 ** get_address_of_localSelectionImage_14() { return &___localSelectionImage_14; }
	inline void set_localSelectionImage_14(Image_t2670269651 * value)
	{
		___localSelectionImage_14 = value;
		Il2CppCodeGenWriteBarrier((&___localSelectionImage_14), value);
	}

	inline static int32_t get_offset_of_localSelection_15() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___localSelection_15)); }
	inline int32_t get_localSelection_15() const { return ___localSelection_15; }
	inline int32_t* get_address_of_localSelection_15() { return &___localSelection_15; }
	inline void set_localSelection_15(int32_t value)
	{
		___localSelection_15 = value;
	}

	inline static int32_t get_offset_of_remoteSelectionImage_16() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___remoteSelectionImage_16)); }
	inline Image_t2670269651 * get_remoteSelectionImage_16() const { return ___remoteSelectionImage_16; }
	inline Image_t2670269651 ** get_address_of_remoteSelectionImage_16() { return &___remoteSelectionImage_16; }
	inline void set_remoteSelectionImage_16(Image_t2670269651 * value)
	{
		___remoteSelectionImage_16 = value;
		Il2CppCodeGenWriteBarrier((&___remoteSelectionImage_16), value);
	}

	inline static int32_t get_offset_of_remoteSelection_17() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___remoteSelection_17)); }
	inline int32_t get_remoteSelection_17() const { return ___remoteSelection_17; }
	inline int32_t* get_address_of_remoteSelection_17() { return &___remoteSelection_17; }
	inline void set_remoteSelection_17(int32_t value)
	{
		___remoteSelection_17 = value;
	}

	inline static int32_t get_offset_of_SelectedRock_18() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SelectedRock_18)); }
	inline Sprite_t280657092 * get_SelectedRock_18() const { return ___SelectedRock_18; }
	inline Sprite_t280657092 ** get_address_of_SelectedRock_18() { return &___SelectedRock_18; }
	inline void set_SelectedRock_18(Sprite_t280657092 * value)
	{
		___SelectedRock_18 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedRock_18), value);
	}

	inline static int32_t get_offset_of_SelectedPaper_19() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SelectedPaper_19)); }
	inline Sprite_t280657092 * get_SelectedPaper_19() const { return ___SelectedPaper_19; }
	inline Sprite_t280657092 ** get_address_of_SelectedPaper_19() { return &___SelectedPaper_19; }
	inline void set_SelectedPaper_19(Sprite_t280657092 * value)
	{
		___SelectedPaper_19 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedPaper_19), value);
	}

	inline static int32_t get_offset_of_SelectedScissors_20() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SelectedScissors_20)); }
	inline Sprite_t280657092 * get_SelectedScissors_20() const { return ___SelectedScissors_20; }
	inline Sprite_t280657092 ** get_address_of_SelectedScissors_20() { return &___SelectedScissors_20; }
	inline void set_SelectedScissors_20(Sprite_t280657092 * value)
	{
		___SelectedScissors_20 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedScissors_20), value);
	}

	inline static int32_t get_offset_of_SpriteWin_21() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SpriteWin_21)); }
	inline Sprite_t280657092 * get_SpriteWin_21() const { return ___SpriteWin_21; }
	inline Sprite_t280657092 ** get_address_of_SpriteWin_21() { return &___SpriteWin_21; }
	inline void set_SpriteWin_21(Sprite_t280657092 * value)
	{
		___SpriteWin_21 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteWin_21), value);
	}

	inline static int32_t get_offset_of_SpriteLose_22() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SpriteLose_22)); }
	inline Sprite_t280657092 * get_SpriteLose_22() const { return ___SpriteLose_22; }
	inline Sprite_t280657092 ** get_address_of_SpriteLose_22() { return &___SpriteLose_22; }
	inline void set_SpriteLose_22(Sprite_t280657092 * value)
	{
		___SpriteLose_22 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteLose_22), value);
	}

	inline static int32_t get_offset_of_SpriteDraw_23() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___SpriteDraw_23)); }
	inline Sprite_t280657092 * get_SpriteDraw_23() const { return ___SpriteDraw_23; }
	inline Sprite_t280657092 ** get_address_of_SpriteDraw_23() { return &___SpriteDraw_23; }
	inline void set_SpriteDraw_23(Sprite_t280657092 * value)
	{
		___SpriteDraw_23 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteDraw_23), value);
	}

	inline static int32_t get_offset_of_DisconnectedPanel_24() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___DisconnectedPanel_24)); }
	inline RectTransform_t3704657025 * get_DisconnectedPanel_24() const { return ___DisconnectedPanel_24; }
	inline RectTransform_t3704657025 ** get_address_of_DisconnectedPanel_24() { return &___DisconnectedPanel_24; }
	inline void set_DisconnectedPanel_24(RectTransform_t3704657025 * value)
	{
		___DisconnectedPanel_24 = value;
		Il2CppCodeGenWriteBarrier((&___DisconnectedPanel_24), value);
	}

	inline static int32_t get_offset_of_result_25() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___result_25)); }
	inline int32_t get_result_25() const { return ___result_25; }
	inline int32_t* get_address_of_result_25() { return &___result_25; }
	inline void set_result_25(int32_t value)
	{
		___result_25 = value;
	}

	inline static int32_t get_offset_of_turnManager_26() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___turnManager_26)); }
	inline PunTurnManager_t1223962931 * get_turnManager_26() const { return ___turnManager_26; }
	inline PunTurnManager_t1223962931 ** get_address_of_turnManager_26() { return &___turnManager_26; }
	inline void set_turnManager_26(PunTurnManager_t1223962931 * value)
	{
		___turnManager_26 = value;
		Il2CppCodeGenWriteBarrier((&___turnManager_26), value);
	}

	inline static int32_t get_offset_of_randomHand_27() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___randomHand_27)); }
	inline int32_t get_randomHand_27() const { return ___randomHand_27; }
	inline int32_t* get_address_of_randomHand_27() { return &___randomHand_27; }
	inline void set_randomHand_27(int32_t value)
	{
		___randomHand_27 = value;
	}

	inline static int32_t get_offset_of_IsShowingResults_28() { return static_cast<int32_t>(offsetof(RpsCore_t2154144697, ___IsShowingResults_28)); }
	inline bool get_IsShowingResults_28() const { return ___IsShowingResults_28; }
	inline bool* get_address_of_IsShowingResults_28() { return &___IsShowingResults_28; }
	inline void set_IsShowingResults_28(bool value)
	{
		___IsShowingResults_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPSCORE_T2154144697_H
#ifndef BUTTON_T4055032469_H
#define BUTTON_T4055032469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t4055032469  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t48803504 * ___m_OnClick_18;

public:
	inline static int32_t get_offset_of_m_OnClick_18() { return static_cast<int32_t>(offsetof(Button_t4055032469, ___m_OnClick_18)); }
	inline ButtonClickedEvent_t48803504 * get_m_OnClick_18() const { return ___m_OnClick_18; }
	inline ButtonClickedEvent_t48803504 ** get_address_of_m_OnClick_18() { return &___m_OnClick_18; }
	inline void set_m_OnClick_18(ButtonClickedEvent_t48803504 * value)
	{
		___m_OnClick_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T4055032469_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef YCCIONETWORKMANAGER_T3600504787_H
#define YCCIONETWORKMANAGER_T3600504787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YccioNetworkManager
struct  YccioNetworkManager_t3600504787  : public PunBehaviour_t987309092
{
public:
	// PunTurnManager YccioNetworkManager::turnManager
	PunTurnManager_t1223962931 * ___turnManager_5;
	// SceneTransition YccioNetworkManager::st
	SceneTransition_t1138091307 * ___st_6;
	// PlayerData YccioNetworkManager::pd
	PlayerData_t220878115 * ___pd_7;
	// System.String YccioNetworkManager::myRoomName
	String_t* ___myRoomName_8;
	// RoomList YccioNetworkManager::roomList
	RoomList_t2314265074 * ___roomList_9;
	// StartButton YccioNetworkManager::startButton
	StartButton_t1998348287 * ___startButton_10;
	// PlayerStatusPanel YccioNetworkManager::statusPanel
	PlayerStatusPanel_t1209538314 * ___statusPanel_11;
	// System.Int32 YccioNetworkManager::maxStone
	int32_t ___maxStone_12;
	// System.Int32 YccioNetworkManager::numStone
	int32_t ___numStone_13;
	// System.Int32 YccioNetworkManager::totalStone
	int32_t ___totalStone_14;
	// System.Int32 YccioNetworkManager::playOrder
	int32_t ___playOrder_15;
	// System.Int32 YccioNetworkManager::orderOffset
	int32_t ___orderOffset_16;
	// System.Int32 YccioNetworkManager::playerNumber
	int32_t ___playerNumber_17;
	// System.Boolean YccioNetworkManager::isMyTurn
	bool ___isMyTurn_18;
	// System.Int32 YccioNetworkManager::fieldOwner
	int32_t ___fieldOwner_19;
	// System.Int32 YccioNetworkManager::curOrder
	int32_t ___curOrder_20;
	// System.Boolean YccioNetworkManager::isNoCloud3
	bool ___isNoCloud3_21;
	// System.Boolean YccioNetworkManager::isWaiting
	bool ___isWaiting_22;
	// System.Int32[] YccioNetworkManager::remainStones
	Int32U5BU5D_t385246372* ___remainStones_23;
	// System.Boolean YccioNetworkManager::isWinner
	bool ___isWinner_24;

public:
	inline static int32_t get_offset_of_turnManager_5() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___turnManager_5)); }
	inline PunTurnManager_t1223962931 * get_turnManager_5() const { return ___turnManager_5; }
	inline PunTurnManager_t1223962931 ** get_address_of_turnManager_5() { return &___turnManager_5; }
	inline void set_turnManager_5(PunTurnManager_t1223962931 * value)
	{
		___turnManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___turnManager_5), value);
	}

	inline static int32_t get_offset_of_st_6() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___st_6)); }
	inline SceneTransition_t1138091307 * get_st_6() const { return ___st_6; }
	inline SceneTransition_t1138091307 ** get_address_of_st_6() { return &___st_6; }
	inline void set_st_6(SceneTransition_t1138091307 * value)
	{
		___st_6 = value;
		Il2CppCodeGenWriteBarrier((&___st_6), value);
	}

	inline static int32_t get_offset_of_pd_7() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___pd_7)); }
	inline PlayerData_t220878115 * get_pd_7() const { return ___pd_7; }
	inline PlayerData_t220878115 ** get_address_of_pd_7() { return &___pd_7; }
	inline void set_pd_7(PlayerData_t220878115 * value)
	{
		___pd_7 = value;
		Il2CppCodeGenWriteBarrier((&___pd_7), value);
	}

	inline static int32_t get_offset_of_myRoomName_8() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___myRoomName_8)); }
	inline String_t* get_myRoomName_8() const { return ___myRoomName_8; }
	inline String_t** get_address_of_myRoomName_8() { return &___myRoomName_8; }
	inline void set_myRoomName_8(String_t* value)
	{
		___myRoomName_8 = value;
		Il2CppCodeGenWriteBarrier((&___myRoomName_8), value);
	}

	inline static int32_t get_offset_of_roomList_9() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___roomList_9)); }
	inline RoomList_t2314265074 * get_roomList_9() const { return ___roomList_9; }
	inline RoomList_t2314265074 ** get_address_of_roomList_9() { return &___roomList_9; }
	inline void set_roomList_9(RoomList_t2314265074 * value)
	{
		___roomList_9 = value;
		Il2CppCodeGenWriteBarrier((&___roomList_9), value);
	}

	inline static int32_t get_offset_of_startButton_10() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___startButton_10)); }
	inline StartButton_t1998348287 * get_startButton_10() const { return ___startButton_10; }
	inline StartButton_t1998348287 ** get_address_of_startButton_10() { return &___startButton_10; }
	inline void set_startButton_10(StartButton_t1998348287 * value)
	{
		___startButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___startButton_10), value);
	}

	inline static int32_t get_offset_of_statusPanel_11() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___statusPanel_11)); }
	inline PlayerStatusPanel_t1209538314 * get_statusPanel_11() const { return ___statusPanel_11; }
	inline PlayerStatusPanel_t1209538314 ** get_address_of_statusPanel_11() { return &___statusPanel_11; }
	inline void set_statusPanel_11(PlayerStatusPanel_t1209538314 * value)
	{
		___statusPanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___statusPanel_11), value);
	}

	inline static int32_t get_offset_of_maxStone_12() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___maxStone_12)); }
	inline int32_t get_maxStone_12() const { return ___maxStone_12; }
	inline int32_t* get_address_of_maxStone_12() { return &___maxStone_12; }
	inline void set_maxStone_12(int32_t value)
	{
		___maxStone_12 = value;
	}

	inline static int32_t get_offset_of_numStone_13() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___numStone_13)); }
	inline int32_t get_numStone_13() const { return ___numStone_13; }
	inline int32_t* get_address_of_numStone_13() { return &___numStone_13; }
	inline void set_numStone_13(int32_t value)
	{
		___numStone_13 = value;
	}

	inline static int32_t get_offset_of_totalStone_14() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___totalStone_14)); }
	inline int32_t get_totalStone_14() const { return ___totalStone_14; }
	inline int32_t* get_address_of_totalStone_14() { return &___totalStone_14; }
	inline void set_totalStone_14(int32_t value)
	{
		___totalStone_14 = value;
	}

	inline static int32_t get_offset_of_playOrder_15() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___playOrder_15)); }
	inline int32_t get_playOrder_15() const { return ___playOrder_15; }
	inline int32_t* get_address_of_playOrder_15() { return &___playOrder_15; }
	inline void set_playOrder_15(int32_t value)
	{
		___playOrder_15 = value;
	}

	inline static int32_t get_offset_of_orderOffset_16() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___orderOffset_16)); }
	inline int32_t get_orderOffset_16() const { return ___orderOffset_16; }
	inline int32_t* get_address_of_orderOffset_16() { return &___orderOffset_16; }
	inline void set_orderOffset_16(int32_t value)
	{
		___orderOffset_16 = value;
	}

	inline static int32_t get_offset_of_playerNumber_17() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___playerNumber_17)); }
	inline int32_t get_playerNumber_17() const { return ___playerNumber_17; }
	inline int32_t* get_address_of_playerNumber_17() { return &___playerNumber_17; }
	inline void set_playerNumber_17(int32_t value)
	{
		___playerNumber_17 = value;
	}

	inline static int32_t get_offset_of_isMyTurn_18() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___isMyTurn_18)); }
	inline bool get_isMyTurn_18() const { return ___isMyTurn_18; }
	inline bool* get_address_of_isMyTurn_18() { return &___isMyTurn_18; }
	inline void set_isMyTurn_18(bool value)
	{
		___isMyTurn_18 = value;
	}

	inline static int32_t get_offset_of_fieldOwner_19() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___fieldOwner_19)); }
	inline int32_t get_fieldOwner_19() const { return ___fieldOwner_19; }
	inline int32_t* get_address_of_fieldOwner_19() { return &___fieldOwner_19; }
	inline void set_fieldOwner_19(int32_t value)
	{
		___fieldOwner_19 = value;
	}

	inline static int32_t get_offset_of_curOrder_20() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___curOrder_20)); }
	inline int32_t get_curOrder_20() const { return ___curOrder_20; }
	inline int32_t* get_address_of_curOrder_20() { return &___curOrder_20; }
	inline void set_curOrder_20(int32_t value)
	{
		___curOrder_20 = value;
	}

	inline static int32_t get_offset_of_isNoCloud3_21() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___isNoCloud3_21)); }
	inline bool get_isNoCloud3_21() const { return ___isNoCloud3_21; }
	inline bool* get_address_of_isNoCloud3_21() { return &___isNoCloud3_21; }
	inline void set_isNoCloud3_21(bool value)
	{
		___isNoCloud3_21 = value;
	}

	inline static int32_t get_offset_of_isWaiting_22() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___isWaiting_22)); }
	inline bool get_isWaiting_22() const { return ___isWaiting_22; }
	inline bool* get_address_of_isWaiting_22() { return &___isWaiting_22; }
	inline void set_isWaiting_22(bool value)
	{
		___isWaiting_22 = value;
	}

	inline static int32_t get_offset_of_remainStones_23() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___remainStones_23)); }
	inline Int32U5BU5D_t385246372* get_remainStones_23() const { return ___remainStones_23; }
	inline Int32U5BU5D_t385246372** get_address_of_remainStones_23() { return &___remainStones_23; }
	inline void set_remainStones_23(Int32U5BU5D_t385246372* value)
	{
		___remainStones_23 = value;
		Il2CppCodeGenWriteBarrier((&___remainStones_23), value);
	}

	inline static int32_t get_offset_of_isWinner_24() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787, ___isWinner_24)); }
	inline bool get_isWinner_24() const { return ___isWinner_24; }
	inline bool* get_address_of_isWinner_24() { return &___isWinner_24; }
	inline void set_isWinner_24(bool value)
	{
		___isWinner_24 = value;
	}
};

struct YccioNetworkManager_t3600504787_StaticFields
{
public:
	// YccioNetworkManager YccioNetworkManager::<Instance>k__BackingField
	YccioNetworkManager_t3600504787 * ___U3CInstanceU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(YccioNetworkManager_t3600504787_StaticFields, ___U3CInstanceU3Ek__BackingField_25)); }
	inline YccioNetworkManager_t3600504787 * get_U3CInstanceU3Ek__BackingField_25() const { return ___U3CInstanceU3Ek__BackingField_25; }
	inline YccioNetworkManager_t3600504787 ** get_address_of_U3CInstanceU3Ek__BackingField_25() { return &___U3CInstanceU3Ek__BackingField_25; }
	inline void set_U3CInstanceU3Ek__BackingField_25(YccioNetworkManager_t3600504787 * value)
	{
		___U3CInstanceU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YCCIONETWORKMANAGER_T3600504787_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_31;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_32;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_33;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_34;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_35;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_36;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_37;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_38;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_39;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_40;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_41;

public:
	inline static int32_t get_offset_of_m_Sprite_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_31)); }
	inline Sprite_t280657092 * get_m_Sprite_31() const { return ___m_Sprite_31; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_31() { return &___m_Sprite_31; }
	inline void set_m_Sprite_31(Sprite_t280657092 * value)
	{
		___m_Sprite_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_31), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_32)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_32() const { return ___m_OverrideSprite_32; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_32() { return &___m_OverrideSprite_32; }
	inline void set_m_OverrideSprite_32(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_32), value);
	}

	inline static int32_t get_offset_of_m_Type_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_33)); }
	inline int32_t get_m_Type_33() const { return ___m_Type_33; }
	inline int32_t* get_address_of_m_Type_33() { return &___m_Type_33; }
	inline void set_m_Type_33(int32_t value)
	{
		___m_Type_33 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_34)); }
	inline bool get_m_PreserveAspect_34() const { return ___m_PreserveAspect_34; }
	inline bool* get_address_of_m_PreserveAspect_34() { return &___m_PreserveAspect_34; }
	inline void set_m_PreserveAspect_34(bool value)
	{
		___m_PreserveAspect_34 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_35)); }
	inline bool get_m_FillCenter_35() const { return ___m_FillCenter_35; }
	inline bool* get_address_of_m_FillCenter_35() { return &___m_FillCenter_35; }
	inline void set_m_FillCenter_35(bool value)
	{
		___m_FillCenter_35 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_36)); }
	inline int32_t get_m_FillMethod_36() const { return ___m_FillMethod_36; }
	inline int32_t* get_address_of_m_FillMethod_36() { return &___m_FillMethod_36; }
	inline void set_m_FillMethod_36(int32_t value)
	{
		___m_FillMethod_36 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_37)); }
	inline float get_m_FillAmount_37() const { return ___m_FillAmount_37; }
	inline float* get_address_of_m_FillAmount_37() { return &___m_FillAmount_37; }
	inline void set_m_FillAmount_37(float value)
	{
		___m_FillAmount_37 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_38)); }
	inline bool get_m_FillClockwise_38() const { return ___m_FillClockwise_38; }
	inline bool* get_address_of_m_FillClockwise_38() { return &___m_FillClockwise_38; }
	inline void set_m_FillClockwise_38(bool value)
	{
		___m_FillClockwise_38 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_39() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_39)); }
	inline int32_t get_m_FillOrigin_39() const { return ___m_FillOrigin_39; }
	inline int32_t* get_address_of_m_FillOrigin_39() { return &___m_FillOrigin_39; }
	inline void set_m_FillOrigin_39(int32_t value)
	{
		___m_FillOrigin_39 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_40() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_40)); }
	inline float get_m_AlphaHitTestMinimumThreshold_40() const { return ___m_AlphaHitTestMinimumThreshold_40; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_40() { return &___m_AlphaHitTestMinimumThreshold_40; }
	inline void set_m_AlphaHitTestMinimumThreshold_40(float value)
	{
		___m_AlphaHitTestMinimumThreshold_40 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_41() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Tracked_41)); }
	inline bool get_m_Tracked_41() const { return ___m_Tracked_41; }
	inline bool* get_address_of_m_Tracked_41() { return &___m_Tracked_41; }
	inline void set_m_Tracked_41(bool value)
	{
		___m_Tracked_41 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_30;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_42;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_43;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_44;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_45;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t4142344393 * ___m_TrackedTexturelessImages_46;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_47;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.UI.Image::<>f__mg$cache0
	Action_1_t819399007 * ___U3CU3Ef__mgU24cache0_48;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_30() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_30)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_30() const { return ___s_ETC1DefaultUI_30; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_30() { return &___s_ETC1DefaultUI_30; }
	inline void set_s_ETC1DefaultUI_30(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_30), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_42)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_42() const { return ___s_VertScratch_42; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_42() { return &___s_VertScratch_42; }
	inline void set_s_VertScratch_42(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_42), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_43() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_43)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_43() const { return ___s_UVScratch_43; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_43() { return &___s_UVScratch_43; }
	inline void set_s_UVScratch_43(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_43), value);
	}

	inline static int32_t get_offset_of_s_Xy_44() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_44)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_44() const { return ___s_Xy_44; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_44() { return &___s_Xy_44; }
	inline void set_s_Xy_44(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_44 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_44), value);
	}

	inline static int32_t get_offset_of_s_Uv_45() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_45)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_45() const { return ___s_Uv_45; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_45() { return &___s_Uv_45; }
	inline void set_s_Uv_45(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_45 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_45), value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_46() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___m_TrackedTexturelessImages_46)); }
	inline List_1_t4142344393 * get_m_TrackedTexturelessImages_46() const { return ___m_TrackedTexturelessImages_46; }
	inline List_1_t4142344393 ** get_address_of_m_TrackedTexturelessImages_46() { return &___m_TrackedTexturelessImages_46; }
	inline void set_m_TrackedTexturelessImages_46(List_1_t4142344393 * value)
	{
		___m_TrackedTexturelessImages_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedTexturelessImages_46), value);
	}

	inline static int32_t get_offset_of_s_Initialized_47() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Initialized_47)); }
	inline bool get_s_Initialized_47() const { return ___s_Initialized_47; }
	inline bool* get_address_of_s_Initialized_47() { return &___s_Initialized_47; }
	inline void set_s_Initialized_47(bool value)
	{
		___s_Initialized_47 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_48() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___U3CU3Ef__mgU24cache0_48)); }
	inline Action_1_t819399007 * get_U3CU3Ef__mgU24cache0_48() const { return ___U3CU3Ef__mgU24cache0_48; }
	inline Action_1_t819399007 ** get_address_of_U3CU3Ef__mgU24cache0_48() { return &___U3CU3Ef__mgU24cache0_48; }
	inline void set_U3CU3Ef__mgU24cache0_48(Action_1_t819399007 * value)
	{
		___U3CU3Ef__mgU24cache0_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_30)); }
	inline FontData_t746620069 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t746620069 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t746620069 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_32)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_34)); }
	inline Material_t340375123 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_t340375123 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2383250302  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GUIStyle_t3956901511 * m_Items[1];

public:
	inline GUIStyle_t3956901511 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GUIStyle_t3956901511 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GUIStyle_t3956901511 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GUIStyle_t3956901511 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GUIStyle_t3956901511 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GUIStyle_t3956901511 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2510215842  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GUILayoutOption_t811797299 * m_Items[1];

public:
	inline GUILayoutOption_t811797299 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GUILayoutOption_t811797299 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GUILayoutOption_t811797299 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GUILayoutOption_t811797299 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GUILayoutOption_t811797299 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GUILayoutOption_t811797299 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Stone[]
struct StoneU5BU5D_t1191912092  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Stone_t255273793 * m_Items[1];

public:
	inline Stone_t255273793 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Stone_t255273793 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Stone_t255273793 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Stone_t255273793 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Stone_t255273793 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Stone_t255273793 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AudioClip_t3680889665 * m_Items[1];

public:
	inline AudioClip_t3680889665 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AudioClip_t3680889665 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AudioClip_t3680889665 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline AudioClip_t3680889665 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AudioClip_t3680889665 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AudioClip_t3680889665 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Sprite_t280657092 * m_Items[1];

public:
	inline Sprite_t280657092 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sprite_t280657092 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// RoomInfo[]
struct RoomInfoU5BU5D_t1491207981  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RoomInfo_t3170295620 * m_Items[1];

public:
	inline RoomInfo_t3170295620 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RoomInfo_t3170295620 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RoomInfo_t3170295620 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RoomInfo_t3170295620 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RoomInfo_t3170295620 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RoomInfo_t3170295620 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PhotonPlayer[]
struct PhotonPlayerU5BU5D_t2880637464  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) PhotonPlayer_t3305149557 * m_Items[1];

public:
	inline PhotonPlayer_t3305149557 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PhotonPlayer_t3305149557 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PhotonPlayer_t3305149557 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PhotonPlayer_t3305149557 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PhotonPlayer_t3305149557 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PhotonPlayer_t3305149557 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m3280774074_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* Component_GetComponentsInChildren_TisRuntimeObject_m1308288322_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void System.Array::Sort<System.Int32>(!!0[])
extern "C" IL2CPP_METHOD_ATTR void Array_Sort_TisInt32_t2950945753_m2060455863_gshared (RuntimeObject * __this /* static, unused */, Int32U5BU5D_t385246372* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_m2387223709_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m2278349286_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::TryGetValue(!0,!1&)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m1372101825_gshared (Dictionary_2_t1405253484 * __this, uint8_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m1061214600_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Transform_t3600365921 * p1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CanvasGroup::set_interactable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void CanvasGroup_set_interactable_m1698617177 (CanvasGroup_t4083511760 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// System.Void RpsCore::StartTurn()
extern "C" IL2CPP_METHOD_ATTR void RpsCore_StartTurn_m297138532 (RpsCore_t2154144697 * __this, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C" IL2CPP_METHOD_ATTR Scene_t2348375561  SceneManager_GetActiveScene_m1825203488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Scene_get_name_m622963475 (Scene_t2348375561 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t Scene_get_buildIndex_m270272723 (Scene_t2348375561 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<YccioNetworkManager>()
inline YccioNetworkManager_t3600504787 * Component_GetComponent_TisYccioNetworkManager_t3600504787_m4071155424 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  YccioNetworkManager_t3600504787 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void SceneTransition::GoToLobbyScene(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SceneTransition_GoToLobbyScene_m984678998 (SceneTransition_t1138091307 * __this, bool ___isEnter0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m2298600132 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean PhotonNetwork::get_connected()
extern "C" IL2CPP_METHOD_ATTR bool PhotonNetwork_get_connected_m1765367833 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void YccioNetworkManager::EnterLobby()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_EnterLobby_m3924952965 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void ExitGames.Client.Photon.Hashtable::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Hashtable__ctor_m3127574091 (Hashtable_t1048209202 * __this, const RuntimeMethod* method);
// System.Void ExitGames.Client.Photon.Hashtable::set_Item(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR void Hashtable_set_Item_m963063516 (Hashtable_t1048209202 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void PhotonPlayer::SetCustomProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PhotonPlayer_SetCustomProperties_m1444427373 (PhotonPlayer_t3305149557 * __this, Hashtable_t1048209202 * ___propertiesToSet0, Hashtable_t1048209202 * ___expectedValues1, bool ___webForward2, const RuntimeMethod* method);
// System.Int32 ScoreExtensions::GetScore(PhotonPlayer)
extern "C" IL2CPP_METHOD_ATTR int32_t ScoreExtensions_GetScore_m1181906475 (RuntimeObject * __this /* static, unused */, PhotonPlayer_t3305149557 * ___player0, const RuntimeMethod* method);
// ExitGames.Client.Photon.Hashtable PhotonPlayer::get_CustomProperties()
extern "C" IL2CPP_METHOD_ATTR Hashtable_t1048209202 * PhotonPlayer_get_CustomProperties_m3745757186 (PhotonPlayer_t3305149557 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m3280774074 (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t132545152 *, RuntimeObject *, RuntimeObject **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3280774074_gshared)(__this, p0, p1, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_m706204246 (List_1_t3319525431 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m1310743131 (ScriptableObject_t2528358522 * __this, const RuntimeMethod* method);
// System.Void System.Guid::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void Guid__ctor_m2423264394 (Guid_t * __this, String_t* p0, const RuntimeMethod* method);
// CloudRegionCode PhotonHandler::get_BestRegionCodeInPreferences()
extern "C" IL2CPP_METHOD_ATTR int32_t PhotonHandler_get_BestRegionCodeInPreferences_m3593100049 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void PhotonHandler::set_BestRegionCodeInPreferences(CloudRegionCode)
extern "C" IL2CPP_METHOD_ATTR void PhotonHandler_set_BestRegionCodeInPreferences_m1477608017 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern "C" IL2CPP_METHOD_ATTR void GUI_set_skin_m3073574632 (RuntimeObject * __this /* static, unused */, GUISkin_t1244372282 * p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern "C" IL2CPP_METHOD_ATTR GUISkin_t1244372282 * GUI_get_skin_m1874615010 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_box()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * GUISkin_get_box_m1243835431 (GUISkin_t1244372282 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.GUIStyle)
extern "C" IL2CPP_METHOD_ATTR void GUILayout_BeginArea_m1332121664 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, GUIStyle_t3956901511 * p1, const RuntimeMethod* method);
// System.String ShowStatusWhenConnecting::GetConnectingDots()
extern "C" IL2CPP_METHOD_ATTR String_t* ShowStatusWhenConnecting_GetConnectingDots_m4281251292 (ShowStatusWhenConnecting_t1063567576 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// UnityEngine.GUIStyle[] UnityEngine.GUISkin::get_customStyles()
extern "C" IL2CPP_METHOD_ATTR GUIStyleU5BU5D_t2383250302* GUISkin_get_customStyles_m2118532212 (GUISkin_t1244372282 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR void GUILayout_Label_m1096010274 (RuntimeObject * __this /* static, unused */, String_t* p0, GUIStyle_t3956901511 * p1, GUILayoutOptionU5BU5D_t2510215842* p2, const RuntimeMethod* method);
// ClientState PhotonNetwork::get_connectionStateDetailed()
extern "C" IL2CPP_METHOD_ATTR int32_t PhotonNetwork_get_connectionStateDetailed_m2029000665 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR void GUILayout_Label_m1960000298 (RuntimeObject * __this /* static, unused */, String_t* p0, GUILayoutOptionU5BU5D_t2510215842* p1, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::EndArea()
extern "C" IL2CPP_METHOD_ATTR void GUILayout_EndArea_m2046611416 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean PhotonNetwork::get_inRoom()
extern "C" IL2CPP_METHOD_ATTR bool PhotonNetwork_get_inRoom_m1604252032 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern "C" IL2CPP_METHOD_ATTR float Time_get_timeSinceLevelLoad_m2224611026 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m1870542928 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Photon.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1256967409 (MonoBehaviour_t3225183318 * __this, const RuntimeMethod* method);
// PhotonView Photon.MonoBehaviour::get_photonView()
extern "C" IL2CPP_METHOD_ATTR PhotonView_t2207721820 * MonoBehaviour_get_photonView_m1395439011 (MonoBehaviour_t3225183318 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Component>::GetEnumerator()
inline Enumerator_t989985774  List_1_GetEnumerator_m4128318975 (List_1_t3395709193 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t989985774  (*) (List_1_t3395709193 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::get_Current()
inline Component_t1923634451 * Enumerator_get_Current_m1782500462 (Enumerator_t989985774 * __this, const RuntimeMethod* method)
{
	return ((  Component_t1923634451 * (*) (Enumerator_t989985774 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::MoveNext()
inline bool Enumerator_MoveNext_m4232616038 (Enumerator_t989985774 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t989985774 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::Dispose()
inline void Enumerator_Dispose_m4132484595 (Enumerator_t989985774 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t989985774 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean PhotonStream::get_isWriting()
extern "C" IL2CPP_METHOD_ATTR bool PhotonStream_get_isWriting_m2805645485 (PhotonStream_t1003850889 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void PhotonStream::SendNext(System.Object)
extern "C" IL2CPP_METHOD_ATTR void PhotonStream_SendNext_m3094139315 (PhotonStream_t1003850889 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Object PhotonStream::ReceiveNext()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * PhotonStream_ReceiveNext_m3398442404 (PhotonStream_t1003850889 * __this, const RuntimeMethod* method);
// System.Boolean PhotonView::get_isMine()
extern "C" IL2CPP_METHOD_ATTR bool PhotonView_get_isMine_m4153946987 (PhotonView_t2207721820 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_Lerp_m407887542 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_Lerp_m1238806789 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_rotation_m3524318132 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method);
// SortByColor SortByColor::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SortByColor_t1786351735 * SortByColor_get_Instance_m2199486831 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void SortByColor::set_Instance(SortByColor)
extern "C" IL2CPP_METHOD_ATTR void SortByColor_set_Instance_m125504054 (RuntimeObject * __this /* static, unused */, SortByColor_t1786351735 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
inline Button_t4055032469 * Component_GetComponent_TisButton_t4055032469_m1381873113 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Button_t4055032469 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// Deck Deck::get_Instance()
extern "C" IL2CPP_METHOD_ATTR Deck_t2172403585 * Deck_get_Instance_m2996391552 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<Stone>()
inline StoneU5BU5D_t1191912092* Component_GetComponentsInChildren_TisStone_t255273793_m1217350261 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  StoneU5BU5D_t1191912092* (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m1308288322_gshared)(__this, method);
}
// System.Int32 Stone::get_ColorNumber()
extern "C" IL2CPP_METHOD_ATTR int32_t Stone_get_ColorNumber_m934454503 (Stone_t255273793 * __this, const RuntimeMethod* method);
// System.Void System.Array::Sort<System.Int32>(!!0[])
inline void Array_Sort_TisInt32_t2950945753_m2060455863 (RuntimeObject * __this /* static, unused */, Int32U5BU5D_t385246372* p0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject * /* static, unused */, Int32U5BU5D_t385246372*, const RuntimeMethod*))Array_Sort_TisInt32_t2950945753_m2060455863_gshared)(__this /* static, unused */, p0, method);
}
// System.Void UnityEngine.Transform::SetSiblingIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Transform_SetSiblingIndex_m1077399982 (Transform_t3600365921 * __this, int32_t p0, const RuntimeMethod* method);
// SoundManager SoundManager::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SoundManager_t2102329059 * SoundManager_get_Instance_m3963388714 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void SoundManager::PlaySound(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void SoundManager_PlaySound_m862056565 (SoundManager_t2102329059 * __this, int32_t ___num0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Selectable_set_interactable_m3105888815 (Selectable_t3250028441 * __this, bool p0, const RuntimeMethod* method);
// SortByNumber SortByNumber::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SortByNumber_t227753584 * SortByNumber_get_Instance_m183889917 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void SortByNumber::set_Instance(SortByNumber)
extern "C" IL2CPP_METHOD_ATTR void SortByNumber_set_Instance_m357922602 (RuntimeObject * __this /* static, unused */, SortByNumber_t227753584 * ___value0, const RuntimeMethod* method);
// System.Int32 Stone::get_PointNumber()
extern "C" IL2CPP_METHOD_ATTR int32_t Stone_get_PointNumber_m493988397 (Stone_t255273793 * __this, const RuntimeMethod* method);
// System.Void SoundManager::set_Instance(SoundManager)
extern "C" IL2CPP_METHOD_ATTR void SoundManager_set_Instance_m1537600706 (RuntimeObject * __this /* static, unused */, SoundManager_t2102329059 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_t3935305588 * Component_GetComponent_TisAudioSource_t3935305588_m1977431131 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  AudioSource_t3935305588 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern "C" IL2CPP_METHOD_ATTR void AudioSource_PlayOneShot_m1688286683 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * p0, const RuntimeMethod* method);
// SpriteContainer SpriteContainer::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SpriteContainer_t2500302117 * SpriteContainer_get_Instance_m3971019802 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void SpriteContainer::set_Instance(SpriteContainer)
extern "C" IL2CPP_METHOD_ATTR void SpriteContainer_set_Instance_m610192580 (RuntimeObject * __this /* static, unused */, SpriteContainer_t2500302117 * ___value0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * GameObject_FindGameObjectWithTag_m2129039296 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<YccioNetworkManager>()
inline YccioNetworkManager_t3600504787 * GameObject_GetComponent_TisYccioNetworkManager_t3600504787_m610539862 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  YccioNetworkManager_t3600504787 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void YccioNetworkManager::StartGame()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_StartGame_m3707574726 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void StartButton::ActiveButton(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void StartButton_ActiveButton_m2363554299 (StartButton_t1998348287 * __this, bool ___active0, const RuntimeMethod* method);
// System.Void Stone::OnSelectStone(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Stone_OnSelectStone_m370821794 (Stone_t255273793 * __this, bool ___select0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t2670269651 * Component_GetComponent_TisImage_t2670269651_m980647750 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// YccioNetworkManager YccioNetworkManager::get_Instance()
extern "C" IL2CPP_METHOD_ATTR YccioNetworkManager_t3600504787 * YccioNetworkManager_get_Instance_m1769066715 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean Stone::get_IsSelected()
extern "C" IL2CPP_METHOD_ATTR bool Stone_get_IsSelected_m1204973374 (Stone_t255273793 * __this, const RuntimeMethod* method);
// System.Void Stone::set_IsSelected(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Stone_set_IsSelected_m2213372755 (Stone_t255273793 * __this, bool ___value0, const RuntimeMethod* method);
// SubmitButton SubmitButton::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SubmitButton_t4135388028 * SubmitButton_get_Instance_m3888875242 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void SubmitButton::set_Instance(SubmitButton)
extern "C" IL2CPP_METHOD_ATTR void SubmitButton_set_Instance_m853857809 (RuntimeObject * __this /* static, unused */, SubmitButton_t4135388028 * ___value0, const RuntimeMethod* method);
// System.Void Deck::Submit()
extern "C" IL2CPP_METHOD_ATTR void Deck_Submit_m4224624699 (Deck_t2172403585 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_m2093116449 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<SupportLogging>()
inline SupportLogging_t3610999087 * GameObject_AddComponent_TisSupportLogging_t3610999087_m3906415171 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  SupportLogging_t3610999087 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_InvokeRepeating_m650519629 (MonoBehaviour_t3962482529 * __this, String_t* p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_CancelInvoke_m4090783926 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.String PhotonNetwork::NetworkStatisticsToString()
extern "C" IL2CPP_METHOD_ATTR String_t* PhotonNetwork_NetworkStatisticsToString_m397446506 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void StringBuilder__ctor_m3121283359 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_AppendFormat_m3016532472 (StringBuilder_t * __this, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Substring_m1610150815 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.String NetworkingPeer::get_AppVersion()
extern "C" IL2CPP_METHOD_ATTR String_t* NetworkingPeer_get_AppVersion_m600122273 (NetworkingPeer_t264212356 * __this, const RuntimeMethod* method);
// System.String ExitGames.Client.Photon.PhotonPeer::get_PeerID()
extern "C" IL2CPP_METHOD_ATTR String_t* PhotonPeer_get_PeerID_m3871481171 (PhotonPeer_t1608153861 * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_AppendFormat_m2403596038 (StringBuilder_t * __this, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
// System.String PhotonNetwork::get_ServerAddress()
extern "C" IL2CPP_METHOD_ATTR String_t* PhotonNetwork_get_ServerAddress_m16117006 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// CloudRegionCode NetworkingPeer::get_CloudRegion()
extern "C" IL2CPP_METHOD_ATTR int32_t NetworkingPeer_get_CloudRegion_m608168615 (NetworkingPeer_t264212356 * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_AppendFormat_m3255666490 (StringBuilder_t * __this, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void SupportLogging::LogBasics()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_LogBasics_m2059416039 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method);
// System.Void PhotonNetwork::set_NetworkStatisticsEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PhotonNetwork_set_NetworkStatisticsEnabled_m3937783244 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// TypedLobby PhotonNetwork::get_lobby()
extern "C" IL2CPP_METHOD_ATTR TypedLobby_t3336582029 * PhotonNetwork_get_lobby_m933525236 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// Room PhotonNetwork::get_room()
extern "C" IL2CPP_METHOD_ATTR Room_t3759828263 * PhotonNetwork_get_room_m36124698 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean PhotonNetwork::get_connectedAndReady()
extern "C" IL2CPP_METHOD_ATTR bool PhotonNetwork_get_connectedAndReady_m3099072993 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// PunTeams/Team TeamExtensions::GetTeam(PhotonPlayer)
extern "C" IL2CPP_METHOD_ATTR uint8_t TeamExtensions_GetTeam_m369910217 (RuntimeObject * __this /* static, unused */, PhotonPlayer_t3305149557 * ___player0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
inline void Dictionary_2_Add_m2387223709 (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t132545152 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method);
}
// ExitGames.Client.Photon.Hashtable RoomInfo::get_CustomProperties()
extern "C" IL2CPP_METHOD_ATTR Hashtable_t1048209202 * RoomInfo_get_CustomProperties_m3452865736 (RoomInfo_t3170295620 * __this, const RuntimeMethod* method);
// System.Int32 PhotonNetwork::get_ServerTimestamp()
extern "C" IL2CPP_METHOD_ATTR int32_t PhotonNetwork_get_ServerTimestamp_m4164654910 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Room::SetCustomProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Room_SetCustomProperties_m233552519 (Room_t3759828263 * __this, Hashtable_t1048209202 * ___propertiesToSet0, Hashtable_t1048209202 * ___expectedValues1, bool ___webForward2, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m2278349286 (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t132545152 *, RuntimeObject *, const RuntimeMethod*))Dictionary_2_ContainsKey_m2278349286_gshared)(__this, p0, method);
}
// System.Object ExitGames.Client.Photon.Hashtable::get_Item(System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Hashtable_get_Item_m4119173712 (Hashtable_t1048209202 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 PhotonPlayer::get_ID()
extern "C" IL2CPP_METHOD_ATTR int32_t PhotonPlayer_get_ID_m3529408589 (PhotonPlayer_t3305149557 * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2556382932 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void TypedLobby::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TypedLobby__ctor_m815421660 (TypedLobby_t3336582029 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m630303134 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method);
// System.Void YccioNetworkManager::EndGame(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_EndGame_m4061492958 (YccioNetworkManager_t3600504787 * __this, bool ___reset0, const RuntimeMethod* method);
// PlayerStatusPanel PlayerStatusPanel::get_Instance()
extern "C" IL2CPP_METHOD_ATTR PlayerStatusPanel_t1209538314 * PlayerStatusPanel_get_Instance_m52077214 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Byte,System.Object>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m1372101825 (Dictionary_2_t1405253484 * __this, uint8_t p0, RuntimeObject ** p1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t1405253484 *, uint8_t, RuntimeObject **, const RuntimeMethod*))Dictionary_2_TryGetValue_m1372101825_gshared)(__this, p0, p1, method);
}
// System.Void WebRpcResponse::set_Name(System.String)
extern "C" IL2CPP_METHOD_ATTR void WebRpcResponse_set_Name_m2676495348 (WebRpcResponse_t4177102182 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void WebRpcResponse::set_ReturnCode(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void WebRpcResponse_set_ReturnCode_m3992245468 (WebRpcResponse_t4177102182 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void WebRpcResponse::set_Parameters(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" IL2CPP_METHOD_ATTR void WebRpcResponse_set_Parameters_m3975464742 (WebRpcResponse_t4177102182 * __this, Dictionary_2_t2865362463 * ___value0, const RuntimeMethod* method);
// System.Void WebRpcResponse::set_DebugMessage(System.String)
extern "C" IL2CPP_METHOD_ATTR void WebRpcResponse_set_DebugMessage_m3578678403 (WebRpcResponse_t4177102182 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String WebRpcResponse::get_Name()
extern "C" IL2CPP_METHOD_ATTR String_t* WebRpcResponse_get_Name_m1648311298 (WebRpcResponse_t4177102182 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,System.Object> WebRpcResponse::get_Parameters()
extern "C" IL2CPP_METHOD_ATTR Dictionary_2_t2865362463 * WebRpcResponse_get_Parameters_m77974431 (WebRpcResponse_t4177102182 * __this, const RuntimeMethod* method);
// System.String ExitGames.Client.Photon.SupportClass::DictionaryToString(System.Collections.IDictionary)
extern "C" IL2CPP_METHOD_ATTR String_t* SupportClass_DictionaryToString_m1908829707 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Int32 WebRpcResponse::get_ReturnCode()
extern "C" IL2CPP_METHOD_ATTR int32_t WebRpcResponse_get_ReturnCode_m3292208677 (WebRpcResponse_t4177102182 * __this, const RuntimeMethod* method);
// System.String WebRpcResponse::get_DebugMessage()
extern "C" IL2CPP_METHOD_ATTR String_t* WebRpcResponse_get_DebugMessage_m4109436483 (WebRpcResponse_t4177102182 * __this, const RuntimeMethod* method);
// WinLosePanel WinLosePanel::get_Instance()
extern "C" IL2CPP_METHOD_ATTR WinLosePanel_t3114026018 * WinLosePanel_get_Instance_m3091037315 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void WinLosePanel::set_Instance(WinLosePanel)
extern "C" IL2CPP_METHOD_ATTR void WinLosePanel_set_Instance_m3033113386 (RuntimeObject * __this /* static, unused */, WinLosePanel_t3114026018 * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m17791917 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Void WinLosePanel::OnWin()
extern "C" IL2CPP_METHOD_ATTR void WinLosePanel_OnWin_m3713210824 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method);
// System.Void WinLosePanel::OnLose()
extern "C" IL2CPP_METHOD_ATTR void WinLosePanel_OnLose_m3303681697 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator WinLosePanel::C_OnWin()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* WinLosePanel_C_OnWin_m1621631832 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Collections.IEnumerator WinLosePanel::C_OnLose()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* WinLosePanel_C_OnLose_m3097372575 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method);
// System.Void WinLosePanel/<C_OnWin>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CC_OnWinU3Ec__Iterator0__ctor_m3446074108 (U3CC_OnWinU3Ec__Iterator0_t1703244554 * __this, const RuntimeMethod* method);
// System.Void WinLosePanel/<C_OnLose>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CC_OnLoseU3Ec__Iterator1__ctor_m3515859002 (U3CC_OnLoseU3Ec__Iterator1_t1181911525 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_t1901882714 * GameObject_GetComponent_TisText_t1901882714_m2114913816 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Text_t1901882714 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// UnityEngine.Color UnityEngine.Color::get_gray()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_gray_m1471337008 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
inline GameObject_t1113636619 * Object_Instantiate_TisGameObject_t1113636619_m3215236302 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * p0, Transform_t3600365921 * p1, const RuntimeMethod* method)
{
	return ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Transform_t3600365921 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1061214600_gshared)(__this /* static, unused */, p0, p1, method);
}
// !!0 UnityEngine.GameObject::GetComponent<WinLoseParticle>()
inline WinLoseParticle_t1640804235 * GameObject_GetComponent_TisWinLoseParticle_t1640804235_m3979570490 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  WinLoseParticle_t1640804235 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void WinLoseParticle::Init(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void WinLoseParticle_Init_m4007159944 (WinLoseParticle_t1640804235 * __this, bool ___win0, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::op_Subtraction(UnityEngine.Color,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_op_Subtraction_m181229690 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, Color_t2555686324  p1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_white_m332174077 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void Color32__ctor_m4150508762 (Color32_t2600501292 * __this, uint8_t p0, uint8_t p1, uint8_t p2, uint8_t p3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color32_op_Implicit_m213813866 (RuntimeObject * __this /* static, unused */, Color32_t2600501292  p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_t939494601 * Component_GetComponent_TisRigidbody2D_t939494601_m1531613439 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_t939494601 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Random_Range_m2202990745 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_m1099013366 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m3118546832 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, float p1, const RuntimeMethod* method);
// System.Void Photon.PunBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PunBehaviour__ctor_m1042702634 (PunBehaviour_t987309092 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::set_Instance(YccioNetworkManager)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_set_Instance_m1649774145 (RuntimeObject * __this /* static, unused */, YccioNetworkManager_t3600504787 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<PunTurnManager>()
inline PunTurnManager_t1223962931 * Component_GetComponent_TisPunTurnManager_t1223962931_m3266571455 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  PunTurnManager_t1223962931 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<SceneTransition>()
inline SceneTransition_t1138091307 * Component_GetComponent_TisSceneTransition_t1138091307_m3994718925 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  SceneTransition_t1138091307 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<PlayerData>()
inline PlayerData_t220878115 * Component_GetComponent_TisPlayerData_t220878115_m3167838379 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  PlayerData_t220878115 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void PhotonNetwork/EventCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void EventCallback__ctor_m951134603 (EventCallback_t1220598991 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void PhotonNetwork::add_OnEventCall(PhotonNetwork/EventCallback)
extern "C" IL2CPP_METHOD_ATTR void PhotonNetwork_add_OnEventCall_m3003581615 (RuntimeObject * __this /* static, unused */, EventCallback_t1220598991 * ___value0, const RuntimeMethod* method);
// System.Void PhotonNetwork::remove_OnEventCall(PhotonNetwork/EventCallback)
extern "C" IL2CPP_METHOD_ATTR void PhotonNetwork_remove_OnEventCall_m1048168083 (RuntimeObject * __this /* static, unused */, EventCallback_t1220598991 * ___value0, const RuntimeMethod* method);
// System.Int32 Room::get_PlayerCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Room_get_PlayerCount_m1320583396 (Room_t3759828263 * __this, const RuntimeMethod* method);
// System.Void RoomInfo::set_IsPlaying(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void RoomInfo_set_IsPlaying_m3623793460 (RoomInfo_t3170295620 * __this, bool ___value0, const RuntimeMethod* method);
// PlayerData PlayerData::get_Instance()
extern "C" IL2CPP_METHOD_ATTR PlayerData_t220878115 * PlayerData_get_Instance_m3742910714 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 PlayerData::get_Chips()
extern "C" IL2CPP_METHOD_ATTR int32_t PlayerData_get_Chips_m1748726914 (PlayerData_t220878115 * __this, const RuntimeMethod* method);
// System.Void PlayerData::set_Chips(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayerData_set_Chips_m1881252529 (PlayerData_t220878115 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void SortByColor::ActiveButton(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SortByColor_ActiveButton_m1446296454 (SortByColor_t1786351735 * __this, bool ___active0, const RuntimeMethod* method);
// System.Void SortByNumber::ActiveButton(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SortByNumber_ActiveButton_m2261818842 (SortByNumber_t227753584 * __this, bool ___active0, const RuntimeMethod* method);
// System.Void Deck::ClearDeck()
extern "C" IL2CPP_METHOD_ATTR void Deck_ClearDeck_m200245175 (Deck_t2172403585 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::SetPlayOrder()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_SetPlayOrder_m745047584 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::ShuffleSetting()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_ShuffleSetting_m970800901 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::Shuffle()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_Shuffle_m1267737554 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::ClearField()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_ClearField_m3891704080 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::GetMyStone(System.Int32[])
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_GetMyStone_m2847881598 (YccioNetworkManager_t3600504787 * __this, Int32U5BU5D_t385246372* ___stone0, const RuntimeMethod* method);
// System.Void YccioNetworkManager::DistributeStatus()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_DistributeStatus_m2345907618 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::UpdateStatus(System.Object[])
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_UpdateStatus_m2094236186 (YccioNetworkManager_t3600504787 * __this, ObjectU5BU5D_t2843939325* ___data0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_Invoke_m4227543964 (MonoBehaviour_t3962482529 * __this, String_t* p0, float p1, const RuntimeMethod* method);
// System.Void PunTurnManager::BeginTurn()
extern "C" IL2CPP_METHOD_ATTR void PunTurnManager_BeginTurn_m4041073334 (PunTurnManager_t1223962931 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::UpdateRemainStone(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_UpdateRemainStone_m4039245266 (YccioNetworkManager_t3600504787 * __this, int32_t ___order0, int32_t ___remain1, const RuntimeMethod* method);
// System.Void YccioNetworkManager::GetImoji(System.Int32[])
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_GetImoji_m3146625778 (YccioNetworkManager_t3600504787 * __this, Int32U5BU5D_t385246372* ___data0, const RuntimeMethod* method);
// System.Void YccioNetworkManager::MyTurn()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_MyTurn_m3314630788 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::NotMyTurn()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_NotMyTurn_m3026768657 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// Field Field::get_Instance()
extern "C" IL2CPP_METHOD_ATTR Field_t4115194983 * Field_get_Instance_m2724432273 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Field::RemoveAll()
extern "C" IL2CPP_METHOD_ATTR void Field_RemoveAll_m4213314319 (Field_t4115194983 * __this, const RuntimeMethod* method);
// System.Void Field::AddStone(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Field_AddStone_m158341126 (Field_t4115194983 * __this, int32_t ___stoneNum0, const RuntimeMethod* method);
// System.Collections.IEnumerator YccioNetworkManager::CR_BetSound(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* YccioNetworkManager_CR_BetSound_m2763905888 (YccioNetworkManager_t3600504787 * __this, int32_t ___num0, const RuntimeMethod* method);
// System.Void Field::SortStone(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Field_SortStone_m279184223 (Field_t4115194983 * __this, int32_t ___point0, const RuntimeMethod* method);
// System.Void Field::SetFieldColor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Field_SetFieldColor_m2119387286 (Field_t4115194983 * __this, int32_t ___num0, const RuntimeMethod* method);
// System.Void YccioNetworkManager/<CR_BetSound>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCR_BetSoundU3Ec__Iterator0__ctor_m267680118 (U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::UpdateRoom()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_UpdateRoom_m2402611726 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void SceneTransition::GoToGameScene()
extern "C" IL2CPP_METHOD_ATTR void SceneTransition_GoToGameScene_m3230274153 (SceneTransition_t1138091307 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::UpdatePlayersInfo()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_UpdatePlayersInfo_m337863159 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Boolean RoomInfo::get_IsPlaying()
extern "C" IL2CPP_METHOD_ATTR bool RoomInfo_get_IsPlaying_m470967899 (RoomInfo_t3170295620 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager::SetStartButton()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_SetStartButton_m3526135598 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Boolean PhotonNetwork::ConnectUsingSettings(System.String)
extern "C" IL2CPP_METHOD_ATTR bool PhotonNetwork_ConnectUsingSettings_m3997032868 (RuntimeObject * __this /* static, unused */, String_t* ___gameVersion0, const RuntimeMethod* method);
// System.Void RoomOptions::.ctor()
extern "C" IL2CPP_METHOD_ATTR void RoomOptions__ctor_m3263086371 (RoomOptions_t1787645948 * __this, const RuntimeMethod* method);
// System.Void RoomOptions::set_IsVisible(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void RoomOptions_set_IsVisible_m4267195445 (RoomOptions_t1787645948 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void RoomOptions::set_IsOpen(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void RoomOptions_set_IsOpen_m502747827 (RoomOptions_t1787645948 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean PhotonNetwork::CreateRoom(System.String,RoomOptions,TypedLobby)
extern "C" IL2CPP_METHOD_ATTR bool PhotonNetwork_CreateRoom_m4158244447 (RuntimeObject * __this /* static, unused */, String_t* ___roomName0, RoomOptions_t1787645948 * ___roomOptions1, TypedLobby_t3336582029 * ___typedLobby2, const RuntimeMethod* method);
// System.Boolean PhotonNetwork::JoinOrCreateRoom(System.String,RoomOptions,TypedLobby)
extern "C" IL2CPP_METHOD_ATTR bool PhotonNetwork_JoinOrCreateRoom_m963856079 (RuntimeObject * __this /* static, unused */, String_t* ___roomName0, RoomOptions_t1787645948 * ___roomOptions1, TypedLobby_t3336582029 * ___typedLobby2, const RuntimeMethod* method);
// RoomInfo[] PhotonNetwork::GetRoomList()
extern "C" IL2CPP_METHOD_ATTR RoomInfoU5BU5D_t1491207981* PhotonNetwork_GetRoomList_m1678125907 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<RoomList>()
inline RoomList_t2314265074 * GameObject_GetComponent_TisRoomList_t2314265074_m2995111646 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  RoomList_t2314265074 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void RoomList::UpdateList()
extern "C" IL2CPP_METHOD_ATTR void RoomList_UpdateList_m3942554692 (RoomList_t2314265074 * __this, const RuntimeMethod* method);
// System.Void RaiseEventOptions::.ctor()
extern "C" IL2CPP_METHOD_ATTR void RaiseEventOptions__ctor_m3573707678 (RaiseEventOptions_t1229553678 * __this, const RuntimeMethod* method);
// System.Boolean PhotonNetwork::RaiseEvent(System.Byte,System.Object,System.Boolean,RaiseEventOptions)
extern "C" IL2CPP_METHOD_ATTR bool PhotonNetwork_RaiseEvent_m71206761 (RuntimeObject * __this /* static, unused */, uint8_t ___eventCode0, RuntimeObject * ___eventContent1, bool ___sendReliable2, RaiseEventOptions_t1229553678 * ___options3, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<StartButton>()
inline StartButton_t1998348287 * GameObject_GetComponent_TisStartButton_t1998348287_m2462033924 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  StartButton_t1998348287 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Boolean PhotonNetwork::get_isMasterClient()
extern "C" IL2CPP_METHOD_ATTR bool PhotonNetwork_get_isMasterClient_m3379552944 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean PhotonNetwork::LeaveRoom(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool PhotonNetwork_LeaveRoom_m299983054 (RuntimeObject * __this /* static, unused */, bool ___becomeInactive0, const RuntimeMethod* method);
// PhotonPlayer[] PhotonNetwork::get_playerList()
extern "C" IL2CPP_METHOD_ATTR PhotonPlayerU5BU5D_t2880637464* PhotonNetwork_get_playerList_m2152942251 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String PhotonPlayer::get_UserId()
extern "C" IL2CPP_METHOD_ATTR String_t* PhotonPlayer_get_UserId_m488372118 (PhotonPlayer_t3305149557 * __this, const RuntimeMethod* method);
// PhotonPlayer PhotonNetwork::get_player()
extern "C" IL2CPP_METHOD_ATTR PhotonPlayer_t3305149557 * PhotonNetwork_get_player_m1573803587 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void Deck::AddStone(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Deck_AddStone_m2587771987 (Deck_t2172403585 * __this, int32_t ___stoneNum0, const RuntimeMethod* method);
// System.Void PlayerStatusPanel::RemoveAll()
extern "C" IL2CPP_METHOD_ATTR void PlayerStatusPanel_RemoveAll_m4234547583 (PlayerStatusPanel_t1209538314 * __this, const RuntimeMethod* method);
// System.String PlayerData::get_PlayerName()
extern "C" IL2CPP_METHOD_ATTR String_t* PlayerData_get_PlayerName_m1238444232 (PlayerData_t220878115 * __this, const RuntimeMethod* method);
// System.Void PlayerStatusPanel::AddStatus(System.Int32,System.String,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayerStatusPanel_AddStatus_m3083601519 (PlayerStatusPanel_t1209538314 * __this, int32_t ___playOrder0, String_t* ___playerName1, int32_t ___chip2, int32_t ___remain3, const RuntimeMethod* method);
// System.Int32 YccioNetworkManager::IsFirstOrder()
extern "C" IL2CPP_METHOD_ATTR int32_t YccioNetworkManager_IsFirstOrder_m3390042890 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// PassButton PassButton::get_Instance()
extern "C" IL2CPP_METHOD_ATTR PassButton_t4045823955 * PassButton_get_Instance_m1637646120 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void PassButton::OnClickPass()
extern "C" IL2CPP_METHOD_ATTR void PassButton_OnClickPass_m1917316564 (PassButton_t4045823955 * __this, const RuntimeMethod* method);
// System.Void SubmitButton::ActiveButton(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SubmitButton_ActiveButton_m3820152260 (SubmitButton_t4135388028 * __this, bool ___active0, const RuntimeMethod* method);
// System.Void PassButton::ActiveButton(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PassButton_ActiveButton_m477051212 (PassButton_t4045823955 * __this, bool ___active0, const RuntimeMethod* method);
// System.Void Deck::SetActiveColor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Deck_SetActiveColor_m2307680960 (Deck_t2172403585 * __this, bool ___active0, const RuntimeMethod* method);
// System.Void PlayerStatusPanel::SetActiveColor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayerStatusPanel_SetActiveColor_m907504136 (PlayerStatusPanel_t1209538314 * __this, int32_t ___activePlayOrder0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_m3145433196 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C" IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m3117905507 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t1871169219  p1, const RuntimeMethod* method);
// System.Void PunTurnManager::SendMove(System.Object,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PunTurnManager_SendMove_m385030697 (PunTurnManager_t1223962931 * __this, RuntimeObject * ___move0, bool ___finished1, const RuntimeMethod* method);
// RemainTime RemainTime::get_Instance()
extern "C" IL2CPP_METHOD_ATTR RemainTime_t3232366768 * RemainTime_get_Instance_m3815012167 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void RemainTime::SetCurrentPlayerStatus()
extern "C" IL2CPP_METHOD_ATTR void RemainTime_SetCurrentPlayerStatus_m3509695785 (RemainTime_t3232366768 * __this, const RuntimeMethod* method);
// System.Void Field::ClearField()
extern "C" IL2CPP_METHOD_ATTR void Field_ClearField_m1103382306 (Field_t4115194983 * __this, const RuntimeMethod* method);
// System.Void TurnExtensions::SetTurn(Room,System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TurnExtensions_SetTurn_m2738922425 (RuntimeObject * __this /* static, unused */, Room_t3759828263 * ___room0, int32_t ___turn1, bool ___setStartTime2, const RuntimeMethod* method);
// System.Int32 Deck::CalRemain()
extern "C" IL2CPP_METHOD_ATTR int32_t Deck_CalRemain_m2239694271 (Deck_t2172403585 * __this, const RuntimeMethod* method);
// System.Void RemainTime::Invisible()
extern "C" IL2CPP_METHOD_ATTR void RemainTime_Invisible_m132284693 (RemainTime_t3232366768 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator YccioNetworkManager::CR_Calchips()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* YccioNetworkManager_CR_Calchips_m80184455 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method);
// System.Void YccioNetworkManager/<CR_Calchips>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCR_CalchipsU3Ec__Iterator1__ctor_m3089154205 (U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * __this, const RuntimeMethod* method);
// System.Void PlayerStatusPanel::UpdateRemainStone(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayerStatusPanel_UpdateRemainStone_m3573178345 (PlayerStatusPanel_t1209538314 * __this, int32_t ___activePlayOrder0, int32_t ___remain1, const RuntimeMethod* method);
// System.Void PlayerStatusPanel::GetImoji(System.Int32[])
extern "C" IL2CPP_METHOD_ATTR void PlayerStatusPanel_GetImoji_m1615372909 (PlayerStatusPanel_t1209538314 * __this, Int32U5BU5D_t385246372* ___data0, const RuntimeMethod* method);
// IncreaseEffectPanel IncreaseEffectPanel::get_Instance()
extern "C" IL2CPP_METHOD_ATTR IncreaseEffectPanel_t3055077437 * IncreaseEffectPanel_get_Instance_m2930160416 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void IncreaseEffectPanel::InitPanel()
extern "C" IL2CPP_METHOD_ATTR void IncreaseEffectPanel_InitPanel_m4011756956 (IncreaseEffectPanel_t3055077437 * __this, const RuntimeMethod* method);
// System.Void IncreaseEffectPanel::NewIncrease(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IncreaseEffectPanel_NewIncrease_m271767927 (IncreaseEffectPanel_t3055077437 * __this, int32_t ___value0, int32_t ___num1, const RuntimeMethod* method);
// InGamePanel InGamePanel::get_Instance()
extern "C" IL2CPP_METHOD_ATTR InGamePanel_t2211285949 * InGamePanel_get_Instance_m4112776688 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.Int32::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCycleRemoteHandCoroutineU3Ec__Iterator1__ctor_m3411249116 (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CCycleRemoteHandCoroutineU3Ec__Iterator1_MoveNext_m4258493291 (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_MoveNext_m4258493291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0057;
			}
		}
	}
	{
		goto IL_0063;
	}

IL_0021:
	{
		RpsCore_t2154144697 * L_2 = __this->get_U24this_0();
		int32_t L_3 = Random_Range_m4054026115(NULL /*static, unused*/, 1, 4, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_randomHand_27(L_3);
		WaitForSeconds_t1699091251 * L_4 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_4, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		bool L_5 = __this->get_U24disposing_2();
		if (L_5)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0052:
	{
		goto IL_0065;
	}

IL_0057:
	{
		goto IL_0021;
	}
	// Dead block : IL_005c: ldarg.0

IL_0063:
	{
		return (bool)0;
	}

IL_0065:
	{
		return (bool)1;
	}
}
// System.Object RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCycleRemoteHandCoroutineU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1954859309 (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCycleRemoteHandCoroutineU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1674560634 (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CCycleRemoteHandCoroutineU3Ec__Iterator1_Dispose_m424560696 (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void RpsCore/<CycleRemoteHandCoroutine>c__Iterator1::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CCycleRemoteHandCoroutineU3Ec__Iterator1_Reset_m3326500309 (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t811054456 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_Reset_m3326500309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CCycleRemoteHandCoroutineU3Ec__Iterator1_Reset_m3326500309_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0__ctor_m403207095 (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_MoveNext_m2597396364 (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_MoveNext_m2597396364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Image_t2670269651 * G_B6_0 = NULL;
	Image_t2670269651 * G_B5_0 = NULL;
	Sprite_t280657092 * G_B7_0 = NULL;
	Image_t2670269651 * G_B7_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00e5;
			}
		}
	}
	{
		goto IL_00f7;
	}

IL_0021:
	{
		RpsCore_t2154144697 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		CanvasGroup_t4083511760 * L_3 = L_2->get_ButtonCanvasGroup_7();
		NullCheck(L_3);
		CanvasGroup_set_interactable_m1698617177(L_3, (bool)0, /*hidden argument*/NULL);
		RpsCore_t2154144697 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		L_4->set_IsShowingResults_28((bool)1);
		RpsCore_t2154144697 * L_5 = __this->get_U24this_0();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_result_25();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_006f;
		}
	}
	{
		RpsCore_t2154144697 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		Image_t2670269651 * L_8 = L_7->get_WinOrLossImage_13();
		RpsCore_t2154144697 * L_9 = __this->get_U24this_0();
		NullCheck(L_9);
		Sprite_t280657092 * L_10 = L_9->get_SpriteDraw_23();
		NullCheck(L_8);
		Image_set_sprite_m2369174689(L_8, L_10, /*hidden argument*/NULL);
		goto IL_00ab;
	}

IL_006f:
	{
		RpsCore_t2154144697 * L_11 = __this->get_U24this_0();
		NullCheck(L_11);
		Image_t2670269651 * L_12 = L_11->get_WinOrLossImage_13();
		RpsCore_t2154144697 * L_13 = __this->get_U24this_0();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_result_25();
		G_B5_0 = L_12;
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			G_B6_0 = L_12;
			goto IL_009b;
		}
	}
	{
		RpsCore_t2154144697 * L_15 = __this->get_U24this_0();
		NullCheck(L_15);
		Sprite_t280657092 * L_16 = L_15->get_SpriteWin_21();
		G_B7_0 = L_16;
		G_B7_1 = G_B5_0;
		goto IL_00a6;
	}

IL_009b:
	{
		RpsCore_t2154144697 * L_17 = __this->get_U24this_0();
		NullCheck(L_17);
		Sprite_t280657092 * L_18 = L_17->get_SpriteLose_22();
		G_B7_0 = L_18;
		G_B7_1 = G_B6_0;
	}

IL_00a6:
	{
		NullCheck(G_B7_1);
		Image_set_sprite_m2369174689(G_B7_1, G_B7_0, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		RpsCore_t2154144697 * L_19 = __this->get_U24this_0();
		NullCheck(L_19);
		Image_t2670269651 * L_20 = L_19->get_WinOrLossImage_13();
		NullCheck(L_20);
		GameObject_t1113636619 * L_21 = Component_get_gameObject_m442555142(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		GameObject_SetActive_m796801857(L_21, (bool)1, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_22 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_22, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_22);
		bool L_23 = __this->get_U24disposing_2();
		if (L_23)
		{
			goto IL_00e0;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_00e0:
	{
		goto IL_00f9;
	}

IL_00e5:
	{
		RpsCore_t2154144697 * L_24 = __this->get_U24this_0();
		NullCheck(L_24);
		RpsCore_StartTurn_m297138532(L_24, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00f7:
	{
		return (bool)0;
	}

IL_00f9:
	{
		return (bool)1;
	}
}
// System.Object RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1043208220 (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2603541291 (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_Dispose_m2854587385 (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void RpsCore/<ShowResultsBeginNextTurnCoroutine>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_Reset_m633430967 (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t2002201197 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_Reset_m633430967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_Reset_m633430967_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SceneManagerHelper::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SceneManagerHelper__ctor_m776891481 (SceneManagerHelper_t3665721098 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SceneManagerHelper::get_ActiveSceneName()
extern "C" IL2CPP_METHOD_ATTR String_t* SceneManagerHelper_get_ActiveSceneName_m1633867580 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m622963475((Scene_t2348375561 *)(&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 SceneManagerHelper::get_ActiveSceneBuildIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t SceneManagerHelper_get_ActiveSceneBuildIndex_m4039024987 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m270272723((Scene_t2348375561 *)(&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SceneTransition::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SceneTransition__ctor_m2695454095 (SceneTransition_t1138091307 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SceneTransition::Awake()
extern "C" IL2CPP_METHOD_ATTR void SceneTransition_Awake_m1543823431 (SceneTransition_t1138091307 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneTransition_Awake_m1543823431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		YccioNetworkManager_t3600504787 * L_1 = Component_GetComponent_TisYccioNetworkManager_t3600504787_m4071155424(__this, /*hidden argument*/Component_GetComponent_TisYccioNetworkManager_t3600504787_m4071155424_RuntimeMethod_var);
		__this->set_ynm_5(L_1);
		bool L_2 = __this->get_isTestMode_4();
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		SceneTransition_GoToLobbyScene_m984678998(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void SceneTransition::GoToLobbyScene(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SceneTransition_GoToLobbyScene_m984678998 (SceneTransition_t1138091307 * __this, bool ___isEnter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneTransition_GoToLobbyScene_m984678998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m2298600132(NULL /*static, unused*/, _stringLiteral2353668337, 0, /*hidden argument*/NULL);
		bool L_0 = ___isEnter0;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		bool L_1 = PhotonNetwork_get_connected_m1765367833(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}

IL_001b:
	{
		YccioNetworkManager_t3600504787 * L_2 = __this->get_ynm_5();
		NullCheck(L_2);
		YccioNetworkManager_EnterLobby_m3924952965(L_2, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void SceneTransition::GoToGameScene()
extern "C" IL2CPP_METHOD_ATTR void SceneTransition_GoToGameScene_m3230274153 (SceneTransition_t1138091307 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneTransition_GoToGameScene_m3230274153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m2298600132(NULL /*static, unused*/, _stringLiteral62725266, 0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ScoreExtensions::SetScore(PhotonPlayer,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ScoreExtensions_SetScore_m650688815 (RuntimeObject * __this /* static, unused */, PhotonPlayer_t3305149557 * ___player0, int32_t ___newScore1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreExtensions_SetScore_m650688815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t1048209202 * V_0 = NULL;
	{
		Hashtable_t1048209202 * L_0 = (Hashtable_t1048209202 *)il2cpp_codegen_object_new(Hashtable_t1048209202_il2cpp_TypeInfo_var);
		Hashtable__ctor_m3127574091(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Hashtable_t1048209202 * L_1 = V_0;
		int32_t L_2 = ___newScore1;
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		Hashtable_set_Item_m963063516(L_1, _stringLiteral1512030231, L_4, /*hidden argument*/NULL);
		PhotonPlayer_t3305149557 * L_5 = ___player0;
		Hashtable_t1048209202 * L_6 = V_0;
		NullCheck(L_5);
		PhotonPlayer_SetCustomProperties_m1444427373(L_5, L_6, (Hashtable_t1048209202 *)NULL, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreExtensions::AddScore(PhotonPlayer,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ScoreExtensions_AddScore_m3409814160 (RuntimeObject * __this /* static, unused */, PhotonPlayer_t3305149557 * ___player0, int32_t ___scoreToAddToCurrent1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreExtensions_AddScore_m3409814160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Hashtable_t1048209202 * V_1 = NULL;
	{
		PhotonPlayer_t3305149557 * L_0 = ___player0;
		int32_t L_1 = ScoreExtensions_GetScore_m1181906475(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = ___scoreToAddToCurrent1;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)L_3));
		Hashtable_t1048209202 * L_4 = (Hashtable_t1048209202 *)il2cpp_codegen_object_new(Hashtable_t1048209202_il2cpp_TypeInfo_var);
		Hashtable__ctor_m3127574091(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		Hashtable_t1048209202 * L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		Hashtable_set_Item_m963063516(L_5, _stringLiteral1512030231, L_8, /*hidden argument*/NULL);
		PhotonPlayer_t3305149557 * L_9 = ___player0;
		Hashtable_t1048209202 * L_10 = V_1;
		NullCheck(L_9);
		PhotonPlayer_SetCustomProperties_m1444427373(L_9, L_10, (Hashtable_t1048209202 *)NULL, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ScoreExtensions::GetScore(PhotonPlayer)
extern "C" IL2CPP_METHOD_ATTR int32_t ScoreExtensions_GetScore_m1181906475 (RuntimeObject * __this /* static, unused */, PhotonPlayer_t3305149557 * ___player0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreExtensions_GetScore_m1181906475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		PhotonPlayer_t3305149557 * L_0 = ___player0;
		NullCheck(L_0);
		Hashtable_t1048209202 * L_1 = PhotonPlayer_get_CustomProperties_m3745757186(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Dictionary_2_TryGetValue_m3280774074(L_1, _stringLiteral1512030231, (RuntimeObject **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3280774074_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject * L_3 = V_0;
		return ((*(int32_t*)((int32_t*)UnBox(L_3, Int32_t2950945753_il2cpp_TypeInfo_var))));
	}

IL_001e:
	{
		return 0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ServerSettings::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ServerSettings__ctor_m1448665227 (ServerSettings_t2755303613 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings__ctor_m1448665227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_AppID_4(L_0);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_VoiceAppID_5(L_1);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_ChatAppID_6(L_2);
		__this->set_EnabledRegions_9((-1));
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_ServerAddress_11(L_3);
		__this->set_ServerPort_12(((int32_t)5055));
		__this->set_VoiceServerPort_13(((int32_t)5055));
		__this->set_NetworkLogging_17(1);
		__this->set_RunInBackground_18((bool)1);
		List_1_t3319525431 * L_4 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_4, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		__this->set_RpcList_19(L_4);
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ServerSettings::UseCloudBestRegion(System.String)
extern "C" IL2CPP_METHOD_ATTR void ServerSettings_UseCloudBestRegion_m3618407569 (ServerSettings_t2755303613 * __this, String_t* ___cloudAppid0, const RuntimeMethod* method)
{
	{
		__this->set_HostType_7(4);
		String_t* L_0 = ___cloudAppid0;
		__this->set_AppID_4(L_0);
		return;
	}
}
// System.Void ServerSettings::UseCloud(System.String)
extern "C" IL2CPP_METHOD_ATTR void ServerSettings_UseCloud_m3696146211 (ServerSettings_t2755303613 * __this, String_t* ___cloudAppid0, const RuntimeMethod* method)
{
	{
		__this->set_HostType_7(1);
		String_t* L_0 = ___cloudAppid0;
		__this->set_AppID_4(L_0);
		return;
	}
}
// System.Void ServerSettings::UseCloud(System.String,CloudRegionCode)
extern "C" IL2CPP_METHOD_ATTR void ServerSettings_UseCloud_m3034448918 (ServerSettings_t2755303613 * __this, String_t* ___cloudAppid0, int32_t ___code1, const RuntimeMethod* method)
{
	{
		__this->set_HostType_7(1);
		String_t* L_0 = ___cloudAppid0;
		__this->set_AppID_4(L_0);
		int32_t L_1 = ___code1;
		__this->set_PreferredRegion_8(L_1);
		return;
	}
}
// System.Void ServerSettings::UseMyServer(System.String,System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void ServerSettings_UseMyServer_m2379333996 (ServerSettings_t2755303613 * __this, String_t* ___serverAddress0, int32_t ___serverPort1, String_t* ___application2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings_UseMyServer_m2379333996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ServerSettings_t2755303613 * G_B2_0 = NULL;
	ServerSettings_t2755303613 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	ServerSettings_t2755303613 * G_B3_1 = NULL;
	{
		__this->set_HostType_7(2);
		String_t* L_0 = ___application2;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0014;
		}
	}
	{
		String_t* L_1 = ___application2;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0019;
	}

IL_0014:
	{
		G_B3_0 = _stringLiteral2752530441;
		G_B3_1 = G_B2_0;
	}

IL_0019:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_AppID_4(G_B3_0);
		String_t* L_2 = ___serverAddress0;
		__this->set_ServerAddress_11(L_2);
		int32_t L_3 = ___serverPort1;
		__this->set_ServerPort_12(L_3);
		return;
	}
}
// System.Boolean ServerSettings::IsAppId(System.String)
extern "C" IL2CPP_METHOD_ATTR bool ServerSettings_IsAppId_m2900859773 (RuntimeObject * __this /* static, unused */, String_t* ___val0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings_IsAppId_m2900859773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___val0;
		Guid_t  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Guid__ctor_m2423264394((&L_1), L_0, /*hidden argument*/NULL);
		goto IL_0014;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_000c;
		throw e;
	}

CATCH_000c:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0016;
	} // end catch (depth: 1)

IL_0014:
	{
		return (bool)1;
	}

IL_0016:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// CloudRegionCode ServerSettings::get_BestRegionCodeInPreferences()
extern "C" IL2CPP_METHOD_ATTR int32_t ServerSettings_get_BestRegionCodeInPreferences_m2341611427 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings_get_BestRegionCodeInPreferences_m2341611427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t2139970417_il2cpp_TypeInfo_var);
		int32_t L_0 = PhotonHandler_get_BestRegionCodeInPreferences_m3593100049(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void ServerSettings::ResetBestRegionCodeInPreferences()
extern "C" IL2CPP_METHOD_ATTR void ServerSettings_ResetBestRegionCodeInPreferences_m2677648169 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings_ResetBestRegionCodeInPreferences_m2677648169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonHandler_t2139970417_il2cpp_TypeInfo_var);
		PhotonHandler_set_BestRegionCodeInPreferences_m1477608017(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.String ServerSettings::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* ServerSettings_ToString_m2874290298 (ServerSettings_t2755303613 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings_ToString_m2874290298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral2328556060);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2328556060);
		ObjectU5BU5D_t2843939325* L_2 = L_1;
		int32_t L_3 = __this->get_HostType_7();
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(HostingOption_t2949276063_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_5);
		ObjectU5BU5D_t2843939325* L_6 = L_2;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3452614528);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3452614528);
		ObjectU5BU5D_t2843939325* L_7 = L_6;
		String_t* L_8 = __this->get_ServerAddress_11();
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2971454694(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShowStatusWhenConnecting::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ShowStatusWhenConnecting__ctor_m3022673622 (ShowStatusWhenConnecting_t1063567576 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowStatusWhenConnecting::OnGUI()
extern "C" IL2CPP_METHOD_ATTR void ShowStatusWhenConnecting_OnGUI_m2526820743 (ShowStatusWhenConnecting_t1063567576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShowStatusWhenConnecting_OnGUI_m2526820743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GUISkin_t1244372282 * L_0 = __this->get_Skin_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		GUISkin_t1244372282 * L_2 = __this->get_Skin_4();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_skin_m3073574632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		V_0 = (400.0f);
		V_1 = (100.0f);
		int32_t L_3 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = V_0;
		int32_t L_5 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = V_1;
		float L_7 = V_0;
		float L_8 = V_1;
		Rect__ctor_m2614021312((Rect_t2360479859 *)(&V_2), ((float)((float)((float)il2cpp_codegen_subtract((float)(((float)((float)L_3))), (float)L_4))/(float)(2.0f))), ((float)((float)((float)il2cpp_codegen_subtract((float)(((float)((float)L_5))), (float)L_6))/(float)(2.0f))), L_7, L_8, /*hidden argument*/NULL);
		Rect_t2360479859  L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_10 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyle_t3956901511 * L_11 = GUISkin_get_box_m1243835431(L_10, /*hidden argument*/NULL);
		GUILayout_BeginArea_m1332121664(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ShowStatusWhenConnecting_GetConnectingDots_m4281251292(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2745515193, L_12, /*hidden argument*/NULL);
		GUISkin_t1244372282 * L_14 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		GUIStyleU5BU5D_t2383250302* L_15 = GUISkin_get_customStyles_m2118532212(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = 0;
		GUIStyle_t3956901511 * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		GUILayoutOptionU5BU5D_t2510215842* L_18 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0);
		GUILayout_Label_m1096010274(NULL /*static, unused*/, L_13, L_17, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		int32_t L_19 = PhotonNetwork_get_connectionStateDetailed_m2029000665(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = L_19;
		RuntimeObject * L_21 = Box(ClientState_t1348705391_il2cpp_TypeInfo_var, &L_20);
		String_t* L_22 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3928422333, L_21, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_23 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0);
		GUILayout_Label_m1960000298(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		GUILayout_EndArea_m2046611416(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_24 = PhotonNetwork_get_inRoom_m1604252032(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b9;
		}
	}
	{
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		return;
	}
}
// System.String ShowStatusWhenConnecting::GetConnectingDots()
extern "C" IL2CPP_METHOD_ATTR String_t* ShowStatusWhenConnecting_GetConnectingDots_m4281251292 (ShowStatusWhenConnecting_t1063567576 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShowStatusWhenConnecting_GetConnectingDots_m4281251292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_0;
		float L_1 = Time_get_timeSinceLevelLoad_m2224611026(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, (fmodf(((float)il2cpp_codegen_multiply((float)L_1, (float)(3.0f))), (4.0f))), /*hidden argument*/NULL);
		V_1 = L_2;
		V_2 = 0;
		goto IL_0034;
	}

IL_0024:
	{
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral3450648448, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0034:
	{
		int32_t L_6 = V_2;
		int32_t L_7 = V_1;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_8 = V_0;
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmoothSyncMovement::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SmoothSyncMovement__ctor_m1559543711 (SmoothSyncMovement_t1809568931 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothSyncMovement__ctor_m1559543711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_SmoothingDelay_5((5.0f));
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_correctPlayerPos_6(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_1 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_correctPlayerRot_7(L_1);
		MonoBehaviour__ctor_m1256967409(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothSyncMovement::Awake()
extern "C" IL2CPP_METHOD_ATTR void SmoothSyncMovement_Awake_m593888716 (SmoothSyncMovement_t1809568931 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothSyncMovement_Awake_m593888716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Component_t1923634451 * V_1 = NULL;
	Enumerator_t989985774  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
		PhotonView_t2207721820 * L_0 = MonoBehaviour_get_photonView_m1395439011(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t3395709193 * L_1 = L_0->get_ObservedComponents_17();
		NullCheck(L_1);
		Enumerator_t989985774  L_2 = List_1_GetEnumerator_m4128318975(L_1, /*hidden argument*/List_1_GetEnumerator_m4128318975_RuntimeMethod_var);
		V_2 = L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0033;
		}

IL_0018:
		{
			Component_t1923634451 * L_3 = Enumerator_get_Current_m1782500462((Enumerator_t989985774 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m1782500462_RuntimeMethod_var);
			V_1 = L_3;
			Component_t1923634451 * L_4 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, __this, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0033;
			}
		}

IL_002c:
		{
			V_0 = (bool)1;
			goto IL_003f;
		}

IL_0033:
		{
			bool L_6 = Enumerator_MoveNext_m4232616038((Enumerator_t989985774 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m4232616038_RuntimeMethod_var);
			if (L_6)
			{
				goto IL_0018;
			}
		}

IL_003f:
		{
			IL2CPP_LEAVE(0x52, FINALLY_0044);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4132484595((Enumerator_t989985774 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m4132484595_RuntimeMethod_var);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0052:
	{
		bool L_7 = V_0;
		if (L_7)
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m904156431(NULL /*static, unused*/, __this, _stringLiteral2688095987, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void SmoothSyncMovement::OnPhotonSerializeView(PhotonStream,PhotonMessageInfo)
extern "C" IL2CPP_METHOD_ATTR void SmoothSyncMovement_OnPhotonSerializeView_m1517933553 (SmoothSyncMovement_t1809568931 * __this, PhotonStream_t1003850889 * ___stream0, PhotonMessageInfo_t3855471533  ___info1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothSyncMovement_OnPhotonSerializeView_m1517933553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonStream_t1003850889 * L_0 = ___stream0;
		NullCheck(L_0);
		bool L_1 = PhotonStream_get_isWriting_m2805645485(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		PhotonStream_t1003850889 * L_2 = ___stream0;
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = L_4;
		RuntimeObject * L_6 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_2);
		PhotonStream_SendNext_m3094139315(L_2, L_6, /*hidden argument*/NULL);
		PhotonStream_t1003850889 * L_7 = ___stream0;
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Quaternion_t2301928331  L_9 = Transform_get_rotation_m3502953881(L_8, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_10 = L_9;
		RuntimeObject * L_11 = Box(Quaternion_t2301928331_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		PhotonStream_SendNext_m3094139315(L_7, L_11, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_003c:
	{
		PhotonStream_t1003850889 * L_12 = ___stream0;
		NullCheck(L_12);
		RuntimeObject * L_13 = PhotonStream_ReceiveNext_m3398442404(L_12, /*hidden argument*/NULL);
		__this->set_correctPlayerPos_6(((*(Vector3_t3722313464 *)((Vector3_t3722313464 *)UnBox(L_13, Vector3_t3722313464_il2cpp_TypeInfo_var)))));
		PhotonStream_t1003850889 * L_14 = ___stream0;
		NullCheck(L_14);
		RuntimeObject * L_15 = PhotonStream_ReceiveNext_m3398442404(L_14, /*hidden argument*/NULL);
		__this->set_correctPlayerRot_7(((*(Quaternion_t2301928331 *)((Quaternion_t2301928331 *)UnBox(L_15, Quaternion_t2301928331_il2cpp_TypeInfo_var)))));
	}

IL_005e:
	{
		return;
	}
}
// System.Void SmoothSyncMovement::Update()
extern "C" IL2CPP_METHOD_ATTR void SmoothSyncMovement_Update_m340845157 (SmoothSyncMovement_t1809568931 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothSyncMovement_Update_m340845157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonView_t2207721820 * L_0 = MonoBehaviour_get_photonView_m1395439011(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = PhotonView_get_isMine_m4153946987(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_006a;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = __this->get_correctPlayerPos_6();
		float L_6 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_SmoothingDelay_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_8 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_4, L_5, ((float)il2cpp_codegen_multiply((float)L_6, (float)L_7)), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3387557959(L_2, L_8, /*hidden argument*/NULL);
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Quaternion_t2301928331  L_11 = Transform_get_rotation_m3502953881(L_10, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_12 = __this->get_correctPlayerRot_7();
		float L_13 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = __this->get_SmoothingDelay_5();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_15 = Quaternion_Lerp_m1238806789(NULL /*static, unused*/, L_11, L_12, ((float)il2cpp_codegen_multiply((float)L_13, (float)L_14)), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_rotation_m3524318132(L_9, L_15, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SortByColor::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SortByColor__ctor_m159497091 (SortByColor_t1786351735 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// SortByColor SortByColor::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SortByColor_t1786351735 * SortByColor_get_Instance_m2199486831 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SortByColor_get_Instance_m2199486831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SortByColor_t1786351735 * L_0 = ((SortByColor_t1786351735_StaticFields*)il2cpp_codegen_static_fields_for(SortByColor_t1786351735_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void SortByColor::set_Instance(SortByColor)
extern "C" IL2CPP_METHOD_ATTR void SortByColor_set_Instance_m125504054 (RuntimeObject * __this /* static, unused */, SortByColor_t1786351735 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SortByColor_set_Instance_m125504054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SortByColor_t1786351735 * L_0 = ___value0;
		((SortByColor_t1786351735_StaticFields*)il2cpp_codegen_static_fields_for(SortByColor_t1786351735_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void SortByColor::Awake()
extern "C" IL2CPP_METHOD_ATTR void SortByColor_Awake_m119132563 (SortByColor_t1786351735 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SortByColor_Awake_m119132563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SortByColor_t1786351735 * L_0 = SortByColor_get_Instance_m2199486831(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		SortByColor_set_Instance_m125504054(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Button_t4055032469 * L_2 = Component_GetComponent_TisButton_t4055032469_m1381873113(__this, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_b_4(L_2);
		return;
	}
}
// System.Void SortByColor::OnClickSortByColor()
extern "C" IL2CPP_METHOD_ATTR void SortByColor_OnClickSortByColor_m3417539386 (SortByColor_t1786351735 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SortByColor_OnClickSortByColor_m3417539386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StoneU5BU5D_t1191912092* V_0 = NULL;
	Int32U5BU5D_t385246372* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Deck_t2172403585 * L_0 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		StoneU5BU5D_t1191912092* L_1 = Component_GetComponentsInChildren_TisStone_t255273793_m1217350261(L_0, /*hidden argument*/Component_GetComponentsInChildren_TisStone_t255273793_m1217350261_RuntimeMethod_var);
		V_0 = L_1;
		StoneU5BU5D_t1191912092* L_2 = V_0;
		NullCheck(L_2);
		Int32U5BU5D_t385246372* L_3 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))));
		V_1 = L_3;
		V_2 = 0;
		goto IL_002a;
	}

IL_001b:
	{
		Int32U5BU5D_t385246372* L_4 = V_1;
		int32_t L_5 = V_2;
		StoneU5BU5D_t1191912092* L_6 = V_0;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Stone_t255273793 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		int32_t L_10 = Stone_get_ColorNumber_m934454503(L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_10);
		int32_t L_11 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_002a:
	{
		int32_t L_12 = V_2;
		StoneU5BU5D_t1191912092* L_13 = V_0;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Int32U5BU5D_t385246372* L_14 = V_1;
		Array_Sort_TisInt32_t2950945753_m2060455863(NULL /*static, unused*/, L_14, /*hidden argument*/Array_Sort_TisInt32_t2950945753_m2060455863_RuntimeMethod_var);
		V_3 = 0;
		goto IL_007c;
	}

IL_0040:
	{
		V_4 = 0;
		goto IL_006e;
	}

IL_0048:
	{
		Int32U5BU5D_t385246372* L_15 = V_1;
		int32_t L_16 = V_3;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		StoneU5BU5D_t1191912092* L_19 = V_0;
		int32_t L_20 = V_4;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Stone_t255273793 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		int32_t L_23 = Stone_get_ColorNumber_m934454503(L_22, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)L_23))))
		{
			goto IL_0068;
		}
	}
	{
		StoneU5BU5D_t1191912092* L_24 = V_0;
		int32_t L_25 = V_4;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		Stone_t255273793 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		Transform_t3600365921 * L_28 = Component_get_transform_m3162698980(L_27, /*hidden argument*/NULL);
		int32_t L_29 = V_3;
		NullCheck(L_28);
		Transform_SetSiblingIndex_m1077399982(L_28, L_29, /*hidden argument*/NULL);
	}

IL_0068:
	{
		int32_t L_30 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_006e:
	{
		int32_t L_31 = V_4;
		StoneU5BU5D_t1191912092* L_32 = V_0;
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_32)->max_length)))))))
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_33 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_007c:
	{
		int32_t L_34 = V_3;
		StoneU5BU5D_t1191912092* L_35 = V_0;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		SoundManager_t2102329059 * L_36 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_36);
		SoundManager_PlaySound_m862056565(L_36, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SortByColor::ActiveButton(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SortByColor_ActiveButton_m1446296454 (SortByColor_t1786351735 * __this, bool ___active0, const RuntimeMethod* method)
{
	{
		Button_t4055032469 * L_0 = __this->get_b_4();
		bool L_1 = ___active0;
		NullCheck(L_0);
		Selectable_set_interactable_m3105888815(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SortByNumber::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SortByNumber__ctor_m1344063753 (SortByNumber_t227753584 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// SortByNumber SortByNumber::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SortByNumber_t227753584 * SortByNumber_get_Instance_m183889917 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SortByNumber_get_Instance_m183889917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SortByNumber_t227753584 * L_0 = ((SortByNumber_t227753584_StaticFields*)il2cpp_codegen_static_fields_for(SortByNumber_t227753584_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void SortByNumber::set_Instance(SortByNumber)
extern "C" IL2CPP_METHOD_ATTR void SortByNumber_set_Instance_m357922602 (RuntimeObject * __this /* static, unused */, SortByNumber_t227753584 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SortByNumber_set_Instance_m357922602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SortByNumber_t227753584 * L_0 = ___value0;
		((SortByNumber_t227753584_StaticFields*)il2cpp_codegen_static_fields_for(SortByNumber_t227753584_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void SortByNumber::Awake()
extern "C" IL2CPP_METHOD_ATTR void SortByNumber_Awake_m3263631240 (SortByNumber_t227753584 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SortByNumber_Awake_m3263631240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SortByNumber_t227753584 * L_0 = SortByNumber_get_Instance_m183889917(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		SortByNumber_set_Instance_m357922602(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Button_t4055032469 * L_2 = Component_GetComponent_TisButton_t4055032469_m1381873113(__this, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_b_4(L_2);
		return;
	}
}
// System.Void SortByNumber::OnClickSortByNum()
extern "C" IL2CPP_METHOD_ATTR void SortByNumber_OnClickSortByNum_m1575780689 (SortByNumber_t227753584 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SortByNumber_OnClickSortByNum_m1575780689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StoneU5BU5D_t1191912092* V_0 = NULL;
	Int32U5BU5D_t385246372* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Deck_t2172403585 * L_0 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		StoneU5BU5D_t1191912092* L_1 = Component_GetComponentsInChildren_TisStone_t255273793_m1217350261(L_0, /*hidden argument*/Component_GetComponentsInChildren_TisStone_t255273793_m1217350261_RuntimeMethod_var);
		V_0 = L_1;
		StoneU5BU5D_t1191912092* L_2 = V_0;
		NullCheck(L_2);
		Int32U5BU5D_t385246372* L_3 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))));
		V_1 = L_3;
		V_2 = 0;
		goto IL_002a;
	}

IL_001b:
	{
		Int32U5BU5D_t385246372* L_4 = V_1;
		int32_t L_5 = V_2;
		StoneU5BU5D_t1191912092* L_6 = V_0;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Stone_t255273793 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		int32_t L_10 = Stone_get_PointNumber_m493988397(L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_10);
		int32_t L_11 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_002a:
	{
		int32_t L_12 = V_2;
		StoneU5BU5D_t1191912092* L_13 = V_0;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Int32U5BU5D_t385246372* L_14 = V_1;
		Array_Sort_TisInt32_t2950945753_m2060455863(NULL /*static, unused*/, L_14, /*hidden argument*/Array_Sort_TisInt32_t2950945753_m2060455863_RuntimeMethod_var);
		V_3 = 0;
		goto IL_007c;
	}

IL_0040:
	{
		V_4 = 0;
		goto IL_006e;
	}

IL_0048:
	{
		Int32U5BU5D_t385246372* L_15 = V_1;
		int32_t L_16 = V_3;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		StoneU5BU5D_t1191912092* L_19 = V_0;
		int32_t L_20 = V_4;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Stone_t255273793 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		int32_t L_23 = Stone_get_PointNumber_m493988397(L_22, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)L_23))))
		{
			goto IL_0068;
		}
	}
	{
		StoneU5BU5D_t1191912092* L_24 = V_0;
		int32_t L_25 = V_4;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		Stone_t255273793 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		Transform_t3600365921 * L_28 = Component_get_transform_m3162698980(L_27, /*hidden argument*/NULL);
		int32_t L_29 = V_3;
		NullCheck(L_28);
		Transform_SetSiblingIndex_m1077399982(L_28, L_29, /*hidden argument*/NULL);
	}

IL_0068:
	{
		int32_t L_30 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_006e:
	{
		int32_t L_31 = V_4;
		StoneU5BU5D_t1191912092* L_32 = V_0;
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_32)->max_length)))))))
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_33 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_007c:
	{
		int32_t L_34 = V_3;
		StoneU5BU5D_t1191912092* L_35 = V_0;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		SoundManager_t2102329059 * L_36 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_36);
		SoundManager_PlaySound_m862056565(L_36, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SortByNumber::ActiveButton(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SortByNumber_ActiveButton_m2261818842 (SortByNumber_t227753584 * __this, bool ___active0, const RuntimeMethod* method)
{
	{
		Button_t4055032469 * L_0 = __this->get_b_4();
		bool L_1 = ___active0;
		NullCheck(L_0);
		Selectable_set_interactable_m3105888815(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SoundManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SoundManager__ctor_m1311707663 (SoundManager_t2102329059 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// SoundManager SoundManager::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SoundManager_t2102329059 * SoundManager_get_Instance_m3963388714 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_get_Instance_m3963388714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SoundManager_t2102329059 * L_0 = ((SoundManager_t2102329059_StaticFields*)il2cpp_codegen_static_fields_for(SoundManager_t2102329059_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void SoundManager::set_Instance(SoundManager)
extern "C" IL2CPP_METHOD_ATTR void SoundManager_set_Instance_m1537600706 (RuntimeObject * __this /* static, unused */, SoundManager_t2102329059 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_set_Instance_m1537600706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SoundManager_t2102329059 * L_0 = ___value0;
		((SoundManager_t2102329059_StaticFields*)il2cpp_codegen_static_fields_for(SoundManager_t2102329059_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void SoundManager::Awake()
extern "C" IL2CPP_METHOD_ATTR void SoundManager_Awake_m2554457172 (SoundManager_t2102329059 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_Awake_m2554457172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SoundManager_t2102329059 * L_0 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		SoundManager_set_Instance_m1537600706(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		__this->set_source_5(L_2);
		return;
	}
}
// System.Void SoundManager::Update()
extern "C" IL2CPP_METHOD_ATTR void SoundManager_Update_m793118089 (SoundManager_t2102329059 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void SoundManager::PlaySound(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void SoundManager_PlaySound_m862056565 (SoundManager_t2102329059 * __this, int32_t ___num0, const RuntimeMethod* method)
{
	{
		AudioSource_t3935305588 * L_0 = __this->get_source_5();
		AudioClipU5BU5D_t143221404* L_1 = __this->get_clips_4();
		int32_t L_2 = ___num0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		AudioClip_t3680889665 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_0);
		AudioSource_PlayOneShot_m1688286683(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SpriteContainer::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SpriteContainer__ctor_m1305911791 (SpriteContainer_t2500302117 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// SpriteContainer SpriteContainer::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SpriteContainer_t2500302117 * SpriteContainer_get_Instance_m3971019802 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteContainer_get_Instance_m3971019802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteContainer_t2500302117 * L_0 = ((SpriteContainer_t2500302117_StaticFields*)il2cpp_codegen_static_fields_for(SpriteContainer_t2500302117_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void SpriteContainer::set_Instance(SpriteContainer)
extern "C" IL2CPP_METHOD_ATTR void SpriteContainer_set_Instance_m610192580 (RuntimeObject * __this /* static, unused */, SpriteContainer_t2500302117 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteContainer_set_Instance_m610192580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteContainer_t2500302117 * L_0 = ___value0;
		((SpriteContainer_t2500302117_StaticFields*)il2cpp_codegen_static_fields_for(SpriteContainer_t2500302117_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void SpriteContainer::Awake()
extern "C" IL2CPP_METHOD_ATTR void SpriteContainer_Awake_m410995551 (SpriteContainer_t2500302117 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteContainer_Awake_m410995551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteContainer_t2500302117 * L_0 = SpriteContainer_get_Instance_m3971019802(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		SpriteContainer_set_Instance_m610192580(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void StartButton::.ctor()
extern "C" IL2CPP_METHOD_ATTR void StartButton__ctor_m408145518 (StartButton_t1998348287 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartButton::Start()
extern "C" IL2CPP_METHOD_ATTR void StartButton_Start_m1217349344 (StartButton_t1998348287 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartButton_Start_m1217349344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Button_t4055032469 * L_0 = Component_GetComponent_TisButton_t4055032469_m1381873113(__this, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_button_4(L_0);
		GameObject_t1113636619 * L_1 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral531699260, /*hidden argument*/NULL);
		NullCheck(L_1);
		YccioNetworkManager_t3600504787 * L_2 = GameObject_GetComponent_TisYccioNetworkManager_t3600504787_m610539862(L_1, /*hidden argument*/GameObject_GetComponent_TisYccioNetworkManager_t3600504787_m610539862_RuntimeMethod_var);
		__this->set_ynm_5(L_2);
		return;
	}
}
// System.Void StartButton::OnClickStartButton()
extern "C" IL2CPP_METHOD_ATTR void StartButton_OnClickStartButton_m2448158104 (StartButton_t1998348287 * __this, const RuntimeMethod* method)
{
	{
		YccioNetworkManager_t3600504787 * L_0 = __this->get_ynm_5();
		NullCheck(L_0);
		YccioNetworkManager_StartGame_m3707574726(L_0, /*hidden argument*/NULL);
		StartButton_ActiveButton_m2363554299(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartButton::ActiveButton(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void StartButton_ActiveButton_m2363554299 (StartButton_t1998348287 * __this, bool ___active0, const RuntimeMethod* method)
{
	{
		Button_t4055032469 * L_0 = __this->get_button_4();
		bool L_1 = ___active0;
		NullCheck(L_0);
		Selectable_set_interactable_m3105888815(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Stone::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Stone__ctor_m3739407457 (Stone_t255273793 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Stone::get_PointNumber()
extern "C" IL2CPP_METHOD_ATTR int32_t Stone_get_PointNumber_m493988397 (Stone_t255273793 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get_stoneNumber_4();
		if ((((int32_t)L_0) <= ((int32_t)7)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = __this->get_stoneNumber_4();
		G_B3_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)8));
		goto IL_0022;
	}

IL_0019:
	{
		int32_t L_2 = __this->get_stoneNumber_4();
		G_B3_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)((int32_t)52)));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Int32 Stone::get_ColorNumber()
extern "C" IL2CPP_METHOD_ATTR int32_t Stone_get_ColorNumber_m934454503 (Stone_t255273793 * __this, const RuntimeMethod* method)
{
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		int32_t L_0 = __this->get_type_6();
		int32_t L_1 = __this->get_number_5();
		G_B1_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)((int32_t)15)));
		if ((((int32_t)L_1) >= ((int32_t)3)))
		{
			G_B2_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)((int32_t)15)));
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = __this->get_number_5();
		G_B3_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)((int32_t)12)));
		G_B3_1 = G_B1_0;
		goto IL_002b;
	}

IL_0023:
	{
		int32_t L_3 = __this->get_number_5();
		G_B3_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)3));
		G_B3_1 = G_B2_0;
	}

IL_002b:
	{
		return ((int32_t)il2cpp_codegen_add((int32_t)G_B3_1, (int32_t)G_B3_0));
	}
}
// System.Boolean Stone::get_IsSelected()
extern "C" IL2CPP_METHOD_ATTR bool Stone_get_IsSelected_m1204973374 (Stone_t255273793 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isSelected_8();
		return L_0;
	}
}
// System.Void Stone::set_IsSelected(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Stone_set_IsSelected_m2213372755 (Stone_t255273793 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		Stone_OnSelectStone_m370821794(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stone::InitStone(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Stone_InitStone_m1584423283 (Stone_t255273793 * __this, int32_t ___stone0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stone_InitStone_m1584423283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t2670269651 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Image_t2670269651 * L_0 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		V_0 = L_0;
		int32_t L_1 = ___stone0;
		__this->set_stoneNumber_4(L_1);
		int32_t L_2 = __this->get_stoneNumber_4();
		__this->set_number_5(((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)L_2/(int32_t)4)), (int32_t)1)));
		int32_t L_3 = __this->get_stoneNumber_4();
		__this->set_type_6(((int32_t)((int32_t)L_3%(int32_t)4)));
		int32_t L_4 = __this->get_type_6();
		V_1 = L_4;
		int32_t L_5 = V_1;
		switch (L_5)
		{
			case 0:
			{
				goto IL_004e;
			}
			case 1:
			{
				goto IL_006c;
			}
			case 2:
			{
				goto IL_008a;
			}
			case 3:
			{
				goto IL_00a8;
			}
		}
	}
	{
		goto IL_00c6;
	}

IL_004e:
	{
		Image_t2670269651 * L_6 = V_0;
		Deck_t2172403585 * L_7 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		SpriteU5BU5D_t2581906349* L_8 = L_7->get_cloud_14();
		int32_t L_9 = __this->get_number_5();
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1));
		Sprite_t280657092 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		Image_set_sprite_m2369174689(L_6, L_11, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_006c:
	{
		Image_t2670269651 * L_12 = V_0;
		Deck_t2172403585 * L_13 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		SpriteU5BU5D_t2581906349* L_14 = L_13->get_star_15();
		int32_t L_15 = __this->get_number_5();
		NullCheck(L_14);
		int32_t L_16 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)1));
		Sprite_t280657092 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_12);
		Image_set_sprite_m2369174689(L_12, L_17, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_008a:
	{
		Image_t2670269651 * L_18 = V_0;
		Deck_t2172403585 * L_19 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		SpriteU5BU5D_t2581906349* L_20 = L_19->get_moon_16();
		int32_t L_21 = __this->get_number_5();
		NullCheck(L_20);
		int32_t L_22 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)1));
		Sprite_t280657092 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_18);
		Image_set_sprite_m2369174689(L_18, L_23, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_00a8:
	{
		Image_t2670269651 * L_24 = V_0;
		Deck_t2172403585 * L_25 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		SpriteU5BU5D_t2581906349* L_26 = L_25->get_sun_17();
		int32_t L_27 = __this->get_number_5();
		NullCheck(L_26);
		int32_t L_28 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_27, (int32_t)1));
		Sprite_t280657092 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_24);
		Image_set_sprite_m2369174689(L_24, L_29, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_00c6:
	{
		goto IL_00cb;
	}

IL_00cb:
	{
		return;
	}
}
// System.Void Stone::OnStoneClick()
extern "C" IL2CPP_METHOD_ATTR void Stone_OnStoneClick_m4120694212 (Stone_t255273793 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stone_OnStoneClick_m4120694212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Button_t4055032469 * L_0 = Component_GetComponent_TisButton_t4055032469_m1381873113(__this, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(38 /* System.Void UnityEngine.UI.Selectable::Select() */, L_0);
		YccioNetworkManager_t3600504787 * L_1 = YccioNetworkManager_get_Instance_m1769066715(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = L_1->get_isMyTurn_18();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		bool L_3 = Stone_get_IsSelected_m1204973374(__this, /*hidden argument*/NULL);
		Stone_set_IsSelected_m2213372755(__this, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		SoundManager_t2102329059 * L_4 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		SoundManager_PlaySound_m862056565(L_4, 2, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void Stone::OnSelectStone(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Stone_OnSelectStone_m370821794 (Stone_t255273793 * __this, bool ___select0, const RuntimeMethod* method)
{
	int32_t G_B5_0 = 0;
	Deck_t2172403585 * G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	Deck_t2172403585 * G_B4_1 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	Deck_t2172403585 * G_B6_2 = NULL;
	{
		bool L_0 = ___select0;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Deck_t2172403585 * L_1 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = L_1->get_maxSubmit_7();
		Deck_t2172403585 * L_3 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_curSubmit_8();
		if ((((int32_t)L_2) > ((int32_t)L_4)))
		{
			goto IL_0020;
		}
	}
	{
		return;
	}

IL_0020:
	{
		bool L_5 = ___select0;
		__this->set_isSelected_8(L_5);
		GameObject_t1113636619 * L_6 = __this->get_selectImage_7();
		bool L_7 = ___select0;
		NullCheck(L_6);
		GameObject_SetActive_m796801857(L_6, L_7, /*hidden argument*/NULL);
		Deck_t2172403585 * L_8 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		Deck_t2172403585 * L_9 = L_8;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_curSubmit_8();
		bool L_11 = ___select0;
		G_B4_0 = L_10;
		G_B4_1 = L_9;
		if (!L_11)
		{
			G_B5_0 = L_10;
			G_B5_1 = L_9;
			goto IL_004a;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_004b;
	}

IL_004a:
	{
		G_B6_0 = (-1);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_004b:
	{
		NullCheck(G_B6_2);
		G_B6_2->set_curSubmit_8(((int32_t)il2cpp_codegen_add((int32_t)G_B6_1, (int32_t)G_B6_0)));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SubmitButton::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SubmitButton__ctor_m3482371819 (SubmitButton_t4135388028 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// SubmitButton SubmitButton::get_Instance()
extern "C" IL2CPP_METHOD_ATTR SubmitButton_t4135388028 * SubmitButton_get_Instance_m3888875242 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubmitButton_get_Instance_m3888875242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubmitButton_t4135388028 * L_0 = ((SubmitButton_t4135388028_StaticFields*)il2cpp_codegen_static_fields_for(SubmitButton_t4135388028_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void SubmitButton::set_Instance(SubmitButton)
extern "C" IL2CPP_METHOD_ATTR void SubmitButton_set_Instance_m853857809 (RuntimeObject * __this /* static, unused */, SubmitButton_t4135388028 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubmitButton_set_Instance_m853857809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubmitButton_t4135388028 * L_0 = ___value0;
		((SubmitButton_t4135388028_StaticFields*)il2cpp_codegen_static_fields_for(SubmitButton_t4135388028_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void SubmitButton::Awake()
extern "C" IL2CPP_METHOD_ATTR void SubmitButton_Awake_m2577497336 (SubmitButton_t4135388028 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubmitButton_Awake_m2577497336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubmitButton_t4135388028 * L_0 = SubmitButton_get_Instance_m3888875242(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		SubmitButton_set_Instance_m853857809(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Button_t4055032469 * L_2 = Component_GetComponent_TisButton_t4055032469_m1381873113(__this, /*hidden argument*/Component_GetComponent_TisButton_t4055032469_m1381873113_RuntimeMethod_var);
		__this->set_b_5(L_2);
		return;
	}
}
// System.Void SubmitButton::OnClickSubmit()
extern "C" IL2CPP_METHOD_ATTR void SubmitButton_OnClickSubmit_m1309036390 (SubmitButton_t4135388028 * __this, const RuntimeMethod* method)
{
	{
		Deck_t2172403585 * L_0 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Deck_Submit_m4224624699(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubmitButton::ActiveButton(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SubmitButton_ActiveButton_m3820152260 (SubmitButton_t4135388028 * __this, bool ___active0, const RuntimeMethod* method)
{
	{
		Button_t4055032469 * L_0 = __this->get_b_5();
		bool L_1 = ___active0;
		NullCheck(L_0);
		Selectable_set_interactable_m3105888815(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SupportLogger::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SupportLogger__ctor_m3857759642 (SupportLogger_t2840230211 * __this, const RuntimeMethod* method)
{
	{
		__this->set_LogTrafficStats_4((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogger::Start()
extern "C" IL2CPP_METHOD_ATTR void SupportLogger_Start_m4228716924 (SupportLogger_t2840230211 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogger_Start_m4228716924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	SupportLogging_t3610999087 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2668676434, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1113636619 * L_3 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_3, _stringLiteral2668676434, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t1113636619 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = V_0;
		NullCheck(L_5);
		SupportLogging_t3610999087 * L_6 = GameObject_AddComponent_TisSupportLogging_t3610999087_m3906415171(L_5, /*hidden argument*/GameObject_AddComponent_TisSupportLogging_t3610999087_m3906415171_RuntimeMethod_var);
		V_1 = L_6;
		SupportLogging_t3610999087 * L_7 = V_1;
		bool L_8 = __this->get_LogTrafficStats_4();
		NullCheck(L_7);
		L_7->set_LogTrafficStats_4(L_8);
	}

IL_003b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SupportLogging::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging__ctor_m1513032022 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::Start()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_Start_m615266166 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_Start_m615266166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_LogTrafficStats_4();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		MonoBehaviour_InvokeRepeating_m650519629(__this, _stringLiteral2903119551, (10.0f), (10.0f), /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void SupportLogging::OnApplicationPause(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_OnApplicationPause_m1998044826 (SupportLogging_t3610999087 * __this, bool ___pause0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_OnApplicationPause_m1998044826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral1193874114);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral1193874114);
		ObjectU5BU5D_t2843939325* L_2 = L_1;
		bool L_3 = ___pause0;
		bool L_4 = L_3;
		RuntimeObject * L_5 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_5);
		ObjectU5BU5D_t2843939325* L_6 = L_2;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral2500080693);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral2500080693);
		ObjectU5BU5D_t2843939325* L_7 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		bool L_8 = PhotonNetwork_get_connected_m1765367833(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_9 = L_8;
		RuntimeObject * L_10 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2971454694(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnApplicationQuit()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_OnApplicationQuit_m3753711955 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour_CancelInvoke_m4090783926(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::LogStats()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_LogStats_m357709987 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_LogStats_m357709987_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_LogTrafficStats_4();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		String_t* L_1 = PhotonNetwork_NetworkStatisticsToString_m397446506(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1837502969, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void SupportLogging::LogBasics()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_LogBasics_m2059416039 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_LogBasics_m2059416039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_AppendFormat_m3016532472(L_1, _stringLiteral1340198547, _stringLiteral2005346028, /*hidden argument*/NULL);
		StringBuilder_t * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		NetworkingPeer_t264212356 * L_3 = ((PhotonNetwork_t1610183659_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var))->get_networkingPeer_3();
		NullCheck(L_3);
		String_t* L_4 = L_3->get_AppId_41();
		NullCheck(L_4);
		String_t* L_5 = String_Substring_m1610150815(L_4, 0, 8, /*hidden argument*/NULL);
		NetworkingPeer_t264212356 * L_6 = ((PhotonNetwork_t1610183659_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var))->get_networkingPeer_3();
		NullCheck(L_6);
		String_t* L_7 = NetworkingPeer_get_AppVersion_m600122273(L_6, /*hidden argument*/NULL);
		NetworkingPeer_t264212356 * L_8 = ((PhotonNetwork_t1610183659_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var))->get_networkingPeer_3();
		NullCheck(L_8);
		String_t* L_9 = PhotonPeer_get_PeerID_m3871481171(L_8, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_AppendFormat_m2403596038(L_2, _stringLiteral227414809, L_5, L_7, L_9, /*hidden argument*/NULL);
		StringBuilder_t * L_10 = V_0;
		String_t* L_11 = PhotonNetwork_get_ServerAddress_m16117006(NULL /*static, unused*/, /*hidden argument*/NULL);
		NetworkingPeer_t264212356 * L_12 = ((PhotonNetwork_t1610183659_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var))->get_networkingPeer_3();
		NullCheck(L_12);
		int32_t L_13 = NetworkingPeer_get_CloudRegion_m608168615(L_12, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(CloudRegionCode_t1925019500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_10);
		StringBuilder_AppendFormat_m3255666490(L_10, _stringLiteral2918654047, L_11, L_15, /*hidden argument*/NULL);
		StringBuilder_t * L_16 = V_0;
		ServerSettings_t2755303613 * L_17 = ((PhotonNetwork_t1610183659_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var))->get_PhotonServerSettings_6();
		NullCheck(L_17);
		int32_t L_18 = L_17->get_HostType_7();
		int32_t L_19 = L_18;
		RuntimeObject * L_20 = Box(HostingOption_t2949276063_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		StringBuilder_AppendFormat_m3016532472(L_16, _stringLiteral2154486352, L_20, /*hidden argument*/NULL);
		StringBuilder_t * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnConnectedToPhoton()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_OnConnectedToPhoton_m4286413539 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_OnConnectedToPhoton_m4286413539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3428758596, /*hidden argument*/NULL);
		SupportLogging_LogBasics_m2059416039(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_LogTrafficStats_4();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_set_NetworkStatisticsEnabled_m3937783244(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void SupportLogging::OnFailedToConnectToPhoton(DisconnectCause)
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_OnFailedToConnectToPhoton_m1253602912 (SupportLogging_t3610999087 * __this, int32_t ___cause0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_OnFailedToConnectToPhoton_m1253602912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___cause0;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(DisconnectCause_t501870387_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral3086498198, L_2, _stringLiteral3450648455, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		SupportLogging_LogBasics_m2059416039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnJoinedLobby()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_OnJoinedLobby_m1882645112 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_OnJoinedLobby_m1882645112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		TypedLobby_t3336582029 * L_0 = PhotonNetwork_get_lobby_m933525236(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral1475082161, L_0, _stringLiteral3450648455, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnJoinedRoom()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_OnJoinedRoom_m1699194386 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_OnJoinedRoom_m1699194386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)6);
		ObjectU5BU5D_t2843939325* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral2085003243);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2085003243);
		ObjectU5BU5D_t2843939325* L_2 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_3 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral3786186961);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3786186961);
		ObjectU5BU5D_t2843939325* L_5 = L_4;
		TypedLobby_t3336582029 * L_6 = PhotonNetwork_get_lobby_m933525236(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_6);
		ObjectU5BU5D_t2843939325* L_7 = L_5;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral1476776628);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1476776628);
		ObjectU5BU5D_t2843939325* L_8 = L_7;
		String_t* L_9 = PhotonNetwork_get_ServerAddress_m16117006(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2971454694(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnCreatedRoom()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_OnCreatedRoom_m909910432 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_OnCreatedRoom_m909910432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)6);
		ObjectU5BU5D_t2843939325* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral1688728708);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral1688728708);
		ObjectU5BU5D_t2843939325* L_2 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_3 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral3786186961);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3786186961);
		ObjectU5BU5D_t2843939325* L_5 = L_4;
		TypedLobby_t3336582029 * L_6 = PhotonNetwork_get_lobby_m933525236(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_6);
		ObjectU5BU5D_t2843939325* L_7 = L_5;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral1476776628);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1476776628);
		ObjectU5BU5D_t2843939325* L_8 = L_7;
		String_t* L_9 = PhotonNetwork_get_ServerAddress_m16117006(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2971454694(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnLeftRoom()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_OnLeftRoom_m1053673117 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_OnLeftRoom_m1053673117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4292058329, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SupportLogging::OnDisconnectedFromPhoton()
extern "C" IL2CPP_METHOD_ATTR void SupportLogging_OnDisconnectedFromPhoton_m3427121297 (SupportLogging_t3610999087 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportLogging_OnDisconnectedFromPhoton_m3427121297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral176793290, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// PunTeams/Team TeamExtensions::GetTeam(PhotonPlayer)
extern "C" IL2CPP_METHOD_ATTR uint8_t TeamExtensions_GetTeam_m369910217 (RuntimeObject * __this /* static, unused */, PhotonPlayer_t3305149557 * ___player0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TeamExtensions_GetTeam_m369910217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		PhotonPlayer_t3305149557 * L_0 = ___player0;
		NullCheck(L_0);
		Hashtable_t1048209202 * L_1 = PhotonPlayer_get_CustomProperties_m3745757186(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Dictionary_2_TryGetValue_m3280774074(L_1, _stringLiteral3917410033, (RuntimeObject **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3280774074_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject * L_3 = V_0;
		return ((*(uint8_t*)((uint8_t*)UnBox(L_3, Team_t2865224648_il2cpp_TypeInfo_var))));
	}

IL_001e:
	{
		return (uint8_t)(0);
	}
}
// System.Void TeamExtensions::SetTeam(PhotonPlayer,PunTeams/Team)
extern "C" IL2CPP_METHOD_ATTR void TeamExtensions_SetTeam_m2985934832 (RuntimeObject * __this /* static, unused */, PhotonPlayer_t3305149557 * ___player0, uint8_t ___team1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TeamExtensions_SetTeam_m2985934832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0;
	Hashtable_t1048209202 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_connectedAndReady_m3099072993(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		int32_t L_1 = PhotonNetwork_get_connectionStateDetailed_m2029000665(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(ClientState_t1348705391_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral2950688325, L_3, _stringLiteral2277785952, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		PhotonPlayer_t3305149557 * L_5 = ___player0;
		uint8_t L_6 = TeamExtensions_GetTeam_m369910217(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		uint8_t L_7 = V_0;
		uint8_t L_8 = ___team1;
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_0057;
		}
	}
	{
		PhotonPlayer_t3305149557 * L_9 = ___player0;
		Hashtable_t1048209202 * L_10 = (Hashtable_t1048209202 *)il2cpp_codegen_object_new(Hashtable_t1048209202_il2cpp_TypeInfo_var);
		Hashtable__ctor_m3127574091(L_10, /*hidden argument*/NULL);
		V_1 = L_10;
		Hashtable_t1048209202 * L_11 = V_1;
		uint8_t L_12 = ___team1;
		uint8_t L_13 = ((uint8_t)L_12);
		RuntimeObject * L_14 = Box(Byte_t1134296376_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		Dictionary_2_Add_m2387223709(L_11, _stringLiteral3917410033, L_14, /*hidden argument*/Dictionary_2_Add_m2387223709_RuntimeMethod_var);
		Hashtable_t1048209202 * L_15 = V_1;
		NullCheck(L_9);
		PhotonPlayer_SetCustomProperties_m1444427373(L_9, L_15, (Hashtable_t1048209202 *)NULL, (bool)0, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TurnExtensions::SetTurn(Room,System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TurnExtensions_SetTurn_m2738922425 (RuntimeObject * __this /* static, unused */, Room_t3759828263 * ___room0, int32_t ___turn1, bool ___setStartTime2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnExtensions_SetTurn_m2738922425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t1048209202 * V_0 = NULL;
	{
		Room_t3759828263 * L_0 = ___room0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Room_t3759828263 * L_1 = ___room0;
		NullCheck(L_1);
		Hashtable_t1048209202 * L_2 = RoomInfo_get_CustomProperties_m3452865736(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0012;
		}
	}

IL_0011:
	{
		return;
	}

IL_0012:
	{
		Hashtable_t1048209202 * L_3 = (Hashtable_t1048209202 *)il2cpp_codegen_object_new(Hashtable_t1048209202_il2cpp_TypeInfo_var);
		Hashtable__ctor_m3127574091(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		Hashtable_t1048209202 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		String_t* L_5 = ((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->get_TurnPropKey_0();
		int32_t L_6 = ___turn1;
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_4);
		Hashtable_set_Item_m963063516(L_4, L_5, L_8, /*hidden argument*/NULL);
		bool L_9 = ___setStartTime2;
		if (!L_9)
		{
			goto IL_0044;
		}
	}
	{
		Hashtable_t1048209202 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		String_t* L_11 = ((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->get_TurnStartPropKey_1();
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		int32_t L_12 = PhotonNetwork_get_ServerTimestamp_m4164654910(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_10);
		Hashtable_set_Item_m963063516(L_10, L_11, L_14, /*hidden argument*/NULL);
	}

IL_0044:
	{
		Room_t3759828263 * L_15 = ___room0;
		Hashtable_t1048209202 * L_16 = V_0;
		NullCheck(L_15);
		Room_SetCustomProperties_m233552519(L_15, L_16, (Hashtable_t1048209202 *)NULL, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 TurnExtensions::GetTurn(RoomInfo)
extern "C" IL2CPP_METHOD_ATTR int32_t TurnExtensions_GetTurn_m4131414938 (RuntimeObject * __this /* static, unused */, RoomInfo_t3170295620 * ___room0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnExtensions_GetTurn_m4131414938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RoomInfo_t3170295620 * L_0 = ___room0;
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		RoomInfo_t3170295620 * L_1 = ___room0;
		NullCheck(L_1);
		Hashtable_t1048209202 * L_2 = RoomInfo_get_CustomProperties_m3452865736(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		RoomInfo_t3170295620 * L_3 = ___room0;
		NullCheck(L_3);
		Hashtable_t1048209202 * L_4 = RoomInfo_get_CustomProperties_m3452865736(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		String_t* L_5 = ((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->get_TurnPropKey_0();
		NullCheck(L_4);
		bool L_6 = Dictionary_2_ContainsKey_m2278349286(L_4, L_5, /*hidden argument*/Dictionary_2_ContainsKey_m2278349286_RuntimeMethod_var);
		if (L_6)
		{
			goto IL_0028;
		}
	}

IL_0026:
	{
		return 0;
	}

IL_0028:
	{
		RoomInfo_t3170295620 * L_7 = ___room0;
		NullCheck(L_7);
		Hashtable_t1048209202 * L_8 = RoomInfo_get_CustomProperties_m3452865736(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		String_t* L_9 = ((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->get_TurnPropKey_0();
		NullCheck(L_8);
		RuntimeObject * L_10 = Hashtable_get_Item_m4119173712(L_8, L_9, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox(L_10, Int32_t2950945753_il2cpp_TypeInfo_var))));
	}
}
// System.Int32 TurnExtensions::GetTurnStart(RoomInfo)
extern "C" IL2CPP_METHOD_ATTR int32_t TurnExtensions_GetTurnStart_m3702491223 (RuntimeObject * __this /* static, unused */, RoomInfo_t3170295620 * ___room0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnExtensions_GetTurnStart_m3702491223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RoomInfo_t3170295620 * L_0 = ___room0;
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		RoomInfo_t3170295620 * L_1 = ___room0;
		NullCheck(L_1);
		Hashtable_t1048209202 * L_2 = RoomInfo_get_CustomProperties_m3452865736(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		RoomInfo_t3170295620 * L_3 = ___room0;
		NullCheck(L_3);
		Hashtable_t1048209202 * L_4 = RoomInfo_get_CustomProperties_m3452865736(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		String_t* L_5 = ((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->get_TurnStartPropKey_1();
		NullCheck(L_4);
		bool L_6 = Dictionary_2_ContainsKey_m2278349286(L_4, L_5, /*hidden argument*/Dictionary_2_ContainsKey_m2278349286_RuntimeMethod_var);
		if (L_6)
		{
			goto IL_0028;
		}
	}

IL_0026:
	{
		return 0;
	}

IL_0028:
	{
		RoomInfo_t3170295620 * L_7 = ___room0;
		NullCheck(L_7);
		Hashtable_t1048209202 * L_8 = RoomInfo_get_CustomProperties_m3452865736(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		String_t* L_9 = ((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->get_TurnStartPropKey_1();
		NullCheck(L_8);
		RuntimeObject * L_10 = Hashtable_get_Item_m4119173712(L_8, L_9, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox(L_10, Int32_t2950945753_il2cpp_TypeInfo_var))));
	}
}
// System.Int32 TurnExtensions::GetFinishedTurn(PhotonPlayer)
extern "C" IL2CPP_METHOD_ATTR int32_t TurnExtensions_GetFinishedTurn_m96341076 (RuntimeObject * __this /* static, unused */, PhotonPlayer_t3305149557 * ___player0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnExtensions_GetFinishedTurn_m96341076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Room_t3759828263 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_0 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Room_t3759828263 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Room_t3759828263 * L_2 = V_0;
		NullCheck(L_2);
		Hashtable_t1048209202 * L_3 = RoomInfo_get_CustomProperties_m3452865736(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Room_t3759828263 * L_4 = V_0;
		NullCheck(L_4);
		Hashtable_t1048209202 * L_5 = RoomInfo_get_CustomProperties_m3452865736(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		String_t* L_6 = ((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->get_TurnPropKey_0();
		NullCheck(L_5);
		bool L_7 = Dictionary_2_ContainsKey_m2278349286(L_5, L_6, /*hidden argument*/Dictionary_2_ContainsKey_m2278349286_RuntimeMethod_var);
		if (L_7)
		{
			goto IL_002e;
		}
	}

IL_002c:
	{
		return 0;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		String_t* L_8 = ((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->get_FinishedTurnPropKey_2();
		PhotonPlayer_t3305149557 * L_9 = ___player0;
		NullCheck(L_9);
		int32_t L_10 = PhotonPlayer_get_ID_m3529408589(L_9, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m904156431(NULL /*static, unused*/, L_8, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		Room_t3759828263 * L_14 = V_0;
		NullCheck(L_14);
		Hashtable_t1048209202 * L_15 = RoomInfo_get_CustomProperties_m3452865736(L_14, /*hidden argument*/NULL);
		String_t* L_16 = V_1;
		NullCheck(L_15);
		RuntimeObject * L_17 = Hashtable_get_Item_m4119173712(L_15, L_16, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox(L_17, Int32_t2950945753_il2cpp_TypeInfo_var))));
	}
}
// System.Void TurnExtensions::SetFinishedTurn(PhotonPlayer,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TurnExtensions_SetFinishedTurn_m862379605 (RuntimeObject * __this /* static, unused */, PhotonPlayer_t3305149557 * ___player0, int32_t ___turn1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnExtensions_SetFinishedTurn_m862379605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Room_t3759828263 * V_0 = NULL;
	String_t* V_1 = NULL;
	Hashtable_t1048209202 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_0 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Room_t3759828263 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Room_t3759828263 * L_2 = V_0;
		NullCheck(L_2);
		Hashtable_t1048209202 * L_3 = RoomInfo_get_CustomProperties_m3452865736(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0018;
		}
	}

IL_0017:
	{
		return;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		String_t* L_4 = ((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->get_FinishedTurnPropKey_2();
		PhotonPlayer_t3305149557 * L_5 = ___player0;
		NullCheck(L_5);
		int32_t L_6 = PhotonPlayer_get_ID_m3529408589(L_5, /*hidden argument*/NULL);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m904156431(NULL /*static, unused*/, L_4, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Hashtable_t1048209202 * L_10 = (Hashtable_t1048209202 *)il2cpp_codegen_object_new(Hashtable_t1048209202_il2cpp_TypeInfo_var);
		Hashtable__ctor_m3127574091(L_10, /*hidden argument*/NULL);
		V_2 = L_10;
		Hashtable_t1048209202 * L_11 = V_2;
		String_t* L_12 = V_1;
		int32_t L_13 = ___turn1;
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		Hashtable_set_Item_m963063516(L_11, L_12, L_15, /*hidden argument*/NULL);
		Room_t3759828263 * L_16 = V_0;
		Hashtable_t1048209202 * L_17 = V_2;
		NullCheck(L_16);
		Room_SetCustomProperties_m233552519(L_16, L_17, (Hashtable_t1048209202 *)NULL, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TurnExtensions::.cctor()
extern "C" IL2CPP_METHOD_ATTR void TurnExtensions__cctor_m1640371730 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnExtensions__cctor_m1640371730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->set_TurnPropKey_0(_stringLiteral3596229116);
		((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->set_TurnStartPropKey_1(_stringLiteral950452602);
		((TurnExtensions_t3150044944_StaticFields*)il2cpp_codegen_static_fields_for(TurnExtensions_t3150044944_il2cpp_TypeInfo_var))->set_FinishedTurnPropKey_2(_stringLiteral3815572937);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TypedLobby::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TypedLobby__ctor_m815421660 (TypedLobby_t3336582029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypedLobby__ctor_m815421660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_Name_0(L_0);
		__this->set_Type_1(0);
		return;
	}
}
// System.Void TypedLobby::.ctor(System.String,LobbyType)
extern "C" IL2CPP_METHOD_ATTR void TypedLobby__ctor_m260949797 (TypedLobby_t3336582029 * __this, String_t* ___name0, uint8_t ___type1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_0(L_0);
		uint8_t L_1 = ___type1;
		__this->set_Type_1(L_1);
		return;
	}
}
// System.Boolean TypedLobby::get_IsDefault()
extern "C" IL2CPP_METHOD_ATTR bool TypedLobby_get_IsDefault_m342755869 (TypedLobby_t3336582029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypedLobby_get_IsDefault_m342755869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		uint8_t L_0 = __this->get_Type_1();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_1 = __this->get_Name_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// System.String TypedLobby::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* TypedLobby_ToString_m2822648706 (TypedLobby_t3336582029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypedLobby_ToString_m2822648706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_Name_0();
		uint8_t L_1 = __this->get_Type_1();
		uint8_t L_2 = L_1;
		RuntimeObject * L_3 = Box(LobbyType_t3695323860_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral4016121227, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void TypedLobby::.cctor()
extern "C" IL2CPP_METHOD_ATTR void TypedLobby__cctor_m1081382309 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypedLobby__cctor_m1081382309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypedLobby_t3336582029 * L_0 = (TypedLobby_t3336582029 *)il2cpp_codegen_object_new(TypedLobby_t3336582029_il2cpp_TypeInfo_var);
		TypedLobby__ctor_m815421660(L_0, /*hidden argument*/NULL);
		((TypedLobby_t3336582029_StaticFields*)il2cpp_codegen_static_fields_for(TypedLobby_t3336582029_il2cpp_TypeInfo_var))->set_Default_2(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TypedLobbyInfo::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TypedLobbyInfo__ctor_m701921937 (TypedLobbyInfo_t2504508049 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypedLobbyInfo__ctor_m701921937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypedLobby_t3336582029_il2cpp_TypeInfo_var);
		TypedLobby__ctor_m815421660(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String TypedLobbyInfo::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* TypedLobbyInfo_ToString_m241642779 (TypedLobbyInfo_t2504508049 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypedLobbyInfo_ToString_m241642779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_1 = L_0;
		String_t* L_2 = ((TypedLobby_t3336582029 *)__this)->get_Name_0();
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
		ObjectU5BU5D_t2843939325* L_3 = L_1;
		uint8_t L_4 = ((TypedLobby_t3336582029 *)__this)->get_Type_1();
		uint8_t L_5 = L_4;
		RuntimeObject * L_6 = Box(LobbyType_t3695323860_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_6);
		ObjectU5BU5D_t2843939325* L_7 = L_3;
		int32_t L_8 = __this->get_RoomCount_4();
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_10);
		ObjectU5BU5D_t2843939325* L_11 = L_7;
		int32_t L_12 = __this->get_PlayerCount_3();
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral4274446839, L_11, /*hidden argument*/NULL);
		return L_15;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WaitingPanel::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaitingPanel__ctor_m1861695837 (WaitingPanel_t1716730091 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaitingPanel::OnClickReset()
extern "C" IL2CPP_METHOD_ATTR void WaitingPanel_OnClickReset_m1553224632 (WaitingPanel_t1716730091 * __this, const RuntimeMethod* method)
{
	{
		YccioNetworkManager_t3600504787 * L_0 = YccioNetworkManager_get_Instance_m1769066715(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		YccioNetworkManager_EndGame_m4061492958(L_0, (bool)1, /*hidden argument*/NULL);
		PlayerStatusPanel_t1209538314 * L_1 = PlayerStatusPanel_get_Instance_m52077214(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_isWaiting_6((bool)0);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebRpcResponse::.ctor(ExitGames.Client.Photon.OperationResponse)
extern "C" IL2CPP_METHOD_ATTR void WebRpcResponse__ctor_m4000971832 (WebRpcResponse_t4177102182 * __this, OperationResponse_t423627973 * ___response0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebRpcResponse__ctor_m4000971832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	WebRpcResponse_t4177102182 * G_B2_0 = NULL;
	WebRpcResponse_t4177102182 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	WebRpcResponse_t4177102182 * G_B3_1 = NULL;
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		OperationResponse_t423627973 * L_0 = ___response0;
		NullCheck(L_0);
		Dictionary_2_t1405253484 * L_1 = L_0->get_Parameters_3();
		NullCheck(L_1);
		Dictionary_2_TryGetValue_m1372101825(L_1, (uint8_t)((int32_t)209), (RuntimeObject **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1372101825_RuntimeMethod_var);
		RuntimeObject * L_2 = V_0;
		WebRpcResponse_set_Name_m2676495348(__this, ((String_t*)IsInstSealed((RuntimeObject*)L_2, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		OperationResponse_t423627973 * L_3 = ___response0;
		NullCheck(L_3);
		Dictionary_2_t1405253484 * L_4 = L_3->get_Parameters_3();
		NullCheck(L_4);
		Dictionary_2_TryGetValue_m1372101825(L_4, (uint8_t)((int32_t)207), (RuntimeObject **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1372101825_RuntimeMethod_var);
		RuntimeObject * L_5 = V_0;
		G_B1_0 = __this;
		if (!L_5)
		{
			G_B2_0 = __this;
			goto IL_004a;
		}
	}
	{
		RuntimeObject * L_6 = V_0;
		G_B3_0 = ((int32_t)(((*(uint8_t*)((uint8_t*)UnBox(L_6, Byte_t1134296376_il2cpp_TypeInfo_var))))));
		G_B3_1 = G_B1_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B3_0 = (-1);
		G_B3_1 = G_B2_0;
	}

IL_004b:
	{
		NullCheck(G_B3_1);
		WebRpcResponse_set_ReturnCode_m3992245468(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		OperationResponse_t423627973 * L_7 = ___response0;
		NullCheck(L_7);
		Dictionary_2_t1405253484 * L_8 = L_7->get_Parameters_3();
		NullCheck(L_8);
		Dictionary_2_TryGetValue_m1372101825(L_8, (uint8_t)((int32_t)208), (RuntimeObject **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1372101825_RuntimeMethod_var);
		RuntimeObject * L_9 = V_0;
		WebRpcResponse_set_Parameters_m3975464742(__this, ((Dictionary_2_t2865362463 *)IsInstClass((RuntimeObject*)L_9, Dictionary_2_t2865362463_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		OperationResponse_t423627973 * L_10 = ___response0;
		NullCheck(L_10);
		Dictionary_2_t1405253484 * L_11 = L_10->get_Parameters_3();
		NullCheck(L_11);
		Dictionary_2_TryGetValue_m1372101825(L_11, (uint8_t)((int32_t)206), (RuntimeObject **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1372101825_RuntimeMethod_var);
		RuntimeObject * L_12 = V_0;
		WebRpcResponse_set_DebugMessage_m3578678403(__this, ((String_t*)IsInstSealed((RuntimeObject*)L_12, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.String WebRpcResponse::get_Name()
extern "C" IL2CPP_METHOD_ATTR String_t* WebRpcResponse_get_Name_m1648311298 (WebRpcResponse_t4177102182 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void WebRpcResponse::set_Name(System.String)
extern "C" IL2CPP_METHOD_ATTR void WebRpcResponse_set_Name_m2676495348 (WebRpcResponse_t4177102182 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 WebRpcResponse::get_ReturnCode()
extern "C" IL2CPP_METHOD_ATTR int32_t WebRpcResponse_get_ReturnCode_m3292208677 (WebRpcResponse_t4177102182 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CReturnCodeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void WebRpcResponse::set_ReturnCode(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void WebRpcResponse_set_ReturnCode_m3992245468 (WebRpcResponse_t4177102182 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CReturnCodeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String WebRpcResponse::get_DebugMessage()
extern "C" IL2CPP_METHOD_ATTR String_t* WebRpcResponse_get_DebugMessage_m4109436483 (WebRpcResponse_t4177102182 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDebugMessageU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void WebRpcResponse::set_DebugMessage(System.String)
extern "C" IL2CPP_METHOD_ATTR void WebRpcResponse_set_DebugMessage_m3578678403 (WebRpcResponse_t4177102182 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDebugMessageU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> WebRpcResponse::get_Parameters()
extern "C" IL2CPP_METHOD_ATTR Dictionary_2_t2865362463 * WebRpcResponse_get_Parameters_m77974431 (WebRpcResponse_t4177102182 * __this, const RuntimeMethod* method)
{
	{
		Dictionary_2_t2865362463 * L_0 = __this->get_U3CParametersU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void WebRpcResponse::set_Parameters(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" IL2CPP_METHOD_ATTR void WebRpcResponse_set_Parameters_m3975464742 (WebRpcResponse_t4177102182 * __this, Dictionary_2_t2865362463 * ___value0, const RuntimeMethod* method)
{
	{
		Dictionary_2_t2865362463 * L_0 = ___value0;
		__this->set_U3CParametersU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String WebRpcResponse::ToStringFull()
extern "C" IL2CPP_METHOD_ATTR String_t* WebRpcResponse_ToStringFull_m3199251698 (WebRpcResponse_t4177102182 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebRpcResponse_ToStringFull_m3199251698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_1 = L_0;
		String_t* L_2 = WebRpcResponse_get_Name_m1648311298(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
		ObjectU5BU5D_t2843939325* L_3 = L_1;
		Dictionary_2_t2865362463 * L_4 = WebRpcResponse_get_Parameters_m77974431(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SupportClass_t2974952451_il2cpp_TypeInfo_var);
		String_t* L_5 = SupportClass_DictionaryToString_m1908829707(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_5);
		ObjectU5BU5D_t2843939325* L_6 = L_3;
		int32_t L_7 = WebRpcResponse_get_ReturnCode_m3292208677(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		RuntimeObject * L_9 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_9);
		ObjectU5BU5D_t2843939325* L_10 = L_6;
		String_t* L_11 = WebRpcResponse_get_DebugMessage_m4109436483(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m630303134(NULL /*static, unused*/, _stringLiteral2663913931, L_10, /*hidden argument*/NULL);
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WinLosePanel::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WinLosePanel__ctor_m3517685226 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// WinLosePanel WinLosePanel::get_Instance()
extern "C" IL2CPP_METHOD_ATTR WinLosePanel_t3114026018 * WinLosePanel_get_Instance_m3091037315 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinLosePanel_get_Instance_m3091037315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WinLosePanel_t3114026018 * L_0 = ((WinLosePanel_t3114026018_StaticFields*)il2cpp_codegen_static_fields_for(WinLosePanel_t3114026018_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void WinLosePanel::set_Instance(WinLosePanel)
extern "C" IL2CPP_METHOD_ATTR void WinLosePanel_set_Instance_m3033113386 (RuntimeObject * __this /* static, unused */, WinLosePanel_t3114026018 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinLosePanel_set_Instance_m3033113386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WinLosePanel_t3114026018 * L_0 = ___value0;
		((WinLosePanel_t3114026018_StaticFields*)il2cpp_codegen_static_fields_for(WinLosePanel_t3114026018_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void WinLosePanel::Awake()
extern "C" IL2CPP_METHOD_ATTR void WinLosePanel_Awake_m994853271 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinLosePanel_Awake_m994853271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WinLosePanel_t3114026018 * L_0 = WinLosePanel_get_Instance_m3091037315(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		WinLosePanel_set_Instance_m3033113386(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void WinLosePanel::Update()
extern "C" IL2CPP_METHOD_ATTR void WinLosePanel_Update_m505985956 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinLosePanel_Update_m505985956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)120), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		WinLosePanel_OnWin_m3713210824(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)122), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		WinLosePanel_OnLose_m3303681697(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void WinLosePanel::OnWin()
extern "C" IL2CPP_METHOD_ATTR void WinLosePanel_OnWin_m3713210824 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = WinLosePanel_C_OnWin_m1621631832(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WinLosePanel::OnLose()
extern "C" IL2CPP_METHOD_ATTR void WinLosePanel_OnLose_m3303681697 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = WinLosePanel_C_OnLose_m3097372575(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator WinLosePanel::C_OnWin()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* WinLosePanel_C_OnWin_m1621631832 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinLosePanel_C_OnWin_m1621631832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CC_OnWinU3Ec__Iterator0_t1703244554 * V_0 = NULL;
	{
		U3CC_OnWinU3Ec__Iterator0_t1703244554 * L_0 = (U3CC_OnWinU3Ec__Iterator0_t1703244554 *)il2cpp_codegen_object_new(U3CC_OnWinU3Ec__Iterator0_t1703244554_il2cpp_TypeInfo_var);
		U3CC_OnWinU3Ec__Iterator0__ctor_m3446074108(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CC_OnWinU3Ec__Iterator0_t1703244554 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CC_OnWinU3Ec__Iterator0_t1703244554 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator WinLosePanel::C_OnLose()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* WinLosePanel_C_OnLose_m3097372575 (WinLosePanel_t3114026018 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinLosePanel_C_OnLose_m3097372575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CC_OnLoseU3Ec__Iterator1_t1181911525 * V_0 = NULL;
	{
		U3CC_OnLoseU3Ec__Iterator1_t1181911525 * L_0 = (U3CC_OnLoseU3Ec__Iterator1_t1181911525 *)il2cpp_codegen_object_new(U3CC_OnLoseU3Ec__Iterator1_t1181911525_il2cpp_TypeInfo_var);
		U3CC_OnLoseU3Ec__Iterator1__ctor_m3515859002(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CC_OnLoseU3Ec__Iterator1_t1181911525 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CC_OnLoseU3Ec__Iterator1_t1181911525 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WinLosePanel/<C_OnLose>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CC_OnLoseU3Ec__Iterator1__ctor_m3515859002 (U3CC_OnLoseU3Ec__Iterator1_t1181911525 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WinLosePanel/<C_OnLose>c__Iterator1::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CC_OnLoseU3Ec__Iterator1_MoveNext_m1702681965 (U3CC_OnLoseU3Ec__Iterator1_t1181911525 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CC_OnLoseU3Ec__Iterator1_MoveNext_m1702681965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_t1113636619 * V_2 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_0076;
			}
			case 2:
			{
				goto IL_00e5;
			}
			case 3:
			{
				goto IL_0149;
			}
		}
	}
	{
		goto IL_017c;
	}

IL_0029:
	{
		SoundManager_t2102329059 * L_2 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SoundManager_PlaySound_m862056565(L_2, 6, /*hidden argument*/NULL);
		WinLosePanel_t3114026018 * L_3 = __this->get_U24this_2();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = L_3->get_lose_5();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
		WinLosePanel_t3114026018 * L_5 = __this->get_U24this_2();
		NullCheck(L_5);
		GameObject_t1113636619 * L_6 = L_5->get_lose_5();
		NullCheck(L_6);
		Text_t1901882714 * L_7 = GameObject_GetComponent_TisText_t1901882714_m2114913816(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m2114913816_RuntimeMethod_var);
		__this->set_U3CtU3E__0_0(L_7);
		__this->set_U24current_3(NULL);
		bool L_8 = __this->get_U24disposing_4();
		if (L_8)
		{
			goto IL_0071;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0071:
	{
		goto IL_017e;
	}

IL_0076:
	{
		Text_t1901882714 * L_9 = __this->get_U3CtU3E__0_0();
		Color_t2555686324  L_10 = Color_get_gray_m1471337008(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		V_1 = 0;
		goto IL_00b9;
	}

IL_008d:
	{
		WinLosePanel_t3114026018 * L_11 = __this->get_U24this_2();
		NullCheck(L_11);
		GameObject_t1113636619 * L_12 = L_11->get_particle_6();
		WinLosePanel_t3114026018 * L_13 = __this->get_U24this_2();
		NullCheck(L_13);
		Transform_t3600365921 * L_14 = Component_get_transform_m3162698980(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_15 = Object_Instantiate_TisGameObject_t1113636619_m3215236302(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3215236302_RuntimeMethod_var);
		V_2 = L_15;
		GameObject_t1113636619 * L_16 = V_2;
		NullCheck(L_16);
		WinLoseParticle_t1640804235 * L_17 = GameObject_GetComponent_TisWinLoseParticle_t1640804235_m3979570490(L_16, /*hidden argument*/GameObject_GetComponent_TisWinLoseParticle_t1640804235_m3979570490_RuntimeMethod_var);
		NullCheck(L_17);
		WinLoseParticle_Init_m4007159944(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_00b9:
	{
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) < ((int32_t)((int32_t)50))))
		{
			goto IL_008d;
		}
	}
	{
		WaitForSeconds_t1699091251 * L_20 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_20, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_20);
		bool L_21 = __this->get_U24disposing_4();
		if (L_21)
		{
			goto IL_00e0;
		}
	}
	{
		__this->set_U24PC_5(2);
	}

IL_00e0:
	{
		goto IL_017e;
	}

IL_00e5:
	{
		__this->set_U3CiU3E__1_1(0);
		goto IL_0157;
	}

IL_00f1:
	{
		Text_t1901882714 * L_22 = __this->get_U3CtU3E__0_0();
		Text_t1901882714 * L_23 = __this->get_U3CtU3E__0_0();
		NullCheck(L_23);
		Color_t2555686324  L_24 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_23);
		Color_t2555686324  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Color__ctor_m2943235014((&L_25), (0.0f), (0.0f), (0.0f), (0.03f), /*hidden argument*/NULL);
		Color_t2555686324  L_26 = Color_op_Subtraction_m181229690(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_22);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_22, L_26);
		WaitForSeconds_t1699091251 * L_27 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_27, (0.01f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_27);
		bool L_28 = __this->get_U24disposing_4();
		if (L_28)
		{
			goto IL_0144;
		}
	}
	{
		__this->set_U24PC_5(3);
	}

IL_0144:
	{
		goto IL_017e;
	}

IL_0149:
	{
		int32_t L_29 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1)));
	}

IL_0157:
	{
		int32_t L_30 = __this->get_U3CiU3E__1_1();
		if ((((int32_t)L_30) < ((int32_t)((int32_t)30))))
		{
			goto IL_00f1;
		}
	}
	{
		WinLosePanel_t3114026018 * L_31 = __this->get_U24this_2();
		NullCheck(L_31);
		GameObject_t1113636619 * L_32 = L_31->get_lose_5();
		NullCheck(L_32);
		GameObject_SetActive_m796801857(L_32, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_017c:
	{
		return (bool)0;
	}

IL_017e:
	{
		return (bool)1;
	}
}
// System.Object WinLosePanel/<C_OnLose>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CC_OnLoseU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m987636335 (U3CC_OnLoseU3Ec__Iterator1_t1181911525 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object WinLosePanel/<C_OnLose>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CC_OnLoseU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2118743115 (U3CC_OnLoseU3Ec__Iterator1_t1181911525 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void WinLosePanel/<C_OnLose>c__Iterator1::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CC_OnLoseU3Ec__Iterator1_Dispose_m3248422520 (U3CC_OnLoseU3Ec__Iterator1_t1181911525 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void WinLosePanel/<C_OnLose>c__Iterator1::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CC_OnLoseU3Ec__Iterator1_Reset_m3434917950 (U3CC_OnLoseU3Ec__Iterator1_t1181911525 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CC_OnLoseU3Ec__Iterator1_Reset_m3434917950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CC_OnLoseU3Ec__Iterator1_Reset_m3434917950_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WinLosePanel/<C_OnWin>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CC_OnWinU3Ec__Iterator0__ctor_m3446074108 (U3CC_OnWinU3Ec__Iterator0_t1703244554 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WinLosePanel/<C_OnWin>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CC_OnWinU3Ec__Iterator0_MoveNext_m296220714 (U3CC_OnWinU3Ec__Iterator0_t1703244554 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CC_OnWinU3Ec__Iterator0_MoveNext_m296220714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_t1113636619 * V_2 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_0076;
			}
			case 2:
			{
				goto IL_00e5;
			}
			case 3:
			{
				goto IL_0149;
			}
		}
	}
	{
		goto IL_017c;
	}

IL_0029:
	{
		SoundManager_t2102329059 * L_2 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SoundManager_PlaySound_m862056565(L_2, 4, /*hidden argument*/NULL);
		WinLosePanel_t3114026018 * L_3 = __this->get_U24this_2();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = L_3->get_win_4();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
		WinLosePanel_t3114026018 * L_5 = __this->get_U24this_2();
		NullCheck(L_5);
		GameObject_t1113636619 * L_6 = L_5->get_win_4();
		NullCheck(L_6);
		Text_t1901882714 * L_7 = GameObject_GetComponent_TisText_t1901882714_m2114913816(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m2114913816_RuntimeMethod_var);
		__this->set_U3CtU3E__0_0(L_7);
		__this->set_U24current_3(NULL);
		bool L_8 = __this->get_U24disposing_4();
		if (L_8)
		{
			goto IL_0071;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0071:
	{
		goto IL_017e;
	}

IL_0076:
	{
		Text_t1901882714 * L_9 = __this->get_U3CtU3E__0_0();
		Color_t2555686324  L_10 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		V_1 = 0;
		goto IL_00b9;
	}

IL_008d:
	{
		WinLosePanel_t3114026018 * L_11 = __this->get_U24this_2();
		NullCheck(L_11);
		GameObject_t1113636619 * L_12 = L_11->get_particle_6();
		WinLosePanel_t3114026018 * L_13 = __this->get_U24this_2();
		NullCheck(L_13);
		Transform_t3600365921 * L_14 = Component_get_transform_m3162698980(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_15 = Object_Instantiate_TisGameObject_t1113636619_m3215236302(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3215236302_RuntimeMethod_var);
		V_2 = L_15;
		GameObject_t1113636619 * L_16 = V_2;
		NullCheck(L_16);
		WinLoseParticle_t1640804235 * L_17 = GameObject_GetComponent_TisWinLoseParticle_t1640804235_m3979570490(L_16, /*hidden argument*/GameObject_GetComponent_TisWinLoseParticle_t1640804235_m3979570490_RuntimeMethod_var);
		NullCheck(L_17);
		WinLoseParticle_Init_m4007159944(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_00b9:
	{
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) < ((int32_t)((int32_t)50))))
		{
			goto IL_008d;
		}
	}
	{
		WaitForSeconds_t1699091251 * L_20 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_20, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_20);
		bool L_21 = __this->get_U24disposing_4();
		if (L_21)
		{
			goto IL_00e0;
		}
	}
	{
		__this->set_U24PC_5(2);
	}

IL_00e0:
	{
		goto IL_017e;
	}

IL_00e5:
	{
		__this->set_U3CiU3E__1_1(0);
		goto IL_0157;
	}

IL_00f1:
	{
		Text_t1901882714 * L_22 = __this->get_U3CtU3E__0_0();
		Text_t1901882714 * L_23 = __this->get_U3CtU3E__0_0();
		NullCheck(L_23);
		Color_t2555686324  L_24 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_23);
		Color_t2555686324  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Color__ctor_m2943235014((&L_25), (0.0f), (0.0f), (0.0f), (0.03f), /*hidden argument*/NULL);
		Color_t2555686324  L_26 = Color_op_Subtraction_m181229690(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_22);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_22, L_26);
		WaitForSeconds_t1699091251 * L_27 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_27, (0.01f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_27);
		bool L_28 = __this->get_U24disposing_4();
		if (L_28)
		{
			goto IL_0144;
		}
	}
	{
		__this->set_U24PC_5(3);
	}

IL_0144:
	{
		goto IL_017e;
	}

IL_0149:
	{
		int32_t L_29 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1)));
	}

IL_0157:
	{
		int32_t L_30 = __this->get_U3CiU3E__1_1();
		if ((((int32_t)L_30) < ((int32_t)((int32_t)30))))
		{
			goto IL_00f1;
		}
	}
	{
		WinLosePanel_t3114026018 * L_31 = __this->get_U24this_2();
		NullCheck(L_31);
		GameObject_t1113636619 * L_32 = L_31->get_win_4();
		NullCheck(L_32);
		GameObject_SetActive_m796801857(L_32, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_017c:
	{
		return (bool)0;
	}

IL_017e:
	{
		return (bool)1;
	}
}
// System.Object WinLosePanel/<C_OnWin>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CC_OnWinU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3570282222 (U3CC_OnWinU3Ec__Iterator0_t1703244554 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object WinLosePanel/<C_OnWin>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CC_OnWinU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3446042174 (U3CC_OnWinU3Ec__Iterator0_t1703244554 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void WinLosePanel/<C_OnWin>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CC_OnWinU3Ec__Iterator0_Dispose_m124446221 (U3CC_OnWinU3Ec__Iterator0_t1703244554 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void WinLosePanel/<C_OnWin>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CC_OnWinU3Ec__Iterator0_Reset_m2251615767 (U3CC_OnWinU3Ec__Iterator0_t1703244554 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CC_OnWinU3Ec__Iterator0_Reset_m2251615767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CC_OnWinU3Ec__Iterator0_Reset_m2251615767_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WinLoseParticle::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WinLoseParticle__ctor_m2255364755 (WinLoseParticle_t1640804235 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WinLoseParticle::Init(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void WinLoseParticle_Init_m4007159944 (WinLoseParticle_t1640804235 * __this, bool ___win0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinLoseParticle_Init_m4007159944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = ___win0;
		if (!L_0)
		{
			goto IL_0141;
		}
	}
	{
		int32_t L_1 = Random_Range_m4054026115(NULL /*static, unused*/, 0, 5, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_002d;
			}
			case 1:
			{
				goto IL_0055;
			}
			case 2:
			{
				goto IL_0080;
			}
			case 3:
			{
				goto IL_00a8;
			}
			case 4:
			{
				goto IL_00d3;
			}
		}
	}
	{
		goto IL_00fe;
	}

IL_002d:
	{
		Image_t2670269651 * L_3 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color32_t2600501292  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color32__ctor_m4150508762((&L_4), (uint8_t)((int32_t)255), (uint8_t)((int32_t)31), (uint8_t)((int32_t)31), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_t2555686324  L_5 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_3, L_5);
		goto IL_0103;
	}

IL_0055:
	{
		Image_t2670269651 * L_6 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color32_t2600501292  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color32__ctor_m4150508762((&L_7), (uint8_t)((int32_t)31), (uint8_t)((int32_t)140), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_t2555686324  L_8 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_6, L_8);
		goto IL_0103;
	}

IL_0080:
	{
		Image_t2670269651 * L_9 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color32_t2600501292  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color32__ctor_m4150508762((&L_10), (uint8_t)((int32_t)31), (uint8_t)((int32_t)195), (uint8_t)((int32_t)31), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_t2555686324  L_11 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_11);
		goto IL_0103;
	}

IL_00a8:
	{
		Image_t2670269651 * L_12 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color32_t2600501292  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color32__ctor_m4150508762((&L_13), (uint8_t)((int32_t)255), (uint8_t)((int32_t)196), (uint8_t)((int32_t)31), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_t2555686324  L_14 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_14);
		goto IL_0103;
	}

IL_00d3:
	{
		Image_t2670269651 * L_15 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color32_t2600501292  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Color32__ctor_m4150508762((&L_16), (uint8_t)((int32_t)195), (uint8_t)((int32_t)86), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_t2555686324  L_17 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_15, L_17);
		goto IL_0103;
	}

IL_00fe:
	{
		goto IL_0103;
	}

IL_0103:
	{
		Rigidbody2D_t939494601 * L_18 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		float L_19 = Random_Range_m2202990745(NULL /*static, unused*/, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_20 = Random_Range_m2202990745(NULL /*static, unused*/, (0.0f), (2.0f), /*hidden argument*/NULL);
		Vector2_t2156229523  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3970636864((&L_21), L_19, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_22 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_21, (60000.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		Rigidbody2D_AddForce_m1099013366(L_18, L_22, 0, /*hidden argument*/NULL);
		goto IL_01d7;
	}

IL_0141:
	{
		int32_t L_23 = Random_Range_m4054026115(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = V_1;
		if (!L_24)
		{
			goto IL_015b;
		}
	}
	{
		int32_t L_25 = V_1;
		if ((((int32_t)L_25) == ((int32_t)1)))
		{
			goto IL_0170;
		}
	}
	{
		goto IL_0199;
	}

IL_015b:
	{
		Image_t2670269651 * L_26 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color_t2555686324  L_27 = Color_get_gray_m1471337008(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_26, L_27);
		goto IL_019e;
	}

IL_0170:
	{
		Image_t2670269651 * L_28 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color_t2555686324  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Color__ctor_m2943235014((&L_29), (0.2f), (0.2f), (0.2f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_28);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_28, L_29);
		goto IL_019e;
	}

IL_0199:
	{
		goto IL_019e;
	}

IL_019e:
	{
		Rigidbody2D_t939494601 * L_30 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		float L_31 = Random_Range_m2202990745(NULL /*static, unused*/, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_32 = Random_Range_m2202990745(NULL /*static, unused*/, (0.0f), (2.0f), /*hidden argument*/NULL);
		Vector2_t2156229523  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector2__ctor_m3970636864((&L_33), L_31, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_34 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_33, (30000.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		Rigidbody2D_AddForce_m1099013366(L_30, L_34, 0, /*hidden argument*/NULL);
	}

IL_01d7:
	{
		GameObject_t1113636619 * L_35 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_35, (5.0f), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void YccioNetworkManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager__ctor_m1879442940 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager__ctor_m1879442940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_myRoomName_8(L_0);
		__this->set_maxStone_12(1);
		__this->set_numStone_13(1);
		__this->set_totalStone_14(1);
		__this->set_fieldOwner_19((-1));
		__this->set_isWaiting_22((bool)1);
		PunBehaviour__ctor_m1042702634(__this, /*hidden argument*/NULL);
		return;
	}
}
// YccioNetworkManager YccioNetworkManager::get_Instance()
extern "C" IL2CPP_METHOD_ATTR YccioNetworkManager_t3600504787 * YccioNetworkManager_get_Instance_m1769066715 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_get_Instance_m1769066715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		YccioNetworkManager_t3600504787 * L_0 = ((YccioNetworkManager_t3600504787_StaticFields*)il2cpp_codegen_static_fields_for(YccioNetworkManager_t3600504787_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_25();
		return L_0;
	}
}
// System.Void YccioNetworkManager::set_Instance(YccioNetworkManager)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_set_Instance_m1649774145 (RuntimeObject * __this /* static, unused */, YccioNetworkManager_t3600504787 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_set_Instance_m1649774145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		YccioNetworkManager_t3600504787 * L_0 = ___value0;
		((YccioNetworkManager_t3600504787_StaticFields*)il2cpp_codegen_static_fields_for(YccioNetworkManager_t3600504787_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_25(L_0);
		return;
	}
}
// System.Void YccioNetworkManager::Awake()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_Awake_m4169508252 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_Awake_m4169508252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		YccioNetworkManager_t3600504787 * L_0 = YccioNetworkManager_get_Instance_m1769066715(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		YccioNetworkManager_set_Instance_m1649774145(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		PunTurnManager_t1223962931 * L_2 = Component_GetComponent_TisPunTurnManager_t1223962931_m3266571455(__this, /*hidden argument*/Component_GetComponent_TisPunTurnManager_t1223962931_m3266571455_RuntimeMethod_var);
		__this->set_turnManager_5(L_2);
		PunTurnManager_t1223962931 * L_3 = __this->get_turnManager_5();
		NullCheck(L_3);
		L_3->set_TurnManagerListener_6(__this);
		PunTurnManager_t1223962931 * L_4 = __this->get_turnManager_5();
		NullCheck(L_4);
		L_4->set_TurnDuration_5((20.0f));
		SceneTransition_t1138091307 * L_5 = Component_GetComponent_TisSceneTransition_t1138091307_m3994718925(__this, /*hidden argument*/Component_GetComponent_TisSceneTransition_t1138091307_m3994718925_RuntimeMethod_var);
		__this->set_st_6(L_5);
		PlayerData_t220878115 * L_6 = Component_GetComponent_TisPlayerData_t220878115_m3167838379(__this, /*hidden argument*/Component_GetComponent_TisPlayerData_t220878115_m3167838379_RuntimeMethod_var);
		__this->set_pd_7(L_6);
		return;
	}
}
// System.Void YccioNetworkManager::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnEnable_m3274924364 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_OnEnable_m3274924364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = (intptr_t)YccioNetworkManager_OnPhotonEvent_m152539995_RuntimeMethod_var;
		EventCallback_t1220598991 * L_1 = (EventCallback_t1220598991 *)il2cpp_codegen_object_new(EventCallback_t1220598991_il2cpp_TypeInfo_var);
		EventCallback__ctor_m951134603(L_1, __this, (intptr_t)L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_add_OnEventCall_m3003581615(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnDisable_m3549428848 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_OnDisable_m3549428848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = (intptr_t)YccioNetworkManager_OnPhotonEvent_m152539995_RuntimeMethod_var;
		EventCallback_t1220598991 * L_1 = (EventCallback_t1220598991 *)il2cpp_codegen_object_new(EventCallback_t1220598991_il2cpp_TypeInfo_var);
		EventCallback__ctor_m951134603(L_1, __this, (intptr_t)L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_remove_OnEventCall_m1048168083(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::OnPhotonEvent(System.Byte,System.Object,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnPhotonEvent_m152539995 (YccioNetworkManager_t3600504787 * __this, uint8_t ___eventCode0, RuntimeObject * ___content1, int32_t ___senderId2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_OnPhotonEvent_m152539995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t385246372* V_1 = NULL;
	{
		uint8_t L_0 = ___eventCode0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_2 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Room_get_PlayerCount_m1320583396(L_2, /*hidden argument*/NULL);
		__this->set_playerNumber_17(L_3);
		__this->set_isWaiting_22((bool)0);
		int32_t L_4 = __this->get_playerNumber_17();
		Int32U5BU5D_t385246372* L_5 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)L_4);
		__this->set_remainStones_23(L_5);
		Room_t3759828263 * L_6 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		RoomInfo_set_IsPlaying_m3623793460(L_6, (bool)1, /*hidden argument*/NULL);
		PlayerData_t220878115 * L_7 = PlayerData_get_Instance_m3742910714(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayerData_t220878115 * L_8 = L_7;
		NullCheck(L_8);
		int32_t L_9 = PlayerData_get_Chips_m1748726914(L_8, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_playerNumber_17();
		NullCheck(L_8);
		PlayerData_set_Chips_m1881252529(L_8, ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_10)))), /*hidden argument*/NULL);
		SortByColor_t1786351735 * L_11 = SortByColor_get_Instance_m2199486831(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		SortByColor_ActiveButton_m1446296454(L_11, (bool)1, /*hidden argument*/NULL);
		SortByNumber_t227753584 * L_12 = SortByNumber_get_Instance_m183889917(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		SortByNumber_ActiveButton_m2261818842(L_12, (bool)1, /*hidden argument*/NULL);
		Deck_t2172403585 * L_13 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Deck_ClearDeck_m200245175(L_13, /*hidden argument*/NULL);
		YccioNetworkManager_SetPlayOrder_m745047584(__this, /*hidden argument*/NULL);
		YccioNetworkManager_ShuffleSetting_m970800901(__this, /*hidden argument*/NULL);
		YccioNetworkManager_Shuffle_m1267737554(__this, /*hidden argument*/NULL);
		YccioNetworkManager_ClearField_m3891704080(__this, /*hidden argument*/NULL);
		goto IL_017a;
	}

IL_0091:
	{
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00aa;
		}
	}
	{
		RuntimeObject * L_15 = ___content1;
		YccioNetworkManager_GetMyStone_m2847881598(__this, ((Int32U5BU5D_t385246372*)IsInst((RuntimeObject*)L_15, Int32U5BU5D_t385246372_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_017a;
	}

IL_00aa:
	{
		int32_t L_16 = V_0;
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00bd;
		}
	}
	{
		YccioNetworkManager_DistributeStatus_m2345907618(__this, /*hidden argument*/NULL);
		goto IL_017a;
	}

IL_00bd:
	{
		int32_t L_17 = V_0;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_00d6;
		}
	}
	{
		RuntimeObject * L_18 = ___content1;
		YccioNetworkManager_UpdateStatus_m2094236186(__this, ((ObjectU5BU5D_t2843939325*)IsInst((RuntimeObject*)L_18, ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_017a;
	}

IL_00d6:
	{
		int32_t L_19 = V_0;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00f3;
		}
	}
	{
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral2372802512, (0.1f), /*hidden argument*/NULL);
		goto IL_017a;
	}

IL_00f3:
	{
		int32_t L_20 = V_0;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0119;
		}
	}
	{
		RuntimeObject * L_21 = ___content1;
		__this->set_orderOffset_16(((int32_t)il2cpp_codegen_subtract((int32_t)((*(int32_t*)((int32_t*)UnBox(L_21, Int32_t2950945753_il2cpp_TypeInfo_var)))), (int32_t)1)));
		PunTurnManager_t1223962931 * L_22 = __this->get_turnManager_5();
		NullCheck(L_22);
		PunTurnManager_BeginTurn_m4041073334(L_22, /*hidden argument*/NULL);
		goto IL_017a;
	}

IL_0119:
	{
		int32_t L_23 = V_0;
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0166;
		}
	}
	{
		RuntimeObject * L_24 = ___content1;
		V_1 = ((Int32U5BU5D_t385246372*)IsInst((RuntimeObject*)L_24, Int32U5BU5D_t385246372_il2cpp_TypeInfo_var));
		bool L_25 = __this->get_isWaiting_22();
		if (!L_25)
		{
			goto IL_0148;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_26 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = Room_get_PlayerCount_m1320583396(L_26, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_28 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)L_27);
		__this->set_remainStones_23(L_28);
	}

IL_0148:
	{
		Int32U5BU5D_t385246372* L_29 = __this->get_remainStones_23();
		Int32U5BU5D_t385246372* L_30 = V_1;
		NullCheck(L_30);
		int32_t L_31 = 0;
		int32_t L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		Int32U5BU5D_t385246372* L_33 = V_1;
		NullCheck(L_33);
		int32_t L_34 = 1;
		int32_t L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (int32_t)L_35);
		Int32U5BU5D_t385246372* L_36 = V_1;
		NullCheck(L_36);
		int32_t L_37 = 0;
		int32_t L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		Int32U5BU5D_t385246372* L_39 = V_1;
		NullCheck(L_39);
		int32_t L_40 = 1;
		int32_t L_41 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		YccioNetworkManager_UpdateRemainStone_m4039245266(__this, L_38, L_41, /*hidden argument*/NULL);
		goto IL_017a;
	}

IL_0166:
	{
		int32_t L_42 = V_0;
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_017a;
		}
	}
	{
		RuntimeObject * L_43 = ___content1;
		YccioNetworkManager_GetImoji_m3146625778(__this, ((Int32U5BU5D_t385246372*)IsInst((RuntimeObject*)L_43, Int32U5BU5D_t385246372_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_017a:
	{
		return;
	}
}
// System.Void YccioNetworkManager::OnTurnBegins(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnTurnBegins_m1889923224 (YccioNetworkManager_t3600504787 * __this, int32_t ___turn0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___turn0;
		int32_t L_1 = __this->get_orderOffset_16();
		int32_t L_2 = __this->get_playerNumber_17();
		__this->set_curOrder_20(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1))%(int32_t)L_2)));
		int32_t L_3 = __this->get_fieldOwner_19();
		int32_t L_4 = __this->get_curOrder_20();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_002c;
		}
	}
	{
		YccioNetworkManager_ClearField_m3891704080(__this, /*hidden argument*/NULL);
	}

IL_002c:
	{
		int32_t L_5 = __this->get_playOrder_15();
		int32_t L_6 = __this->get_curOrder_20();
		if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
		{
			goto IL_0048;
		}
	}
	{
		YccioNetworkManager_MyTurn_m3314630788(__this, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_0048:
	{
		YccioNetworkManager_NotMyTurn_m3026768657(__this, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void YccioNetworkManager::OnPlayerMove(PhotonPlayer,System.Int32,System.Object)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnPlayerMove_m3104743202 (YccioNetworkManager_t3600504787 * __this, PhotonPlayer_t3305149557 * ___photonPlayer0, int32_t ___turn1, RuntimeObject * ___move2, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void YccioNetworkManager::OnTurnCompleted(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnTurnCompleted_m2303772581 (YccioNetworkManager_t3600504787 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void YccioNetworkManager::OnPlayerFinished(PhotonPlayer,System.Int32,System.Object)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnPlayerFinished_m2557697077 (YccioNetworkManager_t3600504787 * __this, PhotonPlayer_t3305149557 * ___photonPlayer0, int32_t ___turn1, RuntimeObject * ___move2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_OnPlayerFinished_m2557697077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t385246372* V_0 = NULL;
	int32_t V_1 = 0;
	{
		RuntimeObject * L_0 = ___move2;
		V_0 = ((Int32U5BU5D_t385246372*)IsInst((RuntimeObject*)L_0, Int32U5BU5D_t385246372_il2cpp_TypeInfo_var));
		Int32U5BU5D_t385246372* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = 0;
		int32_t L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Int32U5BU5D_t385246372* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		int32_t L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		PunTurnManager_t1223962931 * L_7 = __this->get_turnManager_5();
		NullCheck(L_7);
		PunTurnManager_BeginTurn_m4041073334(L_7, /*hidden argument*/NULL);
		goto IL_00e6;
	}

IL_0029:
	{
		Int32U5BU5D_t385246372* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		int32_t L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_00e6;
		}
	}
	{
		Field_t4115194983 * L_11 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = 1;
		int32_t L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_11);
		L_11->set_prevPoint_8(L_14);
		Field_t4115194983 * L_15 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = 2;
		int32_t L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_15);
		L_15->set_numOfStone_9(L_18);
		Field_t4115194983 * L_19 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		Field_RemoveAll_m4213314319(L_19, /*hidden argument*/NULL);
		V_1 = 3;
		goto IL_006e;
	}

IL_005d:
	{
		Field_t4115194983 * L_20 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_21 = V_0;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_20);
		Field_AddStone_m158341126(L_20, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
	}

IL_006e:
	{
		int32_t L_26 = V_1;
		Int32U5BU5D_t385246372* L_27 = V_0;
		NullCheck(L_27);
		int32_t L_28 = 2;
		int32_t L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		if ((((int32_t)L_26) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)3, (int32_t)L_29)))))
		{
			goto IL_005d;
		}
	}
	{
		Int32U5BU5D_t385246372* L_30 = V_0;
		NullCheck(L_30);
		int32_t L_31 = 2;
		int32_t L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		RuntimeObject* L_33 = YccioNetworkManager_CR_BetSound_m2763905888(__this, L_32, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_33, /*hidden argument*/NULL);
		Field_t4115194983 * L_34 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_34);
		L_34->set_isFirstField_7((bool)0);
		Field_t4115194983 * L_35 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_36 = V_0;
		NullCheck(L_36);
		int32_t L_37 = 1;
		int32_t L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_35);
		Field_SortStone_m279184223(L_35, L_38, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_39 = V_0;
		NullCheck(L_39);
		int32_t L_40 = 0;
		int32_t L_41 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		if ((!(((uint32_t)L_41) == ((uint32_t)2))))
		{
			goto IL_00b6;
		}
	}
	{
		YccioNetworkManager_EndGame_m4061492958(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_00e6;
	}

IL_00b6:
	{
		int32_t L_42 = ___turn1;
		int32_t L_43 = __this->get_orderOffset_16();
		int32_t L_44 = __this->get_playerNumber_17();
		__this->set_fieldOwner_19(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)L_43))%(int32_t)L_44)));
		Field_t4115194983 * L_45 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_46 = __this->get_fieldOwner_19();
		NullCheck(L_45);
		Field_SetFieldColor_m2119387286(L_45, L_46, /*hidden argument*/NULL);
		PunTurnManager_t1223962931 * L_47 = __this->get_turnManager_5();
		NullCheck(L_47);
		PunTurnManager_BeginTurn_m4041073334(L_47, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		return;
	}
}
// System.Collections.IEnumerator YccioNetworkManager::CR_BetSound(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* YccioNetworkManager_CR_BetSound_m2763905888 (YccioNetworkManager_t3600504787 * __this, int32_t ___num0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_CR_BetSound_m2763905888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * V_0 = NULL;
	{
		U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * L_0 = (U3CCR_BetSoundU3Ec__Iterator0_t1415787259 *)il2cpp_codegen_object_new(U3CCR_BetSoundU3Ec__Iterator0_t1415787259_il2cpp_TypeInfo_var);
		U3CCR_BetSoundU3Ec__Iterator0__ctor_m267680118(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * L_1 = V_0;
		int32_t L_2 = ___num0;
		NullCheck(L_1);
		L_1->set_num_1(L_2);
		U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * L_3 = V_0;
		return L_3;
	}
}
// System.Void YccioNetworkManager::OnTurnTimeEnds(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnTurnTimeEnds_m369754152 (YccioNetworkManager_t3600504787 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_OnTurnTimeEnds_m369754152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1035202610, /*hidden argument*/NULL);
		PunTurnManager_t1223962931 * L_0 = __this->get_turnManager_5();
		NullCheck(L_0);
		PunTurnManager_BeginTurn_m4041073334(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::OnJoinedLobby()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnJoinedLobby_m1656922781 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		YccioNetworkManager_UpdateRoom_m2402611726(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::OnJoinedRoom()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnJoinedRoom_m1409881903 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_OnJoinedRoom_m1409881903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneTransition_t1138091307 * L_0 = __this->get_st_6();
		NullCheck(L_0);
		SceneTransition_GoToGameScene_m3230274153(L_0, /*hidden argument*/NULL);
		YccioNetworkManager_SetPlayOrder_m745047584(__this, /*hidden argument*/NULL);
		__this->set_isWaiting_22((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_1 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Room_get_PlayerCount_m1320583396(L_1, /*hidden argument*/NULL);
		__this->set_playerNumber_17(((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)));
		return;
	}
}
// System.Void YccioNetworkManager::OnPhotonCreateRoomFailed(System.Object[])
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnPhotonCreateRoomFailed_m1280733107 (YccioNetworkManager_t3600504787 * __this, ObjectU5BU5D_t2843939325* ___codeAndMsg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_OnPhotonCreateRoomFailed_m1280733107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___codeAndMsg0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1605126663, (RuntimeObject *)(RuntimeObject *)L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::OnReceivedRoomListUpdate()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnReceivedRoomListUpdate_m1321051270 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		YccioNetworkManager_UpdateRoom_m2402611726(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::OnCreatedRoom()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnCreatedRoom_m2068531976 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void YccioNetworkManager::OnPhotonPlayerConnected(PhotonPlayer)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnPhotonPlayerConnected_m2687873397 (YccioNetworkManager_t3600504787 * __this, PhotonPlayer_t3305149557 * ___newPlayer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_OnPhotonPlayerConnected_m2687873397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3219175999, /*hidden argument*/NULL);
		SoundManager_t2102329059 * L_0 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SoundManager_PlaySound_m862056565(L_0, 8, /*hidden argument*/NULL);
		YccioNetworkManager_UpdatePlayersInfo_m337863159(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::OnPhotonPlayerDisconnected(PhotonPlayer)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnPhotonPlayerDisconnected_m1767781834 (YccioNetworkManager_t3600504787 * __this, PhotonPlayer_t3305149557 * ___otherPlayer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_OnPhotonPlayerDisconnected_m1767781834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3348952898, /*hidden argument*/NULL);
		SoundManager_t2102329059 * L_0 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SoundManager_PlaySound_m862056565(L_0, ((int32_t)9), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_1 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = RoomInfo_get_IsPlaying_m470967899(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		YccioNetworkManager_UpdatePlayersInfo_m337863159(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void YccioNetworkManager::OnPhotonMaxCccuReached()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_OnPhotonMaxCccuReached_m2903336004 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void YccioNetworkManager::UpdatePlayersInfo()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_UpdatePlayersInfo_m337863159 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_UpdatePlayersInfo_m337863159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		YccioNetworkManager_SetPlayOrder_m745047584(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_0 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = RoomInfo_get_IsPlaying_m470967899(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		YccioNetworkManager_SetStartButton_m3526135598(__this, /*hidden argument*/NULL);
	}

IL_001b:
	{
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral1708225543, (0.12f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::EnterLobby()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_EnterLobby_m3924952965 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_EnterLobby_m3924952965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_ConnectUsingSettings_m3997032868(NULL /*static, unused*/, _stringLiteral824334121, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::NewGame(System.String)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_NewGame_m858465561 (YccioNetworkManager_t3600504787 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_NewGame_m858465561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RoomOptions_t1787645948 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		__this->set_myRoomName_8(L_0);
		String_t* L_1 = __this->get_myRoomName_8();
		RoomOptions_t1787645948 * L_2 = (RoomOptions_t1787645948 *)il2cpp_codegen_object_new(RoomOptions_t1787645948_il2cpp_TypeInfo_var);
		RoomOptions__ctor_m3263086371(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		RoomOptions_t1787645948 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_MaxPlayers_2((uint8_t)5);
		RoomOptions_t1787645948 * L_4 = V_0;
		NullCheck(L_4);
		RoomOptions_set_IsVisible_m4267195445(L_4, (bool)1, /*hidden argument*/NULL);
		RoomOptions_t1787645948 * L_5 = V_0;
		NullCheck(L_5);
		RoomOptions_set_IsOpen_m502747827(L_5, (bool)1, /*hidden argument*/NULL);
		RoomOptions_t1787645948 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TypedLobby_t3336582029_il2cpp_TypeInfo_var);
		TypedLobby_t3336582029 * L_7 = ((TypedLobby_t3336582029_StaticFields*)il2cpp_codegen_static_fields_for(TypedLobby_t3336582029_il2cpp_TypeInfo_var))->get_Default_2();
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_CreateRoom_m4158244447(NULL /*static, unused*/, L_1, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::JoinGame(System.String)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_JoinGame_m723493021 (YccioNetworkManager_t3600504787 * __this, String_t* ___roomName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_JoinGame_m723493021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RoomOptions_t1787645948 * V_0 = NULL;
	{
		String_t* L_0 = ___roomName0;
		RoomOptions_t1787645948 * L_1 = (RoomOptions_t1787645948 *)il2cpp_codegen_object_new(RoomOptions_t1787645948_il2cpp_TypeInfo_var);
		RoomOptions__ctor_m3263086371(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		RoomOptions_t1787645948 * L_2 = V_0;
		NullCheck(L_2);
		L_2->set_MaxPlayers_2((uint8_t)5);
		RoomOptions_t1787645948 * L_3 = V_0;
		NullCheck(L_3);
		RoomOptions_set_IsVisible_m4267195445(L_3, (bool)1, /*hidden argument*/NULL);
		RoomOptions_t1787645948 * L_4 = V_0;
		NullCheck(L_4);
		RoomOptions_set_IsOpen_m502747827(L_4, (bool)1, /*hidden argument*/NULL);
		RoomOptions_t1787645948 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TypedLobby_t3336582029_il2cpp_TypeInfo_var);
		TypedLobby_t3336582029 * L_6 = ((TypedLobby_t3336582029_StaticFields*)il2cpp_codegen_static_fields_for(TypedLobby_t3336582029_il2cpp_TypeInfo_var))->get_Default_2();
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_JoinOrCreateRoom_m963856079(NULL /*static, unused*/, L_0, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// RoomInfo[] YccioNetworkManager::GetGameList()
extern "C" IL2CPP_METHOD_ATTR RoomInfoU5BU5D_t1491207981* YccioNetworkManager_GetGameList_m3683534293 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_GetGameList_m3683534293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		RoomInfoU5BU5D_t1491207981* L_0 = PhotonNetwork_GetRoomList_m1678125907(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void YccioNetworkManager::UpdateRoom()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_UpdateRoom_m2402611726 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_UpdateRoom_m2402611726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RoomList_t2314265074 * L_0 = __this->get_roomList_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t1113636619 * L_2 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral1309440680, /*hidden argument*/NULL);
		NullCheck(L_2);
		RoomList_t2314265074 * L_3 = GameObject_GetComponent_TisRoomList_t2314265074_m2995111646(L_2, /*hidden argument*/GameObject_GetComponent_TisRoomList_t2314265074_m2995111646_RuntimeMethod_var);
		__this->set_roomList_9(L_3);
	}

IL_0026:
	{
		RoomList_t2314265074 * L_4 = __this->get_roomList_9();
		NullCheck(L_4);
		RoomList_UpdateList_m3942554692(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::StartGame()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_StartGame_m3707574726 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_StartGame_m3707574726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaiseEventOptions_t1229553678 * V_0 = NULL;
	RaiseEventOptions_t1229553678 * V_1 = NULL;
	{
		YccioNetworkManager_SetPlayOrder_m745047584(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_0 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Room_get_PlayerCount_m1320583396(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)2)))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral1708225543, (0.1f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_2 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		RoomInfo_set_IsPlaying_m3623793460(L_2, (bool)1, /*hidden argument*/NULL);
		RaiseEventOptions_t1229553678 * L_3 = (RaiseEventOptions_t1229553678 *)il2cpp_codegen_object_new(RaiseEventOptions_t1229553678_il2cpp_TypeInfo_var);
		RaiseEventOptions__ctor_m3573707678(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		RaiseEventOptions_t1229553678 * L_4 = V_1;
		NullCheck(L_4);
		L_4->set_CachingOption_1(0);
		RaiseEventOptions_t1229553678 * L_5 = V_1;
		NullCheck(L_5);
		L_5->set_Receivers_4(1);
		RaiseEventOptions_t1229553678 * L_6 = V_1;
		V_0 = L_6;
		RaiseEventOptions_t1229553678 * L_7 = V_0;
		PhotonNetwork_RaiseEvent_m71206761(NULL /*static, unused*/, (uint8_t)0, NULL, (bool)0, L_7, /*hidden argument*/NULL);
		RaiseEventOptions_t1229553678 * L_8 = V_0;
		PhotonNetwork_RaiseEvent_m71206761(NULL /*static, unused*/, (uint8_t)((int32_t)13), NULL, (bool)0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::SetStartButton()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_SetStartButton_m3526135598 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_SetStartButton_m3526135598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StartButton_t1998348287 * L_0 = __this->get_startButton_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t1113636619 * L_2 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral993523893, /*hidden argument*/NULL);
		NullCheck(L_2);
		StartButton_t1998348287 * L_3 = GameObject_GetComponent_TisStartButton_t1998348287_m2462033924(L_2, /*hidden argument*/GameObject_GetComponent_TisStartButton_t1998348287_m2462033924_RuntimeMethod_var);
		__this->set_startButton_10(L_3);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_4 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Room_get_PlayerCount_m1320583396(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)1)))
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		bool L_6 = PhotonNetwork_get_isMasterClient_m3379552944(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		StartButton_t1998348287 * L_7 = __this->get_startButton_10();
		NullCheck(L_7);
		StartButton_ActiveButton_m2363554299(L_7, (bool)1, /*hidden argument*/NULL);
		goto IL_005d;
	}

IL_0051:
	{
		StartButton_t1998348287 * L_8 = __this->get_startButton_10();
		NullCheck(L_8);
		StartButton_ActiveButton_m2363554299(L_8, (bool)0, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void YccioNetworkManager::LeaveGame()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_LeaveGame_m4146386290 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_LeaveGame_m4146386290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_LeaveRoom_m299983054(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		SceneTransition_t1138091307 * L_0 = __this->get_st_6();
		NullCheck(L_0);
		SceneTransition_GoToLobbyScene_m984678998(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::SetPlayOrder()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_SetPlayOrder_m745047584 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_SetPlayOrder_m745047584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		__this->set_playOrder_15(0);
		V_1 = 0;
		goto IL_0043;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t2880637464* L_0 = PhotonNetwork_get_playerList_m2152942251(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PhotonPlayer_t3305149557 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		String_t* L_4 = PhotonPlayer_get_UserId_m488372118(L_3, /*hidden argument*/NULL);
		PhotonPlayer_t3305149557 * L_5 = PhotonNetwork_get_player_m1573803587(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = PhotonPlayer_get_UserId_m488372118(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t2880637464* L_8 = PhotonNetwork_get_playerList_m2152942251(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		PhotonPlayer_t3305149557 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		int32_t L_12 = PhotonPlayer_get_ID_m3529408589(L_11, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1));
	}

IL_003f:
	{
		int32_t L_13 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t2880637464* L_15 = PhotonNetwork_get_playerList_m2152942251(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_15)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		V_2 = 0;
		goto IL_007d;
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t2880637464* L_16 = PhotonNetwork_get_playerList_m2152942251(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		PhotonPlayer_t3305149557 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_19);
		int32_t L_20 = PhotonPlayer_get_ID_m3529408589(L_19, /*hidden argument*/NULL);
		int32_t L_21 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)1))) >= ((int32_t)L_21)))
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_22 = __this->get_playOrder_15();
		__this->set_playOrder_15(((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1)));
	}

IL_0079:
	{
		int32_t L_23 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_007d:
	{
		int32_t L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonPlayerU5BU5D_t2880637464* L_25 = PhotonNetwork_get_playerList_m2152942251(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_25)->max_length)))))))
		{
			goto IL_0057;
		}
	}
	{
		return;
	}
}
// System.Void YccioNetworkManager::ShuffleSetting()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_ShuffleSetting_m970800901 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_ShuffleSetting_m970800901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_0 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Room_get_PlayerCount_m1320583396(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0028;
			}
			case 1:
			{
				goto IL_0045;
			}
			case 2:
			{
				goto IL_0062;
			}
			case 3:
			{
				goto IL_007f;
			}
		}
	}
	{
		goto IL_009c;
	}

IL_0028:
	{
		__this->set_maxStone_12(((int32_t)10));
		__this->set_numStone_13(((int32_t)16));
		__this->set_totalStone_14(((int32_t)40));
		goto IL_00c3;
	}

IL_0045:
	{
		__this->set_maxStone_12(((int32_t)9));
		__this->set_numStone_13(((int32_t)12));
		__this->set_totalStone_14(((int32_t)36));
		goto IL_00c3;
	}

IL_0062:
	{
		__this->set_maxStone_12(((int32_t)13));
		__this->set_numStone_13(((int32_t)13));
		__this->set_totalStone_14(((int32_t)52));
		goto IL_00c3;
	}

IL_007f:
	{
		__this->set_maxStone_12(((int32_t)15));
		__this->set_numStone_13(((int32_t)12));
		__this->set_totalStone_14(((int32_t)60));
		goto IL_00c3;
	}

IL_009c:
	{
		__this->set_maxStone_12(((int32_t)10));
		__this->set_numStone_13(((int32_t)16));
		__this->set_totalStone_14(((int32_t)40));
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2815969589, /*hidden argument*/NULL);
		goto IL_00c3;
	}

IL_00c3:
	{
		V_1 = 0;
		goto IL_00dc;
	}

IL_00ca:
	{
		Int32U5BU5D_t385246372* L_3 = __this->get_remainStones_23();
		int32_t L_4 = V_1;
		int32_t L_5 = __this->get_numStone_13();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (int32_t)L_5);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_00dc:
	{
		int32_t L_7 = V_1;
		Int32U5BU5D_t385246372* L_8 = __this->get_remainStones_23();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))))))
		{
			goto IL_00ca;
		}
	}
	{
		return;
	}
}
// System.Void YccioNetworkManager::Shuffle()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_Shuffle_m1267737554 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_Shuffle_m1267737554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t385246372* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	RaiseEventOptions_t1229553678 * V_5 = NULL;
	RaiseEventOptions_t1229553678 * V_6 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_isMasterClient_m3379552944(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		int32_t L_1 = __this->get_totalStone_14();
		Int32U5BU5D_t385246372* L_2 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)L_1);
		V_0 = L_2;
		V_1 = 0;
		goto IL_0026;
	}

IL_001e:
	{
		Int32U5BU5D_t385246372* L_3 = V_0;
		int32_t L_4 = V_1;
		int32_t L_5 = V_1;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (int32_t)L_5);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0026:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = __this->get_totalStone_14();
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_001e;
		}
	}
	{
		V_2 = 0;
		goto IL_005b;
	}

IL_0039:
	{
		Int32U5BU5D_t385246372* L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_3 = L_12;
		int32_t L_13 = V_2;
		int32_t L_14 = __this->get_totalStone_14();
		int32_t L_15 = Random_Range_m4054026115(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		Int32U5BU5D_t385246372* L_16 = V_0;
		int32_t L_17 = V_2;
		Int32U5BU5D_t385246372* L_18 = V_0;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_16);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (int32_t)L_21);
		Int32U5BU5D_t385246372* L_22 = V_0;
		int32_t L_23 = V_4;
		int32_t L_24 = V_3;
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(L_23), (int32_t)L_24);
		int32_t L_25 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
	}

IL_005b:
	{
		int32_t L_26 = V_2;
		int32_t L_27 = __this->get_totalStone_14();
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_0039;
		}
	}
	{
		RaiseEventOptions_t1229553678 * L_28 = (RaiseEventOptions_t1229553678 *)il2cpp_codegen_object_new(RaiseEventOptions_t1229553678_il2cpp_TypeInfo_var);
		RaiseEventOptions__ctor_m3573707678(L_28, /*hidden argument*/NULL);
		V_6 = L_28;
		RaiseEventOptions_t1229553678 * L_29 = V_6;
		NullCheck(L_29);
		L_29->set_CachingOption_1(0);
		RaiseEventOptions_t1229553678 * L_30 = V_6;
		NullCheck(L_30);
		L_30->set_Receivers_4(1);
		RaiseEventOptions_t1229553678 * L_31 = V_6;
		V_5 = L_31;
		Int32U5BU5D_t385246372* L_32 = V_0;
		RaiseEventOptions_t1229553678 * L_33 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_RaiseEvent_m71206761(NULL /*static, unused*/, (uint8_t)((int32_t)10), (RuntimeObject *)(RuntimeObject *)L_32, (bool)0, L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::GetMyStone(System.Int32[])
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_GetMyStone_m2847881598 (YccioNetworkManager_t3600504787 * __this, Int32U5BU5D_t385246372* ___stone0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_playOrder_15();
		int32_t L_1 = __this->get_numStone_13();
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)L_1));
		V_1 = 0;
		goto IL_0028;
	}

IL_0015:
	{
		Deck_t2172403585 * L_2 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_3 = ___stone0;
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		int32_t L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_2);
		Deck_AddStone_m2587771987(L_2, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = __this->get_numStone_13();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_11 = __this->get_playerNumber_17();
		if ((!(((uint32_t)L_11) == ((uint32_t)2))))
		{
			goto IL_006b;
		}
	}
	{
		__this->set_isNoCloud3_21((bool)0);
		V_2 = ((int32_t)32);
		goto IL_0063;
	}

IL_004f:
	{
		Int32U5BU5D_t385246372* L_12 = ___stone0;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		if ((!(((uint32_t)L_15) == ((uint32_t)8))))
		{
			goto IL_005f;
		}
	}
	{
		__this->set_isNoCloud3_21((bool)1);
	}

IL_005f:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0063:
	{
		int32_t L_17 = V_2;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)40))))
		{
			goto IL_004f;
		}
	}

IL_006b:
	{
		return;
	}
}
// System.Void YccioNetworkManager::RequestDistribute()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_RequestDistribute_m2698563940 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_RequestDistribute_m2698563940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaiseEventOptions_t1229553678 * V_0 = NULL;
	RaiseEventOptions_t1229553678 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		bool L_0 = PhotonNetwork_get_isMasterClient_m3379552944(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		RaiseEventOptions_t1229553678 * L_1 = (RaiseEventOptions_t1229553678 *)il2cpp_codegen_object_new(RaiseEventOptions_t1229553678_il2cpp_TypeInfo_var);
		RaiseEventOptions__ctor_m3573707678(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		RaiseEventOptions_t1229553678 * L_2 = V_1;
		NullCheck(L_2);
		L_2->set_CachingOption_1(0);
		RaiseEventOptions_t1229553678 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_Receivers_4(1);
		RaiseEventOptions_t1229553678 * L_4 = V_1;
		V_0 = L_4;
		RaiseEventOptions_t1229553678 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_RaiseEvent_m71206761(NULL /*static, unused*/, (uint8_t)((int32_t)11), NULL, (bool)0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::DistributeStatus()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_DistributeStatus_m2345907618 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_DistributeStatus_m2345907618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	RaiseEventOptions_t1229553678 * V_1 = NULL;
	RaiseEventOptions_t1229553678 * V_2 = NULL;
	{
		PlayerStatusPanel_t1209538314 * L_0 = __this->get_statusPanel_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		PlayerStatusPanel_t1209538314 * L_2 = PlayerStatusPanel_get_Instance_m52077214(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_statusPanel_11(L_2);
	}

IL_001c:
	{
		PlayerStatusPanel_t1209538314 * L_3 = __this->get_statusPanel_11();
		NullCheck(L_3);
		PlayerStatusPanel_RemoveAll_m4234547583(L_3, /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_4 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		V_0 = L_4;
		ObjectU5BU5D_t2843939325* L_5 = V_0;
		int32_t L_6 = __this->get_playOrder_15();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_8);
		ObjectU5BU5D_t2843939325* L_9 = V_0;
		PlayerData_t220878115 * L_10 = __this->get_pd_7();
		NullCheck(L_10);
		String_t* L_11 = PlayerData_get_PlayerName_m1238444232(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_11);
		ObjectU5BU5D_t2843939325* L_12 = V_0;
		PlayerData_t220878115 * L_13 = __this->get_pd_7();
		NullCheck(L_13);
		int32_t L_14 = PlayerData_get_Chips_m1748726914(L_13, /*hidden argument*/NULL);
		int32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_16);
		ObjectU5BU5D_t2843939325* L_17 = V_0;
		Deck_t2172403585 * L_18 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = L_18->get_numOfStone_9();
		int32_t L_20 = L_19;
		RuntimeObject * L_21 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_21);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_21);
		RaiseEventOptions_t1229553678 * L_22 = (RaiseEventOptions_t1229553678 *)il2cpp_codegen_object_new(RaiseEventOptions_t1229553678_il2cpp_TypeInfo_var);
		RaiseEventOptions__ctor_m3573707678(L_22, /*hidden argument*/NULL);
		V_2 = L_22;
		RaiseEventOptions_t1229553678 * L_23 = V_2;
		NullCheck(L_23);
		L_23->set_CachingOption_1(0);
		RaiseEventOptions_t1229553678 * L_24 = V_2;
		NullCheck(L_24);
		L_24->set_Receivers_4(1);
		RaiseEventOptions_t1229553678 * L_25 = V_2;
		V_1 = L_25;
		ObjectU5BU5D_t2843939325* L_26 = V_0;
		RaiseEventOptions_t1229553678 * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_RaiseEvent_m71206761(NULL /*static, unused*/, (uint8_t)((int32_t)12), (RuntimeObject *)(RuntimeObject *)L_26, (bool)0, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::UpdateStatus(System.Object[])
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_UpdateStatus_m2094236186 (YccioNetworkManager_t3600504787 * __this, ObjectU5BU5D_t2843939325* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_UpdateStatus_m2094236186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IndexOutOfRangeException_t1578797820 * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectU5BU5D_t2843939325* L_0 = ___data0;
		NullCheck(L_0);
		int32_t L_1 = 0;
		RuntimeObject * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		int32_t L_3 = __this->get_playOrder_15();
		if ((((int32_t)((*(int32_t*)((int32_t*)UnBox(L_2, Int32_t2950945753_il2cpp_TypeInfo_var))))) == ((int32_t)L_3)))
		{
			goto IL_0072;
		}
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		PlayerStatusPanel_t1209538314 * L_4 = __this->get_statusPanel_11();
		ObjectU5BU5D_t2843939325* L_5 = ___data0;
		NullCheck(L_5);
		int32_t L_6 = 0;
		RuntimeObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		ObjectU5BU5D_t2843939325* L_8 = ___data0;
		NullCheck(L_8);
		int32_t L_9 = 1;
		RuntimeObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ObjectU5BU5D_t2843939325* L_11 = ___data0;
		NullCheck(L_11);
		int32_t L_12 = 2;
		RuntimeObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t2843939325* L_14 = ___data0;
		NullCheck(L_14);
		int32_t L_15 = 3;
		RuntimeObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_4);
		PlayerStatusPanel_AddStatus_m3083601519(L_4, ((*(int32_t*)((int32_t*)UnBox(L_7, Int32_t2950945753_il2cpp_TypeInfo_var)))), ((String_t*)CastclassSealed((RuntimeObject*)L_10, String_t_il2cpp_TypeInfo_var)), ((*(int32_t*)((int32_t*)UnBox(L_13, Int32_t2950945753_il2cpp_TypeInfo_var)))), ((*(int32_t*)((int32_t*)UnBox(L_16, Int32_t2950945753_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0072;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (IndexOutOfRangeException_t1578797820_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0043;
		throw e;
	}

CATCH_0043:
	{ // begin catch(System.IndexOutOfRangeException)
		V_0 = ((IndexOutOfRangeException_t1578797820 *)__exception_local);
		PlayerStatusPanel_t1209538314 * L_17 = __this->get_statusPanel_11();
		ObjectU5BU5D_t2843939325* L_18 = ___data0;
		NullCheck(L_18);
		int32_t L_19 = 0;
		RuntimeObject * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		ObjectU5BU5D_t2843939325* L_21 = ___data0;
		NullCheck(L_21);
		int32_t L_22 = 1;
		RuntimeObject * L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		ObjectU5BU5D_t2843939325* L_24 = ___data0;
		NullCheck(L_24);
		int32_t L_25 = 2;
		RuntimeObject * L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		int32_t L_27 = __this->get_numStone_13();
		NullCheck(L_17);
		PlayerStatusPanel_AddStatus_m3083601519(L_17, ((*(int32_t*)((int32_t*)UnBox(L_20, Int32_t2950945753_il2cpp_TypeInfo_var)))), ((String_t*)CastclassSealed((RuntimeObject*)L_23, String_t_il2cpp_TypeInfo_var)), ((*(int32_t*)((int32_t*)UnBox(L_26, Int32_t2950945753_il2cpp_TypeInfo_var)))), L_27, /*hidden argument*/NULL);
		goto IL_0072;
	} // end catch (depth: 1)

IL_0072:
	{
		return;
	}
}
// System.Int32 YccioNetworkManager::IsFirstOrder()
extern "C" IL2CPP_METHOD_ATTR int32_t YccioNetworkManager_IsFirstOrder_m3390042890 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		Deck_t2172403585 * L_0 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_isFirst_6();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Deck_t2172403585 * L_2 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_isFirst_6((bool)0);
		int32_t L_3 = __this->get_playOrder_15();
		return L_3;
	}

IL_0021:
	{
		bool L_4 = __this->get_isNoCloud3_21();
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_5 = __this->get_playOrder_15();
		if (L_5)
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_6 = Random_Range_m4054026115(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		return L_6;
	}

IL_003f:
	{
		return (-1);
	}
}
// System.Void YccioNetworkManager::SetFirstOrder()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_SetFirstOrder_m516433491 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_SetFirstOrder_m516433491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RaiseEventOptions_t1229553678 * V_1 = NULL;
	RaiseEventOptions_t1229553678 * V_2 = NULL;
	{
		int32_t L_0 = YccioNetworkManager_IsFirstOrder_m3390042890(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		RaiseEventOptions_t1229553678 * L_1 = (RaiseEventOptions_t1229553678 *)il2cpp_codegen_object_new(RaiseEventOptions_t1229553678_il2cpp_TypeInfo_var);
		RaiseEventOptions__ctor_m3573707678(L_1, /*hidden argument*/NULL);
		V_2 = L_1;
		RaiseEventOptions_t1229553678 * L_2 = V_2;
		NullCheck(L_2);
		L_2->set_CachingOption_1(0);
		RaiseEventOptions_t1229553678 * L_3 = V_2;
		NullCheck(L_3);
		L_3->set_Receivers_4(1);
		RaiseEventOptions_t1229553678 * L_4 = V_2;
		V_1 = L_4;
		ObjectU5BU5D_t2843939325* L_5 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t2843939325* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral823592379);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral823592379);
		ObjectU5BU5D_t2843939325* L_7 = L_6;
		int32_t L_8 = V_0;
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_t2843939325* L_11 = L_7;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral3452614529);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3452614529);
		ObjectU5BU5D_t2843939325* L_12 = L_11;
		int32_t L_13 = __this->get_playOrder_15();
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2971454694(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) <= ((int32_t)(-1))))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_18 = V_0;
		int32_t L_19 = L_18;
		RuntimeObject * L_20 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_19);
		RaiseEventOptions_t1229553678 * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_RaiseEvent_m71206761(NULL /*static, unused*/, (uint8_t)((int32_t)14), L_20, (bool)0, L_21, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void YccioNetworkManager::MyTurn()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_MyTurn_m3314630788 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_MyTurn_m3314630788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isWaiting_22();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		PassButton_t4045823955 * L_1 = PassButton_get_Instance_m1637646120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		PassButton_OnClickPass_m1917316564(L_1, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		__this->set_isMyTurn_18((bool)1);
		SubmitButton_t4135388028 * L_2 = SubmitButton_get_Instance_m3888875242(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SubmitButton_ActiveButton_m3820152260(L_2, (bool)1, /*hidden argument*/NULL);
		PassButton_t4045823955 * L_3 = PassButton_get_Instance_m1637646120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		PassButton_ActiveButton_m477051212(L_3, (bool)1, /*hidden argument*/NULL);
		Deck_t2172403585 * L_4 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Deck_SetActiveColor_m2307680960(L_4, (bool)1, /*hidden argument*/NULL);
		PlayerStatusPanel_t1209538314 * L_5 = PlayerStatusPanel_get_Instance_m52077214(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_curOrder_20();
		NullCheck(L_5);
		PlayerStatusPanel_SetActiveColor_m907504136(L_5, L_6, /*hidden argument*/NULL);
		Field_t4115194983 * L_7 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_m3145433196(L_8, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral481352081, ((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)2))))), (float)(0.1f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::NotMyTurn()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_NotMyTurn_m3026768657 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_NotMyTurn_m3026768657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t385246372* V_0 = NULL;
	{
		__this->set_isMyTurn_18((bool)0);
		SubmitButton_t4135388028 * L_0 = SubmitButton_get_Instance_m3888875242(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SubmitButton_ActiveButton_m3820152260(L_0, (bool)0, /*hidden argument*/NULL);
		PassButton_t4045823955 * L_1 = PassButton_get_Instance_m1637646120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		PassButton_ActiveButton_m477051212(L_1, (bool)0, /*hidden argument*/NULL);
		Deck_t2172403585 * L_2 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Deck_SetActiveColor_m2307680960(L_2, (bool)0, /*hidden argument*/NULL);
		PlayerStatusPanel_t1209538314 * L_3 = PlayerStatusPanel_get_Instance_m52077214(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_curOrder_20();
		NullCheck(L_3);
		PlayerStatusPanel_SetActiveColor_m907504136(L_3, L_4, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_5 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)8);
		Int32U5BU5D_t385246372* L_6 = L_5;
		RuntimeFieldHandle_t1871169219  L_7 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2D9E5175008751D08F361488C9927086B276B965FA_5_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_6;
		PunTurnManager_t1223962931 * L_8 = __this->get_turnManager_5();
		Int32U5BU5D_t385246372* L_9 = V_0;
		NullCheck(L_8);
		PunTurnManager_SendMove_m385030697(L_8, (RuntimeObject *)(RuntimeObject *)L_9, (bool)1, /*hidden argument*/NULL);
		RemainTime_t3232366768 * L_10 = RemainTime_get_Instance_m3815012167(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		RemainTime_SetCurrentPlayerStatus_m3509695785(L_10, /*hidden argument*/NULL);
		Field_t4115194983 * L_11 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m3145433196(L_12, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral1275403523, ((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)2))))), (float)(0.1f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::MyTurnSound()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_MyTurnSound_m477276421 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		SoundManager_t2102329059 * L_0 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SoundManager_PlaySound_m862056565(L_0, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::NotMyTurnSound()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_NotMyTurnSound_m689173213 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		SoundManager_t2102329059 * L_0 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SoundManager_PlaySound_m862056565(L_0, 7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::ClearField()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_ClearField_m3891704080 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		Field_t4115194983 * L_0 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Field_ClearField_m1103382306(L_0, /*hidden argument*/NULL);
		Field_t4115194983 * L_1 = Field_get_Instance_m2724432273(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Field_SetFieldColor_m2119387286(L_1, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::EndGame(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_EndGame_m4061492958 (YccioNetworkManager_t3600504787 * __this, bool ___reset0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_EndGame_m4061492958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		Room_t3759828263 * L_0 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurnExtensions_t3150044944_il2cpp_TypeInfo_var);
		TurnExtensions_SetTurn_m2738922425(NULL /*static, unused*/, L_0, 0, (bool)0, /*hidden argument*/NULL);
		YccioNetworkManager_SetStartButton_m3526135598(__this, /*hidden argument*/NULL);
		Deck_t2172403585 * L_1 = Deck_get_Instance_m2996391552(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Deck_CalRemain_m2239694271(L_1, /*hidden argument*/NULL);
		Room_t3759828263 * L_2 = PhotonNetwork_get_room_m36124698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		RoomInfo_set_IsPlaying_m3623793460(L_2, (bool)0, /*hidden argument*/NULL);
		RemainTime_t3232366768 * L_3 = RemainTime_get_Instance_m3815012167(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		RemainTime_Invisible_m132284693(L_3, /*hidden argument*/NULL);
		bool L_4 = ___reset0;
		if (L_4)
		{
			goto IL_0058;
		}
	}
	{
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral3675857332, (0.2f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral2979092519, (0.3f), /*hidden argument*/NULL);
	}

IL_0058:
	{
		PlayerData_t220878115 * L_5 = PlayerData_get_Instance_m3742910714(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayerData_t220878115 * L_6 = L_5;
		NullCheck(L_6);
		int32_t L_7 = PlayerData_get_Chips_m1748726914(L_6, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_playerNumber_17();
		NullCheck(L_6);
		PlayerData_set_Chips_m1881252529(L_6, ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_8)))), /*hidden argument*/NULL);
		SubmitButton_t4135388028 * L_9 = SubmitButton_get_Instance_m3888875242(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		SubmitButton_ActiveButton_m3820152260(L_9, (bool)0, /*hidden argument*/NULL);
		PassButton_t4045823955 * L_10 = PassButton_get_Instance_m1637646120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		PassButton_ActiveButton_m477051212(L_10, (bool)0, /*hidden argument*/NULL);
		SortByColor_t1786351735 * L_11 = SortByColor_get_Instance_m2199486831(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		SortByColor_ActiveButton_m1446296454(L_11, (bool)0, /*hidden argument*/NULL);
		SortByNumber_t227753584 * L_12 = SortByNumber_get_Instance_m183889917(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		SortByNumber_ActiveButton_m2261818842(L_12, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::SendRemainStones(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_SendRemainStones_m1163236977 (YccioNetworkManager_t3600504787 * __this, int32_t ___order0, int32_t ___remain1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_SendRemainStones_m1163236977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t385246372* V_0 = NULL;
	RaiseEventOptions_t1229553678 * V_1 = NULL;
	RaiseEventOptions_t1229553678 * V_2 = NULL;
	{
		bool L_0 = __this->get_isWaiting_22();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Int32U5BU5D_t385246372* L_1 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)2);
		V_0 = L_1;
		Int32U5BU5D_t385246372* L_2 = V_0;
		int32_t L_3 = ___order0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_3);
		Int32U5BU5D_t385246372* L_4 = V_0;
		int32_t L_5 = ___remain1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_5);
		RaiseEventOptions_t1229553678 * L_6 = (RaiseEventOptions_t1229553678 *)il2cpp_codegen_object_new(RaiseEventOptions_t1229553678_il2cpp_TypeInfo_var);
		RaiseEventOptions__ctor_m3573707678(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		RaiseEventOptions_t1229553678 * L_7 = V_2;
		NullCheck(L_7);
		L_7->set_CachingOption_1(0);
		RaiseEventOptions_t1229553678 * L_8 = V_2;
		NullCheck(L_8);
		L_8->set_Receivers_4(1);
		RaiseEventOptions_t1229553678 * L_9 = V_2;
		V_1 = L_9;
		Int32U5BU5D_t385246372* L_10 = V_0;
		RaiseEventOptions_t1229553678 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_RaiseEvent_m71206761(NULL /*static, unused*/, (uint8_t)((int32_t)15), (RuntimeObject *)(RuntimeObject *)L_10, (bool)0, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::CalChips()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_CalChips_m1900543950 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isWaiting_22();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		RuntimeObject* L_1 = YccioNetworkManager_CR_Calchips_m80184455(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator YccioNetworkManager::CR_Calchips()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* YccioNetworkManager_CR_Calchips_m80184455 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_CR_Calchips_m80184455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * V_0 = NULL;
	{
		U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * L_0 = (U3CCR_CalchipsU3Ec__Iterator1_t2144217578 *)il2cpp_codegen_object_new(U3CCR_CalchipsU3Ec__Iterator1_t2144217578_il2cpp_TypeInfo_var);
		U3CCR_CalchipsU3Ec__Iterator1__ctor_m3089154205(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * L_2 = V_0;
		return L_2;
	}
}
// System.Void YccioNetworkManager::UpdateRemainStone(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_UpdateRemainStone_m4039245266 (YccioNetworkManager_t3600504787 * __this, int32_t ___order0, int32_t ___remain1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_UpdateRemainStone_m4039245266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002d;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_1);
		Int32U5BU5D_t385246372* L_3 = __this->get_remainStones_23();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1715369213(NULL /*static, unused*/, L_2, _stringLiteral3921725357, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002d:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = __this->get_playerNumber_17();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0007;
		}
	}
	{
		PlayerStatusPanel_t1209538314 * L_13 = PlayerStatusPanel_get_Instance_m52077214(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_14 = ___order0;
		int32_t L_15 = ___remain1;
		NullCheck(L_13);
		PlayerStatusPanel_UpdateRemainStone_m3573178345(L_13, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::WinOrLose()
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_WinOrLose_m289986293 (YccioNetworkManager_t3600504787 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isWaiting_22();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = __this->get_isWinner_24();
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		WinLosePanel_t3114026018 * L_2 = WinLosePanel_get_Instance_m3091037315(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		WinLosePanel_OnWin_m3713210824(L_2, /*hidden argument*/NULL);
		goto IL_0030;
	}

IL_0026:
	{
		WinLosePanel_t3114026018 * L_3 = WinLosePanel_get_Instance_m3091037315(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		WinLosePanel_OnLose_m3303681697(L_3, /*hidden argument*/NULL);
	}

IL_0030:
	{
		__this->set_isWinner_24((bool)0);
		return;
	}
}
// System.Void YccioNetworkManager::SendImoji(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_SendImoji_m1285368662 (YccioNetworkManager_t3600504787 * __this, int32_t ___num0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YccioNetworkManager_SendImoji_m1285368662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t385246372* V_0 = NULL;
	RaiseEventOptions_t1229553678 * V_1 = NULL;
	RaiseEventOptions_t1229553678 * V_2 = NULL;
	{
		Int32U5BU5D_t385246372* L_0 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)2);
		V_0 = L_0;
		Int32U5BU5D_t385246372* L_1 = V_0;
		int32_t L_2 = __this->get_playOrder_15();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_2);
		Int32U5BU5D_t385246372* L_3 = V_0;
		int32_t L_4 = ___num0;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_4);
		RaiseEventOptions_t1229553678 * L_5 = (RaiseEventOptions_t1229553678 *)il2cpp_codegen_object_new(RaiseEventOptions_t1229553678_il2cpp_TypeInfo_var);
		RaiseEventOptions__ctor_m3573707678(L_5, /*hidden argument*/NULL);
		V_2 = L_5;
		RaiseEventOptions_t1229553678 * L_6 = V_2;
		NullCheck(L_6);
		L_6->set_CachingOption_1(0);
		RaiseEventOptions_t1229553678 * L_7 = V_2;
		NullCheck(L_7);
		L_7->set_Receivers_4(1);
		RaiseEventOptions_t1229553678 * L_8 = V_2;
		V_1 = L_8;
		Int32U5BU5D_t385246372* L_9 = V_0;
		RaiseEventOptions_t1229553678 * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_t1610183659_il2cpp_TypeInfo_var);
		PhotonNetwork_RaiseEvent_m71206761(NULL /*static, unused*/, (uint8_t)((int32_t)16), (RuntimeObject *)(RuntimeObject *)L_9, (bool)0, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YccioNetworkManager::GetImoji(System.Int32[])
extern "C" IL2CPP_METHOD_ATTR void YccioNetworkManager_GetImoji_m3146625778 (YccioNetworkManager_t3600504787 * __this, Int32U5BU5D_t385246372* ___data0, const RuntimeMethod* method)
{
	{
		PlayerStatusPanel_t1209538314 * L_0 = PlayerStatusPanel_get_Instance_m52077214(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_1 = ___data0;
		NullCheck(L_0);
		PlayerStatusPanel_GetImoji_m1615372909(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void YccioNetworkManager/<CR_BetSound>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCR_BetSoundU3Ec__Iterator0__ctor_m267680118 (U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean YccioNetworkManager/<CR_BetSound>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CCR_BetSoundU3Ec__Iterator0_MoveNext_m3891831477 (U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCR_BetSoundU3Ec__Iterator0_MoveNext_m3891831477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_005c;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__1_0(0);
		goto IL_006a;
	}

IL_002d:
	{
		SoundManager_t2102329059 * L_2 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SoundManager_PlaySound_m862056565(L_2, 5, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_3 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_3, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_3);
		bool L_4 = __this->get_U24disposing_3();
		if (L_4)
		{
			goto IL_0057;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0057:
	{
		goto IL_0084;
	}

IL_005c:
	{
		int32_t L_5 = __this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
	}

IL_006a:
	{
		int32_t L_6 = __this->get_U3CiU3E__1_0();
		int32_t L_7 = __this->get_num_1();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
}
// System.Object YccioNetworkManager/<CR_BetSound>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCR_BetSoundU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1073982614 (U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object YccioNetworkManager/<CR_BetSound>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCR_BetSoundU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1382766912 (U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void YccioNetworkManager/<CR_BetSound>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CCR_BetSoundU3Ec__Iterator0_Dispose_m2502343037 (U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void YccioNetworkManager/<CR_BetSound>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CCR_BetSoundU3Ec__Iterator0_Reset_m1012664378 (U3CCR_BetSoundU3Ec__Iterator0_t1415787259 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCR_BetSoundU3Ec__Iterator0_Reset_m1012664378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CCR_BetSoundU3Ec__Iterator0_Reset_m1012664378_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void YccioNetworkManager/<CR_Calchips>c__Iterator1::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CCR_CalchipsU3Ec__Iterator1__ctor_m3089154205 (U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean YccioNetworkManager/<CR_Calchips>c__Iterator1::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CCR_CalchipsU3Ec__Iterator1_MoveNext_m3873318284 (U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCR_CalchipsU3Ec__Iterator1_MoveNext_m3873318284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B10_0 = 0;
	IncreaseEffectPanel_t3055077437 * G_B10_1 = NULL;
	int32_t G_B9_0 = 0;
	IncreaseEffectPanel_t3055077437 * G_B9_1 = NULL;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	IncreaseEffectPanel_t3055077437 * G_B11_2 = NULL;
	int32_t G_B14_0 = 0;
	PlayerData_t220878115 * G_B14_1 = NULL;
	int32_t G_B12_0 = 0;
	PlayerData_t220878115 * G_B12_1 = NULL;
	int32_t G_B13_0 = 0;
	PlayerData_t220878115 * G_B13_1 = NULL;
	int32_t G_B15_0 = 0;
	int32_t G_B15_1 = 0;
	PlayerData_t220878115 * G_B15_2 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00c4;
			}
		}
	}
	{
		goto IL_01ce;
	}

IL_0021:
	{
		YccioNetworkManager_t3600504787 * L_2 = __this->get_U24this_2();
		NullCheck(L_2);
		Int32U5BU5D_t385246372* L_3 = L_2->get_remainStones_23();
		YccioNetworkManager_t3600504787 * L_4 = __this->get_U24this_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_playOrder_15();
		NullCheck(L_3);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_U3CmyU3E__0_0(L_7);
		IncreaseEffectPanel_t3055077437 * L_8 = IncreaseEffectPanel_get_Instance_m2930160416(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		IncreaseEffectPanel_InitPanel_m4011756956(L_8, /*hidden argument*/NULL);
		__this->set_U3CiU3E__1_1(0);
		goto IL_01af;
	}

IL_0054:
	{
		int32_t L_9 = __this->get_U3CiU3E__1_1();
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_10);
		YccioNetworkManager_t3600504787 * L_12 = __this->get_U24this_2();
		NullCheck(L_12);
		Int32U5BU5D_t385246372* L_13 = L_12->get_remainStones_23();
		int32_t L_14 = __this->get_U3CiU3E__1_1();
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1715369213(NULL /*static, unused*/, L_11, _stringLiteral3551321050, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_U3CiU3E__1_1();
		YccioNetworkManager_t3600504787 * L_21 = __this->get_U24this_2();
		NullCheck(L_21);
		int32_t L_22 = L_21->get_playOrder_15();
		if ((!(((uint32_t)L_20) == ((uint32_t)L_22))))
		{
			goto IL_00a0;
		}
	}
	{
		goto IL_01a1;
	}

IL_00a0:
	{
		WaitForSeconds_t1699091251 * L_23 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_23, (0.2f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_23);
		bool L_24 = __this->get_U24disposing_4();
		if (L_24)
		{
			goto IL_00bf;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00bf:
	{
		goto IL_01d0;
	}

IL_00c4:
	{
		IncreaseEffectPanel_t3055077437 * L_25 = IncreaseEffectPanel_get_Instance_m2930160416(NULL /*static, unused*/, /*hidden argument*/NULL);
		YccioNetworkManager_t3600504787 * L_26 = __this->get_U24this_2();
		NullCheck(L_26);
		Int32U5BU5D_t385246372* L_27 = L_26->get_remainStones_23();
		int32_t L_28 = __this->get_U3CiU3E__1_1();
		NullCheck(L_27);
		int32_t L_29 = L_28;
		int32_t L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		int32_t L_31 = __this->get_U3CmyU3E__0_0();
		int32_t L_32 = __this->get_U3CiU3E__1_1();
		YccioNetworkManager_t3600504787 * L_33 = __this->get_U24this_2();
		NullCheck(L_33);
		int32_t L_34 = L_33->get_playOrder_15();
		G_B9_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)L_31));
		G_B9_1 = L_25;
		if ((((int32_t)L_32) <= ((int32_t)L_34)))
		{
			G_B10_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)L_31));
			G_B10_1 = L_25;
			goto IL_0103;
		}
	}
	{
		int32_t L_35 = __this->get_U3CiU3E__1_1();
		G_B11_0 = L_35;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		goto IL_010b;
	}

IL_0103:
	{
		int32_t L_36 = __this->get_U3CiU3E__1_1();
		G_B11_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_36, (int32_t)1));
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
	}

IL_010b:
	{
		NullCheck(G_B11_2);
		IncreaseEffectPanel_NewIncrease_m271767927(G_B11_2, G_B11_1, G_B11_0, /*hidden argument*/NULL);
		PlayerData_t220878115 * L_37 = PlayerData_get_Instance_m3742910714(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayerData_t220878115 * L_38 = L_37;
		NullCheck(L_38);
		int32_t L_39 = PlayerData_get_Chips_m1748726914(L_38, /*hidden argument*/NULL);
		YccioNetworkManager_t3600504787 * L_40 = __this->get_U24this_2();
		NullCheck(L_40);
		Int32U5BU5D_t385246372* L_41 = L_40->get_remainStones_23();
		int32_t L_42 = __this->get_U3CiU3E__1_1();
		NullCheck(L_41);
		int32_t L_43 = L_42;
		int32_t L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		int32_t L_45 = __this->get_U3CmyU3E__0_0();
		G_B12_0 = L_39;
		G_B12_1 = L_38;
		if (((int32_t)il2cpp_codegen_subtract((int32_t)L_44, (int32_t)L_45)))
		{
			G_B14_0 = L_39;
			G_B14_1 = L_38;
			goto IL_0150;
		}
	}
	{
		YccioNetworkManager_t3600504787 * L_46 = __this->get_U24this_2();
		NullCheck(L_46);
		bool L_47 = L_46->get_isWinner_24();
		G_B13_0 = G_B12_0;
		G_B13_1 = G_B12_1;
		if (!L_47)
		{
			G_B14_0 = G_B12_0;
			G_B14_1 = G_B12_1;
			goto IL_0150;
		}
	}
	{
		G_B15_0 = ((int32_t)40);
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		goto IL_0169;
	}

IL_0150:
	{
		YccioNetworkManager_t3600504787 * L_48 = __this->get_U24this_2();
		NullCheck(L_48);
		Int32U5BU5D_t385246372* L_49 = L_48->get_remainStones_23();
		int32_t L_50 = __this->get_U3CiU3E__1_1();
		NullCheck(L_49);
		int32_t L_51 = L_50;
		int32_t L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		int32_t L_53 = __this->get_U3CmyU3E__0_0();
		G_B15_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_52, (int32_t)L_53));
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
	}

IL_0169:
	{
		NullCheck(G_B15_2);
		PlayerData_set_Chips_m1881252529(G_B15_2, ((int32_t)il2cpp_codegen_add((int32_t)G_B15_1, (int32_t)G_B15_0)), /*hidden argument*/NULL);
		InGamePanel_t2211285949 * L_54 = InGamePanel_get_Instance_m4112776688(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_54);
		Text_t1901882714 * L_55 = L_54->get_chips_5();
		PlayerData_t220878115 * L_56 = PlayerData_get_Instance_m3742910714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_56);
		int32_t L_57 = PlayerData_get_Chips_m1748726914(L_56, /*hidden argument*/NULL);
		V_1 = L_57;
		String_t* L_58 = Int32_ToString_m141394615((int32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck(L_55);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_55, L_58);
		SoundManager_t2102329059 * L_59 = SoundManager_get_Instance_m3963388714(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_59);
		SoundManager_PlaySound_m862056565(L_59, 1, /*hidden argument*/NULL);
	}

IL_01a1:
	{
		int32_t L_60 = __this->get_U3CiU3E__1_1();
		__this->set_U3CiU3E__1_1(((int32_t)il2cpp_codegen_add((int32_t)L_60, (int32_t)1)));
	}

IL_01af:
	{
		int32_t L_61 = __this->get_U3CiU3E__1_1();
		YccioNetworkManager_t3600504787 * L_62 = __this->get_U24this_2();
		NullCheck(L_62);
		Int32U5BU5D_t385246372* L_63 = L_62->get_remainStones_23();
		NullCheck(L_63);
		if ((((int32_t)L_61) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_63)->max_length)))))))
		{
			goto IL_0054;
		}
	}
	{
		__this->set_U24PC_5((-1));
	}

IL_01ce:
	{
		return (bool)0;
	}

IL_01d0:
	{
		return (bool)1;
	}
}
// System.Object YccioNetworkManager/<CR_Calchips>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCR_CalchipsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3635297353 (U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object YccioNetworkManager/<CR_Calchips>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CCR_CalchipsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4063193128 (U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void YccioNetworkManager/<CR_Calchips>c__Iterator1::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CCR_CalchipsU3Ec__Iterator1_Dispose_m378804642 (U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void YccioNetworkManager/<CR_Calchips>c__Iterator1::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CCR_CalchipsU3Ec__Iterator1_Reset_m1362035897 (U3CCR_CalchipsU3Ec__Iterator1_t2144217578 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCR_CalchipsU3Ec__Iterator1_Reset_m1362035897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CCR_CalchipsU3Ec__Iterator1_Reset_m1362035897_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
